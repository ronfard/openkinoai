# OpenKinoAI
OpenKinoAI is an Open Source Framework for Intelligent Cinematography and Editing of Live Performances

User documentation can be found in the
[User guide](https://gitlab.inria.fr/ronfard/OpenKinoAi/wikis/User-guide)

Developer documentation can be found in the
[Developer guide](https://gitlab.inria.fr/ronfard/OpenKinoAi/wikis/Developer-guide)

Instructions for setting up a new OpenKinoAI server can be found in the
[Installation guide](https://gitlab.inria.fr/ronfard/OpenKinoAi/wikis/How-to-setup)

Useful commands and short cuts can be found in the
[Quick reference](https://gitlab.inria.fr/ronfard/OpenKinoAi/wikis/Shortcuts)

Please visit the
[Wiki](https://gitlab.inria.fr/ronfard/OpenKinoAi/wikis/Home)
for additional information

