/*
This file is part of KinoAi project.
It is subject to the license terms in the LICENSE file found in the top-level directory
*/

/**
 * [GoButton => a button to set the time at the begining of a tracklet]
 * @param       {[float]} tempW  [width]
 * @param       {[float]} tempH  [height]
 * @param       {[tracklet object]} t      [tracklet]
 * @param       {[html button]} g      [element]
 * @constructor
 */
function GoButton(tempW, tempH, t, g)  {
  // Button location and size
  // this.x  = tempX;
  // this.y  = tempY;
  this.w = tempW;
  this.h = tempH;
  // Is the button on or off?
  // Button always starts as off
  this.on = false;

  this.track = t;

  this.time = 0;

  this.elem = g;

  /**
   * [click => handle mouse click event]
   * @param  {[float]} mx               [mouse x position]
   * @param  {[float]} my               [mouse y position]
   */
  this.click = function(mx, my) {
    // Check to see if a point is inside the rectangle
    var x1 = preparation_editor.table_scroll.position().x+this.elem.position().x;
    var y1 = preparation_editor.table_scroll.position().y+this.elem.position().y - preparation_editor.table_scroll.elt.scrollTop;
    my += can.elt.offsetTop;

    if (my > preparation_editor.table_scroll.position().y && mx > x1 && mx < x1 + this.w && my > y1 && my < y1 + this.h) {
      this.on = !this.on;
      video.time((this.track.first_frame+1)/frame_rate);
      video.pause();
      playing = false;
      for(let t of preparation_editor.tracklets_line) {
        t.on = false;
      }
      this.track.on = true;
      // console.log(this.track.first_frame);
      // console.log(table_scroll.elt.scrollTop);
    }
  };

  /**
   * [setPosition => set the button position]
   * @param  {[float]} tx               [x position]
   * @param  {[float]} ty               [y position]
   */
  this.setPosition = function(tx, ty) {
    this.x = tx;
    this.y = ty;
  }

  /**
   * [setRad => set the size of the button]
   * @param  {[float]} trad               [radius]
   */
  this.setRad = function(trad) {
    this.rad = trad;
  }

  /**
   * [setTrack => set the tracklet object]
   * @param  {[tracklet object]} t               [tracklet]
   */
  this.setTrack = function(t) {
    this.track = t;
  }

  /**
   * [display => function called in the main p5 draw function]
   */
  this.display = function() {
    fill(255);
    ellipse(this.x,this.y,this.rad);
  }
}
