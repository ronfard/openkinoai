/*
This file is part of KinoAi project.
It is subject to the license terms in the LICENSE file found in the top-level directory
*/

/**
 * [Shot => a shot object]
 * @constructor
 */
function Shot(){

  this.x  = 0;
  this.y  = 0;
  this.w  = 0;
  this.h  = 0;

  this.bboxes = [];

  this.bbox_show = [];

  this.type;

  this.start_frame;

  this.start;

  // this.img_start;

  this.aspect_ratio;

  this.end_frame;

  this.end;

  this.is_keep_out = false;

  this.is_pull_in = false;

  this.is_stage_position = false;

  this.is_gaze_direction = false;

  this.is_modified = false;

  this.is_personalize = false;

  this.is_split_screen_shot = false;

  this.actors_involved = [];

  this.temp_tab_act = [];

  this.scenes_involved = [];

  this.on = false;

  this.drag;

  this.in_stabilize = false;
  this.split_and_merge = false;

  this.accuracy_rate;

  this.constraints_split_screen = [];

  this.constraint_left_right = [];

  /**
   * [click => handle mouse click event]
   * @param  {[float]} mx               [mouse x position]
   * @param  {[float]} my               [mouse y position]
   */
  this.click = function(mx, my) {
    if(!(!montage_editor.is_show_context && this.type == 'WS') && mx > this.x && mx < this.x + this.w && my > this.y && my < this.y + this.h) {
      if(is_cadrage_editor) {
        this.selected = !this.selected;
        this.clickSelect();
      }
      this.on = !this.on;
      if(!is_cadrage_editor && this.on && !this.in_stabilize) {
        this.drag = true;
      }
      if(is_montage_editor && this.selected && !(keyIsPressed && keyCode == 16)) {
        shots_timeline.addShotOnCursor(this);
      }
      if(is_montage_editor && this.selected && keyIsPressed && keyCode == 16) {
        event.preventDefault();
        shots_timeline.replaceShot(this);
      }
    }
  }

  /**
   * [clickSelect => click on a shot to select it in the video editing tab]
   */
  this.clickSelect = function() {
    let ind;
    for(let i=0;i<this.scenes_involved.length;i++) {
      if(this.scenes_involved[i] == shots_timeline.name_elem.elt.innerText+'._.'+username) {
        ind = i;
        break;
      }
    }
    if(ind != undefined && !this.selected) {
      this.scenes_involved.splice(ind,1);
      this.is_modified = true;
    } else if(ind == undefined) {
      this.scenes_involved.push(shots_timeline.name_elem.elt.innerText+'._.'+username);
      this.is_modified = true;
    }
  }

  this.draggin = function(mx, my) {

  }

  /**
   * [showInViewer => show a shot in the player]
   * @param  {[float]} mx               [mouse x position]
   * @param  {[float]} my               [mouse y position]
   * @return {[type]}    [description]
   */
  this.showInViewer = function(mx, my) {
    if(this.bbox_show.length>1 && mx > this.bbox_show[0] && mx < this.bbox_show[0]+this.bbox_show[2] && my > this.bbox_show[1] && my < this.bbox_show[1]+this.bbox_show[3]) {
      // console.log('show ',this.type, mx, my);
      return true;
    } else {
      return false;
    }
  }

  /**
   * [equalTo => check if a shot s is equal to this one]
   * @param  {[shot object]}  s                            [s]
   * @param  {Boolean} [updated=true]               [update size]
   * @return {[Boolean]}                 [return true if it's the same]
   */
  this.equalTo = function(s, updated=true) {
    let b = false;
    let act_inv = this.actors_involved;
    let type = this.type;
    let s_act_inv = s.actors_involved;
    let s_type = s.type;
    if(this.is_personalize && this.is_personalize == s.is_personalize && this.type == s.type) {
      return true;
    } else if(s.is_personalize) {
      return false;
    }
    if(updated) {
      act_inv = this.getUpdateActInvolved();
      type = this.getUpdatedSizeShot(this.getCurrStabShot(frame_num)[3]);
      s_act_inv = s.getUpdateActInvolved();
      s_type = s.getUpdatedSizeShot(s.getCurrStabShot(frame_num)[3]);
    }
    if(s.aspect_ratio == this.aspect_ratio && s_type == type && s.is_keep_out == this.is_keep_out && s.is_pull_in == this.is_pull_in
      && s.is_stage_position == this.is_stage_position && s.is_gaze_direction == this.is_gaze_direction) {
      let b1 = true;
      for(let name of s_act_inv) {
        if(!act_inv.includes(name)) {
          b1 = false;
          break;
        }
      }
      if(b1 && s_act_inv.length == act_inv.length) {
        b = true;
      }
    }

    return b;
  }

  /**
   * [isOnStage => check if the actors involved are on stage at the current frame]
   * @return {[Boolean]} [return true if is on stage]
   */
  this.isOnStage = function() {
    for(let a of this.actors_involved) {
      if(a.isOnStage()) {
        return true;
      }
    }
    return false;
  }

  /**
   * [getCSBbox => get a close up view for a specific actor]
   * @param  {[integer]} f_n               [frame number]
   * @param  {[actor object]} a                 [actor object]
   * @return {[list]}     [bounding box]
   */
  this.getCSBbox = function(f_n, a) {
    var bbox;
    for(let t of a.tracks) {
      var keypointsB = frames_data[f_n];
      var detections_track = t.detections;
      var first_frame = t.first_frame;
      if(first_frame < f_n && detections_track.length > (f_n-first_frame)) {
        if(keypointsB[detections_track[f_n-first_frame]]) {
          bbox = getBBoxShotAdapted(aspect_ratio, keypointsB[detections_track[f_n-first_frame]]['KeyPoints'], getFactor('CU'), a);
        }
      }
    }
    if(!bbox) {
      for(let t of a.track_bbox_shot) {
        if(t.first_frame < f_n && t.last_frame > f_n) {
          let b = t.bboxes[f_n-t.first_frame];
          let curr_bbox = [b.x, b.y,b.x+b.w, b.y+b.h];
          var boxB = getBBoxShotAdapted(aspect_ratio, undefined,  getFactor('CU'), a, false, curr_bbox, b.center_x, b.center_y);
          if(boxB) {
            bbox = boxB;
          }
        }
      }
    }
    return bbox;
  }

  /**
   * [calcCropingFactor => compute the crop factor]
   * @param  {[float]} aspectRatio               [aspect ratio]
   * @param  {[float]} threshold                 [threshold]
   * @return {[list]}             [return a crop factor value for each frame]
   */
  this.calcCropingFactor = function(aspectRatio, threshold) {
    let arr = [];
    for(let a of this.actors_involved) {
      let obj = [];
      for(let i=0; i<this.bboxes.length-1; i++) {
        let bb = this.getCSBbox(i, a);
        let bb1 = this.getCSBbox(i+1, a);
        let bbC = [0,0,0];
        if(bb && bb1) {
          let fx = (bb[0] + bb[2])/2;
          let fy = (bb[1] + bb[3])/2;
          let fs = (bb[3] - bb[1])/2;
          let fx1 = (bb1[0] + bb1[2])/2;
          let fy1 = (bb1[1] + bb1[3])/2;
          let fs1 = (bb1[3] - bb1[1])/2;
          let cx = 0;
          let cy = 0;
          let cs = 0;
          if(abs(fx1-fx)<threshold){
            cx = 1;
          }
          if(abs(fy1-fy)<threshold){
            cy = 1;
          }
          if(abs(fs1-fs)<threshold){
            cs = 1;
          }
          bbC = [cx, cy, cs];
        }
        obj.push(bbC);
      }
      arr.push(obj);
    }
    return arr;
  }

  /**
   * [calcApparentMotion => compute for each frame the apparent motion of the virtual camera]
   * @param  {[float]} aspectRatio               [shot aspect ratio]
   * @return {[list]}             [apparent motion value for each frame]
   */
  this.calcApparentMotion = function(aspectRatio) {
    let arr = [];
    for(let a of this.actors_involved) {
      let obj = [];
      for(let i=0; i<this.bboxes.length-1; i++) {
        let bb = this.getCSBbox(i, a);
        let bb1 = this.getCSBbox(i+1, a);
        let bbC = [0,0,0];
        if(bb && bb1) {
          let fx = (bb[0] + bb[2])/2;
          let fy = (bb[1] + bb[3])/2;
          let fs = (bb[3] - bb[1])/2;
          let fx1 = (bb1[0] + bb1[2])/2;
          let fy1 = (bb1[1] + bb1[3])/2;
          let fs1 = (bb1[3] - bb1[1])/2;
          let bx = (fx1 - fx)/2 || 0;
          let by = (fy1 - fy)/2 || 0;
          let bs = (fs1 - fs)/2 || 0;
          bbC = [bx, by, bs];
        }
        obj.push(bbC);
      }
      arr.push(obj);
    }
    // console.log(arr);
    return arr;
  }

  /**
   * [calcExternalBoundaries => compute the external boundaries term for each frame]
   * @return {[list ]} [external boundaries]
   */
  this.calcExternalBoundaries = function() {
    let arr = [];
    let names_involved = [];
    for(let a of this.actors_involved) {
      names_involved.push(a.actor_name);
    }
    for(let i=0; i<this.bboxes.length; i++) {
      let val_l = Number.MAX_VALUE;
      let val_r = Number.MAX_VALUE;
      let xl = 0;
      let xl1 = 0;
      let xr = 0;
      let xr1 = 0;
      let tl = 0;
      let tr = 0;
      for(let a of preparation_editor.actors_timeline) {
        if(!names_involved.includes(a.actor_name)) {
          let bb = this.getCSBbox(i, a);
          if(bb && bb[0] < this.bboxes[i][0]) {
            if(abs(bb[0], this.bboxes[i][0]) < val_l) {
              val_l = abs(bb[2], this.bboxes[i][0]);
              xl = bb[0];
              xl1 = bb[2];
              if(bb[2]<this.bboxes[i][0]) {
                tl = 0;
              } else {
                tl = 1;
              }
            }
          } else if(bb && bb[2] > this.bboxes[i][2]) {
            if(abs(bb[2], this.bboxes[i][2])< val_r) {
              val_r = abs(bb[2], this.bboxes[i][2]);
              xr = bb[2];
              xr1 = bb[0];
              if(bb[0]>this.bboxes[i][2]) {
                tr = 0;
              } else {
                tr = 1;
              }
            }
          }
        }
      }
      let obj = [xl, xl1, xr, xr1, tl, tr];
      arr.push(obj);
    }
    // console.log(arr);
    return arr;
  }

  /**
   * [calcVectorScreenPos => compute a term for each frame, -1 if the actors involved are on the right side of the screen, 1 if the actors involved are on the left side of the screen]
   * @return {[list]} [list of screen positions term]
   */
  this.calcVectorScreenPos = function() {
    let arr = [];
    let names_involved = [];
    for(let a of this.actors_involved) {
      names_involved.push(a.actor_name);
    }
    for (let i=0; i<this.bboxes.length; i++) {
      let left = false;
      let right = false;
      for(let a of preparation_editor.actors_timeline) {
        if(!names_involved.includes(a.actor_name)) {
          let c = a.getCenterAct(i);
          if(c.x < this.bboxes[i][0]) {
            left = true;
          }
          if(c.x > this.bboxes[i][2]) {
            right = true;
          }
        }
      }
      if(left && right) {
        arr.push(0);
      } else if(right && !left) {
        arr.push(-1);
      } else if(!right && left) {
        arr.push(1);
      } else {
        arr.push(0);
      }
    }
    return arr;
  }

  /**
   * [getInOutBBox => compute a bounding box to fill the gap when an actor leave the stage]
   * @param  {[integer]} act_in                [frame number when the actor is not detected]
   * @param  {[integer]} act_out               [frame number when the actor is detected]
   * @return {[list]}         [bounding box (x,y, width, height)]
   */
  this.getInOutBBox = function(act_in, act_out) {
    let bbox_out;
    if(this.bboxes[act_out] && !this.bboxes[act_out].every(v => v === this.bboxes[act_out][0]) && this.bboxes[act_out].indexOf(undefined)==-1) {
      bbox_out = this.bboxes[act_out];
    }
    let bbox_in;
    if(this.bboxes[act_in] && !this.bboxes[act_in].every(v => v === this.bboxes[act_in][0]) && this.bboxes[act_in].indexOf(undefined)==-1) {
      bbox_in = this.bboxes[act_in];
    }

    if(bbox_in && bbox_out) {
      let bbox = [];
      bbox[0] = min(bbox_out[0], bbox_in[0]);
      bbox[1] = min(bbox_out[1], bbox_in[1]);
      bbox[2] = max(bbox_out[2], bbox_in[2]);
      bbox[3] = max(bbox_out[3], bbox_in[3]);
      bbox = getAdaptedBBox(bbox, this.aspect_ratio);
      return bbox;
    } else if(bbox_in && !bbox_out) {
      return bbox_in;
    } else if(!bbox_in && bbox_out) {
      return bbox_out;
    } else {
      return undefined;
    }
  }

  /**
   * [calcBboxes => This function compute a bounding box for each frame based on the framing tab description ('size', 'actors involved', 'keep out / pull in', 'gaze', 'stage pos', 'aspect ratio')]
   * @param  {[float]} aspectRatio               [shot aspect ratio width/height]
   */
  this.calcBboxes = function(aspectRatio) {
    this.bboxes = [];
    var mask = [];
    let curr;
    let curr_mask = [];
    for(var i=0; i< total_frame; i++) {
      var f_num = i;
      if(this.actors_involved.length<1) {
        this.bboxes.push([0,0,Number(original_width),Number(original_height)]);
      } else {
        var bbox = getBBoxShot(this.type, this.aspect_ratio, f_num);
        if(bbox) {
          let index = bbox.findIndex(Number.isNaN);
          const testUndefined = bbox.findIndex(bbox => bbox === undefined);
          if(index!=-1 || testUndefined!=-1) {
            this.bboxes.push([0,0,0,0]);
            mask.push(f_num);
            curr = undefined;
          } else {
            this.bboxes.push(bbox);
            curr = bbox;
          }
        } else {
          if(curr_mask.length<1) {
            curr_mask.push(f_num);
          }else if(curr_mask[curr_mask.length-1] == f_num-1) {
            curr_mask.push(f_num);
          } else {
            mask.push(curr_mask);
            curr_mask = [];
            curr_mask.push(f_num);
          }
          this.bboxes.push([0,0,0,0]);
          curr = undefined;
        }
      }
    }
    if(curr_mask.length>=1) {
        mask.push(curr_mask);
    }

    curr_mask = [];
    for(let i=0; i< mask.length; i++) {
      curr_mask.push(mask[i]);
    }
    mask = [];

    // For each list of frame (length > 1) where the actor is not detected, we compute an in out bounding box which is
    // the combination of the previous detected bounding box with the next one
    for(let c of curr_mask) {
      let in_out_bbox = this.getInOutBBox(Math.max(0,c[0]-2), Math.min(c[c.length-1]+1,total_frame-1));
      if(in_out_bbox) {
        for(f_num of c) {
          this.bboxes[f_num] = in_out_bbox;
        }
      }
    }

    if(this.actors_involved.length>=1) {
      var tab = [];
      for(let a of this.actors_involved) {
        tab.push(a.actor_name);
      }
      let vid_w = Number(original_width);
      let vid_h = Number(original_height);
      var data = {'abs_path':abs_path, 'aspect_ratio':this.aspect_ratio, 'video_width': vid_w, 'video_height': vid_h,
      'shot':JSON.stringify(this.bboxes), 'mask':JSON.stringify(mask), 'type':this.type, 'actors_involved': JSON.stringify(tab),
      'keep_out':this.is_keep_out, 'pull_in':this.is_pull_in, 'stage':this.is_stage_position, 'gaze':this.is_gaze_direction, 'split_screen_shot':this.is_split_screen_shot,
      'crop_factor': JSON.stringify(this.calcCropingFactor(this.aspect_ratio,vid_w*1/100)), 'apparent_motion': JSON.stringify(this.calcApparentMotion(this.aspect_ratio)),
      'external_bound': JSON.stringify(this.calcExternalBoundaries()), 'screen_pos':JSON.stringify(this.calcVectorScreenPos())};
      this.doAnAjax('stabilize', data, this.callbackBBox);
    } else {
      this.type = 'WS';
    }
  }

  this.doAnAjax = function(newUrl, data, callBack) {
    $.post({
        url: newUrl,
        async: true,
        dataType: 'json',
        data: data,
        cache: false,
        success: function(data, i) {
            myRtnA = "succes";
            return callBack( myRtnA, data);  // return callBack() with myRtna
        },
        error: function(data) {
            myRtnA = "error";
            return callBack ( myRtnA, data ); // return callBack() with myRtna
        }
    });
    }

  /**
   * [callbackBBox => get the stabilized framing for each frame from the server]
   * @param  {[string]} myRtnA               [success or error]
   * @param  {[list]} data                 [shot data]
   */
  this.callbackBBox = function(myRtnA, data) {
    if(myRtnA == "succes") {
      console.log(data);
      let pullin = data['pull_in'] == 'true';
      let keepout = data['keep_out'] == 'true';
      let gaze_dir = data['gaze'] == 'true';
      let stage_pos = data['stage'] == 'true';
      let split_screen_shot = data['split_screen_shot'] == 'true';
      console.log(keepout, pullin, stage_pos, gaze_dir, parseFloat(data['aspect_ratio']));
      let i = montage_editor.getShot(cadrage_editor.add_shot, data['type'], data['actors_involved'], parseFloat(data['aspect_ratio']), keepout, pullin, stage_pos, gaze_dir, split_screen_shot);

      cadrage_editor.add_shot[i].bboxes = data['bboxes'];
      cadrage_editor.add_shot[i].in_stabilize = false;
      // add_shot[i].accuracy_rate = add_shot[i].getAccuracyRate();
      cadrage_editor.add_shot.splice(i,1);
      console.log(cadrage_editor.add_shot.length);
      if(split_screen_editor.shots_in_creation) {
        let b = true;
        for(let s of cadrage_editor.add_shot) {
          if(s.type == split_screen_editor.size_in_creation && s.is_split_screen_shot) {
            b = false;
            break;
          }
        }
        if(cadrage_editor.add_shot.length == 0 || b) {
          split_screen_editor.callbackLastShot();
        }
      }
    } else if(myRtnA == "error") {
      console.error('error');
    }
  }

  /**
   * [stabilizeSplitAndMerge => apply the split and merge constraint for a split shot]
   */
  this.stabilizeSplitAndMerge = function() {
    this.split_and_merge = true;
    console.log(this.split_and_merge);
    var tab = [];
    for(let a of this.actors_involved) {
      tab.push(a.actor_name);
    }
    for(let a of preparation_editor.actors_timeline) {
      if(tab.includes(a.actor_name)) {
        a.on = true;
      } else {
        a.on = false;
      }
    }
    let vid_w = Number(original_width);
    let vid_h = Number(original_height);
    let bboxes_to_stabilise = [];
    for(var i=0; i< total_frame; i++) {
      let f_num = i;
      let b = false;
      for(let c of this.constraints_split_screen) {
        if(f_num == c.Frame || f_num == c.Frame-1 || f_num == c.Frame+1) {
          bboxes_to_stabilise.push([c.X,c.Y,c.X+c.W,c.Y+c.H]);
          b = true;
          break;
        }
      }
      if(!b) {
        bboxes_to_stabilise.push(this.bboxes[f_num]);
      }
    }
    var data = {'abs_path':abs_path, 'aspect_ratio':this.aspect_ratio, 'video_width': vid_w, 'video_height': vid_h,
    'shot':JSON.stringify(bboxes_to_stabilise),'type':this.type, 'actors_involved': JSON.stringify(tab),
    'keep_out':this.is_keep_out, 'pull_in':this.is_pull_in, 'stage':this.is_stage_position, 'gaze':this.is_gaze_direction, 'split_screen_shot':this.is_split_screen_shot,
    'constraints':JSON.stringify(this.constraints_split_screen),'screen_pos':JSON.stringify(this.calcVectorScreenPos())};
     // console.log(data);
    this.doAnAjax('stabilize_split_merge', data, this.callbackSplitAndMerge);
  }

  /**
   * [callbackBBox => get the stabilized with the split and merge constraint framing for each frame from the server]
   * @param  {[string]} myRtnA               [success or error]
   * @param  {[list]} data                 [shot data]
   */
  this.callbackSplitAndMerge = function(myRtnA, data) {
    if(myRtnA == "succes") {
      console.log(data);
      let pullin = data['pull_in'] == 'true';
      let keepout = data['keep_out'] == 'true';
      let gaze_dir = data['gaze'] == 'true';
      let stage_pos = data['stage'] == 'true';
      let split_screen_shot = data['split_screen_shot'] == 'true';
      // console.log(keepout, pullin, stage_pos, gaze_dir, parseFloat(data['aspect_ratio']));
      console.log(data['type'], data['actors_involved']);
      let act_inv = [];
      for(let name of data['actors_involved']) {
        act_inv.push(preparation_editor.getAct(name));
      }
      let shot = split_screen_editor.getSplitShot(data['type'], act_inv, act_inv.length)
      shot.bboxes = data['bboxes'];
      console.log(split_screen_editor.getSplitShot(data['type'], act_inv, act_inv.length).bboxes);

      shot.split_and_merge = false;
      let last_shot = true;
      for(let s of split_screen_editor.split_screen_shots) {
        console.log(s.type, s.split_and_merge);
        if(s.split_and_merge) {
          last_shot = false;
          break;
        }
      }
      if(last_shot) {
        split_screen_editor.callbackLastSplitAndMerge(data['type']);
      }
    } else if(myRtnA == "error") {
      console.log("error");
      for(let s of split_screen_editor.split_screen_shots) {
        if(s.split_and_merge) {
          split_screen_editor.callbackLastSplitAndMerge(s.type);
          break;
        }
      }
    }
  }

  /**
   * [isFrameBbox => check if all the actors are detected at the frame f_n]
   * @param  {[integer]} [f_n=undefined]               [frame number]
   * @return {[Boolean]}                 [true if they are all detected]
   */
  this.isFrameBbox = function(f_n = undefined) {
    if(f_n == undefined) {
      f_n = frame_num;
    }
    let ret = false;
    let is_t_b = false;
    for(let act of this.actors_involved) {
      for(let t of act.tracks) {
        var detections_track = t.detections;
        var first_frame = t.first_frame;
        if(first_frame < f_n) {
          if(detections_track.length > (f_n-first_frame)) {
            ret = true;
            break;
          }
        }
      }
      for(let t of act.track_bbox_shot) {
        if(t.first_frame < f_n && t.last_frame > f_n) {
          is_t_b = true;
          break;
        }
      }
    }
    if(ret || is_t_b){ret = true;}
    return ret;
  }

  /**
   * [getCurrStabShot => get the bounding box of the frame]
   * @param  {[integer]} frame_num               [frame number]
   * @return {[list]}           [(x,y,width,height)]
   */
  this.getCurrStabShot = function(frame_num) {
    if(this.bbox_perso) {
      return this.bbox_perso;
    }
    var ret;
    var bb;
    bb = this.bboxes[frame_num];
    if(bb && bb[0] != "null") {
      ret = bb;
    }
    return ret;
  }

  /**
   * [getCurrStabShotScale => get the bounding box of the frame
   * (each value are scaled by a factor video source height / height of compressed video 540p)]
   * @param  {[integer]} frame_num               [frame number]
   * @return {[list]}           [(x,y, width, height)]
   */
  this.getCurrStabShotScale = function(frame_num) {
    if(this.bbox_perso) {
      let bb = this.bbox_perso;
      return [bb[0]*scale_ratio, bb[1]*scale_ratio, bb[2]*scale_ratio, bb[3]*scale_ratio];
    }
    var ret;
    var bb;
    bb = this.bboxes[frame_num];
    if(bb && bb[0] != "null") {
      ret = [bb[0]*scale_ratio, bb[1]*scale_ratio, bb[2]*scale_ratio, bb[3]*scale_ratio];
    }
    return ret;
  }

  /**
   * [getActNameInvolved => get the actors name involved in the shot]
   * @return {[list]} [list of string]
   */
  this.getActNameInvolved = function() {
    var tab = [];
    for(var j=0; j<this.actors_involved.length;j++) {
        tab.push(this.actors_involved[j].actor_name);
    }
    return tab;
  }

  /**
   * [getUpdateActInvolved => add the actors not involved in the shot but included the framing at the frame f_n]
   * @param  {[integer]} [f_n=undefined]               [frame number]
   * @return {[list]}                 [list of actors name]
   */
  this.getUpdateActInvolved = function(f_n = undefined) {
    if(f_n == undefined) {
      f_n = frame_num;
    }
    let tab = [];
    let bbox = this.getCurrStabShot(f_n);
    if(bbox) {
      for(let a of preparation_editor.actors_timeline) {
        let center_act = a.getCenterAct(f_n);
        if(center_act.x > bbox[0] && center_act.x < bbox[2] && center_act.y > bbox[1] && center_act.y < bbox[3]) {
          tab.push(a.actor_name);
        }
      }
    }
    return tab;
  }

  /**
   * [getAccuracyRate => get the accuracy rate of shot, the number of frame where the shot respect the size required]
   * @return {[float]} [ratio of accuracy]
   */
  this.getAccuracyRate = function() {
    let cpt=0;
    let total=this.bboxes.length;
    for(let i=0; i<this.bboxes.length; i++) {
      if(this.isFrameBbox(i) && this.getUpdatedSizeShot(this.getCurrStabShot(i)[3],i)==this.type) {
        cpt++;
      } else if(!this.isFrameBbox(i)) {
        total--;
      }
    }
    return cpt/total;
  }

  /**
   * [setActInvoled => get the actor object based on their name and add it to the actors involved shot]
   * @param  {[string]} tab               [list of name]
   */
  this.setActInvoled = function(tab) {
    for(var j=0; j<tab.length; j++) {
      for(var i=0; i<preparation_editor.actors_timeline.length; i++) {
        if(preparation_editor.actors_timeline[i].actor_name == tab[j]) {
          this.actors_involved.push(preparation_editor.actors_timeline[i]);
        }
      }
    }
  }

  /**
   * [getUpdatedSizeShot => update the real size of the shot]
   * @param  {[float]} low_bound                     [low boundary of the framing]
   * @param  {[integer]} [f_n=undefined]               [frame number]
   * @return {[string]}                 [return updated size]
   */
  this.getUpdatedSizeShot = function(low_bound, f_n=undefined) {
    if(f_n == undefined) {
      f_n = frame_num;
    }
    let size = 0;
    let acts = this.getUpdateActInvolved(f_n);
    let dist = Number.MAX_VALUE;
    for(let name of acts) {
      let a = preparation_editor.getAct(name);
      let keypoints = a.getKeyPoints(f_n);
      if(keypoints) {
        let shot_factors = [1/9,1/7,1/5,1/3,3/5,1];
        for(let fact of shot_factors) {
          let bbox = getBBoxShotAdapted(this.aspectRatio, keypoints, fact, a);
          if(abs(low_bound-bbox[3]) < dist && fact > size) {
            size = fact;
            dist = abs(low_bound-bbox[3]);
          }
        }
      }
    }
    let ret;
    switch (size){
      case 1/9:
        ret = 'BCU';
        break;
      case 1/7:
        ret = 'CU';
        break;
      case 1/5:
        ret = 'MCU';
        break;
      case 1/3:
        ret = 'MS';
        break;
      case 3/5:
        ret = 'MLS';
        break;
      case 1:
        ret = 'FS';
        break;
      default:
        ret = 'WS';
        break;
    }
    return ret;
  }

  /**
   * [getPanMove => compute the movement of the shot]
   * @param  {[integer]} [f_n=undefined]               [frame number]
   * @return {[object]}                 [movement zoom, horizontal, vertical]
   */
  this.getPanMove = function(f_n = undefined) {
    if(f_n == undefined) {
      f_n = frame_num;
    }
    let pan = {};
    let w=Math.round(this.getCurrStabShot(f_n-5)[2]-this.getCurrStabShot(f_n-5)[0]);
    let x=Math.round((this.getCurrStabShot(f_n-5)[2]+this.getCurrStabShot(f_n-5)[0])/2);
    let y=Math.round((this.getCurrStabShot(f_n-5)[1]+this.getCurrStabShot(f_n-5)[3])/2);
    // for(let i=0; i<5; i++) {
    //   let off = f_n - i;
    //   w+=Math.round(this.getCurrStabShot(off)[2]-this.getCurrStabShot(off)[0]);
    //   x+=Math.round((this.getCurrStabShot(off)[2]+this.getCurrStabShot(off)[0])/2);
    //   y+=Math.round((this.getCurrStabShot(off)[3]+this.getCurrStabShot(off)[1])/2);
    // }
    // w /=5;
    // x /=5;
    // y /=5;
    pan.zoom = Math.round(Math.round(this.getCurrStabShot(f_n)[2]-this.getCurrStabShot(f_n)[0])- w);// / abs(Math.round(this.getCurrStabShot(f_n)[2]-this.getCurrStabShot(f_n)[0])- w);
    pan.horizontal = Math.round(Math.round((this.getCurrStabShot(f_n)[2]+this.getCurrStabShot(f_n)[0])/2)-x);// / abs(Math.round((this.getCurrStabShot(f_n)[2]+this.getCurrStabShot(f_n)[0])/2)-x);
    pan.vertical = Math.round(Math.round((this.getCurrStabShot(f_n)[1]+this.getCurrStabShot(f_n)[3])/2)-y);// / abs(Math.round((this.getCurrStabShot(f_n)[1]+this.getCurrStabShot(f_n)[3])/2)-y);
    return pan;
  }

  /**
   * [getPlacement => get the placement of the actors on stage]
   * @param  {[object]} act                           [actor]
   * @param  {[integer]} [f_n=undefined]               [frame number]
   * @return {[string]}                 [left, center left, center right, right]
   */
  this.getPlacement = function(act, f_n = undefined) {
    let ret;
    if(f_n == undefined) {
      f_n = frame_num;
    }
    let bbox = this.getCurrStabShot(f_n);
    let c = act.getCenterAct(f_n);
    let quarter = Math.round((bbox[2]-bbox[0])/4);
    let fact;
    let ratio;
    for(let i=1; i<=4; i++) {
      if(c.x < bbox[0]+quarter*i) {
        fact = i;
        ratio = round_prec((c.x-bbox[0])/(bbox[2]-bbox[0]),2);
        break;
      }
    }
    switch(fact) {
      case 1:
        ret = 'left '+ratio;
        break;
      case 2:
        ret = 'center left '+ratio;
        break;
      case 3:
        ret = 'center rigth '+ratio;
        break;
      case 4:
        ret = 'rigth '+ratio;
        break;
    }
    return ret;
  }

  this.setPosition = function(tX, tY, tW, tH) {
    this.x  = tX;
    this.y  = tY;
    this.w  = tW;
    this.h  = tH;
  }

  /**
   * [displayText => function called in the main p5 draw function]
   */
  this.displayText = function() {
    // let acts = this.getUpdateActInvolved();
    push();
    fill(255);;
    let type;
    if(this.getCurrStabShot(frame_num)) {
      type = this.getUpdatedSizeShot(this.getCurrStabShot(frame_num)[3]);
    }
    if(!type) {
      type = this.type;
    }
    let keep = "";
    if(this.is_keep_out) {
      keep = " K";
    }
    let pull = "";
    if(this.is_pull_in) {
      pull = " P";
    }
    let gaze = "";
    if(this.is_gaze_direction) {
      gaze = " G";
    }
    let stage_pos = "";
    if(this.is_stage_position) {
      stage_pos = " S";
    }
    text(this.type+' '+round_prec(this.aspect_ratio,2)+keep+pull+gaze+stage_pos/*+Math.round(this.accuracy_rate*100)+'%'*/, this.x, this.y+10);
    for(let i=0; i<this.actors_involved.length; i++) {
      text(this.actors_involved[i].actor_name, this.x, this.y+20+i*10);
    }
    if(this.in_stabilize) {
      text('STABILIZATION IN PROCESS',this.x + 100, this.y + 20);
    }
    pop();
  }
}
