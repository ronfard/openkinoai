"""
This file is part of KinoAi project.
It is subject to the license terms in the LICENSE file found in the top-level directory
"""


from django.urls import path

from . import views
from . import KinoAiIndexViews
from . import VideoEditingViews
from . import VideoBookViews
from . import VideosLibraryViews
from . import CorpusViews
from . import NotingViews

app_name = 'kino_app'
urlpatterns = [
    path('', KinoAiIndexViews.IndexView.as_view(), name='index'),
    path('set_previous', KinoAiIndexViews.set_previous, name='set_previous'),
    path('modify_password', KinoAiIndexViews.modify_password, name='modify_password'),
    path('create_project', KinoAiIndexViews.create_project, name='create_project'),
    path('check_project', KinoAiIndexViews.check_project, name='check_project'),
    path('edit_project', KinoAiIndexViews.edit_project, name='edit_project'),
    path('remove_project', KinoAiIndexViews.remove_project, name='remove_project'),
    path('modified_path', KinoAiIndexViews.modified_path, name='modified_path'),
    path('update_json_shots', KinoAiIndexViews.update_json_shots, name='update_json_shots'),
    path('preprocess', KinoAiIndexViews.preprocess, name='preprocess'),
    path('upload', KinoAiIndexViews.upload_view, name='upload'),

    path('video_book/<int:id>/<str:name_notebook>', VideoBookViews.video_book, name='video_book'),
    path('video_book/save_note_video', VideoBookViews.save_note_video, name='save_note_video'),
    path('video_book/extract_image_full_crop', VideoBookViews.extract_image_full_crop, name='extract_image_full_crop'),
    path('video_book/get_full_data_shots', VideoBookViews.get_full_data_shots, name='get_full_data_shots'),

    path('corpus_search/<project>', CorpusViews.corpus_search, name='corpus_search'),
    path('add_text', CorpusViews.add_text, name='add_text'),
    path('remove_text', CorpusViews.remove_text, name='remove_text'),
    path('add_actor_corpus', CorpusViews.add_actor_corpus, name='add_actor_corpus'),
    path('save_partition_text', CorpusViews.save_partition_text, name='save_partition_text'),

    path('videos_library/<project>', VideosLibraryViews.videos_library_app, name='videos_library_app'),
    path('check_password_library', VideosLibraryViews.check_password_library, name='check_password_library'),
    path('set_library_false', VideosLibraryViews.set_library_false, name='set_library_false'),
    path('upload_video_library', VideosLibraryViews.upload_video_library, name='upload_video_library'),
    path('remove_video_library', VideosLibraryViews.remove_video_library, name='remove_video_library'),

    path('noting_app/<project>', NotingViews.noting_app, name='noting_app'),
    path('download_notes', NotingViews.download_notes, name='download_notes'),
    path('save_notes', NotingViews.save_notes, name='save_notes'),
    path('download_subs', NotingViews.download_subs, name='download_subs'),

    path('video_editing/<int:id>', VideoEditingViews.video_editing, name='video_editing'),
    path('video_editing/get_data_detec', VideoEditingViews.get_data_detec, name='get_data_detec'),
    path('video_editing/get_data_shots', VideoEditingViews.get_data_shots, name='get_data_shots'),
    path('video_editing/save_shot_perso', VideoEditingViews.save_shot_perso, name='save_shot_perso'),
    path('video_editing/submit', VideoEditingViews.submit, name='submit'),
    path('video_editing/remove_shot', VideoEditingViews.remove_shot, name='remove_shot'),
    path('video_editing/save_timeline', VideoEditingViews.save_timeline, name='save_timeline'),
    path('video_editing/save_note', VideoEditingViews.save_note, name='save_note'),
    path('video_editing/save_partitions', VideoEditingViews.save_partitions, name='save_partitions'),
    path('video_editing/upload_rough_cut', VideoEditingViews.upload_rough_cut, name='upload_rough'),
    path('video_editing/load_sub', VideoEditingViews.load_sub, name='load_sub'),
    path('video_editing/load_json', VideoEditingViews.load_json, name='load_json'),
    path('video_editing/download_vtt', VideoEditingViews.download_vtt, name='download_vtt'),
    path('video_editing/stabilize', VideoEditingViews.stabilize, name='stabilize'),
    path('video_editing/stabilize_split_merge', VideoEditingViews.stabilize_split_merge, name='stabilize_split_merge'),
    path('video_editing/split_screen_layout_configuration', VideoEditingViews.split_screen_layout_configuration, name='split_screen_layout_configuration'),
    path('video_editing/save_split_screen_final', VideoEditingViews.save_split_screen_final, name='save_split_screen_final'),
    path('video_editing/remove_split_screen', VideoEditingViews.remove_split_screen, name='remove_split_screen'),
    path('video_editing/fullhd', VideoEditingViews.fullhd, name='fullhd'),
    path('video_editing/processKeyFrames', VideoEditingViews.processKeyFrames, name='processKeyFrames'),
    path('video_editing/check_progress_bar', VideoEditingViews.check_progress_bar, name='check_progress_bar'),
    path('video_editing/delete_video', VideoEditingViews.delete_video, name='delete_video'),
    path('video_editing/reframeMov', VideoEditingViews.reframeMov, name='reframeMov')
]
