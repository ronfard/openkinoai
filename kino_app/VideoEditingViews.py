"""
This file is part of KinoAi project.
It is subject to the license terms in the LICENSE file found in the top-level directory
"""

from django.shortcuts import get_object_or_404, render, redirect
from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect
from django.views import generic
from django.urls import reverse
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.hashers import make_password, check_password, is_password_usable
from django.contrib.auth.models import Permission, User
from django.views.decorators.csrf import csrf_exempt
from .models import FolderPath, Detections, Shots, Project
from django.core.files.storage import FileSystemStorage
from django.views.decorators.cache import never_cache
from django.utils.dateparse import parse_date

from . import utils
import os
import json
import re
import cvxpy as cvx
import numpy as np
import scipy as sp
import scipy.sparse
from . import StabilizeOptimizer as stab
from . import StabilizeOptimizerConstraintStartEnd as stab_cons
from . import StabilizeSplitAndMerge as stab_split_merge
from . import SplitScreenOptimizer as split_opt
from moviepy.editor import VideoFileClip, ImageSequenceClip, AudioFileClip
import subprocess
import shutil
import ctypes
import math, random
import time
import cv2

"""
This file contains all the functions to interact with the video editing interface
"""

def video_editing(request, id):

    dir = get_object_or_404(FolderPath, pk=id)
    print(dir.owner)
    video_path = dir.abs_path+'/original540.mp4'
    # print(os.path.join(settings.BASE_DIR, 'kino_app/static/kino_app/data/', dir.path)+'/detections.json')
    info = str(subprocess.check_output('ffprobe -i {0} -show_entries stream=width,height,r_frame_rate -v quiet -of csv="p=0"'.format(video_path), shell=True ,stderr=subprocess.STDOUT))
    width = int(re.findall(r'\d+', info)[0])
    height = int(re.findall(r'\d+', info)[1])
    frame_rate = round(int(re.findall(r'\d+', info)[2])/int(re.findall(r'\d+', info)[3]))
    print(width, height, frame_rate)
    start = time.time()
    # detec_obj = Detections.objects.get(path=dir)
    # # print(os.sys.getsizeof(detec_obj.json_data))
    # detec = detec_obj.json_data
    print(time.time()-start)
    split_title = dir.path.split('/')
    part = ''
    if len(split_title) >1:
        title = split_title[0]+'/'+split_title[1]
        if len(split_title) >2:
            part = split_title[2]
    else:
        title = split_title[0]
    new_path=''
    for i in range(len(split_title)-1):
        new_path+=split_title[i]+'/'
    prev=''
    next =''
    if len(part)>1 and part.split('t')[1].isdigit():
        check_next = FolderPath.objects.filter(path=new_path+'part{:02d}'.format(int(part.split('t')[1])+1)).count()
        check_prev = FolderPath.objects.filter(path=new_path+'part{:02d}'.format(int(part.split('t')[1])-1)).count()

        if check_next>0:
            next = FolderPath.objects.get(path=new_path+'part{:02d}'.format(int(part.split('t')[1])+1)).id
        if check_prev>0:
            prev = FolderPath.objects.get(path=new_path+'part{:02d}'.format(int(part.split('t')[1])-1)).id


    if not os.path.isdir(os.path.join(dir.abs_path,'user_data')):
        os.makedirs(os.path.join(dir.abs_path,'user_data'))
    if not os.path.isdir(os.path.join(dir.abs_path,'user_data',request.user.username)):
        os.makedirs(os.path.join(dir.abs_path,'user_data',request.user.username))

    for file in os.listdir(dir.abs_path):
        if '_note.json' in file:
            user = file.split('_')[0]
            if not os.path.isdir(os.path.join(dir.abs_path,'user_data',user)):
                os.makedirs(os.path.join(dir.abs_path,'user_data',user))
            shutil.move(os.path.join(dir.abs_path,file),os.path.join(dir.abs_path,'user_data',user,file))
        if '_timelines.json' in file:
            user = file.split('_')[0]
            if not os.path.isdir(os.path.join(dir.abs_path,'user_data',user)):
                os.makedirs(os.path.join(dir.abs_path,'user_data',user))
            shutil.move(os.path.join(dir.abs_path,file),os.path.join(dir.abs_path,'user_data',user,file))
        if '_partitions_objects.json' in file:
            user = file.split('_')[0]
            if not os.path.isdir(os.path.join(dir.abs_path,'user_data',user)):
                os.makedirs(os.path.join(dir.abs_path,'user_data',user))
            shutil.move(os.path.join(dir.abs_path,file),os.path.join(dir.abs_path,'user_data',user,file))

    if not os.path.isfile(os.path.join(dir.abs_path,'user_data',str(request.user.username),str(request.user.username)+'_note.json')):
        file = open(os.path.join(dir.abs_path,'user_data',str(request.user.username),str(request.user.username)+'_note.json'),"w")
        file.write("[]")
        file.close()

    data_note = []
    user_timeline = []
    user_partitions = []
    request.session['previous_name'] = title
    for root, subdirs, files in os.walk(os.path.join(dir.abs_path,'user_data')):
        for d in subdirs:
            for r, s, f in os.walk(os.path.join(root,d)):
                for file in f:
                    if '_note.json' in file:
                        note_obj = {}
                        note_obj['User'] = file.split('_')[0]
                        tab = []
                        with open(os.path.join(r,file)) as f:
                            tab = json.load(f)
                        for t in tab:
                            t['Text'] = t['Text'].replace('\"','\\"').replace('\n','\\n')
                        note_obj['Note'] = tab
                        data_note.append(note_obj)
                    if '_timelines.json' in file:
                        with open(os.path.join(r,file)) as json_file:
                            timeline = json.load(json_file)
                            if len(timeline) != 0:
                                user_timeline.append(file.split('_')[0])
                    if '_partitions_objects.json' in file:
                        with open(os.path.join(r,file)) as json_file:
                            partitions = json.load(json_file)
                            if len(partitions) != 0:
                                user_partitions.append(file.split('_')[0])

    data_path_video_export = []
    for root, subdirs, files in os.walk(dir.abs_path+'/videos_export/'+request.user.username):
        for file in files:
            out_vid = root+'/'+file
            d = out_vid.split('/media/')
            resp = '/media/'+d[-1]
            data_path_video_export.append(resp)

    if request.user is None or request.user.is_authenticated == False:
        print(settings.LOGIN_URL)
        return HttpResponseRedirect(settings.LOGIN_URL)
    return render(request, 'kino_app/video_editing.html', {'id':id, 'title':split_title[1], 'part':part, 'path':dir.path,
    'abs_path':dir.abs_path, 'width':width, 'height':height, 'frame_rate':round(frame_rate), 'next_id':next, 'prev_id':prev,
    'owner':dir.owner, 'data_note':json.dumps(data_note), 'username':request.user.username, 'user_partitions':user_partitions, 'user_timeline':user_timeline,
    'data_path_video_export':json.dumps(data_path_video_export)})
    # return render(request, 'kino_app/index.html', {'image' : data_path})

def get_shot_from_spec(shots, type, actors_involved, aspect_ratio, keepout, pullin, stage_pos, gaze_dir):
    ret = None
    acts = []
    for a in actors_involved:
        acts.append(a)
    for s in shots:
        if s['Type'] == type and s['AspectRatio'] == aspect_ratio and keepout == s['KeepOut'] and pullin == s['PullIn'] \
            and stage_pos == s['StagePos'] and gaze_dir == s['GazeDir']:
            b1 = True
            for a in s['ActInvolved']:
                if a not in acts:
                  b1 = False
                  break
            if b1 and len(s['ActInvolved']) == len(acts):
                ret = s
                break
    return ret

def get_index_shot_from_full_spec(shots, type, actors_involved, aspect_ratio, keepout, pullin, stage_pos, gaze_dir):
    ret = 0;
    for s in shots:
        if 'Intersect'in s and s['Intersect'] == True:
            s['KeepOut'] = True
            s['PullIn'] = True
        else:
            if not 'KeepOut' in s:
                s['KeepOut'] = False
            if not 'PullIn' in s:
                s['PullIn'] = False
        if s['Type'] == type and s['AspectRatio'] == aspect_ratio and keepout == s['KeepOut'] and pullin == s['PullIn'] \
            and stage_pos == s['StagePos'] and gaze_dir == s['GazeDir']:
            b1 = True
            for a in s['ActInvolved']:
                if a not in actors_involved:
                    b1 = False
                    break

            if b1 and len(s['ActInvolved']) == len(actors_involved):
                break
        ret+=1
    return ret;

def get_index_shot_personalize(shots, type, is_personalize):
    ret = 0;
    for s in shots:
        if 'IsPersonalize' in s and s['IsPersonalize'] == is_personalize:
            if s['Type'] == type:
                break
        ret+=1
    return ret;

def get_bbox_from_shot(shots_timeline, frame_num, shots):
    ret = None
    for s in shots_timeline:
        if frame_num <= s['EndFrame'] and frame_num >= s['StartFrame']:
            if 'KeepOut' not in s:
                s['KeepOut'] = False
            if 'PullIn' not in s:
                s['PullIn'] = False
            if 'StagePos' not in s:
                s['StagePos'] = False
            if 'GazeDir' not in s:
                s['GazeDir'] = False
            original_shot = get_shot_from_spec(shots, s['Type'], s['ActInvolved'], s['AspectRatio'], s['KeepOut'], s['PullIn'], s['StagePos'], s['GazeDir'])
            if original_shot:
                bb = original_shot['BBoxes'][frame_num]
                if bb and bb[0] != 'null':
                    ret = bb
            break
    return ret

@csrf_exempt
def get_data_detec(request):
    abs_path = request.POST.get('abs_path','')
    dir = get_object_or_404(FolderPath, abs_path=abs_path)
    detec_obj = Detections.objects.get(path=dir)
    detec = detec_obj.json_data
    if(request.method == 'POST'):
        print('post data detec')
        return HttpResponse(json.dumps({'data_detec':detec}), content_type='application/json')
    return HttpResponse('')

@csrf_exempt
def get_data_shots(request):
    abs_path = request.POST.get('abs_path','')
    dir = get_object_or_404(FolderPath, abs_path=abs_path)
    data_shots = []
    for shot_string in Shots.objects.filter(path=dir):
        data_shots.append(json.loads(shot_string.json_data))

    if(request.method == 'POST'):
        print('post data shots')
        return HttpResponse(json.dumps({'data_shots':data_shots}), content_type='application/json')
    return HttpResponse('')

@csrf_exempt
def save_shot_perso(request):
    aspect_ratio = request.POST.get('aspect_ratio','')
    abs_path = request.POST.get('abs_path','')
    type = request.POST.get('type','')
    is_personalize = request.POST.get('is_personalize','') == 'true'
    end_frame = request.POST.get('end_frame','')
    bbox_perso = json.loads(request.POST.get('bbox_perso',''))
    new_s_obj = {}
    new_s_obj['Type'] = type
    new_s_obj['AspectRatio'] = float(aspect_ratio)
    new_s_obj['StartFrame'] = 0
    new_s_obj['EndFrame'] = int(end_frame)
    new_s_obj['BBoxPerso'] = bbox_perso
    new_s_obj['IsPersonalize'] = is_personalize
    new_s_obj['ScenesInvolved'] = []

    dir = get_object_or_404(FolderPath, abs_path=abs_path)

    obj, created = Shots.objects.get_or_create(
        path=dir,
        json_data=json.dumps(new_s_obj),
    )
    return HttpResponse('')

@csrf_exempt
def save_note(request):
    abs_path = request.POST.get('abs_path','')
    json_notes = json.loads(request.POST.get('notes',''))
    print(json_notes)
    with open(os.path.join(abs_path,'user_data',str(request.user.username),str(request.user.username)+'_note.json'), 'w') as fp:
        json.dump(json_notes, fp, indent=2)
    return HttpResponse('')

@csrf_exempt
def save_partitions(request):
    abs_path = request.POST.get('abs_path','')
    vtt_partitions = json.loads(request.POST.get('partitions',''))
    json_partitions = json.loads(request.POST.get('partitions_json',''))
    utils.save_vtt(vtt_partitions, abs_path+'/subtitle.vtt')
    with open(os.path.join(abs_path,'user_data',request.user.username,request.user.username+'_partitions_objects.json'), 'w') as fp:
        json.dump(json_partitions, fp)
    return HttpResponse('')

def get_queryset_from_name(test_name):
    list_path = FolderPath.objects.filter(path__icontains=test_name)
    remove_path = []
    for folder in list_path:
        name = folder.path.split('/')[1]
        if name != test_name:
            remove_path.append(folder.path)
    for p in remove_path:
        list_path = list_path.exclude(path=p)

    return list_path

@csrf_exempt
def submit(request):
    abs_path = request.POST.get('abs_path','')
    json_detec_path = abs_path+'/detections.json'
    json_timeline_path = abs_path+'/actors_timeline.json'
    json_annot_timeline_path = abs_path+'/annotation_timeline.json'
    json_tracklets_path = abs_path+'/tracklets.json'
    json_shots_path = abs_path+'/shots.json'
    print(json_detec_path)
    data_detec = request.POST.get('detec','')
    dir = get_object_or_404(FolderPath, abs_path=abs_path)

    if data_detec != 'null':
        detec_obj = Detections.objects.get(path=dir)
        detec_obj.json_data = data_detec
        detec_obj.save()
    else:
        print('no detec')

    if os.path.isfile(abs_path+'/subtitle_new.vtt'):
        if os.path.isfile(abs_path+'/subtitle.vtt'):
            os.remove(abs_path+'/subtitle.vtt')
        os.rename(abs_path+'/subtitle_new.vtt',abs_path+'/subtitle.vtt')

    data_annot = request.POST.get('annotation','')
    parse_annot = json.loads(data_annot)
    with open(json_annot_timeline_path, 'w') as fp:
        json.dump(parse_annot, fp)

    data_timelines = request.POST.get('timeline','')
    if data_timelines != 'null':
        print('act timeline')
        parse_timelines = json.loads(data_timelines)
        for a in parse_timelines:
            prev_name = a['PrevName']
            new_name = a['Name']
            if prev_name != new_name:
                for shot_string in Shots.objects.filter(path=dir):
                    s = json.loads(shot_string.json_data)
                    if prev_name in s['ActInvolved']:
                        for i in range(len(s['ActInvolved'])):
                            if s['ActInvolved'][i] == prev_name:
                                s['ActInvolved'][i] = new_name
                        shot_string.json_data = json.dumps(s)
                        shot_string.save()

        with open(json_timeline_path, 'w') as fp:
            json.dump(parse_timelines, fp)

    data_tracks = request.POST.get('track','')
    if data_tracks != 'null':
        print('tracks')
        parse_tracks = json.loads(data_tracks)
        with open(json_tracklets_path, 'w') as fp:
            json.dump(parse_tracks, fp)

    data_shots = request.POST.get('shots','')
    data_scenes = request.POST.get('scenes_involved','')
    if data_shots != 'null':
        print('shots')
        parse_shots = json.loads(data_shots)
        with open(json_shots_path, 'w') as fp:
            json.dump(parse_shots, fp)
        for s in parse_shots:
            obj, created = Shots.objects.get_or_create(
                path=dir,
                json_data=json.dumps(s),
            )
    else:
        parse_scenes = json.loads(data_scenes)
        print('parse scenes')
        if len(parse_scenes) >0:
            for shot_string in Shots.objects.filter(path=dir):
                s = json.loads(shot_string.json_data)
                if 'IsPersonalize' in s:
                    index = get_index_shot_personalize(parse_scenes, s['Type'], s['IsPersonalize'])
                else:
                    index = get_index_shot_from_full_spec(parse_scenes, s['Type'], s['ActInvolved'], s['AspectRatio'], s['KeepOut'], s['PullIn'], s['StagePos'], s['GazeDir'])
                if index < len(parse_scenes):
                    s['ScenesInvolved'] = parse_scenes[index]['ScenesInvolved']
                    shot_string.json_data = json.dumps(s)
                    shot_string.save()

    if(request.method == 'POST'):
        print('post submit')
        return HttpResponse(json.dumps({'success':'Your work is saved'}), content_type='application/json')
    return HttpResponse('')

@csrf_exempt
def remove_shot(request):
    abs_path = request.POST.get('abs_path','')

    dir = get_object_or_404(FolderPath, abs_path=abs_path)

    s = json.loads(request.POST.get('shot',''))
    print(s)

    for shot_string in Shots.objects.filter(path=dir):
        shot = json.loads(shot_string.json_data)
        if 'IsPersonalize' in s and 'IsPersonalize' in shot:
            print(s)
            if shot['IsPersonalize'] == s['IsPersonalize'] and shot['Type'] == s['Type']:
                print(s)
                shot_string.delete()
                break
        else:
            if not 'KeepOut' in shot:
                shot['KeepOut'] = False
            if not 'PullIn' in shot:
                shot['PullIn'] = False
            if not 'StagePos' in shot:
                shot['StagePos'] = False
            if not 'GazeDir' in shot:
                shot['GazeDir'] = False
            if not 'SplitScreenShot' in shot:
                shot['SplitScreenShot'] = False
            if s['Type'] == shot['Type'] and s['AspectRatio'] == shot['AspectRatio'] and s['KeepOut'] == shot['KeepOut'] and s['PullIn'] == shot['PullIn'] \
                and s['StagePos'] == shot['StagePos'] and s['GazeDir'] == shot['GazeDir'] and s['SplitScreenShot'] ==  shot['SplitScreenShot']:
                b1 = True
                for a in s['ActInvolved']:
                    if a not in shot['ActInvolved']:
                        b1 = False
                        break

                if b1 and len(s['ActInvolved']) == len(shot['ActInvolved']):
                    shot_string.delete()

    if(request.method == 'POST'):
        return HttpResponse(json.dumps({'success':'shot removed'}), content_type='application/json')
    return HttpResponse('')

@csrf_exempt
def save_timeline(request):
    abs_path = request.POST.get('abs_path','')
    data_timelines = json.loads(request.POST.get('timeline',''))
    json_shots_path = os.path.join(abs_path,'user_data',request.user.username,request.user.username+'_timelines.json')
    with open(json_shots_path, 'w') as fp:
        json.dump(data_timelines, fp)

    if(request.method == 'POST'):
        print('post submit')
        return HttpResponse(json.dumps({'succes':'geat!!'}), content_type='application/json')
    return HttpResponse('')

def upload_rough_cut(request):
    print(request.FILES)
    # print(request.FILES, request.POST)
    if request.method == 'POST' and request.FILES:
        myfile = request.FILES['fileToUpload']
        abs_path = request.POST.get('abs_path','')
        print(request.FILES, '\n', myfile)
        info = str(request.FILES).split('[')[1].split('(')[-1].split('/')[1].split(')')[0]
        print(info)
        if info == 'json':
            print('json')
            fs = FileSystemStorage()
            filename = fs.save(myfile.name, myfile)
            with open(settings.MEDIA_ROOT+filename) as f:
                data = json.load(f)
            print(json.dumps(data))
            return_data = json.dumps(data)
            os.remove(settings.MEDIA_ROOT+filename)
            print('filename',filename)
            return HttpResponse(json.dumps({'type':'json', 'msg':'Great', 'data':return_data}), content_type='application/json')
        elif info == "vtt" or info == "octet-stream":
            print('vtt')
            fs = FileSystemStorage(location=abs_path)
            if os.path.isfile(abs_path+'/subtitle_new.vtt'):
                os.remove(abs_path+'/subtitle_new.vtt')
            filename = fs.save('subtitle_new.vtt', myfile)
            src = abs_path+'/'+filename
            print('filename',filename)
            sub = utils.parser_vtt(src)
            # os.remove(settings.MEDIA_ROOT+'/'+filename)
            return HttpResponse(json.dumps({'type':'vtt', 'msg':'Great', 'sub':sub}), content_type='application/json')
        else:
            print('not json')
            return HttpResponse(json.dumps({'msg':'Not Json'}), content_type='application/json')

@csrf_exempt
def load_sub(request):
    abs_path = request.POST.get('abs_path','')
    src = abs_path+'/subtitle.vtt'
    sub = utils.parser_vtt(src)
    # print(sub)
    if(request.method == 'POST'):
        print('ret')
        return HttpResponse(json.dumps({'type':'vtt', 'msg':'Great', 'sub':sub}), content_type='application/json')
    return HttpResponse('')

@csrf_exempt
def load_json(request):
    json_path = request.POST.get('json_path','')
    username = request.POST.get('username','')

    if not os.path.isfile(json_path):
        f = open(json_path, "w")
        f.write("[]")
        f.close()
    with open(json_path, "r") as jsonFile:
        data_json = json.load(jsonFile)
    # print(sub)
    if data_json and username =='':
        return HttpResponse(json.dumps(data_json), content_type='application/json')
    elif data_json and username !='':
        obj = {}
        obj['Name'] = username
        obj['Data'] = data_json
        return HttpResponse(json.dumps(obj), content_type='application/json')
    return HttpResponse('')

@csrf_exempt
def download_vtt(request):
    abs_path = request.POST.get('abs_path','')
    sub = json.loads(request.POST.get('tab_sub',''))
    src_vtt = abs_path+'/'+abs_path.split('/')[-2]+'_'+abs_path.split('/')[-1]+'.vtt'
    src_srt = abs_path+'/'+abs_path.split('/')[-2]+'_'+abs_path.split('/')[-1]+'.srt'
    utils.save_vtt(sub, src_vtt)
    utils.save_srt(sub, src_srt)

    resp_vtt = '/media/'+src_vtt.split('/media/')[-1]
    resp_srt = '/media/'+src_srt.split('/media/')[-1]
    if(request.method == 'POST'):
        return HttpResponse(json.dumps({'src_vtt':resp_vtt, 'src_srt':resp_srt}), content_type='application/json')
    return HttpResponse('')

@csrf_exempt
def stabilize(request):
    start = time.time()
    abs_path = request.POST.get('abs_path','')
    data_shots = request.POST.get('shot','')
    data_mask = request.POST.get('mask','')
    aspect_ratio = request.POST.get('aspect_ratio','')
    video_width = request.POST.get('video_width','')
    video_height = request.POST.get('video_height','')
    actors_involved = json.loads(request.POST.get('actors_involved',''))
    crop_factor = np.array(json.loads(request.POST.get('crop_factor','')), dtype = np.float32)
    apparent_motion = np.array(json.loads(request.POST.get('apparent_motion','')), dtype = np.float32)
    external_boundaries = np.array(json.loads(request.POST.get('external_bound','')), dtype = np.float32)
    screen_pos = np.array(json.loads(request.POST.get('screen_pos','')), dtype = np.float32)
    type = request.POST.get('type','')
    gaze = request.POST.get('gaze','')
    pull_in = request.POST.get('pull_in','')
    keep_out = request.POST.get('keep_out','')
    stage = request.POST.get('stage','')
    split_screen_shot = request.POST.get('split_screen_shot','')
    print(gaze, stage, pull_in, keep_out)
    shots_frame = json.loads(data_shots)
    mask = json.loads(data_mask)
    imageSize = [int(video_width), int(video_height)]
    print(imageSize)
    # shots_frame = stabilize_shot(np.array(shots_frame, dtype = np.float32), mask, np.float32(aspect_ratio), int(video_width), int(video_height))
    shots_frame = stab.stabilize_chunk(np.array(shots_frame, dtype = np.float32), np.float32(aspect_ratio), pull_in == 'true', keep_out == 'true', mask, imageSize, 24, crop_factor, apparent_motion, external_boundaries, screen_pos)
    # shots_frame = stab_cons.stabilize_chunk(np.array(shots_frame, dtype = np.float32), np.float32(aspect_ratio), pull_in == 'true', keep_out == 'true', mask, imageSize, 24, crop_factor, apparent_motion, external_boundaries, screen_pos)
    print(type, actors_involved)

    new_s_obj = {}
    new_s_obj['Type'] = type
    new_s_obj['ActInvolved'] = actors_involved
    new_s_obj['AspectRatio'] = float(aspect_ratio)
    new_s_obj['PullIn'] = pull_in == 'true'
    new_s_obj['KeepOut'] = keep_out == 'true'
    new_s_obj['StagePos'] = stage == 'true'
    new_s_obj['GazeDir'] = gaze == 'true'
    new_s_obj['SplitScreenShot'] = split_screen_shot == 'true'
    new_s_obj['Timeline'] = 0
    new_s_obj['StartFrame'] = 0
    new_s_obj['EndFrame'] = len(shots_frame)
    new_s_obj['BBoxes'] = shots_frame.tolist()
    new_s_obj['ScenesInvolved'] = []

    dir = get_object_or_404(FolderPath, abs_path=abs_path)

    obj, created = Shots.objects.get_or_create(
        path=dir,
        json_data=json.dumps(new_s_obj),
    )

    end = time.time()
    print ('Exec time ',end-start)
    return HttpResponse(json.dumps({'bboxes':shots_frame.tolist(), 'type':type, 'actors_involved':actors_involved,
    'gaze':gaze, 'stage':stage,'keep_out':keep_out, 'pull_in':pull_in, 'split_screen_shot':split_screen_shot, 'aspect_ratio':aspect_ratio}), content_type='application/json')

@csrf_exempt
def split_screen_layout_configuration(request):
    abs_path = request.POST.get('abs_path','')
    size = request.POST.get('size','')
    layout_actors_info = json.loads(request.POST.get('layout_actors_info',''))
    layout_frame = split_opt.layout_selection(layout_actors_info)

    sequence_obj = {}
    sequence_obj['Size'] = size
    sequence_obj['Sequence'] = layout_frame[2]

    json_split_screen_sequence_path = abs_path+'/split_screen_sequence.json'
    if os.path.isfile(json_split_screen_sequence_path):
        f = open(json_split_screen_sequence_path)
        sequence_list = json.load(f)
        f.close()
    else:
        sequence_list = []

    sequence_list.append(sequence_obj)
    with open(json_split_screen_sequence_path, 'w') as fp:
        json.dump(sequence_list, fp)
    # print(layout_actors_info)
    return HttpResponse(json.dumps({'msg':'success','data':layout_frame,'size':size}), content_type='application/json')

@csrf_exempt
def stabilize_split_merge(request):
    abs_path = request.POST.get('abs_path','')
    data_shots = request.POST.get('shot','')
    actors_involved = json.loads(request.POST.get('actors_involved',''))
    aspect_ratio = request.POST.get('aspect_ratio','')
    video_width = request.POST.get('video_width','')
    video_height = request.POST.get('video_height','')
    type = request.POST.get('type','')
    gaze = request.POST.get('gaze','')
    pull_in = request.POST.get('pull_in','')
    keep_out = request.POST.get('keep_out','')
    stage = request.POST.get('stage','')
    split_screen_shot = request.POST.get('split_screen_shot','')
    constraints = json.loads(request.POST.get('constraints',''))
    screen_pos = np.array(json.loads(request.POST.get('screen_pos','')), dtype = np.float32)

    # print(constraints)

    shots_frame = json.loads(data_shots)
    imageSize = [int(video_width), int(video_height)]

    shots_frame = stab_split_merge.stabilize_split_and_merge_constraints(np.array(shots_frame, dtype = np.float32), np.float32(aspect_ratio), imageSize, 24, constraints, screen_pos, actors_involved)

    # return HttpResponse(json.dumps({'msg':'success'}), content_type='application/json')
    return HttpResponse(json.dumps({'bboxes':shots_frame.tolist(), 'type':type, 'actors_involved':actors_involved,
    'gaze':gaze, 'stage':stage,'keep_out':keep_out, 'pull_in':pull_in, 'split_screen_shot':split_screen_shot, 'aspect_ratio':aspect_ratio}), content_type='application/json')

@csrf_exempt
def save_split_screen_final(request):
    abs_path = request.POST.get('abs_path','')
    size = request.POST.get('size','')
    shots_info = json.loads(request.POST.get('shots_info',''))

    sequence_obj = {}
    sequence_obj['Size'] = size
    sequence_obj['Shots'] = shots_info

    json_split_screen_shots_path = abs_path+'/split_screen_shots.json'
    if os.path.isfile(json_split_screen_shots_path):
        f = open(json_split_screen_shots_path)
        sequence_list = json.load(f)
        f.close()
    else:
        sequence_list = []

    sequence_list.append(sequence_obj)
    with open(json_split_screen_shots_path, 'w') as fp:
        json.dump(sequence_list, fp)
    return HttpResponse(json.dumps({'msg':'success'}), content_type='application/json')

@csrf_exempt
def remove_split_screen(request):
    abs_path = request.POST.get('abs_path','')
    size = request.POST.get('size','')

    json_split_screen_shots_path = abs_path+'/split_screen_shots.json'
    if os.path.isfile(json_split_screen_shots_path):
        f = open(json_split_screen_shots_path)
        shots_list = json.load(f)
        f.close()
    else:
        shots_list = []

    json_split_screen_sequence_path = abs_path+'/split_screen_sequence.json'
    if os.path.isfile(json_split_screen_sequence_path):
        f = open(json_split_screen_sequence_path)
        sequence_list = json.load(f)
        f.close()
    else:
        sequence_list = []

    i=0
    for seq in sequence_list:
        if seq['Size']==size:
            sequence_list.pop(i)
            break
        i+=1
    i=0
    for shots in shots_list:
        if shots['Size']==size:
            shots_list.pop(i)
            break
        i+=1

    with open(json_split_screen_shots_path, 'w') as fp:
        json.dump(shots_list, fp)
    with open(json_split_screen_sequence_path, 'w') as fp:
        json.dump(sequence_list, fp)

    return HttpResponse(json.dumps({'msg':'success'}), content_type='application/json')

@csrf_exempt
def fullhd(request):
    start = time.time()
    img_time = float(request.POST.get('time',''))
    abs_path = request.POST.get('abs_path','')
    # print(abs_path)
    num = math.floor(img_time)
    data_path = os.path.join(settings.MEDIA_ROOT, os.path.join("kino_app","image"))
    if os.path.isdir(data_path):
        shutil.rmtree(data_path)
    os.makedirs(data_path)
    img_path = os.path.join(data_path,str(num)+'.jpg')
    filename = os.path.join(abs_path,'original_hevc.mov')
    subprocess.check_call("ffmpeg -nostdin -y -v quiet -ss {0} -i {1} -q:v 2 -frames:v 1 {2}".format(img_time, filename, img_path), shell=True)
    data_path = "/media/kino_app/image/"+str(num)+".jpg"
    end = time.time()
    sec = (end-start)%60
    # print('Real exec time ',math.floor((end-start)/60),':',math.floor(sec), ':', int((sec%1)*100), ' in seconds ', end-start)
    return HttpResponse(json.dumps({'src':data_path}), content_type='application/json')

@csrf_exempt
def processKeyFrames(request):
    abs_path = request.POST.get('abs_path','')
    key_frames = json.loads(request.POST.get('KeyFrames',''))
    data_rushes = json.loads(request.POST.get('data',''))
    name = request.POST.get('name','')
    width = int(request.POST.get('width',''))
    sub_notebook = json.loads(request.POST.get('sub_notebook',''))

    filename = os.path.join(abs_path,'original_hevc.mov')

    hevc_w = int(subprocess.check_output('ffprobe -i {0} -show_entries stream=width -v quiet -of csv="p=0"'.format(filename), shell=True ,stderr=subprocess.STDOUT))
    factor = hevc_w / width

    folder= os.path.join(abs_path,'images',name)

    if not os.path.isdir(folder):
        os.makedirs(folder)
    else:
        return HttpResponse(json.dumps({'msg':'already created'}), content_type='application/json')

    subtitle = os.path.join(folder,name+'.vtt')
    json_bboxes = os.path.join(folder,name+'.json')
    with open(json_bboxes, 'w') as fp:
        json.dump(data_rushes, fp)

    utils.save_vtt(sub_notebook, subtitle)

    for k in key_frames:
        print(k['Time'])
        img_out_e = os.path.join(folder,str(k['Time'])+".jpg")
        img_out_r = os.path.join(folder,str(k['Time'])+"_crop.jpg")
        bbox = [int(k['BBox'][0])*factor, int(k['BBox'][1])*factor, int(k['BBox'][2])*factor, int(k['BBox'][3])*factor]
        print(bbox)
        w = math.ceil((bbox[2] - bbox[0])/2)*2
        h = math.ceil((bbox[3] - bbox[1])/2)*2
        x = bbox[0]
        y = bbox[1]
        print(x,y,w,h)
        img_inter = os.path.join(folder,"inter.png")
        subprocess.check_call("ffmpeg -nostdin -y -v quiet -ss {0} -i {1} -q:v 2 -frames:v 1 {2}".format(float(k['Time']), filename, img_inter), shell=True)
        subprocess.check_call("convert -quality 50 {0} {1}".format(img_inter, img_out_e), shell=True)
        os.remove(img_inter)
        subprocess.check_call("convert {0} -crop {1}x{2}+{3}+{4} +repage {5}".format(img_out_e, w, h, x, y, img_out_r), shell=True)
    return HttpResponse(json.dumps({'name':name}), content_type='application/json')

def crop(image):
    crop.progress = round(crop.counter/len(crop.bboxes)*100)
    with open(os.path.join(crop.abs_path,'progress.txt'),'w') as fp:
        fp.write('{0}'.format(crop.progress))

    if crop.counter < len(crop.bboxes) :
        bbox = crop.bboxes[crop.counter]
        crop.counter += 1
        x = int(bbox[0])
        y = int(bbox[1])
        w = int(round((bbox[2] - bbox[0])/2)*2)
        h = int(round((bbox[3] - bbox[1])/2)*2)
        # print(w/h, crop.size[0]/crop.size[1], image.dtype)
        if math.isclose((bbox[2] - bbox[0])/(bbox[3] - bbox[1]), crop.size[0]/crop.size[1], rel_tol=1e-2):
            return cv2.resize(image[y:y+h, x:x+w],(crop.size[0],crop.size[1]),interpolation=cv2.INTER_LANCZOS4)
        else:
            # print((bbox[2] - bbox[0]), (bbox[3] - bbox[1]), (bbox[2] - bbox[0])/(bbox[3] - bbox[1]))
            blank_image = np.zeros((crop.size[1],crop.size[0],3), dtype='uint8')
            if w>h:
                height = int(crop.size[1])
                width = int(height*(w/h))
                if width > int(crop.size[0]):
                    width=int(crop.size[0])
                img = cv2.resize(image[y:y+h, x:x+w],(width,height),interpolation=cv2.INTER_LANCZOS4)
                x_off = int(abs(crop.size[0]-width)/2)

                if x_off+width > int(crop.size[0]):
                    x_off=0
                blank_image[0:height, x_off:x_off+width] = img

            else:
                width = int(crop.size[0])
                height = int(width/(w/h))
                if height > int(crop.size[1]):
                    height=int(crop.size[1])
                img = cv2.resize(image[y:y+h, x:x+w],(width,height),interpolation=cv2.INTER_LANCZOS4)
                y_off = int(abs(crop.size[1]-height)/2)
                if y_off+height > int(crop.size[1]):
                    y_off=0
                blank_image[y_off:y_off+height, 0:width]= img
            return blank_image
    elif crop.counter >= len(crop.bboxes):
        bbox = crop.bboxes[len(crop.bboxes)-1]
        crop.counter += 1
        x = int(bbox[0])
        y = int(bbox[1])
        w = int(round((bbox[2] - bbox[0])/2)*2)
        h = int(round((bbox[3] - bbox[1])/2)*2)
        # print(w/h, crop.size[0]/crop.size[1], image.dtype)
        if math.isclose((bbox[2] - bbox[0])/(bbox[3] - bbox[1]), crop.size[0]/crop.size[1], rel_tol=1e-2):
            return cv2.resize(image[y:y+h, x:x+w],(crop.size[0],crop.size[1]),interpolation=cv2.INTER_LANCZOS4)
    else:
        return image

def crop_split(image, bbox, size):
    x = int(bbox[0])
    y = int(bbox[1])
    w = int(math.ceil((bbox[2] - bbox[0])/2)*2)
    h = int(math.ceil((bbox[3] - bbox[1])/2)*2)
    # print(w/h, crop.size[0]/crop.size[1], image.dtype)
    if math.isclose((bbox[2] - bbox[0])/(bbox[3] - bbox[1]), size[0]/size[1], rel_tol=1e-2):
        return cv2.resize(image[y:y+h, x:x+w],(size[0],size[1]),interpolation=cv2.INTER_LANCZOS4)
    else:
        # print((bbox[2] - bbox[0]), (bbox[3] - bbox[1]), (bbox[2] - bbox[0])/(bbox[3] - bbox[1]))
        blank_image = np.zeros((size[1],size[0],3), dtype='uint8')
        if w<h:
            height = int(size[1])
            width = int(height*(w/h))
            if width > int(size[0]):
                width=int(size[0])
            img = cv2.resize(image[y:y+h, x:x+w],(width,height),interpolation=cv2.INTER_LANCZOS4)
            x_off = int(abs(size[0]-width)/2)

            if x_off+width > int(size[0]):
                x_off=0
            blank_image[0:height, x_off:x_off+width] = img

        else:
            width = int(size[0])
            height = int(width/(w/h))
            if height > int(size[1]):
                height=int(size[1])
            img = cv2.resize(image[y:y+h, x:x+w],(width,height),interpolation=cv2.INTER_LANCZOS4)
            y_off = int(abs(size[1]-height)/2)
            if y_off+height > int(size[1]):
                y_off=0
            blank_image[y_off:y_off+height, 0:width]= img
        return blank_image

def split_screen(image):
    split_screen.progress = round(split_screen.counter/len(split_screen.bboxes)*100)
    with open(os.path.join(split_screen.abs_path,'progress.txt'),'w') as fp:
        fp.write('{0}'.format(split_screen.progress))
    if split_screen.counter < len(split_screen.bboxes) :
        bbox = split_screen.bboxes[split_screen.counter]
    else:
        bbox = split_screen.bboxes[len(split_screen.bboxes)-1]

    height, width = image.shape[:2]
    x = 0
    y = split_screen.top_pos
    w = width
    h = int(height/2)
    top_image = cv2.resize(image[y:y+h, x:x+w],(int(width/(split_screen.scale_factor/2)),int(height/split_screen.scale_factor)),interpolation=cv2.INTER_LANCZOS4)
    # print(width, height)
    new_image = crop_split(image, bbox[0], split_screen.size)
    i=0
    for bb in bbox:
        if i>0:
            new_image = np.hstack((new_image,crop_split(image, bb, split_screen.size)))
        i+=1
        # print(crop_split(image, bb, split_screen.size))
    split_screen.counter += 1
    if new_image.size !=0:
        return np.vstack((new_image,top_image))
    else:
        return image

@csrf_exempt
def check_progress_bar(request):
    abs_path = request.POST.get('abs_path','')
    progress = None
    if os.path.isfile(os.path.join(abs_path,'progress.txt')):
        with open(os.path.join(abs_path,'progress.txt'),'r') as fp:
            progress = int(fp.readline())
    if progress != None and progress >=100:
        os.remove(os.path.join(abs_path,'progress.txt'))
    if progress != None:
        # print(crop.progress)
        return HttpResponse(json.dumps({'progress':progress}))
    else:
        return HttpResponse(json.dumps({'progress':0}))

@csrf_exempt
def delete_video(request):
    video_name = request.POST.get('video_name','')
    abs_path = request.POST.get('abs_path','')
    if os.path.isfile(os.path.join(abs_path,os.path.join('videos_export',os.path.join(request.user.username,video_name)))):
        os.remove(os.path.join(abs_path,os.path.join('videos_export',os.path.join(request.user.username,video_name))))
        list = []
        for root, dir, files in os.walk(os.path.join(abs_path,os.path.join('videos_export',request.user.username))):
            for file in files:
                list.append('/media'+os.path.join(root.split('media')[len(root.split('media'))-1],file))
        print(list)
        return HttpResponse(json.dumps({'message':'done','list':list}))
    return HttpResponse(json.dumps({'message':'not a file'}))

@csrf_exempt
def reframeMov(request):
    start = time.time()
    bbox_string = request.POST.get('bboxes','')
    abs_path = request.POST.get('abs_path','')
    width = int(request.POST.get('width',''))
    aspect_ratio = float(request.POST.get('aspect_ratio',''))
    t_start = float(request.POST.get('t_start',''))
    t_end = float(request.POST.get('t_end',''))
    name_export = str(request.POST.get('name_export','')).replace('\n','')
    name_export = "".join(x for x in name_export if x.isalnum())
    is_full_split_screen = request.POST.get('is_full_split_screen','')!="false"
    split_top_pos = int(request.POST.get('top_position',''))
    bbox = np.array(json.loads(bbox_string), dtype=float)

    videoname = abs_path+'/original_hevc.mov'
    audiofile = abs_path+'/audio.m4a'

    hevc_w = int(subprocess.check_output('ffprobe -i {0} -show_entries stream=width -v quiet -of csv="p=0"'.format(videoname), shell=True ,stderr=subprocess.STDOUT))
    factor = hevc_w / width
    print(hevc_w, width, factor, aspect_ratio)
    print(request.user.username)
    scale_factor = factor
    if is_full_split_screen:
        bbox[:][:,]*=factor
        split_screen.bboxes = bbox
        split_screen.counter = 0
        split_screen.progress = 0
        split_screen.abs_path = abs_path
        split_screen.progress = None
        split_screen.top_pos = int(split_top_pos*factor)
        split_screen.scale_factor = factor
        print(bbox[:])
    else:
        bbox[:,] *= factor
        res = bbox[:,2] - bbox[:,0]
        min_w = min(res)
        # print('min_w', min_w, hevc_w, min_w==hevc_w, max(res))
        if min_w==hevc_w:
            scale_factor = 1
        crop.bboxes = bbox
        crop.counter = 0
        crop.progress = 0
        crop.abs_path = abs_path
        print(max, scale_factor)

    print(len(bbox))
    if not os.path.isdir(abs_path+'/videos_export'):
        os.makedirs(abs_path+'/videos_export')
    if not os.path.isdir(abs_path+'/videos_export/'+request.user.username):
        os.makedirs(abs_path+'/videos_export/'+request.user.username)

    out_vid = abs_path+'/videos_export/'+request.user.username+'/'+name_export+'.mp4'
    if is_full_split_screen:
        if os.path.isfile(out_vid):
            os.remove(out_vid)
    if not os.path.isfile(out_vid):
        print(videoname)
        clip = VideoFileClip(videoname)
        width = clip.size[0]
        height = clip.size[1]
        if aspect_ratio < clip.size[0]/clip.size[1]:
            height = clip.size[1]
            width = height*aspect_ratio
        elif aspect_ratio > clip.size[0]/clip.size[1]:
            width = clip.size[0]
            height = width*aspect_ratio
        clip.size = [int(width/scale_factor),int(height/scale_factor)]
        print(clip.size)

        if is_full_split_screen:
            if os.path.isfile(out_vid):
                os.remove(out_vid)
            sub_clip = clip.subclip(t_start,t_end)
            sub_clip.size = clip.size
            split_screen.size = [int(sub_clip.size[0]/2),sub_clip.size[1]]
            # sub_clip.size = [sub_clip.size[0],sub_clip.size[1]]
            print(sub_clip.size, split_screen.size)
            new_clip = sub_clip.fl_image( split_screen )
            if os.path.isfile(audiofile):
                audio_clip = AudioFileClip(audiofile)
                sub_audio = audio_clip.subclip(t_start,t_end)
                new_clip.audio = sub_audio
            new_clip.write_videofile(out_vid, threads=16, preset='ultrafast', logger=None, codec="libx264")
        else:
            sub_clip = clip.subclip(t_start,t_end)
            sub_clip.size = clip.size
            crop.size = sub_clip.size
            new_clip = sub_clip.fl_image( crop )
            if os.path.isfile(audiofile):
                audio_clip = AudioFileClip(audiofile)
                sub_audio = audio_clip.subclip(t_start,t_end)
                new_clip.audio = sub_audio
            new_clip.write_videofile(out_vid, threads=16, preset='ultrafast', logger=None)

        end = time.time()

        d = out_vid.split('/media/')
        resp = '/media/'+d[-1]

        print(end-start)

        if os.path.isfile(out_vid):
            response = HttpResponse(json.dumps({'src':resp}))
            return response
        else:
            print('no file')
            return HttpResponse('')
    else:
        return HttpResponse(json.dumps({'msg':'Already created'}))
