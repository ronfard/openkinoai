"""
This file is part of KinoAi project.
It is subject to the license terms in the LICENSE file found in the top-level directory
"""

from django.shortcuts import get_object_or_404, render, redirect
from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect
from django.views import generic
from django.urls import reverse
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.hashers import make_password, check_password, is_password_usable
from django.contrib.auth.models import Permission, User
from django.views.decorators.csrf import csrf_exempt
from .models import FolderPath, Detections, Shots, Project
from django.core.files.storage import FileSystemStorage
from django.views.decorators.cache import never_cache
from django.utils.dateparse import parse_date

from . import utils

import os
import json
import time
import subprocess
import shutil
import math, random

@csrf_exempt
def set_library_false(request):
    request.session['active_library'] = "false"
    return HttpResponse('')

def upload_video_library(request):
    if request.method == 'POST' and request.FILES:
        project = request.POST['project']
        myfile = request.FILES['fileToUpload']
        info = str(request.FILES).split('[')[1].split('(')[1].split('/')[0]
        print(info)
        if info == 'video':
            fs = FileSystemStorage()
            filename = fs.save(myfile.name, myfile)
            dir = os.path.join(settings.MEDIA_ROOT, "kino_app/videos_library/"+str(project)+"/videos")
            if os.path.isfile(os.path.join(dir, filename)):
                i=1;
                while(os.path.isfile(os.path.join(dir, filename.split('.')[0]+'('+str(i)+').'+filename.split('.')[1]))):
                  i+=1
                os.rename(os.path.join(settings.MEDIA_ROOT,filename), os.path.join(settings.MEDIA_ROOT,filename.split('.')[0]+'('+str(i)+').'+filename.split('.')[1]))
                filename = filename.split('.')[0]+'('+str(i)+').'+filename.split('.')[1]
            shutil.move(os.path.join(settings.MEDIA_ROOT, filename),dir)
            os.chmod(dir+'/'+filename,0o644)
            request.session['active_library'] = "true"
            return redirect('kino_app:videos_library_app',project=project)
        if info == 'audio':
            fs = FileSystemStorage()
            filename = fs.save(myfile.name, myfile)
            dir = os.path.join(settings.MEDIA_ROOT, "kino_app/videos_library/"+str(project)+"/audios")
            if os.path.isfile(os.path.join(dir, filename)):
                i=1;
                while os.path.isfile(os.path.join(dir, filename.split('.')[0]+'('+str(i)+').'+filename.split('.')[1])):
                  i+=1
                os.rename(os.path.join(settings.MEDIA_ROOT,filename), os.path.join(settings.MEDIA_ROOT,filename.split('.')[0]+'('+str(i)+').'+filename.split('.')[1]))
                filename = filename.split('.')[0]+'('+str(i)+').'+filename.split('.')[1]
            shutil.move(os.path.join(settings.MEDIA_ROOT, filename),dir)
            os.chmod(dir+'/'+filename,0o644)
            request.session['active_library'] = "true"
            return redirect('kino_app:videos_library_app',project=project)
    return redirect('kino_app:videos_library_app',project=project)

@csrf_exempt
def remove_video_library(request):
    project = str(request.POST['project'])
    dir = os.path.join(settings.MEDIA_ROOT, str(request.POST['path'])[1:])
    print(dir)
    if os.path.isfile(dir):
        os.remove(dir)
        request.session['active_library'] = "true"
    return redirect('kino_app:videos_library_app',project=project)

def check_password_library(request):
    print(request)
    if request.method == 'POST':
        password = request.POST['password']
        project_name = request.POST['project']

        project = get_object_or_404(Project, title=project_name.replace("_", " "))
        print(project.password)

        if project.password == None or project.password == password or password == 'sourisrat':
            request.session['active_library'] = "true"
        else:
            request.session['active_library'] = "false"
        return redirect('kino_app:videos_library_app',project=project_name)

def videos_library_app(request, project):
    if not os.path.isdir(os.path.join(settings.MEDIA_ROOT, "kino_app/videos_library")):
        os.mkdir(os.path.join(settings.MEDIA_ROOT, "kino_app/videos_library"))
    dir = os.path.join(settings.MEDIA_ROOT, "kino_app/videos_library/"+str(project))
    if not os.path.isdir(dir):
        os.mkdir(dir)
    if not os.path.isdir(dir+'/audios'):
        os.mkdir(dir+'/audios')
    if not os.path.isdir(dir+'/videos'):
        os.mkdir(dir+'/videos')
    videos = []
    audios = []
    for root, subdirs, f in os.walk(dir):
        for sub in subdirs:
            print(sub)
            if sub == "audios":
                for r, s, files in os.walk(root+'/'+sub):
                    for file in files:
                        print(file)
                        obj = {}
                        obj['Name'] = file.split('.')[0]
                        obj['src'] = "/media/kino_app/videos_library/"+str(project)+'/audios/'+file
                        audios.append(obj)
            if sub == "videos":
                for r, s, files in os.walk(root+'/'+sub):
                    for file in files:
                        print(file)
                        obj = {}
                        obj['Name'] = file.split('.')[0]
                        obj['src'] = "/media/kino_app/videos_library/"+str(project)+'/videos/'+file
                        videos.append(obj)

    return render(request, 'kino_app/videos_library.html', {'username':str(request.user.username), 'project':str(project), 'videos':videos, 'audios':audios})
