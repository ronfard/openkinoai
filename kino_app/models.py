"""
This file is part of KinoAi project.
It is subject to the license terms in the LICENSE file found in the top-level directory
"""

from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class FolderPath(models.Model):
    title = models.TextField(default="")
    path = models.TextField()
    abs_path = models.TextField()
    owner = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True, default=None)

class Detections(models.Model):
    json_data = models.TextField()
    path = models.ForeignKey(FolderPath, models.SET_NULL, blank=True, null=True,)

class Shots(models.Model):
    json_data = models.TextField()
    path = models.ForeignKey(FolderPath, models.SET_NULL, blank=True, null=True,)

class Project(models.Model):
    title = models.TextField()
    company = models.TextField()
    date = models.DateField()
    password = models.TextField(blank=True, null=True, default=None)
