"""
This file is part of KinoAi project.
It is subject to the license terms in the LICENSE file found in the top-level directory
"""

from django.shortcuts import get_object_or_404, render, redirect
from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect
from django.views import generic
from django.urls import reverse
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.hashers import make_password, check_password, is_password_usable
from django.contrib.auth.models import Permission, User
from django.views.decorators.csrf import csrf_exempt
from .models import FolderPath, Detections, Shots, Project
from django.core.files.storage import FileSystemStorage
from django.views.decorators.cache import never_cache
from django.utils.dateparse import parse_date

import os
import json
import time
import subprocess
import shutil
import math, random

def list_notebook(project_dir):
    list = []
    for dir in os.listdir(project_dir):
        obj = {}
        obj["Name"] = dir
        if len(FolderPath.objects.filter(abs_path__icontains=os.path.join(project_dir,dir,'part01'))) != 0:
            obj["Title"] = get_object_or_404(FolderPath, abs_path=os.path.join(project_dir,dir,'part01')).title
        else:
            print('not get it')
            obj["Title"] = dir
        for root, dirs, files in os.walk(os.path.join(project_dir,dir)):
            for d in dirs:
                if 'part' in d:
                    print(d)
                    if len(FolderPath.objects.filter(abs_path__icontains=os.path.join(root,d))) == 0:
                        print('New ',os.path.join(root,d))
                        create_folder_path_and_detec_path(os.path.join(root,d),None)
                if d == "images":
                    new_list = os.listdir(os.path.join(root,d))
                    if 'List' not in obj or len(obj["List"]) == 0:
                        obj["List"] = new_list
                    else:
                        for n in new_list:
                            if n not in obj["List"]:
                                obj["List"].append(n)
        list.append(obj)
    return list

def list_files(startpath, user):
    list = []
    list_register_title = []
    img_folder_path = os.path.join(settings.MEDIA_ROOT, "kino_app","index_images")
    for folder_obj in FolderPath.objects.filter(owner=user).filter(abs_path__icontains=startpath).order_by('path'):
        if not os.path.isdir(folder_obj.abs_path):
            print('A suppr',os.path.join(img_folder_path,folder_obj.path))
            if os.path.isdir(os.path.join(img_folder_path,folder_obj.path.split('/part')[0])):
                shutil.rmtree(os.path.join(img_folder_path,folder_obj.path.split('/part')[0]))
            Shots.objects.filter(path=folder_obj).delete()
            Detections.objects.filter(path=folder_obj).delete()
            folder_obj.delete()
        else:
            if folder_obj.title == "":
                folder_obj.title = folder_obj.path.split('/')[-2]
                folder_obj.save()
            if folder_obj.title not in list_register_title:
                list_register_title.append(folder_obj.title)
                list.append({"path":folder_obj.path,"title":folder_obj.title})
    return list

def count_files_project(start_path):
    cpt = 0
    for root, dirs, files in os.walk(start_path):
        for name in dirs:
            if 'part' in name:
                cpt+=1
    return cpt

def create_folder_path_and_detec_path(abs_path, user):
    path = abs_path.split('/kino_app/data/')[1]
    obj_folder, created = FolderPath.objects.get_or_create(
        title=path.split('/')[-2],
        path=path,
        abs_path=abs_path,
        owner = user
    )
    obj_detec = Detections.objects.filter(path=obj_folder)
    if len(obj_detec) == 0:
        with open(os.path.join(settings.MEDIA_ROOT, 'kino_app/data/', obj_folder.path)+'/detections.json') as f:
            data = json.load(f)
        obj, created = Detections.objects.get_or_create(
            path=obj_folder,
            json_data=json.dumps(data),
        )
        folder_path = os.path.join(settings.MEDIA_ROOT, "kino_app","index_images",obj_folder.path)
        if not os.path.isdir(folder_path) :
            video_path = obj_folder.abs_path+'/original540.mp4'
            result = float(subprocess.check_output('ffprobe -i {0} -show_entries format=duration -v quiet -of csv="p=0"'.format(video_path), shell=True ,stderr=subprocess.STDOUT))
            unit = (result-1)/10
            timecodes = []
            for i in range(10):
                timecodes.append(unit*i)
            os.makedirs(folder_path)
            i=0
            for time in timecodes:
                img_path = folder_path+'/poster'+str(i)+'.jpg'
                i+=1
                subprocess.check_call("ffmpeg -nostdin -y -v quiet -ss {0} -i {1} -frames:v 1 {2}".format(time, video_path, img_path), shell=True)

def extract_index_images(path, user):
    for root, dirs, files in os.walk(path):
        for name in dirs:
            if 'part' in name:
                abs = os.path.join(root,name)
                create_folder_path_and_detec_path(abs,user)

def extractImagesAndDelete(list, len_old, user):
    # print(list)
    if len(list) != len_old:
        img_folder_path = os.path.join(settings.MEDIA_ROOT, "kino_app/index_images/")
        for p in FolderPath.objects.all():
            if p.owner == None or p.owner == user:
                if p.path not in list:
                    print('delete ', p)
                    Detections.objects.filter(path=p).delete()
                    p.delete()
                else :
                    obj = Detections.objects.filter(path=p)
                    if len(obj) == 0:
                        with open(os.path.join(settings.MEDIA_ROOT, 'kino_app/data/', p.path)+'/detections.json') as f:
                            data = json.load(f)
                        obj, created = Detections.objects.get_or_create(
                            path=p,
                            json_data=json.dumps(data),
                        )
                        folder_path = img_folder_path+p.path
                        if not os.path.isdir(folder_path) :
                            video_path = p.abs_path+'/original540.mp4'
                            result = float(subprocess.check_output('ffprobe -i {0} -show_entries format=duration -v quiet -of csv="p=0"'.format(video_path), shell=True ,stderr=subprocess.STDOUT))
                            unit = (result-1)/10
                            timecodes = []
                            for i in range(10):
                                timecodes.append(unit*i)
                            os.makedirs(folder_path)
                            i=0
                            for time in timecodes:
                                img_path = folder_path+'/poster'+str(i)+'.jpg'
                                i+=1
                                subprocess.check_call("ffmpeg -nostdin -y -v quiet -ss {0} -i {1} -frames:v 1 {2}".format(time, video_path, img_path), shell=True)

@csrf_exempt
def set_previous(request):
    name = request.POST.get('name','')
    request.session['previous_name'] = name
    request.session['active_project'] = ''
    return HttpResponse('')

def launch_preprocess(name, project_name, user):
    print(user)
    utility = settings.MEDIA_ROOT+'utility'
    shutil.move(settings.MEDIA_ROOT+name,utility)
    duration = float(subprocess.check_output('ffprobe -i "{0}" -show_entries format=duration -v quiet -of csv="p=0"'.format(utility+'/'+name), shell=True ,stderr=subprocess.STDOUT))
    print(duration)
    sec = math.floor(duration%60)
    min = math.floor((duration/60)%60)
    hour = math.floor((duration/60)/60)
    print('Real exec time ',hour,':',min,':',sec, ':', int((duration%1)*100), ' in seconds ', duration)
    dir = name.split('.')[0]
    path = os.path.join(settings.MEDIA_ROOT, 'kino_app/data/'+project_name)
    if not os.path.isdir(os.path.join(path, dir)):
        with open(utility+'/configAutoCAM.sh', 'w') as fp:
            string_config = 'takes=("{0};{1}")\n parts=('.format(name, dir)
            nb_part = 1
            for h in range(hour+1):
                if h != hour:
                    for m in range(5):
                        string_config += '"{0};{1};part{n_p:02d};{h:02d}:{m:02d}:00;00:10:00"\n'.format(name, dir,n_p=nb_part,h=h,m=m*10)
                        nb_part += 1
                    string_config += '"{0};{1};part{n_p:02d};{h:02d}:50:00;00:10:00"\n'.format(name, dir,n_p=nb_part,h=h)
                    nb_part += 1
                else:
                    if min == 0:
                        string_config += '"{0};{1};part01;00:00:00;00:10:00"\n'.format(name, dir)
                    for m in range(math.ceil(min/10)):
                        string_config += '"{0};{1};part{n_p:02d};{h:02d}:{m:02d}:00;00:10:00"\n'.format(name, dir,n_p=nb_part,h=h,m=m*10)
                        nb_part += 1
            string_config += ')'
            fp.write('{0}'.format(string_config))
        subprocess.check_call(["./preprocessAutoCAM.sh"], cwd=utility)
        os.remove(utility+'/shots/'+dir+'/'+name)
        shutil.move(utility+'/shots/'+dir,path)
        print(os.path.join(path,dir))
        extract_index_images(os.path.join(path,dir), user)
        return True
    else:
        os.remove(utility+'/'+name)
        return False

def upload_view(request):
    if request.user is None or request.user.is_authenticated == False:
        print(settings.LOGIN_URL)
        return HttpResponseRedirect(settings.LOGIN_URL)
    if request.method == 'POST' and request.FILES:
        title = request.POST['title']
        private = request.POST.get('private') != None
        myfile = request.FILES['fileToUpload']
        print(request.FILES, '\n', myfile)
        info = str(request.FILES).split('[')[1].split('(')[1].split('/')[0]
        if info == 'video':
            fs = FileSystemStorage()
            filename = fs.save(myfile.name, myfile)
            old = filename
            filename = filename.replace('%22','_').replace("'",'_').replace(" ", "_").replace('\n','')
            os.rename(settings.MEDIA_ROOT+old,settings.MEDIA_ROOT+filename)
            uploaded_file_url = fs.url(filename)
            owner_video = None
            if private:
                owner_video = request.user
            if launch_preprocess(filename, title.replace('%22','_').replace("'",'_').replace(" ", "_").replace('\n',''), owner_video):
                print('finished')
                return redirect('kino_app:index')
            else:
                print('already')
                return render(request, 'kino_app/upload.html', {'msg':'Already added','title':title, 'private':private})
        else:
            print('not video')
            return render(request, 'kino_app/upload.html', {'msg':'Not a video','title':title, 'private':False})
    else:
        if request.method == 'POST':
            title = request.POST['title']
            return render(request, 'kino_app/upload.html', {'title':title, 'private':False})
        return render(request, 'kino_app/upload.html')

def preprocess(request):
    print(request)
    return render(request, 'kino_app/process.html')


##### Project and account management

def modify_password(request):
    if request.method == 'POST':
        old_password = request.POST['old_password']
        new_password = request.POST['new_password']
        if request.user.is_active and request.user.is_authenticated:
            if request.user.check_password(old_password):
                request.user.set_password(new_password)
                request.user.save()

    return redirect('kino_app:index')

@csrf_exempt
def check_project(request):
    if request.method == 'POST':
        title = request.POST['title']
        password = request.POST['password']
        proj = get_object_or_404(Project, title=title)
        if check_password(password, proj.password):
            request.session['active_project'] = title.replace(" ", "_")
        else:
            request.session['active_project'] = 'wrong password'
        return redirect('kino_app:index')

def modif_path(old, new):
    list_word = old.split('/')
    list_word[0] = new
    return '/'.join(list_word)

def modif_abs_path(old, new):
    first_list = old.split('data/')
    second_list = first_list[1].split('/')
    second_list[0] = new
    first_list[1] = '/'.join(second_list)
    return 'data/'.join(first_list)

@csrf_exempt
def edit_project(request):
    if request.method == 'POST':
        title = request.POST['title'].replace("_", " ")
        title_replace = title.replace(' ','_')
        old_title = request.POST['old_title']
        old_replace = old_title.replace("_", " ")
        company = request.POST['company']
        date = request.POST['date']
        print(title, date, company)
        proj = get_object_or_404(Project, title=old_replace)
        proj.title = title
        for root, dirs, files in os.walk(os.path.join(settings.MEDIA_ROOT, 'kino_app/data/')):
            for d in dirs:
                if d == old_title:
                    os.rename(os.path.join(settings.MEDIA_ROOT, 'kino_app/data/'+d), os.path.join(settings.MEDIA_ROOT, 'kino_app/data/'+title_replace))
                    break
            break
        for root, dirs, files in os.walk(os.path.join(settings.MEDIA_ROOT, 'kino_app/index_images/')):
            for d in dirs:
                if d == old_title:
                    os.rename(os.path.join(settings.MEDIA_ROOT, 'kino_app/index_images/'+d), os.path.join(settings.MEDIA_ROOT, 'kino_app/index_images/'+title_replace))
                    break
            break
        folders = FolderPath.objects.filter(path__icontains=old_title)
        for f in folders:
            new_path = modif_path(f.path, title_replace)
            new_abs_path = modif_abs_path(f.abs_path, title_replace)
            f.path = new_path
            f.abs_path = new_abs_path
            f.save()
        proj.company = company
        proj.date = parse_date(date)
        proj.save()
        request.session['active_project'] = old_title.replace(" ", "_")
        print(proj)
    return redirect('kino_app:index')

@csrf_exempt
def modified_path(request):
    path_modified = json.loads(request.POST.get('path_modified',''))
    for obj in path_modified:
        path = obj['path']
        new_title = obj['new_title']
        folder_obj = get_object_or_404(FolderPath, path=path)
        if folder_obj.title != new_title:
            list_path = FolderPath.objects.filter(title=folder_obj.title)
            for obj in list_path:
                if folder_obj.path.split('/part')[0] in obj.path:
                    obj.title = new_title
                    obj.save()

    return HttpResponse('')

@csrf_exempt
def update_json_shots(request):
    json_update = request.POST.get('json_update','') == 'true'
    # print('update', json_update)

    for dir in FolderPath.objects.all():
        json_shots_path = dir.abs_path+'/shots.json'
        with open(json_shots_path, "r") as jsonFile:
            data_json = json.load(jsonFile)
        data_shots = []
        for shot_string in Shots.objects.filter(path=dir):
            data_shots.append(json.loads(shot_string.json_data))
        if len(data_json) != len(data_shots):
            if json_update:
                with open(json_shots_path, 'w') as jsonFile:
                    json.dump(data_shots, jsonFile)
            else:
                for s in data_json:
                    obj, created = Shots.objects.get_or_create(
                        path=dir,
                        json_data=json.dumps(s),
                    )
    return HttpResponse('')

@csrf_exempt
def remove_project(request):
    if request.method == 'POST':
        title = request.POST['title']
        for type in ['data', 'partition', 'notes']:
            path = os.path.join(settings.MEDIA_ROOT, 'kino_app/'+type+'/'+title)
            if os.path.isdir(path):
                if not os.listdir(path):
                    shutil.rmtree(path)
    return HttpResponse(json.dumps({'success':title}), content_type='application/json')

def create_project(request):
    if request.method == 'POST':
        title = request.POST['title']
        date = parse_date(request.POST['date'])
        company = request.POST['company']
        password = request.POST['password']
        if len(Project.objects.filter(title=title))==0:
            if password == '':
                obj, created = Project.objects.get_or_create(
                    title=title,
                    company=company,
                    date = date
                )
            else:
                obj, created = Project.objects.get_or_create(
                    title=title,
                    company=company,
                    date = date,
                    password = make_password(password)
                )
            name = title.replace(" ", "_")
            for type in ['data', 'partition', 'notes']:
                path = os.path.join(settings.MEDIA_ROOT, os.path.join('kino_app',type))
                if not os.path.isdir(path):
                    os.mkdir(path)
                path = os.path.join(path,name)
                if not os.path.isdir(path):
                    os.mkdir(path)
            print(obj, created)
    return redirect('kino_app:index')

class IndexView(generic.ListView):

    template_name = 'kino_app/index.html'
    context_object_name = 'take_list'
    start = time.time()

    @never_cache
    def get(self, request):
        print(self.request.META.get('HTTP_REFERER'))
        if not os.path.isdir(os.path.join(settings.MEDIA_ROOT, 'kino_app')):
            os.mkdir(os.path.join(settings.MEDIA_ROOT, 'kino_app'))
            os.mkdir(os.path.join(settings.MEDIA_ROOT, 'kino_app/data'))

        projects_name = os.listdir(os.path.join(settings.MEDIA_ROOT, 'kino_app/data'))

        # old = len(FolderPath.objects.filter(owner=None)) + len(FolderPath.objects.filter(owner=request.user))
        projects = []
        data_project = Project.objects.all().order_by('title')
        for project_obj in data_project:
            print(project_obj.title.replace(" ", "_"))
            name = project_obj.title.replace(" ", "_")
            path = os.path.join(settings.MEDIA_ROOT, 'kino_app','data',name)
            if not os.path.isdir(path):
                project_obj.delete()
            else:
                project = {}
                project["Name"] = name
                project["ZeroElem"] = "true"
                for type in ['data','notes','partition']:
                    if len(os.listdir(os.path.join(settings.MEDIA_ROOT, 'kino_app',type,name))) != 0 :
                        project["ZeroElem"] = "false"
                        break
                project["Company"] = project_obj.company
                project["Date"] = project_obj.date
                if project_obj.password == None or project_obj.password == "":
                    project["Password"] = False
                else:
                    project["Password"] = True
                project["Notebook"] = list_notebook(path)
                print('here', path)
                project["List"] = list_files(path,None) + list_files(path, request.user)
                # print(project['List'])
                # list += list_files(path, None)
                # list += list_files(path, request.user)
                # project["List"].sort()
                projects.append(project)
        print(os.path.join(settings.MEDIA_ROOT, 'kino_app','data'))
        # print(len(list),old)
        # extractImagesAndDelete(list, old, request.user)
        if request.user is None or request.user.is_authenticated == False:
            print(settings.LOGIN_URL)
            return HttpResponseRedirect(settings.LOGIN_URL)
        else:
            return render(request, 'kino_app/index.html', {'data_projects':data_project, 'projects':projects,'take_list':FolderPath.objects.all().order_by('path')})
