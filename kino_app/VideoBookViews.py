"""
This file is part of KinoAi project.
It is subject to the license terms in the LICENSE file found in the top-level directory
"""

from django.shortcuts import get_object_or_404, render, redirect
from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect
from django.views import generic
from django.urls import reverse
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.hashers import make_password, check_password, is_password_usable
from django.contrib.auth.models import Permission, User
from django.views.decorators.csrf import csrf_exempt
from .models import FolderPath, Detections, Shots, Project
from django.core.files.storage import FileSystemStorage
from django.views.decorators.cache import never_cache
from django.utils.dateparse import parse_date

from . import utils

import os
import json
import time
import subprocess
import shutil
import math, random

@csrf_exempt
def get_full_data_shots(request):
    path = request.POST.get('path','')
    dir = get_object_or_404(FolderPath, path=path)
    data_shots = []
    for shot_string in Shots.objects.filter(path=dir):
        data_shots.append(json.loads(shot_string.json_data))

    if(request.method == 'POST'):
        print('post data detec')
        return HttpResponse(json.dumps({'data_shots':data_shots, 'path':path}), content_type='application/json')
    return HttpResponse('')

def video_book(request, id, name_notebook):

    dir = get_object_or_404(FolderPath, pk=id)
    full_vid = dir.path.split('/')[1]
    full_list = FolderPath.objects.filter(path__icontains=full_vid).order_by('path')
    print(full_list)

    not_new_notebook = name_notebook!='new_new'

    ind = []
    for p in full_list:
        if p.path.split('/')[1] != full_vid:
            full_list = full_list.exclude(path=p.path)

    list_notebooks = []
    for path_obj in full_list:
        if not_new_notebook:
            if not os.path.isdir(os.path.join(path_obj.abs_path,'images')):
                os.makedirs(os.path.join(path_obj.abs_path,'images'))
            if not os.path.isdir(os.path.join(path_obj.abs_path,'images',name_notebook)):
                os.makedirs(os.path.join(path_obj.abs_path,'images',name_notebook))
        for root, dirs, files in os.walk(path_obj.abs_path):
            for d in dirs:
                if d == "images":
                    new_list = os.listdir(os.path.join(root,d))
                    if len(list_notebooks) == 0:
                        list_notebooks = new_list
                    else:
                        for n in new_list:
                            if n not in list_notebooks:
                                list_notebooks.append(n)

    if not_new_notebook and not os.path.isfile(os.path.join(full_list[0].abs_path,'images',name_notebook,str(request.user.username)+'_note.txt')):
        file = open(os.path.join(full_list[0].abs_path,'images',name_notebook,str(request.user.username)+'_note.txt'),"w")
        file.write("[]")
        file.close()

    full_note = []
    for root, dirs, files in os.walk(os.path.join(full_list[0].abs_path,'images',name_notebook)):
        for file in files:
            if "_note.txt" in file:
                obj = {}
                with open(os.path.join(full_list[0].abs_path,'images',name_notebook,file)) as json_file:
                    obj['Notes'] = json.load(json_file)
                obj['Creator'] = file.split('_')[0]
                full_note.append(obj)

    full_script = []
    full_json_shots = []
    full_tab = []

    index_first = 0
    if not_new_notebook:
        for p in full_list:
            if os.path.isfile(os.path.join(p.abs_path,'images',name_notebook,name_notebook+'.vtt')):
                break
            index_first+=1

    for p in full_list:
        if not_new_notebook:
            if not os.path.isdir(os.path.join(p.abs_path,'images')):
                os.makedirs(os.path.join(p.abs_path,'images'))
            if not os.path.isdir(os.path.join(p.abs_path,'images',name_notebook)):
                os.makedirs(os.path.join(p.abs_path,'images',name_notebook))
            if not os.path.isfile(os.path.join(p.abs_path,'images',name_notebook,name_notebook+'.json')):
                file = open(os.path.join(p.abs_path,'images',name_notebook,name_notebook+'.json'),"w")
                file.write("[]")
                file.close()

            if os.path.isfile(os.path.join(p.abs_path,'images',name_notebook,name_notebook+'.vtt')):
                full_script.append(os.path.join(p.abs_path,'images',name_notebook,name_notebook+'.vtt'))
        # else:
        #     return HttpResponseRedirect('/kino_app')
        full_json_shots.append(p)

    cpt=index_first
    for s in full_script:
        tab = utils.parser_vtt(s)
        for t in tab:
            t['id'] = int(t['start']+cpt*600)
            start = '{min:02d}'.format(min=cpt*10+(math.floor(int(t['start'])/60)%60))+':'+'{sec:02d}'.format(sec=math.floor(int(t['start'])%60))
            end = '{min:02d}'.format(min=cpt*10+(math.floor(int(t['end'])/60)%60))+':'+'{sec:02d}'.format(sec=math.floor(int(t['end'])%60))
            t['start'] = start
            t['end'] = end
        cpt+=1
        full_tab.append(tab)

    full_data = []
    for j in full_json_shots:
        shots_timeline = []
        if not_new_notebook:
            with open(os.path.join(j.abs_path,'images',name_notebook,name_notebook+'.json')) as f:
                shots_timeline = json.load(f)

        bbox_crop = []
        full_data.append(shots_timeline)

    full_folders = []
    full_crop = []
    full_full = []
    if not_new_notebook:
        for p in full_list:
            full_folders.append(os.path.join(p.abs_path,'images',name_notebook))

        for images_folder in full_folders:
            crop = []
            full = []
            for root, dirs, files in os.walk(images_folder):
                for f in files:
                    if len(f.split('_crop')) > 1:
                        crop.append(f)
                    else:
                        if len(f.split('jpg')) > 1:
                            full.append(f)
            crop.sort(key=lambda f: int(''.join(filter(str.isdigit, f))))
            full.sort(key=lambda f: int(''.join(filter(str.isdigit, f))))
            crop_dict = {'tab':crop,'id':1}
            full_crop.append(crop_dict)
            full_dict = {'tab':full,'id':1}
            full_full.append(full_dict)

        for i in range(len(full_list)):
            full_crop[i]['id'] = full_list[i].id
            full_crop[i]['path'] = os.path.join(full_list[i].path,'images',name_notebook)
            full_full[i]['id'] = full_list[i].id
            full_full[i]['path'] = os.path.join(full_list[i].path,'images',name_notebook)

    video_path = os.path.join(full_list.reverse()[0].abs_path,'original540.mp4')
    info = str(subprocess.check_output('ffprobe -i {0} -show_entries stream=width,height,r_frame_rate,duration,nb_frames -v quiet -of csv="p=0"'.format(video_path), shell=True ,stderr=subprocess.STDOUT))
    info_list = info.split("'")[1].split('\\')[0].split(',')
    width = info_list[0]
    height = info_list[1]
    frame_rate = round(int(info_list[2].split('/')[0])/int(info_list[2].split('/')[1]))
    duration = float(info_list[3])

    end_total = (len(full_list)-1)*600+duration

    return render(request, 'kino_app/video_book.html', {'username':request.user.username , 'title':name_notebook, 'width':width, 'frame_rate':frame_rate,
    'crop':full_crop, 'full':full_full, 'tab':full_tab, 'path':full_list, 'list_notebooks': list_notebooks, 'index_first':index_first,
    'json_shots':json.dumps(full_data), 'json_notes':json.dumps(full_note).replace('\"','\\"'), 'username':request.user.username, 'end_total':end_total})

@csrf_exempt
def save_note_video(request):

    json_sub = json.loads(request.POST.get('data_sub',''))
    json_notes = json.loads(request.POST.get('note_tab',''))

    path = request.POST.get('path','')
    previous_title = request.POST.get('previous_title','')
    folder = os.path.join(settings.MEDIA_ROOT, "kino_app","data",path,'images',previous_title)
    # print(full_list[0].abs_path)
    with open(os.path.join(folder,str(request.user.username)+'_note.txt'), 'w') as fp:
        json.dump(json_notes, fp)

    for sub_tab in json_sub:
        s = os.path.join(settings.MEDIA_ROOT, "kino_app","data",sub_tab['path'],'images',previous_title,previous_title+'.vtt')
        utils.save_vtt(sub_tab['tab'],s)

    return HttpResponse('')

@csrf_exempt
def extract_image_full_crop(request):

    path = request.POST.get('path','')
    next_path = request.POST.get('next_path',0)
    title = request.POST.get('title','')
    previous_title = request.POST.get('previous_title','')

    obj = request.POST.get('obj','')
    sub = json.loads(request.POST.get('tab_sub',''))

    bboxes = json.loads(request.POST.get('bboxes',''))
    next_bboxes = json.loads(request.POST.get('next_bboxes','[]'))


    folder = os.path.join(settings.MEDIA_ROOT, "kino_app","data")
    filename = os.path.join(folder,path,"original_hevc.mov")
    folder = os.path.join(folder,path,'images',title)
    subtitle = os.path.join(folder,title+".vtt")
    json_data = os.path.join(folder,title+".json")

    if previous_title != title and os.path.isdir(os.path.join(settings.MEDIA_ROOT, "kino_app","data",path,'images',previous_title)):
        shutil.move(os.path.join(settings.MEDIA_ROOT, "kino_app","data",path,'images',previous_title), folder)
        if os.path.isfile(os.path.join(folder,previous_title+".vtt")):
            os.rename(os.path.join(folder,previous_title+".vtt"), subtitle)
        os.rename(os.path.join(folder,previous_title+".json"), json_data)
        if next_path:
            shutil.move(os.path.join(settings.MEDIA_ROOT, "kino_app","data",next_path,'images',previous_title),
            os.path.join(settings.MEDIA_ROOT, "kino_app","data",next_path,'images',title))
            if os.path.isfile(os.path.join(settings.MEDIA_ROOT, "kino_app","data",next_path,'images',title,previous_title+".vtt")):
                os.rename(os.path.join(settings.MEDIA_ROOT, "kino_app","data",next_path,'images',title,previous_title+".vtt"),
                os.path.join(settings.MEDIA_ROOT, "kino_app","data",next_path,'images',title,title+".vtt"))
            os.rename(os.path.join(settings.MEDIA_ROOT, "kino_app","data",next_path,'images',title,previous_title+".json"),
            os.path.join(settings.MEDIA_ROOT, "kino_app","data",next_path,'images',title,title+".json"))

    if not os.path.isdir(folder):
        os.makedirs(folder)

    if next_path:
        if not os.path.isdir(os.path.join(settings.MEDIA_ROOT, "kino_app","data",next_path,"images",title)):
            os.makedirs(os.path.join(settings.MEDIA_ROOT, "kino_app","data",next_path,"images",title))

        with open(os.path.join(settings.MEDIA_ROOT, "kino_app","data",next_path,"images",title,title+".json"), 'w') as outfile:
            json.dump(next_bboxes, outfile)

    utils.save_vtt(sub, subtitle)

    with open(json_data, 'w') as outfile:
        json.dump(bboxes, outfile)

    time = request.POST.get('time','')
    bbox = json.loads(request.POST.get('bbox',''))
    original_width = int(request.POST.get('width',''))
    info = str(subprocess.check_output('ffprobe -i {0} -show_entries stream=width,height,r_frame_rate,duration,nb_frames -v quiet -of csv="p=0"'.format(filename), shell=True ,stderr=subprocess.STDOUT))
    info_list = info.split("'")[1].split('\\')[0].split(',')
    width = int(info_list[0])

    ratio = width/original_width
    bbox = [x * ratio for x in bbox]

    img_out_e = folder+"/"+str(time)+".jpg"
    img_out_r = folder+"/"+str(time)+"_crop.jpg"
    w = round((bbox[2] - bbox[0])/2)*2
    h = round((bbox[3] - bbox[1])/2)*2
    x = round(bbox[0])
    y = round(bbox[1])
    img_inter = folder+"/inter.png"
    subprocess.check_call("ffmpeg -nostdin -y -v quiet -ss {0} -i {1} -q:v 2 -frames:v 1 {2}".format(float(time), filename, img_inter), shell=True)
    subprocess.check_call("convert -quality 50 {0} {1}".format(img_inter, img_out_e), shell=True)
    os.remove(img_inter)
    subprocess.check_call("convert {0} -crop {1}x{2}+{3}+{4} +repage {5}".format(img_out_e, w, h, x, y, img_out_r), shell=True)

    img_out_e = '/media'+img_out_e.split('media').pop()
    img_out_r = '/media'+img_out_r.split('media').pop()

    return HttpResponse(json.dumps({'src_crop':img_out_r, 'src_full':img_out_e, 'obj':obj}), content_type='application/json')
