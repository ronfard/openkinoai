"""
This file is part of KinoAi project.
It is subject to the license terms in the LICENSE file found in the top-level directory
"""

from django.shortcuts import get_object_or_404, render, redirect
from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect
from django.views import generic
from django.urls import reverse
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.hashers import make_password, check_password, is_password_usable
from django.contrib.auth.models import Permission, User
from django.views.decorators.csrf import csrf_exempt
from .models import FolderPath, Detections, Shots, Project
from django.core.files.storage import FileSystemStorage
from django.views.decorators.cache import never_cache
from django.utils.dateparse import parse_date

from . import utils
import os
import json
import re
import cvxpy as cvx
import numpy as np
import scipy as sp
import scipy.sparse
from . import StabilizeOptimizer as stab
from . import StabilizeOptimizerConstraintStartEnd as stab_cons
from . import StabilizeSplitAndMerge as stab_split_merge
from . import SplitScreenOptimizer as split_opt
from moviepy.editor import VideoFileClip, ImageSequenceClip, AudioFileClip
import subprocess
import shutil
import ctypes
import math, random
import time
import cv2
#  command ffmpeg for extract screeshot at 32.375 sec ffmpeg -ss 32.375 -i original_hevc.mov -frames:v 1 out1.jpg

class LoginView(generic.View):
    template_name='kino_app/login.html'
    def post(self, request):
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)

        if user is not None:
            if user.is_active and user.is_authenticated == True:
                login(request, user)
                return HttpResponseRedirect('/kino_app')
            else:
                return render(request, "kino_app/login.html", {'msg':"Inactive user"}, {'left_part':random.randint(1,10), 'right_part':random.randint(1,20)})
        else:
            return render(request, "kino_app/login.html", {'msg':"Username or password invalid"}, {'left_part':random.randint(1,10), 'right_part':random.randint(1,20)})

        return render(request, "kino_app/login.html", {'left_part':random.randint(1,10), 'right_part':random.randint(1,20)})
    def get(self, request):
        return render(request, "kino_app/login.html", {'left_part':random.randint(1,10), 'right_part':random.randint(1,20)})

def logout_view(request):
    print(request.user)
    logout(request)
    return HttpResponseRedirect(settings.LOGIN_URL)

def signup_view(request):
    if request.method == "POST":
        print(request.POST)
        email = request.POST['email']
        username = request.POST['username']
        password = request.POST['password']
        left = int(request.POST['left_part'])
        right = int(request.POST['right_part'])
        if request.POST['answer'] != "":
            answer = int(request.POST['answer'])
            if not ((left+answer)==right or (left-answer)==right):
                return render(request, "kino_app/login.html", {'msg':'Check your math !', 'left_part':random.randint(1,10), 'right_part':random.randint(1,20)})
        else:
            return render(request, "kino_app/login.html", {'msg':'Check your math !', 'left_part':random.randint(1,10), 'right_part':random.randint(1,20)})
        if username:
            user_set = User.objects.filter(username=username)
            if len(user_set) == 0:
                user = User.objects.create_user(username=username, email=email, password=password)
                print(user)
                return HttpResponseRedirect(settings.LOGIN_URL)
            else :
                return render(request, "kino_app/login.html", {'msg':'Username already used', 'left_part':random.randint(1,10), 'right_part':random.randint(1,20)})

    return render(request, "kino_app/login.html", {'left_part':random.randint(1,10), 'right_part':random.randint(1,20)})

def reset_password(request):
    if request.method == 'POST':
        email = request.POST['email']
        username = request.POST['username']
        u = User.objects.get(username__exact=username)

        if u.email == email:
            password = User.objects.make_random_password()
            u.email_user('Reset your password','New password '+password)
            u.set_password(password)
            u.save()
            print('Le nouveau '+password)
            return render(request, "kino_app/reset_password.html", {'msg':'Visit your mailbox to reset your password', 'reset':True})
        else:
            return render(request, "kino_app/reset_password.html", {'msg':'Wrong email or username'})
    return render(request, "kino_app/reset_password.html")
