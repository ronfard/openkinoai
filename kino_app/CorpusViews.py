"""
This file is part of KinoAi project.
It is subject to the license terms in the LICENSE file found in the top-level directory
"""

from django.shortcuts import get_object_or_404, render, redirect
from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect
from django.views import generic
from django.urls import reverse
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.hashers import make_password, check_password, is_password_usable
from django.contrib.auth.models import Permission, User
from django.views.decorators.csrf import csrf_exempt
from .models import FolderPath, Detections, Shots, Project
from django.core.files.storage import FileSystemStorage
from django.views.decorators.cache import never_cache
from django.utils.dateparse import parse_date

from . import utils

import os
import json
import time
import subprocess
import shutil
import math, random

def corpus_search(request, project):

    if not os.path.isdir(os.path.join(settings.MEDIA_ROOT, "kino_app/partition/"+str(project))):
        os.mkdir(os.path.join(settings.MEDIA_ROOT, "kino_app/partition/"+str(project)))

    if not os.path.isfile(os.path.join(settings.MEDIA_ROOT, "kino_app/partition/"+str(project)+"/actors.json")):
        file = open(os.path.join(settings.MEDIA_ROOT, "kino_app/partition/"+str(project)+"/actors.json"),'w')
        file.write('[]')
        file.close()

    with open(os.path.join(settings.MEDIA_ROOT, "kino_app/partition/"+str(project)+"/actors.json")) as json_file:
        actors = json.load(json_file)

    if not os.path.isfile(os.path.join(settings.MEDIA_ROOT, "kino_app/partition/"+str(project)+"/partition_text.json")):
        file = open(os.path.join(settings.MEDIA_ROOT, "kino_app/partition/"+str(project)+"/partition_text.json"),'w')
        file.write('[]')
        file.close()

    with open(os.path.join(settings.MEDIA_ROOT, "kino_app/partition/"+str(project)+"/partition_text.json")) as json_file:
        partitions = json.load(json_file)
        for p in partitions:
            if p['html']:
                p['html'] = p['html'].replace('\n','')
    corpus = []
    for root, dirs, files in os.walk(os.path.join(settings.MEDIA_ROOT, "kino_app/partition/"+str(project))):
        for file in files:
            if ".txt" in file:
                obj = {}
                text = ""
                with open(os.path.join(settings.MEDIA_ROOT, "kino_app/partition/"+str(project)+"/"+file),'r') as json_file:
                    text = json_file.readlines()
                obj['Content'] = "".join(text).replace('\"','\\"').replace('\n','\\n').replace('\t','\\t')
                obj['Title'] = file.split('.')[0].replace('_',' ').replace('\"','\\"').replace('\n','\\n')
                corpus.append(obj)
        break
    return render(request, 'kino_app/corpus_search.html', {'data':json.dumps(corpus), 'partitions':json.dumps(partitions), 'actors':json.dumps(actors), 'project':str(project)})

@csrf_exempt
def add_text(request):
    text = json.loads(request.POST.get('new_text',''))
    project = str(request.POST.get('project',''))
    dir = os.path.join(settings.MEDIA_ROOT, "kino_app/partition/"+project+'/')
    title = text['Title'].replace('\n',' ').replace('\t',' ')
    if not os.path.isfile(dir+text['Title']+'.txt'):
        file = open(dir+title+'.txt',"w")
        file.write(text['Content'])
        file.close()
    return HttpResponse('')

@csrf_exempt
def remove_text(request):
    removed_text = json.loads(request.POST.get('removed_text',''))
    project = str(request.POST.get('project',''))
    dir = os.path.join(settings.MEDIA_ROOT, "kino_app/partition/"+project+'/')
    for title in removed_text:
        os.remove(dir+title+'.txt')
    return HttpResponse('')

@csrf_exempt
def save_partition_text(request):
    partition = json.loads(request.POST.get('partition',''))
    project = str(request.POST.get('project',''))
    dir = os.path.join(settings.MEDIA_ROOT, "kino_app/partition/"+project+'/')
    with open(dir+"partition_text.json", 'w') as fp:
        json.dump(partition, fp, indent=2)
    return HttpResponse('')

@csrf_exempt
def add_actor_corpus(request):
    actors = json.loads(request.POST.get('actors',''))
    project = str(request.POST.get('project',''))
    print(actors)
    dir = os.path.join(settings.MEDIA_ROOT, "kino_app/partition/"+project+"/")
    with open(dir+"actors.json", 'w') as fp:
        json.dump(actors, fp, indent=2)
    return HttpResponse('')
