from django import template
register = template.Library()

@register.filter
def replace_underscore(string):
    return string.replace('_', ' ')

@register.filter
def replace_html_quot(string):
    return string.replace('&#39;', "'")
