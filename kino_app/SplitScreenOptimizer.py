"""
This file is part of KinoAi project.
It is subject to the license terms in the LICENSE file found in the top-level directory
"""


import cvxpy as cvx
import numpy as np
import scipy as sp
import scipy.sparse
from scipy.ndimage.filters import gaussian_filter

def csr_zero_rows(csr, rows_to_zero):
    rows, cols = csr.shape
    mask = np.ones((rows,), dtype=np.bool)
    mask[rows_to_zero] = False
    nnz_per_row = np.diff(csr.indptr)

    mask = np.repeat(mask, nnz_per_row)
    nnz_per_row[rows_to_zero] = 0
    csr.data = csr.data[mask]
    csr.indices = csr.indices[mask]
    csr.indptr[1:] = np.cumsum(nnz_per_row)

def binomial_3(arr):
    x1 = arr[0]
    for i in range(len(arr)-1):
        arr[i] = 0.5 * (arr[i] + arr[i+1])
    for i in range(1, len(arr)-1):
        arr[i] = 0.5 * (arr[i-1] + arr[i])
    arr[0] = x1

def get_overlap_cost(act1, act2):
    overlap_cost = (act1['half_width'] + act2['half_width']) - abs(act2['center_x'] - act1['center_x'])
    return overlap_cost

def fill_overlap_array_for_each_state(tab_overlap):
    # Compute the overlap cost for each state available
    number_digit = "{:0"+str(len(tab_overlap))+"b}"
    tableau_binaire = [number_digit.format(i) for i in range(2**len(tab_overlap))]
    final_tab = []
    overlap_array = []
    for str_chain in tableau_binaire:
        i = 0
        inter_tab = []
        overlap_global = 0
        for s in str_chain:
            if s == "0":
                overlap_global+=float(tab_overlap[i])
                inter_tab.append("+ "+str(1*tab_overlap[i]))
            else:
                overlap_global-=float(tab_overlap[i])
                inter_tab.append("- "+str(-1*tab_overlap[i]))
            i+=1
        overlap_array.append(overlap_global)
        final_tab.append(inter_tab)
        # print(final_tab)
    return overlap_array

def get_smoothness_term(overlap_past_frame, overlap):
    state_prev_frame = overlap_past_frame.index(min(overlap_past_frame))
    state_curr_frame = overlap.index(min(overlap))
    if(state_prev_frame == state_curr_frame):
        # print('same')
        return 0
    elif abs(state_prev_frame - state_curr_frame)<2:
        # print('1 diff')
        return 1
    else:
        # print('min 2 diff')
        return 2
    return 0

def set_index_state(states, overlap_array):
    # For each frame pick the state with the minimum overlap cost
    for frame_overlap in overlap_array:
        states.append(int(np.where(frame_overlap == np.amin(frame_overlap))[0][0]))

def smooth_layout(states):
    # Remove the change of layout when it last less than 0.5s
    window_size = 24/2
    curr_state=0
    old_state=0
    old_len=0
    i=0
    len_segment=0
    tab_for_change = []
    while i<len(states):
        new_state = states[i]
        if(i==0):
            curr_state = states[i]
            old_state = curr_state
        if(new_state != curr_state or i==len(states)-1):
            start = i-len_segment
            if len_segment < window_size:
                for j in range(len_segment):
                    states[start+j]=old_state
                states[i] = old_state
                len_segment += old_len
                curr_state = old_state
            else:
                old_state = curr_state
                curr_state = new_state
                old_len = len_segment
                len_segment = 1
        else:
            len_segment+=1
        i+=1


def layout_selection(layout_actors_info, lambda1=0.1):
    """
    Implementation of the algorithm describe in this article (section 4) https://hal.inria.fr/hal-01482165/document

    For each layout
    E = Eo(r(t)) + lamda * Es(r(t-1)-r(t))

    Eo = overlap term
    Es = smooth term

    """

    overlap_array = []

    # Fill an array with the values of all overlap term for each frame [Oab, Obc, Ocd]
    array_actor_compo = []
    for tab_act in layout_actors_info:
        tab_overlap_by_frame = []
        tab_act_name = []
        for i in range(len(tab_act)-1):
            tab_overlap_by_frame.append(get_overlap_cost(tab_act[i],tab_act[i+1]))
            tab_act_name.append(tab_act[i]['actor_name'])
        tab_act_name.append(tab_act[len(tab_act)-1]['actor_name'])
        array_actor_compo.append(tab_act_name)
        overlap_array.append(tab_overlap_by_frame)

    final_array = []
    overlap_array_for_each_frame = []
    i=0
    for frame_array in overlap_array:
        final_array.append({"Frame":i, "Actors":array_actor_compo[i],"Overlap array":fill_overlap_array_for_each_state(frame_array)})
        overlap_array_for_each_frame.append(fill_overlap_array_for_each_state(frame_array))
        i+=1
    # print(final_array)

    smoothness_array = [0]
    for i in range(len(overlap_array_for_each_frame)-1):
        ind = i+1
        smoothness_array.append(get_smoothness_term(overlap_array_for_each_frame[ind-1],overlap_array_for_each_frame[ind]))

    # print(len(smoothness_array))

    # variable = index of the state
    # expression = overlap term + smoothness term
    # problem = minimize expression to have the sequence of states

    n = len(layout_actors_info)
    num_frame = 0


    r = cvx.Variable(n)
    optimized_states = [] #np.zeros(n)
    # while num_frame < n:

    overlap_array = np.array(overlap_array_for_each_frame)

    set_index_state(optimized_states,overlap_array)
    # print(optimized_states)

    """
    expr = get_overlap_term(r) + smoothness_term(r)

    obj = cvx.Minimize(expr)

    problem = cvx.Problem(obj)
    """

    optimized_states_test = []

    i=0
    while(i<1):
        optimized_states_test = []
        for frame in range(n):
            # for frame_overlap in overlap_array:
            optimized_states_test.append(int(np.where(overlap_array[frame] == np.amin(overlap_array[frame]))[0][0]))
        i+=1

    smooth_layout(optimized_states_test)


    return [overlap_array_for_each_frame,smoothness_array,optimized_states_test]
