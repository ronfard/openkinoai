/*
This file is part of KinoAi project.
It is subject to the license terms in the LICENSE file found in the top-level directory
*/

/**
 * [ShotsTimeline => video editing timeline]
 * @constructor
 */
function ShotsTimeline()  {
  this.x  = 0;
  this.y  = 0;
  this.w  = 0;
  this.h  = 100;

  this.on = false;

  this.duration;

  this.time = 0;

  this.shots = [];

  this.scenes = [];

  this.history_stack = [];

  this.redo_stack = [];

  this.start_frame = 0;

  this.end_frame = total_frame;

  this.drop_shot;

  this.draggin = false;

  this.zoom_scene = false;

  this.new_timeline_input = createInput();
  this.new_timeline_input.side = false;
  html_elements.push(this.new_timeline_input);
  this.new_timeline_input.changed(createNewTimeline);

  this.select_timeline = createSelect();
  this.select_timeline.side = false;
  this.select_timeline.mousePressed(function(){shots_timeline.select_timeline.elt.dispatchEvent(new Event('change'));});
  html_elements.push(this.select_timeline);
  this.select_timeline.changed(selectSceneTimeline);

  this.user_title = createElement('h3','Scene user');
  html_elements.push(this.user_title);
  this.user_title.style('margin',0);
  this.user_title.style('font-size',16);

  this.select_user = createSelect();
  this.select_user.mousePressed(function(){shots_timeline.select_user.elt.dispatchEvent(new Event('change'));});
  html_elements.push(this.select_user);
  this.select_user.changed(selectUserTimeline);

  this.scene_zoom_button = createButton('Scene');
  this.scene_zoom_button.side = false;
  html_elements.push(this.scene_zoom_button);
  this.scene_zoom_button.mousePressed(zoomScene);

  this.name_elem = createElement('h3');
  this.name_elem.elt.contentEditable = true;
  this.name_elem.side = false;
  html_elements.push(this.name_elem);
  this.name_elem.style('margin',0);
  this.name_elem.style('font-size',20);
  this.name_elem.style('text-decoration','underline');

  this.remove_button = {'x':0,'y':0,'rad':10};

  /**
   * [click => handle mouse click event]
   * @param  {[float]} mx               [mouse x]
   * @param  {[float]} my               [mouse y]
   */
  this.click = function(mx, my) {
    for(let l of this.list_data) {
      if(l.On) {
          if(l.UserName == username) {
            for(var i=0; i<this.shots.length; i++) {
              this.shots[i].on = false;
            }
            this.draggin = false;
            if(mx>this.remove_button.x && mx<this.remove_button.x+this.remove_button.rad && this.remove_button.y<my && my<this.remove_button.y+this.remove_button.rad) {
              if(this.select_user.value() == username) {
                if(confirm('Are you sure you want to delete the '+this.name_elem.elt.innerText+' timeline ?')) {
                  this.removeTimeline();
                }
              } else {
                alert("you can't remove a timeline that not belong to your");
              }
              return undefined;
            }
            if(keyIsPressed) {
              this.clickTimeButton(mx,my);
            }
            if (!montage_editor.is_split && mx > this.x && mx < this.x + this.w && my > this.y && my < this.y + this.h) {
              for(var i=0; i<this.shots.length; i++) {
                if(mx > this.shots[i].start && mx < this.shots[i].end) {
                  this.shots[i].on = true;
                }
              }
              this.on = !this.on;
              return this.on
          }
        }
      }
    }
    return undefined;
  };

  /**
   * [clickTimeButton => change the start or end time of the scene by clicking on it]
   * @param  {[float]} mx               [mouse x]
   * @param  {[float]} my               [mouse y]
   */
  this.clickTimeButton = function(mx,my) {
    if(mx > this.start_time_button.x && mx < this.start_time_button.x + this.start_time_button.w && my > this.start_time_button.y && my < this.start_time_button.y + this.start_time_button.h) {
      this.stackHistory();
      if(frame_num<this.end_frame) {
        this.start_frame = frame_num;
        for(let o of this.list_data) {
          if(o.On) {
            o.start_frame = frame_num;
            break;
          }
        }
      }
    }
    if(mx > this.end_time_button.x && mx < this.end_time_button.x + this.end_time_button.w && my > this.end_time_button.y && my < this.end_time_button.y + this.end_time_button.h) {
      this.stackHistory();
      if(frame_num>this.start_frame) {
        this.end_frame = frame_num;
        for(let o of this.list_data) {
          if(o.On) {
            o.end_frame = frame_num;
            break;
          }
        }
      }
    }
  }

  /**
   * [resetButton => reset the start or end time to 0, video.duration]
   * @param  {[float]} mx               [mouse x position]
   * @param  {[float]} my               [mouse y position]
   */
  this.resetButton = function(mx,my) {
    if(mx > this.start_time_button.x && mx < this.start_time_button.x + this.start_time_button.w && my > this.start_time_button.y && my < this.start_time_button.y + this.start_time_button.h) {
      this.stackHistory();
      this.start_frame = 0;
      for(let o of this.list_data) {
        if(o.On) {
          o.start_frame = 0;
          break;
        }
      }
    }
    if(mx > this.end_time_button.x && mx < this.end_time_button.x + this.end_time_button.w && my > this.end_time_button.y && my < this.end_time_button.y + this.end_time_button.h) {
      this.stackHistory();
      this.end_frame = total_frame;
      for(let o of this.list_data) {
        if(o.On) {
          o.end_frame = total_frame;
          break;
        }
      }
    }
  }

  /**
   * [drag => handle drag event]
   * @param  {[float]} mx               [mouse x position]
   * @param  {[float]} my               [mouse y position]
   */
  this.drag = function(mx, my) {
    if (!montage_editor.is_split && mx > this.x && mx < this.x + this.w && my > this.y && my < this.y + this.h) {
      if(keyIsPressed && keyCode == 16) {
        let unit = this.w/(player.total_frame/frame_rate);
        this.time=(player.first/frame_rate)+(mx-this.x)/unit;
        video.time(this.time);
      }
      this.draggin = true;
    } else {
      return undefined;
    }
  }

  /**
   * [drop => handle drop event]
   * @param  {[float]} mx               [mouse x position]
   * @param  {[float]} my               [mouse y position]
   */
  this.drop = function(mx, my) {
    document.body.style.cursor = "default";
    this.draggin = false;
    let s;
    let ext_s;
    for(let i=0; i<montage_editor.shots.length; i++) {
      if(montage_editor.shots[i].on) {
        s = montage_editor.shots[i];
      }
    }
    for(let shot of this.shots) {
      if(shot.on) {
        ext_s = shot;
      }
    }
    if (!montage_editor.is_split && ext_s && mx > 0 && mx < mid_width && my > this.y && my < this.y + this.h && (mx < ext_s.start || mx > ext_s.end)) {
      // Extend ext shot
      var unit = this.w/this.duration;
      if(mx < this.x) {
        mx = this.x;
      }
      if(mx > this.x + this.w) {
        mx = this.x + this.w;
      }
      this.time= (player.first/frame_rate) + (mx-this.x)/unit;
      this.extendShot(ext_s, mx);
      return;
    } else {
      return undefined;
    }
  }

  $(document).bind('keydown', function(e) {
    if(is_montage_editor) {
      // Ctrl + Z
      if((e.ctrlKey || e.metaKey) && (e.which == 90)) {
        e.preventDefault();
        shots_timeline.undo();
      }
      // Ctrl + Y
      if((e.ctrlKey || e.metaKey) && (e.which == 89)) {
        e.preventDefault();
        shots_timeline.redo();
      }
      // Ctrl + R
      if((e.ctrlKey || e.metaKey) && (e.which == 82)) {
        e.preventDefault();
        if(shots_timeline.previous_time) {
          video.time(shots_timeline.previous_time);
        }
      }
    }
  });

  /**
   * [hideElts => hide all the elements]
   */
  this.hideElts = function() {
    this.select_timeline.hide();
    this.select_user.hide();
    this.user_title.hide();
    this.new_timeline_input.hide();
    this.name_elem.hide();
    this.scene_zoom_button.hide();
  }

  /**
   * [showElts => show all the elements]
   */
  this.showElts = function() {
    this.x = player.x;
    this.h = 100;
    this.start_time_button = {'x':5,'y':this.y-10,'w':this.x-5,'h':this.h/2};
    this.end_time_button = {'x':5,'y':this.y+this.h-10,'w':this.x-5,'h':this.h/2};
    this.select_user.show();
    this.user_title.show();
    this.new_timeline_input.show();
    this.name_elem.show();
    this.scene_zoom_button.show();
    this.updateTimeline();
    this.updateSelectedShot();
    this.select_timeline.show();
    this.select_timeline.elt.dispatchEvent(new Event('change'));
  }

  this.updateTimeline = function() {
    if(!this.list_data || this.list_data.length==0) {
      this.list_data = [];
      this.list_data.push({'UserName':username, 'Name':'Default','Data':[],'On':true,'start_frame':0,'end_frame':total_frame});
      this.select_user.option(username);
    }
    let is_user = false;
    for(let c of this.select_user.child()) {
      if(c.innerText==username) {
        is_user = true;
        break;
      }
    }
    if(!is_user) {
      this.list_data.push({'UserName':username, 'Name':'Default','Data':[],'On':true,'start_frame':0,'end_frame':total_frame});
      this.select_user.option(username);
    }
    if(this.select_timeline.child().length==0) {
      for(let o of this.list_data) {
        if(o.UserName == username) {
          this.select_timeline.option(o.Name);
        }
      }
    }
    let obj = this.list_data[0];
    for(let o of this.list_data) {
      if(o.On) {
        obj = o;
        break;
      }
    }
    obj.On = true;
    this.shots = obj.Data;
    this.select_timeline.elt.value = obj.Name;
    this.name_elem.elt.innerText = obj.Name;
    if(!obj.start_frame) {
      obj.start_frame=0;
    }
    if(!obj.end_frame) {
      obj.end_frame=total_frame;
    }
    this.start_frame = obj.start_frame;
    this.end_frame = obj.end_frame;
  }

  /**
   * [resizeElt => resize the elements]
   */
  this.resizeElt = function() {
    this.start_time_button = {'x':5,'y':this.y-10,'w':this.x-5,'h':this.h/2};
    this.end_time_button = {'x':5,'y':this.y+this.h-10,'w':this.x-5,'h':this.h/2};

    this.select_timeline.original_x = mid_width-175;
    this.select_timeline.original_y = viewer_height;
    this.select_timeline.size(150);

    this.new_timeline_input.original_x = 130;
    this.new_timeline_input.original_y = viewer_height;

    this.name_elem.original_x = int(mid_width/2);
    this.name_elem.original_y = viewer_height;

    this.scene_zoom_button.original_x = shots_timeline.new_timeline_input.position().x + shots_timeline.new_timeline_input.size().width+10;
    this.scene_zoom_button.original_y = viewer_height;
    this.scene_zoom_button.size(100);
  }

  /**
   * [createNewTimeline => create a new scene]
   */
  function createNewTimeline() {
    if(shots_timeline.checkName(shots_timeline.new_timeline_input.value())) {
      for(let data of shots_timeline.list_data) {
        data.On = false;
      }
      for(let s of montage_editor.shots) {
        if(s.type != 'WS' && s.selected) {
          s.scenes_involved.push(shots_timeline.new_timeline_input.value()+'._.'+username);
          s.is_modified = true;
        }
      }
      shots_timeline.list_data.push({'UserName':username,'Name':shots_timeline.new_timeline_input.value(),'Data':[],'On':true,'start_frame':0,'end_frame':total_frame});
      shots_timeline.select_timeline.option(shots_timeline.new_timeline_input.value());
      shots_timeline.select_timeline.elt.value = shots_timeline.new_timeline_input.value();
      shots_timeline.shots = [];
      shots_timeline.name_elem.elt.innerText = shots_timeline.new_timeline_input.value();
      shots_timeline.new_timeline_input.value('');
      shots_timeline.start_frame = 0;
      shots_timeline.end_frame = total_frame;
    } else {
      alert('Name already used');
      shots_timeline.new_timeline_input.value('');
    }
  }

  /**
   * [selectSceneTimeline => select a specific scene]
   */
  function selectSceneTimeline() {
    for(let data of shots_timeline.list_data) {
      data.On = false;
      if(data.Name == this.elt.value && data.UserName == shots_timeline.select_user.value()) {
        data.On = true;
      }
    }
    shots_timeline.setTabShots();
    shots_timeline.name_elem.elt.innerText = this.elt.value;
    export_editor.name_video.elt.innerText = this.elt.value;
    export_editor.export_name = this.elt.value;
    shots_timeline.updateSelectedShot();
    if(annotation_editor.is_partition_editor) {
      annotation_editor.partition_editor.selectSceneParts(this.elt.value);
    }
  }

  /**
   * [selectUserTimeline => select a user]
   */
  function selectUserTimeline() {
    while(shots_timeline.select_timeline.child().length!=0){shots_timeline.select_timeline.child()[0].remove();}
    for(let data of shots_timeline.list_data) {
      if(data.UserName == this.elt.value) {
        shots_timeline.select_timeline.option(data.Name);
      }
    }
    shots_timeline.select_timeline.elt.dispatchEvent(new Event('change'));
  }

  /**
   * [zoomScene => set the start and end time with the scene information]
   */
  function zoomScene() {
    shots_timeline.zoom_scene = !shots_timeline.zoom_scene;
    if(shots_timeline.zoom_scene) {
      shots_timeline.scene_zoom_button.elt.innerText = 'Part';
    } else {
      shots_timeline.scene_zoom_button.elt.innerText = 'Scene';
      player.scale = 1;
    }
  }

  /**
   * [setTabShots => select a specific edit (list of shots)]
   */
  this.setTabShots = function() {
    this.shots = [];
    for(let data of this.list_data) {
      if(data.On) {
        this.shots = data.Data;
        this.start_frame = data.start_frame;
        this.end_frame = data.end_frame;
        break;
      }
    }
  }

  /**
   * [updateSelectedShot => select a shot from the framing tab]
   */
  this.updateSelectedShot = function() {
    if(!this.list_data || this.list_data.length==0) {
      this.list_data = [];
      this.createDefault('Default');
    }
    if(this.name_elem.elt.innerText=="") {
      for( let o of this.list_data) {
        if(o.On) {
          this.name_elem.elt.innerText = o.Name;
        }
      }
    }
    for(let s of montage_editor.shots) {
      if(s.type != 'WS')
        s.selected = false;
      for(let scene of s.scenes_involved) {
        let scene_name;
        if(scene.split('._.').length>1) {
          if(scene.split('._.')[1] == username) {
            scene_name = scene.split('._.')[0];
          }
        } else {
          scene_name = scene.split('._.')[0];
        }
        if(scene_name && scene_name == this.name_elem.elt.innerText) {
          s.selected = true;
          break;
        }
      }
    }
    for(let s of this.shots) {
      let shot = montage_editor.getShotAspect(s.type, s.actors_involved, s.aspect_ratio, s.is_pull_in, s.is_keep_out, s.is_stage_position, s.is_gaze_direction, s.is_personalize);
      if(shot) {
        shot.selected = true;
        shot.clickSelect();
      }
    }
  }

  /**
   * [changeName => change scene name]
   * @param  {[string]} new_name               [new name]
   */
  this.changeName = function(new_name) {
    for(let data of shots_timeline.list_data) {
      if(data.On && this.checkName(new_name)) {
        for(let o of this.select_timeline.child()) {
          if(o.innerText == this.select_timeline.elt.value) {
            o.innerText = new_name;
            o.value = new_name;
            break;
          }
        }
        data.Name = new_name;
      }
    }
  }

  /**
   * [checkName => check if the new name doesn't exist]
   * @param  {[string]} new_name               [new name]
   * @return {[Boolean]}          [true if not exist]
   */
  this.checkName = function(new_name) {
    let ret = true;
    for(let o of this.list_data) {
      if(o.Name == new_name && o.UserName == this.select_user.value()) {
        ret = false;
        break;
      }
    }
    return ret;
  }

  /**
   * [removeTimeline => remove a scene]
   */
  this.removeTimeline = function() {
    for(let i=0;i < this.list_data.length;i++) {
      let o = this.list_data[i];
      if(o.On) {
        o.On = false;
        for(let c of this.select_timeline.child()) {
          if(c.innerText == o.Name) {
            c.remove();
            break;
          }
        }
        for(let obj of this.list_data) {
          if(obj.Name == this.select_timeline.elt.value) {
            obj.On = true;
          }
        }
        this.setTabShots();
        this.name_elem.elt.innerText = this.select_timeline.elt.value;
        this.list_data.splice(i,1);
        if(this.list_data.length==0) {
          this.createDefault('Default');
        }
        break;
      }
    }
  }

  /**
   * [stackHistory => save the history of actions]
   * @param  {Boolean} [redo=false]               [redo or undo]
   */
  this.stackHistory = function(redo = false) {
    let tab = [];
    for(let s of this.shots) {
      let obj = {};
      obj.actors_involved = Array.from(s.actors_involved);
      obj.aspect_ratio = s.aspect_ratio;
      obj.end = s.end;
      obj.end_frame = s.end_frame;
      obj.is_gaze_direction = s.is_gaze_direction;
      obj.is_keep_out = s.is_keep_out;
      obj.is_pull_in = s.is_pull_in;
      obj.is_stage_position = s.is_stage_position;
      obj.start = s.start;
      obj.start_frame = s.start_frame;
      obj.type = s.type;
      tab.push(obj);
    }
    if(!redo) {
      if(this.history_stack.length>=5) {
        this.history_stack.splice(0,1);
      }
      this.history_stack.push({'start':this.start_frame,'end':this.end_frame,'shots':tab});
    } else {
      if(this.redo_stack.length>=5) {
        this.redo_stack.splice(0,1);
      }
      this.redo_stack.push({'start':this.start_frame,'end':this.end_frame,'shots':tab});
    }
  }

  /**
   * [undo => undo an action]
   */
  this.undo = function() {
    let obj = this.history_stack.pop();
    if(obj) {
      this.stackHistory(true);
      this.start_frame = obj.start;
      this.end_frame = obj.end;
      this.shots = obj.shots;
    }
  }

  /**
   * [redo => redo an action]
   */
  this.redo = function() {
    let obj = this.redo_stack.pop();
    if(obj) {
      this.stackHistory();
      this.start_frame = obj.start;
      this.end_frame = obj.end;
      this.shots = obj.shots;
    }
  }

  /**
   * [createDefault => create a default scene]
   * @param  {[string]} name               [scene name]
   */
  this.createDefault = function(name) {
    this.list_data.push({'UserName':username,'Name':name,'Data':[],'On':true,'start_frame':0,'end_frame':total_frame});
    this.select_timeline.option(name);
    this.select_timeline.elt.value = name;
    this.select_user.option(username);
    this.shots = [];
    this.name_elem.elt.innerText = name;
  }

  /**
   * [isSameShot => check if the selected shot is already choosen at the current frame]
   * @param  {[shot object]} shot               [selected shot]
   * @return {[Boolean]}      [true if it's the same shot]
   */
  this.isSameShot = function(shot) {
    let ret = false;
    for(let s of this.shots) {
      if(s.start_frame < this.time*frame_rate && this.time*frame_rate < s.end_frame) {
        if(shot.equalTo(s, false)) {
          ret = true;
          break;
        }
      }
    }
    return ret;
  }

  /**
   * [addShotOnCursor => add shot to the scene]
   * @param  {[shot object]} shot               [shot object]
   */
  this.addShotOnCursor = function(shot) {
    this.time= video.time();

    if(this.time < 0.05) {
      this.time = 0;
    }
    if(!this.isSameShot(shot)) {
      this.addShot(shot);
    }
  }

  /**
   * [replaceShot => replace the current shot of the timeline by a new one]
   * @param  {[shot object]} shot                               [new shot object]
   * @param  {[shot object]} [old_shot=undefined]               [old shot if it's a redo]
   */
  this.replaceShot = function(shot, old_shot = undefined) {
    for(let l of this.list_data) {
      if(l.On && l.UserName == username) {
        this.stackHistory();

        let x_pos= player.nav_bar.cursor;
        let new_shot = old_shot;
        if(!old_shot) {
          for(let s of this.shots) {
            if(s.on) {
              new_shot = s;
            }
          }
        }
        if(new_shot) {
          let next = this.getNext(new_shot);
          if(next) {
            if(shot.equalTo(next, false)) {
              new_shot.end_frame = next.end_frame;
              next.on = true;
            }
          }
          let prec = this.getPrec(new_shot);
          if(prec) {
            if(shot.equalTo(prec, false)) {
              new_shot.start_frame = prec.start_frame;
              prec.on = true;
            }
          }
          new_shot.type = shot.type;
          new_shot.actors_involved = shot.actors_involved;
          new_shot.aspect_ratio = shot.aspect_ratio;
          new_shot.is_keep_out = shot.is_keep_out;
          new_shot.is_pull_in = shot.is_pull_in;
          new_shot.is_stage_position = shot.is_stage_position;
          new_shot.is_gaze_direction = shot.is_gaze_direction;
          new_shot.on = false;
        }
        this.removeShot();
      }
    }
  }

  /**
   * [addShot => add a shot to the timeline at the current frame]
   * @param  {[shot object]} shot               [selected shot]
   */
  this.addShot = function(shot) {
    for(let l of this.list_data) {
      if(l.On && l.UserName == username) {
        this.stackHistory();

        for(let s of this.shots) {
          if(s.start_frame == Math.max(this.start_frame,Math.round(frame_rate*this.time)-1)) {
            this.replaceShot(shot, s);
            return;
          }
        }

        var s = {};//new Shot();
        s.type = shot.type;
        if(shot.is_personalize) {
          s.bbox_perso = shot.bbox_perso;
          s.is_personalize = shot.is_personalize;
          s.aspect_ratio = shot.aspect_ratio;
          s.actors_involved = [];
        } else {
          s.actors_involved = shot.actors_involved;
          s.aspect_ratio = shot.aspect_ratio;
          s.is_keep_out = shot.is_keep_out;
          s.is_pull_in = shot.is_pull_in;
          s.is_stage_position = shot.is_stage_position;
          s.is_gaze_direction = shot.is_gaze_direction;
        }

        let unit = this.w/this.duration;
        s.start = this.x+this.time*unit;
        s.start_frame = Math.max(this.start_frame,Math.round(frame_rate*this.time)-1);

        this.shots.push(s);
        this.shots.sort(compare_start);
        let next = this.getNext(s);
        if(next) {
          if(shot.equalTo(next, false)) {
            s.end_frame = next.end_frame;
            next.on = true;
          } else {
            s.end_frame = next.start_frame -1;
            s.end = next.start -1;
          }
        } else {
          s.end_frame = Math.min(player.last,this.end_frame);
          s.end = this.x+this.w;
        }
        let prec = this.getPrec(s);
        if(prec) {
          if(shot.equalTo(prec, false)) {
            s.start_frame = prec.start_frame;
            prec.on = true;
          } else {
            if(prec.end_frame>= s.start_frame) {
              prec.end_frame = s.start_frame -1;
              prec.end = s.start -1;
            }
          }
        }
        this.removeShot();

        this.drop_shot = s;
        for(let obj of this.list_data) {
          if(obj.On) {
            obj.Data = this.shots;
          }
        }
      }
    }

  }

  /**
   * [saveShotsTimeline => save the timeline data on the server]
   */
  this.saveShotsTimeline = function() {
    if(!this.list_data || this.list_data.length==0) {
      this.list_data = [];
      this.list_data.push({'UserName':username, 'Name':'Default','Data':[],'On':true,'start_frame':0,'end_frame':total_frame});
      this.select_user.option(username);
    }
    new_json_shots = [];
    for(let obj of this.list_data) {
      if(obj.UserName == username) {
        let new_obj = {};
        new_obj.Name = obj.Name;
        new_obj.Data = [];
        new_obj.On = obj.On;
        new_obj.start_frame = obj.start_frame;
        new_obj.end_frame = obj.end_frame;
        for(let s of obj.Data) {
          if(s.start_frame && s.end_frame) {
            var shot = {};
            shot.Type = s.type;
            shot.StartFrame = s.start_frame;
            shot.EndFrame = s.end_frame;
            shot.Timeline = 1;
            let tab = [];
            for(let a of s.actors_involved) {
              tab.push(a.actor_name);
            }
            if(s.is_personalize) {
              shot.BBoxPerso = s.bbox_perso;
              shot.IsPersonalize = s.is_personalize;
              shot.AspectRatio = s.aspect_ratio;
            } else {
              shot.ActInvolved = tab;
              shot.AspectRatio = s.aspect_ratio;
              shot.GazeDir = s.is_gaze_direction;
              shot.PullIn = s.is_pull_in;
              shot.KeepOut = s.is_keep_out;
              shot.StagePos = s.is_stage_position;
            }

            new_obj.Data.push(shot);
          }
        }
        new_json_shots.push(new_obj);
      }
    }

    $.post({
      url: "save_timeline",
      async: true,
      data: {'abs_path': abs_path, 'timeline':JSON.stringify(new_json_shots)},
      dataType: 'json',
      success: function (data) {
        // console.log(data);
      }
    });
  }

  /**
   * [extendShot => extend the selected shot]
   * @param  {[shot object]} shot               [shot selected]
   * @param  {[float]} mx                 [mouse x position]
   */
  this.extendShot = function(shot, mx) {
    this.stackHistory();

    if(player.nav_bar.cursor-10 < mx && mx < player.nav_bar.cursor+10) {
      mx = player.nav_bar.cursor;
      this.time = video.time();
    }

    let unit = this.w/this.duration;
    if(mx < shot.start) {
      // Left
      let tab = [];
      for(let i=0; i<this.shots.length; i++) {
        if (this.shots[i].start > mx && this.shots[i].end <= shot.start) {
          tab.push(i);
        }
      }
      for(var i = tab.length -1; i >= 0; i--) {
        this.shots.splice(tab[i],1);
      }
      let prec = this.getPrec(shot);
      shot.start = this.x+this.time*unit;
      shot.start_frame = Math.round(frame_rate*this.time);
      if(prec && mx < prec.end) {
        prec.end_frame = shot.start_frame-1;
        prec.end = shot.start -1;
      }
    } else {
      // Right
      let tab = [];
      for(let i=0; i<this.shots.length; i++) {
        if (this.shots[i].end < mx && this.shots[i].start >= shot.end) {
          tab.push(i);
        }
      }
      for(var i = tab.length -1; i >= 0; i--) {
        this.shots.splice(tab[i],1);
      }
      let next = this.getNext(shot);
      shot.end = this.x+this.time*unit;
      shot.end_frame = Math.round(frame_rate*this.time);
      if(next && mx > next.start) {
        next.start_frame = shot.end_frame+1;
        next.start = shot.end +1;
      }
    }
  }

  /**
   * [testShot => ]
   * @param  {[type]} sh               [description]
   * @return {[type]}    [description]
   */
  this.testShot = function(sh) {
    let b = false;
    for(let s of this.shots) {
      if(s.type == sh.type && s.aspect_ratio === sh.aspect_ratio) {
        var b1 = true;
        for(let i=0; i<sh.actors_involved.length; i++) {
          if(!s.actors_involved.includes(sh.actors_involved[i])) {
            b1 = false;
            break;
          }
        }
        if(b1) {
          if(s.first_frame == sh.first_frame && s.end_frame == sh.end_frame) {
            b = true;
          }
        }
      }
    }
    return b;
  }

  /**
   * [addShotJson => add a shot from a json]
   * @param  {[shot object]}  shot                    [description]
   * @param  {Boolean} [b=false]               [description]
   */
  this.addShotJson = function(shot, b=false) {
    if(!this.testShot(shot)) {
      var unit = this.w/this.duration;
      var t = shot.start_frame/frame_rate;
      shot.start = this.x+t*unit;
      t = shot.end_frame/frame_rate;
      shot.end = this.x+t*unit;
      this.shots.push(shot);
      this.shots.sort(compare_start);
    }
  }

  /**
   * [updatePos => update timeline width based on (end scene - start scene)]
   */
  this.updatePos = function() {
    this.w = player.w;
    for(let s of this.shots) {
      var unit = this.w/this.duration;
      var t = s.start_frame/frame_rate;
      s.start = this.x+t*unit;
      t = s.end_frame/frame_rate;
      s.end = this.x+t*unit;
    }
  }

  this.fillRough = function(frames_no_info) {
    var shot;
    var unit = this.w/this.duration;
    let t = 'FS';
    let act_inv = [];
    for(let a of actors_timeline) {
      act_inv.push(a.actor_name);
    }
    let ind = montage_editor.getShot(montage_editor.shots, t, act_inv);
    if(ind) {
      shot = montage_editor.shots[ind];
    }
    if(shot) {
      var off=0;
      for(let i=0; i<total_frame; i++){
        if(i>off){
          var n_s = {};//new Shot();
          n_s.actors_involved = shot.actors_involved;
          n_s.type = shot.type;
          n_s.aspect_ratio = shot.aspect_ratio;
          var s = this.getCurrShot(i);
          if(!s) {
            var prec = this.getPrecInd(i);
            var next = this.getNextInd(i);
            if(prec && next){
              n_s.start_frame = prec.end_frame+1;
              n_s.end_frame = next.start_frame-1;
              n_s.start = prec.end +1;
              n_s.end = next.start-1;
            } else if(prec && !next) {
              n_s.start_frame = prec.end_frame+1;
              n_s.end_frame = Math.round(frame_rate*this.duration);
              n_s.start = prec.end +1;
              n_s.end = this.x+this.w;
            } else if(!prec && next) {
              n_s.start_frame = 0;
              n_s.end_frame = next.start_frame-1;
              n_s.start = this.x;
              n_s.end = next.start-1;
            } else if(!prec && !next) {
              n_s.start_frame = 0;
              n_s.end_frame = Math.round(frame_rate*this.duration);
              n_s.start = this.x;
              n_s.end = this.x+this.w;
            }
            var b = false;
            for(let tab of frames_no_info) {
              if(tab) {
                if((tab[0] == n_s.start_frame && tab[tab.length-1] == n_s.end_frame) ||
                (tab[0]+1 == n_s.start_frame && tab[tab.length-1] == n_s.end_frame)) {
                  console.log(tab[0], n_s.start_frame, tab[tab.length-1], n_s.end_frame);

                  b = true;
                  off = n_s.end_frame+1;
                  break;
                }
                if(tab[0] == n_s.start_frame && tab[tab.length-1] < n_s.end_frame) {
                  n_s.start_frame = tab[tab.length-1];
                  n_s.start = unit*n_s.start_frame;
                }
                if(tab[0] > n_s.start_frame && tab[tab.length-1] == n_s.end_frame) {
                  n_s.end_frame = tab[0];
                  n_s.end = unit*n_s.end_frame;
                }
                if(n_s.start_frame < tab[0] && tab[tab.length-1] < n_s.end_frame) {
                  console.log(tab[0], n_s.start_frame, tab[tab.length-1], n_s.end_frame);
                  var new_n_s = {};//new Shot();
                  new_n_s.actors_involved = n_s.actors_involved;
                  new_n_s.end_frame = tab[0];
                  new_n_s.end = unit*new_n_s.end_frame;
                  new_n_s.start_frame = n_s.start_frame;
                  new_n_s.start = n_s.start;
                  if(!this.testShot(new_n_s)) {
                    console.log('new',new_n_s);
                    this.shots.push(new_n_s);
                    this.shots.sort(compare_start);
                  }

                  n_s.start_frame = tab[tab.length-1];
                  n_s.start = unit*n_s.start_frame;
                  break;
                }
              }
            }
            if(!b) {
              if(!this.testShot(n_s)) {
                console.log('old',n_s);
                this.shots.push(n_s);
                this.shots.sort(compare_start);
              }
            }
            off = n_s.end_frame+1;
          }
        } else {
          if(i>off)
            off = 0;
        }
      }
    }
  }

  /**
   * [getNext => get the succesor of the shot s in the timeline]
   * @param  {[shot]} s               [shot object]
   * @return {[shot]}   [shot object]
   */
  this.getNext = function(s) {
    var ret;
    for(var i=0; i<this.shots.length; i++) {
      if(this.shots[i].start_frame == s.start_frame) {
        if(this.shots[i+1]) {
          ret = this.shots[i+1];
          break;
        }
      }
    }
    return ret;
  }

  /**
   * [getPrec => get the previous shot in the timeline]
   * @param  {[shot]} s               [shot object]
   * @return {[shot]}   [shot object]
   */
  this.getPrec = function(s) {
    var ret;
    for(var i=0; i<this.shots.length; i++) {
      if(this.shots[i].start_frame == s.start_frame) {
        if(this.shots[i-1]) {
          ret = this.shots[i-1];
          break;
        }
      }
    }
    return ret;
  }

  /**
   * [getNextInd => get the next shot in the timeline based on a frame number]
   * @param  {[integer]} ind               [frame number]
   * @return {[shot]}     [shot object]
   */
  this.getNextInd = function(ind) {
    var ret;
    var dist = Number.MAX_VALUE;
    for(var i=0; i<this.shots.length; i++) {
      if(abs(this.shots[i].start_frame - ind)<dist && this.shots[i].start_frame > ind) {
        ret = this.shots[i];
        dist = abs(this.shots[i].start_frame - ind);
      }
    }
    return ret;
  }

  /**
   * [getPrecInd => get the previous shot on the timeline based on a frame number]
   * @param  {[integer]} ind               [frame number]
   * @return {[shot]}     [shot object]
   */
  this.getPrecInd = function(ind) {
    var ret;
    var dist = Number.MAX_VALUE;
    for(var i=0; i<this.shots.length; i++) {
      if(abs(this.shots[i].end_frame - ind)<dist && this.shots[i].end_frame < ind) {
        ret = this.shots[i];
        dist = abs(this.shots[i].end_frame - ind);
      }
    }
    return ret;
  }

  /**
   * [getCurrStabShot => get the current framing at the current frame in the timeline]
   * @param  {[integer]} frame_num               [frame number]
   * @return {[x,y,w,h]}           [framing bounding box scaled by the scale ratio]
   */
  this.getCurrStabShot = function(frame_num) {
    var ret;
    if(this.start_frame<=frame_num && frame_num<=this.end_frame) {
      var bb;
      for(var i=0; i<this.shots.length; i++) {
        if(frame_num <= this.shots[i].end_frame && frame_num >= this.shots[i].start_frame) {
          let f_f = this.shots[i].start_frame;
          let s = montage_editor.getShotAspect(this.shots[i].type, this.shots[i].actors_involved, this.shots[i].aspect_ratio, this.shots[i].is_keep_out, this.shots[i].is_pull_in, this.shots[i].is_stage_position, this.shots[i].is_gaze_direction, this.shots[i].is_personalize);
          if(s) {
            bb = s.getCurrStabShot(frame_num);
            if(bb && bb[0] != "null") {
              ret = [bb[0]*scale_ratio, bb[1]*scale_ratio, bb[2]*scale_ratio, bb[3]*scale_ratio];
            }
            break;
          }
        }
      }
    }
    return ret;
  }

  /**
   * [getCurrStabShotNoScale => get the current framing of the timeline]
   * @param  {[integer]} frame_num               [frame number]
   * @return {[x,y,w,h]}           [framing bounding box]
   */
  this.getCurrStabShotNoScale = function(frame_num) {
    var ret;
    var bb;
    for(var i=0; i<this.shots.length; i++) {
      if(frame_num <= this.shots[i].end_frame && frame_num >= this.shots[i].start_frame) {
        let f_f = this.shots[i].start_frame;
        if(this.shots[i].bbox_perso) {
          bb = this.shots[i].bbox_perso;
          break;
        }
        bb = montage_editor.getShotAspect(this.shots[i].type, this.shots[i].actors_involved, this.shots[i].aspect_ratio,this.shots[i].is_keep_out, this.shots[i].is_pull_in, this.shots[i].is_stage_position, this.shots[i].is_gaze_direction, this.shots[i].is_personalize).getCurrStabShot(frame_num);
        if(bb && bb[0] != "null") {
          ret = bb;
        }
        break;
      }
    }
    return ret;
  }

  /**
   * [getCurrShot => get the shot in the timeline at the frame frame_num]
   * @param  {[integer]} frame_num               [frame number]
   * @return {[shot]}           [shot object]
   */
  this.getCurrShot = function(frame_num) {
    var ret;
    for(var i=0; i<this.shots.length; i++) {
      if(frame_num <= this.shots[i].end_frame && frame_num >= this.shots[i].start_frame) {
        ret = this.shots[i];
        break;
      }
    }
    return ret;
  }

  this.setPosition = function(tx, ty) {
    this.x = tx;
    this.y = ty;
  }

  function compare_start(a,b) {
    if (a.start_frame < b.start_frame)
      return -1;
    if (a.start_frame > b.start_frame)
      return 1;
    return 0;
  }

  this.setTimer = function(t) {
    this.time = t;
  }

  this.setXCursor = function(tx) {
    this.x_cursor = tx;
  }

  /**
   * [splitShot => split a shot object into two different shots at the current frame]
   */
  this.splitShot = function() {
    this.stackHistory();

    for(var i=0; i<this.shots.length; i++) {
      if(this.shots[i].on) {
        let shot = this.shots[i];
        let new_s = { ... shot};
        shot.on = false;
        shot.end_frame = frame_num-1;
        new_s.start_frame = frame_num;

        this.shots.push(new_s);
        this.shots.sort(compare_start);
        break;
      }
    }

  }

  /**
   * [removeShot => remove the selected shot from the timeline]
   */
  this.removeShot = function() {
    for(var i=0; i<this.shots.length; i++) {
      if(this.shots[i].on) {
        this.shots.splice(i,1);
      }
    }
  }

  /**
   * [removeSpecificShot => remove a specific shot object]
   * @param  {[shot]} shot                                 [shot object to remove]
   * @param  {[list]} [tab_shots=this.shots]               [shots included in the timeline]
   */
  this.removeSpecificShot = function(shot, tab_shots = this.shots) {
    this.stackHistory();

    let ind = [];
    for (let j=0; j<tab_shots.length; j++) {
      let s = tab_shots[j];
      if(s.type == shot.type) {
        let b1 = true;
        let actors_involved = [];
        for(let a of shot.actors_involved ) {
          actors_involved.push(a.actor_name);
        }
        for(let a of s.actors_involved) {
          if(!actors_involved.includes(a.actor_name)) {
            b1 = false;
            break;
          }
        }
        if(b1 && s.actors_involved.length == actors_involved.length) {
          ind.push(j);
        }
      }
    }
    for (let i = ind.length -1; i >= 0; i--) {
      tab_shots.splice(ind[i],1);
    }
  }

  /**
   * [compressBBoxes => get a bounding box for each frame in order to export the video]
   * @return {[list]} [list of bounding box]
   */
  this.compressBBoxes = function() {
    var ret = [];
    for(let i=this.start_frame; i< this.end_frame;i++) {
      let bb = this.getCurrStabShotNoScale(i);
      if(bb) {
        ret.push(bb);
      } else {
        ret.push([0,0,Number(original_width), Number(original_height)]);
      }
    }
    return ret;
  }

  /**
   * [getNotebookBBoxes => get a bounding box for each frame, if there are no shot "null"]
   * @return {[list]} [list of bounding box]
   */
  this.getNotebookBBoxes = function() {
    var ret = [];
    for(let i=0; i< this.end_frame;i++) {
      if (i<this.start_frame) {
        ret.push('null');
      } else {
        let bb = this.getCurrStabShotNoScale(i);
        if(bb) {
          ret.push(bb);
        } else {
          ret.push('null');
        }
      }
    }
    return ret;
  }

  /**
   * [getAspectRatio => get the aspect ratio of the timeline based on the aspect ratio of each shot]
   * @return {[integer]} [aspect ratio to export the video]
   */
  this.getAspectRatio = function() {
    let curr_a_s;
    for(let s of this.shots) {
      curr_a_s = s.aspect_ratio;
      if(curr_a_s == aspect_ratio) {
        break;
      }
    }
    if(!curr_a_s) {
      curr_a_s = aspect_ratio;
    }
    return curr_a_s;
  }

  /**
   * [extractKeyFrames => get a bounding box for each subtitle to create the illustrated note book]
   * @return {[list]} [list of bounding box]
   */
  this.extractKeyFrames = function() {
    let ret = [];
    for(let t of video.elt.textTracks) {
      if(t.mode = "showing") {
        for(let c of t.cues) {
          let key_frame = {};
          key_frame.Time = parseInt(c.startTime);
          let f_n = Math.round((c.startTime+1)*frame_rate);
          let b = this.getCurrStabShotNoScale(f_n);
          if(b) {
            key_frame.BBox = b;
          } else {
            key_frame.BBox = [0,0,Number(original_width), Number(original_height)];
          }
          ret.push(key_frame);
        }
      }
    }
    return ret;
  }

  /**
   * [extractKeyFramesFromTab => get a bounding box for each element in the list]
   * @param  {[list]} tab               [list of object (start frame)]
   * @return {[list]}     [list of bounding box]
   */
  this.extractKeyFramesFromTab = function(tab) {
    let ret = [];
    for(let p of tab) {
      let key_frame = {};
      key_frame.Time = parseInt(p.start);
      let f_n = Math.round((p.start)*frame_rate);
      let b = this.getCurrStabShotNoScale(f_n);
      if(b) {
        key_frame.BBox = b;
      } else {
        key_frame.BBox = [0,0,Number(original_width), Number(original_height)];
      }
      ret.push(key_frame);
    }
    return ret;
  }

  /**
   * [updateShotPos => update the position of the shot to display the timeline below the video player]
   */
  this.updateShotPos = function() {
    for(let s of this.shots) {
      let unit = this.w/player.total_frame;
      let off_x = player.first*unit;
      let start = this.x + Math.round((s.start_frame-1)*unit) - off_x;
      let end = start + Math.round((s.end_frame-s.start_frame-1)*unit);
      s.start = start;
      s.end = end;
    }
  }

  /**
   * [displayPSL => display the PSL in the player]
   */
  this.displayPSL = function() {
    for(var i=0; i<this.shots.length; i++) {
      if(this.shots[i].start < player.x_cursor && this.shots[i].end > player.x_cursor) {
        fill(255);
        let s = show_shot;
        if(!s)
          s = montage_editor.getShotAspect(this.shots[i].type, this.shots[i].actors_involved, this.shots[i].aspect_ratio, this.shots[i].is_keep_out, this.shots[i].is_pull_in, this.shots[i].is_stage_position, this.shots[i].is_gaze_direction, this.shots[i].is_personalize);
        if(s) {
          if(s.type != 'WS') {
            let type = s.getUpdatedSizeShot(s.getCurrStabShot(frame_num)[3]);
            if(!type) {
              type = s.type;
            }
            text(type, 15 ,60);
            let j=0;
            let string_psl_act = 'On '
            for(let act_name of s.getUpdateActInvolved()) {
              if(j!=0)
                string_psl_act += 'With '
              string_psl_act += act_name+'\n';
              j++;
            }
            text(string_psl_act, 15, 75);
          } else {
            text('WS', 15 ,60);
          }

        }
      }
    }
  }

  /**
   * [displayInfo => display scene information below the player]
   */
  this.displayInfo = function() {
    push();
    fill(0);
    textSize(17);
    text('Create scene : ',5,viewer_height+15);
    text('Select a scene : ',mid_width-320,viewer_height+15);
    pop();

    push();
    fill(0);
    textSize(17);
    text('Start ',5,this.y-10);
    text(player.getTimeFrame(this.start_frame),5,this.y+10);
    text(player.getTimeFrame(this.end_frame),5,this.y+this.h-10);
    text('End ',5,this.y+this.h+10);
    pop();

    push();
    fill(0);
    this.remove_button.x = mid_width-17;
    this.remove_button.y = viewer_height+5;
    fill(255);
    rect(this.remove_button.x-this.remove_button.rad/2,this.remove_button.y-this.remove_button.rad/2,this.remove_button.rad*2,this.remove_button.rad*2);
    stroke("#AA0000");
    strokeWeight(3);
    line(this.remove_button.x,this.remove_button.y,this.remove_button.x+this.remove_button.rad,this.remove_button.y+this.remove_button.rad);
    line(this.remove_button.x+this.remove_button.rad,this.remove_button.y,this.remove_button.x,this.remove_button.y+this.remove_button.rad);
    pop();
  }

  /**
   * [display => function called in the main p5 draw function]
   */
  this.display = function() {


    if(is_montage_editor) {
      if(viewer_height+80<mouseY && mouseY<height && mouseX<mid_width) {
        // Display a helping message
        push();
        fill(255);
        rect(0, height-20,windowWidth,20);
        fill('black');
        noStroke();
        textSize(16);
        text('Press Z + scroll the mouse wheel to zoom, ctrl + z undo, ctrl + y redo',0,height-5);
        pop();
      }

      if((mouseX > this.start_time_button.x && mouseX < this.start_time_button.x + this.start_time_button.w && mouseY > this.start_time_button.y && mouseY < this.start_time_button.y + this.start_time_button.h)
      || (mouseX > this.end_time_button.x && mouseX < this.end_time_button.x + this.end_time_button.w && mouseY > this.end_time_button.y && mouseY < this.end_time_button.y + this.end_time_button.h)) {
        // Display a helping message
        push();
        fill(255);
        rect(0, height-20,windowWidth,20);
        fill('black');
        noStroke();
        textSize(16);
        text('Press Maj + click on start or end to set current time',0,height-5);
        pop();
      }
    }

    let bool_curs = false;
    this.duration = (player.total_frame/frame_rate);
    let unit = this.w/player.total_frame;
    let start_frame_x = Math.max((this.start_frame-player.first)*unit,0);
    let end_frame_x = Math.min((this.end_frame-player.first)*unit,this.w);
    this.updateShotPos();
    push();
    noStroke();
    fill(120);
    rect(this.x,this.y,this.w,this.h);
    pop();

    for(var i=0; i<this.shots.length; i++) {
      if(this.shots[i].start_frame<Math.min(player.last,this.end_frame) && this.shots[i].end_frame>Math.max(player.first,this.start_frame)) {
        let x_start = Math.max(this.shots[i].start, start_frame_x+this.x);
        let x_end = Math.min(this.shots[i].end, end_frame_x+this.x);
        if(x_start<mouseX && mouseX<x_end && this.y<mouseY && mouseY<this.y+this.h) {
          // Display a helping message
          push();
          fill(255);
          rect(0, height-20,windowWidth,20);
          fill('black');
          noStroke();
          textSize(16);
          if(!this.shots[i].on) {
            text('Click to select',0,height-5);
          } else {
            if(this.shots[i].start_frame< frame_num && frame_num < this.shots[i].end_frame) {
              text('Press S to split, press del or backspace to remove, drag right / left to extend, press maj + click to an other rush to replace',0,height-5);
            } else {
              text('Press del or backspace to remove, drag right / left to extend, press maj + click to an other rush to replace',0,height-5);
            }
          }
          pop();
        }
        if(this.shots[i].on) {
          push();
          noStroke();
          fill(46,92,156);
          if(this.draggin && mouseX >x_end) {
            x_end=mouseX;
            if(player.nav_bar.cursor-10 < x_end && x_end < player.nav_bar.cursor+10) {
              x_end = player.nav_bar.cursor;
            }
            document.body.style.cursor = "e-resize";
            bool_curs = true;
          } else if(this.draggin && mouseX <x_start) {
            x_start=mouseX;
            if(player.nav_bar.cursor-10 < x_start && x_start < player.nav_bar.cursor+10) {
              x_start = player.nav_bar.cursor;
            }
            document.body.style.cursor = "w-resize";
            bool_curs = true;
          }
          rect(x_start,this.y,x_end-x_start,this.h);

          pop();
        }
        if(this.shots[i]) {
          if(!bool_curs) {
            if((abs(mouseX-x_start)<5 || abs(mouseX-x_end)<5) && (mouseY>this.y && mouseY<this.y+this.h)) {
              document.body.style.cursor = "col-resize";
              bool_curs = true;
            }
          }
          push();
          strokeWeight(2);
          stroke(255);
          line(x_start, this.y+this.h/2, x_end-10, this.y+this.h/2);
          line(x_end, this.y, x_end, this.y+this.h);
          line(x_end, this.y, x_end-10, this.y);
          line(x_end, this.y+this.h, x_end-10, this.y+this.h);
          fill(255);
          noStroke();
          let keepout = "";
          if(this.shots[i].is_keep_out) {
            keepout = " K";
          }
          let pullin = "";
          if(this.shots[i].is_pull_in) {
            pullin = " P";
          }
          let gaze = "";
          if(this.shots[i].is_gaze_direction) {
            gaze = " G";
          }
          let stage_pos = "";
          if(this.shots[i].is_stage_position) {
            stage_pos = " S";
          }
          text(this.shots[i].type+keepout+pullin+gaze+stage_pos, x_start, this.y+10);
          for(var j=0; j<this.shots[i].actors_involved.length; j++) {
            text(this.shots[i].actors_involved[j].actor_name, x_start, this.y+25+j*15);
          }
          pop();
          }
        }
    }

    push();

    push()
    fill(80);
    rect(this.x,this.y,start_frame_x,this.h);
    rect(this.x+end_frame_x,this.y,((this.x+this.w)-end_frame_x)-this.x,this.h);

    strokeWeight(1);
    for(var i=0; i<this.w; i++) {
      var x = this.x + i;
      if(x<player.nav_bar.cursor) {
        stroke(50,50,123);
      } else {
        stroke(255);
      }
      var y = this.y+this.h;
      if(i%10==0)
        line(x, y, x, y-15);
      else {
        line(x, y, x, y-2);
      }
    }
    stroke(0);
    strokeWeight(2);
    fill(0);
    if(player.nav_bar.cursor < this.x+this.w) {
      line(player.nav_bar.cursor,this.y,player.nav_bar.cursor,this.y+this.h);
      triangle(player.nav_bar.cursor-4, this.y-4, player.nav_bar.cursor+4, this.y-4, player.nav_bar.cursor, this.y);
    }
    pop();

    if(is_montage_editor) {
      this.displayPSL();
      this.displayInfo();
    }

    if(!bool_curs) {
      document.body.style.cursor = "default";
    }
  }
}
