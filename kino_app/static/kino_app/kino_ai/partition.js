/*
This file is part of KinoAi project.
It is subject to the license terms in the LICENSE file found in the top-level directory
*/

function PartitionEditor()  {

  this.div_partition_saved = createDiv();
  this.div_partition_saved.hide();
  this.textarea_current_partition = createElement('textarea');
  this.textarea_current_partition.hide();

  this.button_stage_scene_script = createButton('stage script');
  this.button_stage_scene_script.side = false;
  html_elements.push(this.button_stage_scene_script);
  this.button_stage_scene_script.mousePressed(setOriginal);

  this.button_extract_sub = createButton('download subs');
  this.button_extract_sub.side = false;
  html_elements.push(this.button_extract_sub);
  this.button_extract_sub.mousePressed(downloadSubs);

  this.select_user = createSelect();
  this.select_user.side = false;
  this.select_user.mousePressed(function(){annotation_editor.partition_editor.select_user.elt.dispatchEvent(new Event('change'));});
  html_elements.push(this.select_user);
  this.select_user.changed(selectUserPartition);

  this.user_title = createElement('h3','Script user');
  this.user_title.side = false;
  html_elements.push(this.user_title);
  this.user_title.style('margin',0);
  this.user_title.style('font-size',16);

  this.notebook_title = createElement('h3','Title');
  this.notebook_title.side = false;
  html_elements.push(this.notebook_title);
  this.notebook_title.style('margin',0);
  this.notebook_title.style('font-size',18);
  this.notebook_title.style('background','white');
  this.notebook_title.elt.contentEditable = true;
  this.notebook_title.elt.onchange = function(){annotation_editor.partition_editor.create_notebook.position(player.x+140+annotation_editor.partition_editor.notebook_title.size().width+10);};

  this.create_notebook = createButton('Export notebook');
  this.create_notebook.mouseOver(processToolTip('Create the illustrated notebook based on the timeline'));
  this.create_notebook.mouseOut(processToolTip(''));
  this.create_notebook.side = false;
  html_elements.push(this.create_notebook);
  this.create_notebook.size(150,30);
  this.create_notebook.mousePressed(createNotebook);

  this.partitions_saved = [];

  this.partitions_scenes = [];

  this.history_partitions = [];

  this.duration = video.duration();

  this.curr_start = 0;
  this.is_scene = false;

  this.curr_end;

  this.curr_speaker;

  this.curr_repeat;

  /**
   * [click => handle mouse click event]
   * @param  {[float]} mx               [mouse x position]
   * @param  {[float]} my               [mouse y position]
   */
  this.click = function(mx, my) {

    if(keyIsPressed) {
      if(mx > mid_width+10 && mx < mid_width+10+100 && my>this.off_y-10 && my < this.off_y + 25) {
        if(this.button_stage_scene_script.elt.innerText == 'stage script') {
          this.curr_start = video.time();
        } else {
          this.curr_start = video.time() - shots_timeline.start_frame /frame_rate;
        }
        return;
      }
    }

    for(let p of this.partitions_saved) {
      let y_time = p.TimeElt.elt.offsetTop - this.div_partition_saved.elt.scrollTop;
      let h_time = p.TimeElt.height;
      if(p.x && p.w && mx > p.x && mx < p.x + p.w && my > shots_timeline.y && my < shots_timeline.y+shots_timeline.h) {
        p.on = true;
        p.TextElt.style('color','red');
        if(this.is_scene) {
          video.time(Math.min(video.duration(),p.Start + shots_timeline.start_frame/frame_rate));
        }else {
          video.time(p.Start);
        }
      } else if(mx > mid_width && mx < mid_width + p.TimeElt.width && my > y_time && my < y_time+h_time) {
        p.on = true;
        p.TextElt.style('color','red');
        if(this.is_scene) {
          video.time(Math.min(video.duration(),p.Start + shots_timeline.start_frame/frame_rate));
        }else {
          video.time(p.Start);
        }
      } else {
        p.on = false;
        p.TextElt.style('color',p.color);
      }
    }
  };

  /**
   * [doubleClicked => handle doubleClicked event]
   * @param  {[float]} mx               [mouse x position]
   * @param  {[float]} my               [mouse y position]
   */
  this.doubleClicked = function(mx, my) {
    if(mx > mid_width+10 && mx < mid_width+10+100 && my>this.off_y-10 && my < this.off_y + 25) {
      this.curr_start = 0;
    }
  }

  this.drop = function(mx, my) {

  }

  this.drag = function(mx, my) {

  }

  /**
   * [keyPressed => handle keyPressed event]
   * @param  {[code event]} keyCode               [key code]
   */
  this.keyPressed = function(keyCode) {
    // Key Del or backspace to remove sub
    if((keyCode == 46 || keyCode == 8) && this.select_user.value() == username) {
      this.removeSub();
    }
  }

  /**
   * [update => resize and show the partition interface]
   * @param  {[Boolean]} checked               [is partition check box checked]
   */
  this.update = function(checked) {
    if(checked) {
      if(this.select_user.child().length==0) {
        for(let u of user_partitions) {
          this.select_user.option(u);
        }
      }
      $('#div_creation').hide();
      annotation_editor.note_editor.update(false);
      annotation_editor.is_note_book = false;
      annotation_editor.note_book.checked(false);
      $('#div_sub').hide();
      this.showAllElt();
      annotation_editor.partition_editor.button_stage_scene_script.elt.innerText = 'stage scipt';
      setOriginal();
    } else {
      $('#div_creation').show();
      // showAllElt();
      annotation_editor.partition_editor.button_stage_scene_script.elt.innerText = 'scene scipt';
      setOriginal(false);
      this.hideAllElt();
    }
  }

  /**
   * [hideAllElt => hide all elements]
   */
  this.hideAllElt = function() {
    this.select_user.hide();
    this.user_title.hide();
    this.notebook_title.hide();
    montage_editor.show_sub.hide();
    this.div_partition_saved.hide();
    this.textarea_current_partition.hide();
    shots_timeline.select_timeline.hide();
    shots_timeline.user_title.side = true;
    shots_timeline.user_title.hide();
    shots_timeline.select_user.side = true;
    shots_timeline.select_user.hide();
    this.button_stage_scene_script.hide();
    this.button_extract_sub.hide();
    this.create_notebook.hide();
  }

  /**
   * [showAllElt => show all elements]
   */
  this.showAllElt = function() {
    shots_timeline.x = player.x;
    shots_timeline.h = 100;
    this.select_user.show();
    this.user_title.show();
    this.notebook_title.show();
    montage_editor.show_sub.show();
    this.div_partition_saved.show();
    this.textarea_current_partition.show();
    shots_timeline.user_title.side = false;
    shots_timeline.user_title.show();
    shots_timeline.select_timeline.show();
    shots_timeline.select_user.side = false;
    shots_timeline.select_user.show();
    this.create_notebook.show();
    this.setDivSizePos();
    this.button_stage_scene_script.show();
    this.button_extract_sub.show();
    shots_timeline.updateTimeline();
    this.updateSub();
    up_rough = true;
  }

  /**
   * [selectSceneParts => select a specific scene related to a video editing]
   * @param  {[string]} name               [user name]
   */
  this.selectSceneParts = function(name) {
    for(let s of shots_timeline.list_data) {
      let b = false;
      for(let p of this.partitions_scenes) {
        if(p.Name == s.Name) {
          b = true;
          break;
        }
      }
      if(!b)
        this.partitions_scenes.push({'UserName':username,'SceneUser':shots_timeline.select_user.value(),'Name':s.Name,'PartArray':[]});
    }
    let b = false;
    for(let p of this.partitions_scenes) {
      if(p.Name == name && p.UserName == this.select_user.value() && p.SceneUser==shots_timeline.select_user.value()) {
        this.partitions_saved = p.PartArray;
        this.is_scene = true;
        b = true;
        break;
      }
    }
    if(!b && this.select_user.value() == username) {
      this.partitions_scenes.push({'UserName':username,'SceneUser':shots_timeline.select_user.value(),'Name':name,'PartArray':[]});
      this.partitions_saved = [];
      this.is_scene = true;
    }
    this.updateRemoveUndo();
    if(this.partitions_saved[this.partitions_saved.length-1]) {
      this.curr_start =this.partitions_saved[this.partitions_saved.length-1].End;
    } else {
      this.curr_start =0;
    }
    this.updateSub();
  }

  /**
   * [setOriginal => select the original video script]
   * @param {Boolean} [up=true]  [description]
   */
  function setOriginal(up = true) {
    annotation_editor.partition_editor.select_user.value(username);

    if(annotation_editor.partition_editor.button_stage_scene_script.elt.innerText == 'stage script') {
      annotation_editor.partition_editor.button_stage_scene_script.elt.innerText = 'scene script';
      shots_timeline.user_title.show();
      shots_timeline.select_timeline.show();
      shots_timeline.select_user.show();
      annotation_editor.partition_editor.select_user.show();
      annotation_editor.partition_editor.user_title.show();
      annotation_editor.partition_editor.select_user.elt.dispatchEvent(new Event('change'));
    } else {
      annotation_editor.partition_editor.button_stage_scene_script.elt.innerText = 'stage script';
      shots_timeline.user_title.hide();
      shots_timeline.select_timeline.hide();
      shots_timeline.select_user.hide();
      annotation_editor.partition_editor.select_user.hide();
      annotation_editor.partition_editor.user_title.hide();

      let is_sub = false;
      for(let p of annotation_editor.partition_editor.partitions_scenes) {
        if(p.Name == 'TheSubtitles') {
          annotation_editor.partition_editor.partitions_saved = p.PartArray;
          is_sub = true;
          break;
        }
      }
      if(!is_sub && up) {
        annotation_editor.partition_editor.partitions_scenes.push({'Name':'TheSubtitles','PartArray':[]});
      }
      if(up)
        annotation_editor.partition_editor.updateRemoveUndo();
      if(annotation_editor.partition_editor.partitions_saved[annotation_editor.partition_editor.partitions_saved.length-1]) {
        annotation_editor.partition_editor.curr_start =annotation_editor.partition_editor.partitions_saved[annotation_editor.partition_editor.partitions_saved.length-1].End;
      } else {
        annotation_editor.partition_editor.curr_start =0;
      }

      annotation_editor.partition_editor.is_scene = false;
      annotation_editor.partition_editor.updateSub();
    }

  }

  /**
   * [setSpeaker => set an actor as the current speaker]
   */
  function setSpeaker() {

    for(let p of annotation_editor.partition_editor.partitions_saved) {
      if(p.SpeakerElt.child()[0] == this) {
        if(this.value=='None') {
          p.Speaker = this.value;
          p.color = 'black';
          p.SpeakerElt.child()[0].style.color = 'black';
          p.SpeakerElt.child()[1].value =  'black';
          break;
        }
        p.Speaker = this.value;
        p.color = preparation_editor.getAct(this.value).color;
        p.SpeakerElt.child()[0].style.color =  preparation_editor.getAct(this.value).color;
        p.SpeakerElt.child()[1].value =  preparation_editor.getAct(this.value).color;
        break;
      }
    }
  }

  /**
   * [setColor => set the color for the note]
   */
  function setColor() {
    let speaker = 'Unknown';
    for(let p of annotation_editor.partition_editor.partitions_saved) {
      if(p.SpeakerElt.child()[1] == this.elt) {
        p.color = this.elt.value;
        preparation_editor.getAct(p.Speaker).color = this.elt.value;
        p.SpeakerElt.child()[0].style.color = this.elt.value;
        speaker = p.Speaker;
        break;
      }
    }
    for(let p of annotation_editor.partition_editor.partitions_saved) {
      if(p.Speaker == speaker) {
        p.color = this.elt.value;
        preparation_editor.getAct(p.Speaker).color = this.elt.value;
        p.SpeakerElt.child()[0].style.color = this.elt.value;
        p.SpeakerElt.child()[1].value = this.elt.value;
      }
    }
  }

  /**
   * [downloadSubs => download a subtitle file based on the playscript]
   */
  function downloadSubs() {
    let tab = [];
    for(let c of annotation_editor.partition_editor.partitions_saved) {
      let obj = {};
      obj.start = round_prec(c.Start,2);
      obj.end =  round_prec(c.End,2);
      obj.text = c.TextElt.elt.innerHTML.replace(/<br>/g,'\n');
      tab.push(obj);
    }
    $.post({
      url: "download_vtt",
      async: true,
      data: {'abs_path': abs_path, 'tab_sub':JSON.stringify(tab)},
      dataType: 'json',
      success: function (data) {
        let a = createA(data['src_vtt'], 'link');
        a.elt.download = "subtitle.vtt";
        a.id('click');
        a.hide();
        document.getElementById('click').click();
        a.remove();
        let a1 = createA(data['src_srt'], 'link');
        a1.elt.download = "subtitle.srt";
        a1.id('click_srt');
        a1.hide();
        document.getElementById('click_srt').click();
        a1.remove();
      }
    });
  }

  /**
   * [createNotebook => Extract key frames from subtitles and create an illustrated_notebook json]
   */
  function createNotebook() {
    var data_rushes = shots_timeline.getNotebookBBoxes();
    var name = annotation_editor.partition_editor.notebook_title.elt.innerText;
    var tab_part = annotation_editor.partition_editor.getCurrentPartition();
    var keyframes_from_tab;
    if(annotation_editor.partition_editor.button_stage_scene_script.elt.innerText == 'stage script') {
      keyframes_from_tab = [];
      for(let t of tab_part) {
        keyframes_from_tab.push({'Time':parseInt(t.start),'BBox' : [0,0,Number(original_width), Number(original_height)]});
      }
    } else {
      keyframes_from_tab = shots_timeline.extractKeyFramesFromTab(tab_part);
    }

    annotation_editor.partition_editor.is_export_launched = true;
    $.post({
      url : "processKeyFrames",
      async: true,
      data: {'abs_path': abs_path, 'data':JSON.stringify(data_rushes), 'KeyFrames':JSON.stringify(keyframes_from_tab), 'name':name.replace(/ /g,'_'),
      'sub_notebook': JSON.stringify(tab_part), 'width':original_width},
      dataType: 'json',
      success: function (data) {
        annotation_editor.partition_editor.is_export_launched = false;
        if(data['msg']) {
          alert(data['msg']);
        }
        if(data['name']) {
          if (window.confirm('If you click on "ok" you would be redirected toward the notebook '+name+'. Cancel to stay on this page ')) {
            window.location.href='../video_book/'+id_db+'/'+data['name'];
          };
        }
      }
    });
  }

  /**
   * [selectUserPartition => select a user partition]
   */
  function selectUserPartition() {
    annotation_editor.partition_editor.selectSceneParts(shots_timeline.select_timeline.value());
  }

  /**
   * [updateSub => create a new subtitle for each new note]
   */
  this.updateSub = function() {
    if(video.elt.textTracks[0]) {
      while(video.elt.textTracks[0].cues.length!=0)
        for(let s of video.elt.textTracks[0].cues){video.elt.textTracks[0].removeCue(s);}
      for(let c of this.partitions_saved){
        if(this.is_scene) {
          var s = Math.min(video.duration(),c.Start + shots_timeline.start_frame/frame_rate);
          var e = Math.min(video.duration(),c.End + shots_timeline.start_frame/frame_rate);
        } else {
          var s = c.Start;
          var e = c.End;
        }
        let cue = new VTTCue(s, e, c.Text+'\n');
        c.cue = cue;
        video.elt.textTracks[0].addCue(cue);
      }
    }
  }

  /**
   * [removeSub => remove a subtitle]
   */
  this.removeSub = function() {
    for(let i=0; i<this.partitions_saved.length;i++) {
      let curr = this.partitions_saved[i];
      if(curr.on) {
        // let next = this.partitions_saved[i+1];
        // let prec = this.partitions_saved[i-1];
        // curr.prev_index = i;
        // if(prec && next) {
        //   prec.End = (curr.Start + curr.End)/2;
        //   next.Start = (curr.Start + curr.End)/2;
        // } else if(!prec && next) {
        //   next.Start = 0;
        // }
        if(video.elt.textTracks[0].cues[i])
          video.elt.textTracks[0].removeCue(video.elt.textTracks[0].cues[i]);
        this.history_partitions.push(curr);
        this.partitions_saved.splice(i, 1);
        this.partitions_saved.sort(compare_start);
        this.updateRemoveUndo();
      }
    }
    if(this.partitions_saved[this.partitions_saved.length-1]) {
      this.curr_start =this.partitions_saved[this.partitions_saved.length-1].End;
    } else {
      this.curr_start =0;
    }
  }

  /**
   * [undoRemove => undo a remove action]
   */
  this.undoRemove = function() {
    if(this.history_partitions.length!=0) {
      let p = this.history_partitions.pop();
      this.partitions_saved.splice(p.prev_index, 0, p);
      let next = this.partitions_saved[p.prev_index+1];
      let prec = this.partitions_saved[p.prev_index-1];
      if(next) {
        next.Start = p.End;
      }
      if(prec) {
        prec.End = p.Start;
      }
      this.updateRemoveUndo();
    }
  }

  /**
   * [updateRemoveUndo => update the interface]
   */
  this.updateRemoveUndo = function() {
    while(this.div_partition_saved.elt.firstChild){this.div_partition_saved.elt.firstChild.remove();}
    for(let p of this.partitions_saved) {
      this.updatePartitionSaved(p, true);
    }
    this.setDivSizePos();
  }

  /**
   * [mouseWheel => handle mouseWheel event]
   * @param  {[event]} event               [mouse wheel event]
   */
  this.mouseWheel = function(event) {
    // T key
    if(keyIsPressed && keyCode == 116  && this.select_user.value() == username) {
      for(let i=0; i<this.partitions_saved.length; i++) {
        let p = this.partitions_saved[i];
        let y_time = p.TimeElt.elt.offsetTop - this.div_partition_saved.elt.scrollTop;
        let h_time = p.TimeElt.height;
        if(mouseX > mid_width && mouseX < mid_width + p.TimeElt.width && mouseY > y_time && mouseY < y_time+h_time) {
          event.preventDefault();
          p.on = true;
          p.TextElt.style('color','red');
          this.updateTime(p, event.deltaY, i);
        } else {
          p.on = false;
          p.TextElt.style('color',p.color);
        }
      }
    }
  }

  /**
   * [Bind two key pressed events together]
   * @param  {[event]} e               [key pressed event]
   */
  $(document).bind('keydown', function(e) {
    if(is_annotation_editor && annotation_editor.is_partition_editor) {
      // Enter key
      if((e.ctrlKey || e.metaKey) && (e.which == 13)) {
        e.preventDefault();
        annotation_editor.partition_editor.savePartition();
      }
      if((e.ctrlKey || e.metaKey)) {
        let element = annotation_editor.partition_editor.textarea_current_partition.elt;
        for(let i=0; i<preparation_editor.actors_timeline.length; i++) {
          if(e.keyCode == 49+i) {
            e.preventDefault();
            element.value += '   '+preparation_editor.actors_timeline[i].actor_name.toUpperCase()+'\n';
            annotation_editor.partition_editor.curr_speaker = preparation_editor.actors_timeline[i];
            element.focus();
            element.setSelectionRange(element.value.length,element.value.length);
          }
        }
      }
      // Space key
      if((e.ctrlKey || e.metaKey) && (e.which == 32)) {
        e.preventDefault();
        if(playing)
          dash_player.pause();
        else {
          dash_player.play();
          img_hd = undefined;
          annotation_editor.partition_editor.curr_repeat = video.time();
        }
        playing = !playing;
      }
      // R key
      if((e.ctrlKey || e.metaKey) && (e.which == 82)) {
        e.preventDefault();
        video.time(annotation_editor.partition_editor.curr_repeat);
        dash_player.play();
        playing = true;
      }
      // L key
      if((e.ctrlKey || e.metaKey) && (e.which == 76)) {
        e.preventDefault();
        video.time(annotation_editor.partition_editor.getPrecTime());
        dash_player.play();
        playing = true;
      }
      // M key
      if((e.ctrlKey || e.metaKey) && (e.which == 77)) {
        e.preventDefault();
        video.time(annotation_editor.partition_editor.getNextTime());
        dash_player.play();
        playing = true;
      }
      // LeftArrow key
      if((e.ctrlKey || e.metaKey) && (e.which == 37)) {
        e.preventDefault();
        video.time(video.time()-0.05);
      }
      // RightArrow key
      if((e.ctrlKey || e.metaKey) && (e.which == 39)) {
        e.preventDefault();
        video.time(video.time()+0.05);
      }
      // Z key
      if((e.ctrlKey || e.metaKey) && (e.which == 90)) {
        e.preventDefault();
        annotation_editor.partition_editor.undoRemove();
      }
    }
  });

  /**
   * [getPrecTime => get the previous start time note]
   * @return {[float]} [time in seconds]
   */
  this.getPrecTime = function() {
    let off = video.duration();
    let time = 0;
    for(let p of this.partitions_saved) {
      if(video.time() > p.End && off > video.time() - p.End) {
        off = video.time() - p.End;
        time = p.Start;
      }
    }
    return time;
  }

  /**
   * [getNextTime => get the next start time note]
   * @return {[float]} [time in seconds]
   */
  this.getNextTime = function() {
    let off = video.duration();
    let time = 0;
    for(let p of this.partitions_saved) {
      if(video.time() < p.Start && off > abs(video.time() - p.Start)) {
        off = abs(video.time() - p.Start);
        time = p.Start;
      }
    }
    return time;
  }

  /**
   * [updateTime => increase or decrease the time]
   * @param  {[object]} part                [note object]
   * @param  {[Number]} delta               [plus or minus]
   * @param  {[integer]} i                   [index]
   */
  this.updateTime = function(part, delta, i) {
    let next;
    if(this.partitions_saved[i+1]) {
      next = this.partitions_saved[i+1];
    }
    if(delta<0) {
      if(next && part.End +0.5 > next.Start) {
        if(part.End + 0.5 < next.End) {
          part.End += 0.5;
          next.Start = part.End;
          part.TimeElt.elt.innerText = this.timeToString(part.Start)+'  ==>  '+this.timeToString(part.End);
          next.TimeElt.elt.innerText = this.timeToString(next.Start)+'  ==>  '+this.timeToString(next.End);
        }
      } else {
        part.End += 0.5;
        part.End = Math.min(this.duration, part.End);
        part.TimeElt.elt.innerText = this.timeToString(part.Start)+'  ==>  '+this.timeToString(part.End);
      }
    } else {
      if(next && part.End + 0.5 > next.Start) {
        if(part.End -0.5 > part.Start) {
          part.End -= 0.5;
          next.Start = part.End;
          part.TimeElt.elt.innerText = this.timeToString(part.Start)+'  ==>  '+this.timeToString(part.End);
          next.TimeElt.elt.innerText = this.timeToString(next.Start)+'  ==>  '+this.timeToString(next.End);
        }
      } else {
        if(part.End -0.5 > part.Start) {
          part.End -= 0.5;
          part.TimeElt.elt.innerText = this.timeToString(part.Start)+'  ==>  '+this.timeToString(part.End);
        }
      }
    }
  }

  /**
   * [setDivSizePos => resize the div that contain the play script and the other element]
   */
  this.setDivSizePos = function() {
    if(this.partitions_saved.length>0) {
      this.div_partition_saved.show();
      this.div_partition_saved.position(mid_width,can.elt.offsetTop);
      this.div_partition_saved.size((windowWidth - 160)-mid_width-20,this.div_partition_saved.elt.scrollHeight);
      this.div_partition_saved.style('margin','0 10');
      this.textarea_current_partition.size((windowWidth - 160)-mid_width-20,150);
      let new_h = height-(this.textarea_current_partition.height*1.5);
      if(this.div_partition_saved.elt.scrollHeight > new_h) {
        this.div_partition_saved.size((windowWidth - 160)-mid_width-20,new_h);
        this.div_partition_saved.style('overflow','auto');
      }
      this.textarea_current_partition.position(mid_width+10,this.div_partition_saved.position().y+this.div_partition_saved.elt.offsetHeight+60);
    } else {
      this.div_partition_saved.hide();
      this.textarea_current_partition.position(mid_width+10,can.elt.offsetTop+60);
      this.textarea_current_partition.size((windowWidth - 160)-mid_width-20,150);
    }
    this.textarea_current_partition.style('background','rgb(150,150,150)');
    this.textarea_current_partition.style('overflow','auto');
    this.textarea_current_partition.style('font-size','15');
    this.textarea_current_partition.style('resize','none');

    this.button_stage_scene_script.original_x = player.x;
    this.button_stage_scene_script.original_y = viewer_height;
    this.button_stage_scene_script.size(150);

    this.select_user.original_x = mid_width-175-3*160+50;
    this.select_user.original_y = viewer_height+30;
    this.select_user.size(150);

    this.user_title.original_x = this.select_user.original_x-110;
    this.user_title.original_y = viewer_height+30;

    shots_timeline.select_user.original_x = mid_width-175-160;
    shots_timeline.select_user.original_y = viewer_height+30;

    shots_timeline.user_title.original_x = mid_width-175-160-100;
    shots_timeline.user_title.original_y = viewer_height+30;

    shots_timeline.select_timeline.original_y = viewer_height+30;

    this.notebook_title.original_x = player.x+165;
    this.notebook_title.original_y = viewer_height+175;

    this.create_notebook.original_x = this.notebook_title.original_x+this.notebook_title.size().width+15;
    this.create_notebook.original_y = viewer_height+175;
    this.create_notebook.size(150);

    this.button_extract_sub.original_x = mid_width-175;
    this.button_extract_sub.original_y = viewer_height+175;
    this.button_extract_sub.size(150);
  }

  /**
   * [compare_start => sort the partitions by the start time]
   * @param  {[object]} a               [note object]
   * @param  {[object]} b               [note object]
   */
  function compare_start(a,b) {
    if (a.Start < b.Start)
      return -1;
    if (a.Start > b.Start)
      return 1;
    return 0;
  }

  /**
   * [savePartition => add a note to the playscript]
   */
  this.savePartition = function() {
    //Parse <>
    if(this.curr_start < this.curr_end && this.select_user.value() == username) {
      let partition_obj = {};
      partition_obj.Start = round_prec(this.curr_start,2);
      partition_obj.End = round_prec(this.curr_end,2);
      partition_obj.Text = this.textarea_current_partition.elt.value;
      if(this.curr_speaker) {
        partition_obj.Speaker = this.curr_speaker.actor_name;
        partition_obj.color = this.curr_speaker.color;
      } else {
        partition_obj.Speaker = "None";
        partition_obj.color = 'black';
      }
      for(let i=0;i< this.partitions_saved.length;i++) {
        let p = this.partitions_saved[i];
        if(partition_obj.End < p.Start) {
          if(this.partitions_saved[i-1] && p.End < this.partitions_saved[i-1].End) {
            let p_prev = this.partitions_saved[i-1];
            p_prev.End = partition_obj.Start;
            p_prev.TimeElt.elt.innerText = this.timeToString(p_prev.Start)+'  ==>  '+this.timeToString(p_prev.End);
          }
          break;
        }
        if(partition_obj.Start < p.Start && p.Start < partition_obj.End && partition_obj.End < p.End) {
          p.Start = partition_obj.End;
          p.TimeElt.elt.innerText = this.timeToString(p.Start)+'  ==>  '+this.timeToString(p.End);
          break;
        }
        if(p.Start< partition_obj.Start && partition_obj.End < p.End) {
          p.End = partition_obj.Start;
          p.TimeElt.elt.innerText = this.timeToString(p.Start)+'  ==>  '+this.timeToString(p.End);
          break;
        }
      }
      this.partitions_saved.push(partition_obj);
      this.partitions_saved.sort(compare_start);
      if(video.elt.textTracks.length == 0) {
        video.elt.addTextTrack("captions", "French", "fr");
      }
      if(this.is_scene) {
        var s = Math.min(video.duration(),partition_obj.Start + shots_timeline.start_frame/frame_rate);
        var e = Math.min(video.duration(),partition_obj.End + shots_timeline.start_frame/frame_rate);
      } else {
        var s = partition_obj.Start;
        var e = partition_obj.End;
      }
      let cue = new VTTCue(s, e, partition_obj.Text+'\n');
      partition_obj.cue = cue;
      video.elt.textTracks[0].addCue(cue);
      this.textarea_current_partition.elt.value = "";
      this.curr_start = this.curr_end;
      this.updatePartitionSaved(partition_obj);
      this.setDivSizePos();
      this.div_partition_saved.elt.scrollTop = this.div_partition_saved.elt.scrollHeight;
    } else if(this.curr_start < this.curr_end && this.select_user.value() != username){
      alert("Select your own script");
    } else {
      alert("A subtitle can't finished before it starts");
    }
  }

  /**
   * [savePartitionTimeline => save the playscript on the server]
   */
  this.savePartitionTimeline = function() {
    let tab = [];
    let tab_json = [];
    for(let p_s of this.partitions_scenes) {
      if(p_s.UserName == username || p_s.Name == "TheSubtitles") {
        let part = {};
        part.Name = p_s.Name;
        part.SceneUser = p_s.SceneUser;
        part.Array = [];
        for(let p of p_s.PartArray) {
          if(p.Text && p.Text != '<br>') {
            let obj = {};
            obj.start = p.Start;
            obj.end = p.End;
            obj.text = p.Text;
            if(p_s.Name == "TheSubtitles") {
              tab.push(obj);
            } else {
              obj.speaker = p.Speaker;
              obj.color = p.color;
              part.Array.push(obj);
            }
          }
        }
        if(p_s.Name != "TheSubtitles") {
          tab_json.push(part);
        }
      }
    }
    $.post({
      url: "save_partitions",
      async: true,
      data: {'abs_path': abs_path, 'partitions':JSON.stringify(tab), 'partitions_json':JSON.stringify(tab_json)},
      dataType: 'json',
      success: function (data) {
        // console.log(data);
      }
    });
  }

  /**
   * [loadSubtitle => load a playscriptfrom a subtitle file]
   */
  this.loadSubtitle = function() {
    if(this.partitions_saved.length==0) {
      this.partitions_saved = [];
      this.div_partition_saved.remove();
      this.div_partition_saved = createDiv();
      if (video.elt.textTracks.length>0) {
        let i=0;
        for(let t of video.elt.textTracks) {
          if(t.mode == "showing" && i==video.elt.textTracks.length-1) {
            for(let c of t.cues) {
              let obj = {};
              obj.Start = c.startTime;
              obj.End = c.endTime;
              obj.Text = c.text;
              obj.Speaker = "None";
              obj.color = 'black';
              this.partitions_saved.push(obj);
              this.updatePartitionSaved(obj);
              this.setDivSizePos();
              this.curr_start = c.endTime;
            }
          }
          i++;
        }
      }
      this.setDivSizePos();
    }
    if(!annotation_editor.is_partition_editor)
      this.div_partition_saved.hide();
    if(this.partitions_scenes.length==0)
      this.partitions_scenes.push({'Name':'TheSubtitles','PartArray':this.partitions_saved});
  }

  /**
   * [loadPartition => load a playscript]
   * @param  {[object]} data_part               [playscrit data]
   */
  this.loadPartition = function(data_part) {
    let name = username;
    let data_partitions = data_part;
    if(data_part.Name) {
      name = data_partitions.Name;
      data_partitions = data_part.Data;
    }
    if(data_partitions) {
      for(let p_json of Object.values(data_partitions)) {
        let part = {};
        part.UserName = name;
        part.Name = p_json.Name;
        part.SceneUser = p_json.SceneUser;
        part.PartArray = [];
        for(let p of p_json.Array) {
          let obj = {};
          obj.Start = p.start;
          obj.End = p.end;
          obj.Text = p.text;
          obj.Speaker = p.speaker;
          obj.color = p.color;
          part.PartArray.push(obj);
          if(part.Name == 'TheSubtitles') {
            annotation_editor.partition_editor.partitions_saved.push(obj);
            annotation_editor.partition_editor.updatePartitionSaved(obj);
            annotation_editor.partition_editor.setDivSizePos();
            annotation_editor.partition_editor.curr_start = p.end;
          }
        }
        annotation_editor.partition_editor.partitions_scenes.push(part);
      }
      annotation_editor.partition_editor.setDivSizePos();
    }
    if(!annotation_editor.is_partition_editor)
      annotation_editor.partition_editor.div_partition_saved.hide();

    if(annotation_editor.partition_editor.partitions_scenes.length==0) {
      annotation_editor.partition_editor.partitions_scenes.push({'Name':'TheSubtitles','PartArray':annotation_editor.partition_editor.partitions_saved});
    }
    annotation_editor.partition_editor.loadSubtitle();
  }

  /**
   * [changeText => change a text note]
   */
  function changeText() {
    for(let p of annotation_editor.partition_editor.partitions_saved) {
      if(p.TextElt.elt === this) {
        p.Text = annotation_editor.partition_editor.parseActionText(this.innerHTML);
        if(p.cue)
          p.cue.text = p.Text+'\n';
        annotation_editor.partition_editor.setDivSizePos();
      }
    }
  }

  /**
   * [parseTextAction => convert action to text in italic]
   * @param  {[string]} text               [text action]
   */
  this.parseTextAction = function(text) {
    return text.replace(/</g,'<i').replace(/>/g,'</i>').replace(/<i/g,'<i>');
  }

  /**
   * [parseActionText => convert action to text in italic]
   * @param  {[string]} text               [text action]
   */
  this.parseActionText = function(text) {
    return text.replace(/&nbsp;/g,' ').replace(/<br>/g,'\n').replace(/<div>/g,'\n').replace(/<\/div>/g,'').replace(/<i>/g,'<').replace(/<\/i>/g,'>').replace(/&lt;/g,'<').replace(/&gt;/g,'>');
  }

  /**
   * [updatePartitionSaved => add a new note and update partitions list]
   * @param  {[object]}  part                         [partition object]
   * @param  {Boolean} [remove=false]               [is it remove or add]
   */
  this.updatePartitionSaved = function(part, remove = false) {
    let h3 = createElement('p',this.timeToString(part.Start)+'  ==>  '+this.timeToString(part.End));
    let div_speaker = createDiv();
    let color_picker = createColorPicker();
    color_picker.input(setColor);
    let select_speaker = createSelect();
    select_speaker.option('None');
    for(let a of preparation_editor.actors_timeline) {
      select_speaker.option(a.actor_name);
    }
    select_speaker.elt.onclick = setSpeaker;
    select_speaker.elt.className = 'speaker_part_select';
    if(part.Speaker && part.Speaker!='None') {
      select_speaker.elt.style.color =  preparation_editor.getAct(part.Speaker).color;
      color_picker.elt.value = preparation_editor.getAct(part.Speaker).color;
      select_speaker.elt.value = part.Speaker;
    } else {
      select_speaker.elt.style.color = 'black';
      select_speaker.elt.value = 'None';
    }
    div_speaker.child(select_speaker);
    div_speaker.child(color_picker);
    let p_text = createP(part.Text);
    p_text.elt.innerHTML = this.parseTextAction(part.Text).replace(/\n/g,'<br>');
    if(this.select_user.value() == username) {
      p_text.elt.contentEditable = true;
    }
    p_text.elt.onchange = changeText;
    part.TimeElt = h3;
    part.SpeakerElt = div_speaker;
    part.TextElt = p_text;
    let b = false;
    if(!remove) {
      for(let p of this.partitions_saved) {
        if(part.TimeElt != p.TimeElt && part.End < p.Start) {
          this.div_partition_saved.elt.insertBefore(p_text.elt, p.TimeElt.elt);
          this.div_partition_saved.elt.insertBefore(div_speaker.elt, p_text.elt);
          this.div_partition_saved.elt.insertBefore(h3.elt, div_speaker.elt);
          b = true;
          break;
        }
      }
    }
    if(!b) {
      this.div_partition_saved.child(h3);
      this.div_partition_saved.child(div_speaker);
      this.div_partition_saved.child(p_text);
    }
  }

  /**
   * [getPartition => get a playscript from a playscript name]
   * @param  {[string]} name               [partition name]
   * @return {[list]}      [return a partition list]
   */
  this.getPartition = function(name) {
    for(let p of this.partitions_scenes) {
      if(p.SceneUser == shots_timeline.select_user.value() && p.UserName == this.select_user.value() && p.Name == name) {
        let tab = [];
        for(let p1 of p.PartArray) {
          tab.push({'start':round_prec(Math.min(video.duration(),p1.Start + shots_timeline.start_frame/frame_rate)),
           'end':round_prec(Math.min(video.duration(),p1.End + shots_timeline.start_frame/frame_rate)), 'text':p1.Text});
        }
        return tab;
      }
    }
    return
  }

  /**
   * [getCurrentPartition => get the current partition list]
   * @return {[list]} [current partition list]
   */
  this.getCurrentPartition = function() {
    let tab = [];
    for(let p of this.partitions_saved) {
      if(this.button_stage_scene_script.elt.innerText == 'stage script') {
        tab.push({'start':round_prec(p.Start),'end':round_prec(p.End), 'text':p.Text});
      } else {
        tab.push({'start':round_prec(Math.min(video.duration(),p.Start + shots_timeline.start_frame/frame_rate)),
             'end':round_prec(Math.min(video.duration(),p.End + shots_timeline.start_frame/frame_rate)), 'text':p.Text});
      }
    }
    return tab;
  }

  /**
   * [displayParts => display the playscript next to the player]
   */
  this.displayParts = function() {
    let s,e;
    for(let p of this.partitions_saved) {
      // console.log(p);

      let unit = player.w/this.duration;
      if(p.End >= player.first_time && p.Start <= player.last_time) {
        s = Math.max(p.Start - player.first_time,0);
        if(this.is_scene) {
          s =  Math.min(video.duration(),s + shots_timeline.start_frame/frame_rate);
        }
        e = Math.min(p.End - player.first_time,this.duration);
        if(this.is_scene) {
          e =  Math.min(video.duration(),e + shots_timeline.start_frame/frame_rate);
        }
        push();
        if(!p.color) {
          p.color = '#2E5C9C';
        }
        fill(p.color);
        if(p.on || video.time() < e && s < video.time()) {
          fill('rgb(170,56,35)');
        }
        stroke(220);
        rect(player.x + s*unit,shots_timeline.y,e*unit-s*unit,100);
        if(player.x + s*unit<mouseX && mouseX<player.x + s*unit+(e*unit-s*unit) && viewer_height+80<mouseY) {
          // Display a helping message
          push();
          fill(255);
          rect(0, height-20,windowWidth,20);
          fill('black');
          noStroke();
          textSize(16);
          if(!p.on) {
            text('Click to select',0,height-5);
          } else {
            text('Press del or backspace to remove',0,height-5);
          }
          pop();
        }
        p.x = player.x + s*unit;
        p.w = e*unit-s*unit;
        pop();
      }
      if(video.time() < e && s < video.time()) {
        p.TextElt.style('color','red');
        if(playing) {
           this.div_partition_saved.elt.scrollTop = p.TextElt.position().y - (this.div_partition_saved.height / 2);
        }
      } else {
        p.TextElt.style('color',p.color);
      }
    }
  }

  /**
   * [timeToString => convert time in seconds to string format 'min:sec.mil']
   * @param  {[float]} time               [time in seconds]
   * @return {[string]}      [time string]
   */
  this.timeToString = function(time) {
    let min = toTwoDigit(Math.floor(time/60).toString());
    let sec = toTwoDigit(Math.floor(time%60).toString());
    let mil = toTwoDigit(round_prec((time%1)*100,0).toString());
    return min +':'+sec+'.'+mil;
  }

  /**
   * [display => function called in the main p5 draw function]
   */
  this.display = function() {
    let unit = shots_timeline.w/player.total_frame;
    let start_frame_x = Math.max((shots_timeline.start_frame-player.first)*unit,0);
    let end_frame_x = Math.min((shots_timeline.end_frame-player.first)*unit,shots_timeline.w);

    this.duration = (player.total_frame/frame_rate);
    let new_h = height-(this.textarea_current_partition.height*1.5);
    if(this.is_scene) {
      this.curr_end =  Math.max(0,video.time() - shots_timeline.start_frame/frame_rate);
    } else {
      this.curr_end = video.time();
    }
    push();
    fill(150);
    rect(0,viewer_height,mid_width,windowHeight-viewer_height);
    pop();

    push();
    fill(0);
    textSize(18);
    text('Notebook title: ',player.x,viewer_height+190);
    pop();

    // Display a helping message
    push();
    fill(255);
    rect(0, height-20,windowWidth,20);
    fill('black');
    noStroke();
    textSize(16);
    text('Ctrl + space play/pause, Ctrl + enter save sub, Ctrl + L play last, Ctrl + M play next, Ctrl + R repeat last pause, Ctrl + <-/-> move video time',0,height-5);
    pop();


    push();
    this.off_y = this.textarea_current_partition.y-10-can.elt.offsetTop;
    fill(255);
    rect(player.x,shots_timeline.y,player.w,100);
    fill('grey');
    rect(player.x,shots_timeline.y,player.nav_bar.cursor-player.x,100);
    fill(0);
    textSize(18);
    text('Start : '+this.timeToString(this.curr_start),mid_width+10,this.off_y);
    text('End : '+this.timeToString(this.curr_end),mid_width+200,this.off_y);
    if(mid_width+10<mouseX && mouseX < mid_width+160 && this.off_y-20 < mouseY && mouseY < this.off_y+20) {
      fill(255);
      rect(0, height-20,windowWidth,20);
      fill('black');
      noStroke();
      textSize(16);
      text('Shift + click on start set start to the current time',0,height-5);
    }
    if(mid_width+10<mouseX && mouseX < mid_width+this.textarea_current_partition.width && this.off_y < mouseY && mouseY < this.off_y+this.textarea_current_partition.height) {
      fill(255);
      rect(0, height-20,windowWidth,20);
      fill('black');
      noStroke();
      textSize(16);
      text('Ctrl + 1,2,3... select actor, <> open close action, Ctrl + enter save sub',0,height-5);
    }

    pop();
    this.displayParts();

    if(this.is_scene) {
      push();
      fill(80);
      rect(shots_timeline.x,shots_timeline.y,start_frame_x,shots_timeline.h);
      rect(shots_timeline.x+end_frame_x,shots_timeline.y,((shots_timeline.x+shots_timeline.w)-end_frame_x)-shots_timeline.x,shots_timeline.h);
      pop();
    }

    if(this.is_export_launched) {
      push();
      textSize(18);
      text('The export is in progress..',shots_timeline.x,viewer_height+220);
      pop();
    }

  }
}
