/*
This file is part of KinoAi project.
It is subject to the license terms in the LICENSE file found in the top-level directory
*/

function StateButton(tempX, tempY, tempW, tempH, name, c, c_on)  {
  this.x  = tempX;
  this.y  = tempY;
  this.w = tempW;
  this.h = tempH;

  this.prev_x = tempX;
  this.prev_y = tempY;
  // Is the button on or off?
  // Button always starts as off
  this.on = false;

  this.name = name;

  this.color = c;

  this.color_on = c_on;

  this.in_drag = false;

  this.first_frame;
  this.last_frame;

  this.drag = function(mx, my) {
    // Check to see if a point is inside the rectangle
    let b = false;
    for(let t of preparation_editor.tracklets_line) {
      if(t.drag) {
        b = true;
        break;
      }
    }
    if (!b && mx > this.x && mx < this.x + this.w*2 && my > this.y && my < this.y + this.h*2) {
      this.in_drag = true;
      this.x = mx;
      this.y = my;
    }
    if(this.in_drag) {
      this.x = mx;
      this.y = my;
    }
  };

  this.drop = function() {
    this.in_drag = false;
    this.x = this.prev_x;
    this.y = this.prev_y;
  }

  this.setPosition = function(tx, ty) {
    this.x = tx;
    this.y = ty;
  }

  this.setColor = function(c) {
    this.color = c;
  }

  this.setType = function(t) {
    this.type = t;
  }

  // Draw the rectangle
  this.display = function() {
    if(this.in_drag) {
      push();
      fill(this.color);
      rect(mouseX,mouseY,this.w,this.h);
      pop();
    } else {
      push();
      fill('black');
      textSize(15);
      text(this.name,this.x,this.y);
      fill(this.color);
      rect(this.x,this.y+5,this.w,this.h);
      pop();
    }
  }
}
