/*
This file is part of KinoAi project.
It is subject to the license terms in the LICENSE file found in the top-level directory
*/

/**
 * [CadrageEditor => framing tab editor]
 * @constructor
 */
function CadrageEditor()  {

  this.is_shot_creation = false;
  this.is_shot_personalize = false;
  this.is_shots_frame_layout =false;
  this.is_keep_out = false;
  this.is_pull_in = false;
  this.is_stage_position = false;
  this.is_gaze_direction = false;
  this.shot_type='BCU';
  this.ratio_type = 1;
  this.add_shot = [];

  this.bbox_perso = [];
  this.top_left_perso;
  this.bottom_right_perso;

  this.shot_creation = createCheckbox('Create rush', false);
  this.shot_creation.side = false;
  this.shot_creation.mouseOver(processToolTip('Open the creation interface'));
  this.shot_creation.mouseOut(processToolTip(''));
  html_elements.push(this.shot_creation);
  this.shot_creation.position(windowWidth/2 + 10, 10);
  this.shot_creation.size(150,20);
  this.shot_creation.changed(updateShotCreate);

  this.shot_personalize = createCheckbox('Personalize', false);
  this.shot_personalize.side = false;
  this.shot_personalize.mouseOver(processToolTip('Create personalized rush'));
  this.shot_personalize.mouseOut(processToolTip(''));
  html_elements.push(this.shot_personalize);
  this.shot_personalize.position(windowWidth/2 + 10, 10);
  this.shot_personalize.size(150,20);
  this.shot_personalize.changed(updateShotPerso);

  this.title_perso = createElement('h3','rush perso');
  this.title_perso.elt.contentEditable = true;
  this.title_perso.side = false;
  html_elements.push(this.title_perso);
  this.title_perso.style('margin',0);
  this.title_perso.style('font-size',18);

  this.save_shot = createButton('Create');
  this.save_shot.side = false;
  this.save_shot.mouseOver(processToolTip('Launch the stabilization process of the rush'));
  this.save_shot.mouseOut(processToolTip(''));
  html_elements.push(this.save_shot);
  this.save_shot.position(windowWidth/2 + 200, 40);
  this.save_shot.mousePressed(saveShot);

  this.check_render_shot_trace = createCheckbox('Render shots frame', false);
  this.check_render_shot_trace.mouseOver(processToolTip('Show bounding box of the created shots'));
  this.check_render_shot_trace.mouseOut(processToolTip(''));
  html_elements.push(this.check_render_shot_trace);
  this.check_render_shot_trace.changed(updateDrawShotTrace);

  this.create_note_rushes = createButton('Create rushes for annotation');
  this.create_note_rushes.mouseOver(processToolTip('Create the rushes for the annotation'));
  this.create_note_rushes.mouseOut(processToolTip(''));
  html_elements.push(this.create_note_rushes);
  this.create_note_rushes.mousePressed(createAllNotebookRushes);

  this.shot_selector = createSelect();
  this.shot_selector.side = false;
  this.shot_selector.mouseOver(processToolTip('Select the rush size'));
  this.shot_selector.mouseOut(processToolTip(''));
  html_elements.push(this.shot_selector);
  this.shot_selector.position(windowWidth/2 + 10, 40);
  this.shot_selector.option('BCU');
  this.shot_selector.option('CU');
  this.shot_selector.option('MCU');
  this.shot_selector.option('MS');
  this.shot_selector.option('MLS');
  this.shot_selector.option('FS');
  this.shot_selector.changed(selectShotType);

  this.ratio_selector = createSelect();
  this.ratio_selector.side = false;
  this.ratio_selector.mouseOver(processToolTip('Select the rush aspect ratio'));
  this.ratio_selector.mouseOut(processToolTip(''));
  html_elements.push(this.ratio_selector);
  this.ratio_selector.position(windowWidth/2 + 120, 40);
  this.ratio_selector.option("original");
  this.ratio_selector.option("half width");
  this.ratio_selector.option("twice width");
  this.ratio_selector.option("4:3");
  this.ratio_selector.option("16:9");
  this.ratio_selector.option("4k - 2k");
  this.ratio_selector.option("1");
  this.ratio_selector.option("2");
  this.ratio_selector.option("3");
  this.ratio_selector.option("4");
  this.ratio_selector.changed(selectRatio);

  this.keep_out = createCheckbox('Keep out', false);
  this.keep_out.side = false;
  this.keep_out.mouseOver(processToolTip('Keep out of the frame the others actors'));
  this.keep_out.mouseOut(processToolTip(''));
  html_elements.push(this.keep_out);
  this.keep_out.position(windowWidth/2 + 5, 70);
  this.keep_out.changed(updateShotKeepOut);

  this.pull_in = createCheckbox('Pull in', false);
  this.pull_in.side = false;
  this.pull_in.mouseOver(processToolTip('Pull into the frame the actors'));
  this.pull_in.mouseOut(processToolTip(''));
  html_elements.push(this.pull_in);
  this.pull_in.position(windowWidth/2 + 5, 140);
  this.pull_in.changed(updateShotPullIn);

  this.stage_position = createCheckbox('Stage position', false);
  this.stage_position.side = false;
  this.stage_position.mouseOver(processToolTip('Keep the stage position'));
  this.stage_position.mouseOut(processToolTip(''));
  html_elements.push(this.stage_position);
  this.stage_position.position(windowWidth/2 + 140, 70);
  this.stage_position.changed(updateShotStagePosition);

  this.gaze_direction = createCheckbox('Gaze direction', false);
  this.gaze_direction.side = false;
  this.gaze_direction.mouseOver(processToolTip('Use the gaze direction'));
  this.gaze_direction.mouseOut(processToolTip(''));
  html_elements.push(this.gaze_direction);
  this.gaze_direction.position(windowWidth/2 + 315, 70);
  this.gaze_direction.changed(updateShotGazeDirection);

  /**
   * [mousePressed => handle mouse press event for the framing tab]
   * @param  {[float]} mx               [mouse x position]
   * @param  {[float]} my               [mouse y position]
   */
  this.mousePressed = function(mx, my) {
    if(this.is_shot_personalize) {
      let x = mouseX-x_off;
      let y = mouseY-y_off;
      x /=(vid_h/Number(original_height));
      y /=(vid_h/Number(original_height));
      x = Math.round(x);
      y = Math.round(y);
      if(!this.top_left_perso) {
        this.top_left_perso = {'x':x, 'y':y};
      } else if(this.bottom_right_perso) {
         if( mouseButton === CENTER ) {
           this.top_left_perso = undefined;
           this.off_x = undefined;
           this.off_y = undefined;
         } else {
           if(this.top_left_perso.x<x &&x<this.bottom_right_perso.x && this.top_left_perso.y<y && y<this.bottom_right_perso.y ) {
             this.off_x = x-this.top_left_perso.x;
             this.off_y = y-this.top_left_perso.y;
           }
         }
      }
    }
    for(let act of preparation_editor.actors_timeline) {
      act.click(mouseX, mouseY);
    }
    for(let s of montage_editor.shots) {
      s.on = false;
      s.click(mouseX, mouseY);
    }
  };

  /**
   * [drop => handle drop event for the framing tab]
   * @param  {[float]} mx               [mouse x position]
   * @param  {[float]} my               [mouse y position]
   */
  this.drop = function(mx, my) {
    this.is_drag = false;
  }

  /**
   * [drag => handle drag event for the framing tab]
   * @param  {[float]} mx               [mouse x position]
   * @param  {[float]} my               [mouse y position]
   */
  this.drag = function(mx, my) {
    this.is_drag = true;
    let x = mouseX-x_off;
    let y = mouseY-y_off;
    x /=(vid_h/Number(original_height));
    y /=(vid_h/Number(original_height));
    x = Math.round(x);
    y = Math.round(y);
    if(this.is_shot_personalize && this.top_left_perso && (!this.off_x || keyIsPressed )) {
      x = Math.min(x, Number(original_width));
      y = this.top_left_perso.y+(x-this.top_left_perso.x)/this.ratio_type;
      this.bottom_right_perso = {'x':x, 'y':y};
    } else if(this.is_shot_personalize && this.top_left_perso && this.bottom_right_perso) {
      if(this.off_x && this.off_y) {
        let w = this.bottom_right_perso.x - this.top_left_perso.x;
        let h = this.bottom_right_perso.y - this.top_left_perso.y;
        this.top_left_perso.x = Math.min(Math.max(0,x-this.off_x),Number(original_width)-w);
        this.top_left_perso.y = Math.min(Math.max(0,y-this.off_y),Number(original_height)-h);
        this.bottom_right_perso.x = this.top_left_perso.x +w;
        this.bottom_right_perso.y = this.top_left_perso.y +h;
      }
    }
  }

  /**
   * [keyPressed => handle keyPressed event for the framing tab]
   * @param  {[keycode]} keyCode               [key code event]
   */
  this.keyPressed = function(keyCode) {
    // del key or backspace
    if(keyCode===46 || keyCode === 8) {
      montage_editor.removeShot();
    }
    // n key
    if(keyCode === 78) {
      if(!(document.activeElement.contentEditable=='true')) {
        if(this.bottom_right_perso) {
          this.top_left_perso = undefined;
          this.off_x = undefined;
          this.off_y = undefined;
        }
      }
    }
  }

  /**
   * [mouseWheel => handle mouseWheel event for the framing tab]
   * @param  {[event]} event               [mouse wheel javascript event]
   */
  this.mouseWheel = function(event) {
  }

  /**
   * [updateAndShow => reset all tab elements and show framing elements]
   */
  this.updateAndShow = function() {
    resetTabs();
    is_cadrage_editor = true;

    clickTab(cadrage_editor_button);
    act_input.hide();
    this.showElts();
    // Side el
    check_render_pose.show();
    submit.show();
    reset_pos.show();
    reload_button.show();
    hide_show_header_button.show();
    documentation_button.show();
    updateSideElems();
    up_rough = true;
  }

  /**
   * [resizeElt => resize all framing tab elements]
   */
  this.resizeElt = function() {
    this.shot_creation.original_x = mid_width + 10;
    this.shot_personalize.original_x = mid_width + 170;
    this.shot_creation.original_x = mid_width + 10;
    this.shot_selector.original_x = mid_width + 10;
    this.shot_selector.original_y = 50;
    this.ratio_selector.original_x = mid_width + 80;
    this.ratio_selector.original_y = 50;
    this.save_shot.original_x = mid_width + 220;
    this.save_shot.original_y = 50;
    this.title_perso.original_x = mid_width + 10;
    this.title_perso.original_y = 80;
    this.keep_out.original_x = mid_width + 10;
    this.keep_out.original_y = 80;
    this.pull_in.original_x = mid_width+180;
    this.pull_in.original_y = 80;
    this.stage_position.original_x = mid_width + 180;
    this.stage_position.original_y = 110;
    this.gaze_direction.original_x = mid_width + 10;
    this.gaze_direction.original_y = 110;
    montage_editor.all_types.original_x = mid_width + 220;
    montage_editor.all_types.original_y = 50;
    let div_creation = select('#div_creation');
    div_creation.position(mid_width,this.shot_creation.y+135);
    div_creation.size(((windowWidth - 160)-mid_width)-10);

    this.resizeDivWrapRushes();

  }

  /**
   * [resizeDivWrapRushes => resize the div that contain the shots in the canvas]
   */
  this.resizeDivWrapRushes = function() {
    montage_editor.top_shot = this.gaze_direction.original_y + 60;
    if(this.is_shot_creation) {
      let w = (windowWidth-180-mid_width);
      let h = Math.floor(w/this.ratio_type);
      if(h>height/3) {
        h = height/3;
        w = Math.floor(h*this.ratio_type);
      }
      montage_editor.top_shot += h+10;
    }
    let div_wrap_rushes = select('#div_wrap_rushes');
    if(div_wrap_rushes) {
      div_wrap_rushes.size(windowWidth-160-mid_width, height-montage_editor.top_shot);
      div_wrap_rushes.position(mid_width,can.elt.offsetTop+montage_editor.top_shot);
    }
  }

  /**
   * [hideElts => hide all elements]
   */
  this.hideElts = function() {
    this.shot_creation.checked(false);
    this.is_shot_creation = false;
    this.updateShotCreate();
    this.is_shots_frame_layout =false;
    this.check_render_shot_trace.checked(false);
    this.check_render_shot_trace.hide();
    this.title_perso.hide();
    this.shot_creation.hide();
    this.shot_personalize.hide();
    this.shot_personalize.checked(false);
    this.is_shot_personalize = false;
    this.updatePerso(false);
    this.save_shot.hide();
    this.ratio_selector.hide();
    this.stage_position.hide();
    this.gaze_direction.hide();
    this.shot_selector.hide();
    this.create_note_rushes.hide();
    this.keep_out.checked(false);
    this.is_keep_out = false;
    this.keep_out.hide();
    this.pull_in.checked(false);
    this.is_pull_in = false;
    this.pull_in.hide();
    if(select('#div_creation'))
      $('#div_creation').remove();
    let div_wrap_rushes = select('#div_wrap_rushes');
    if(div_wrap_rushes) {
      div_wrap_rushes.hide();
    }
  }

  /**
   * [showElts => show all elements]
   */
  this.showElts = function() {
    this.shot_creation.show();
    this.shot_personalize.show();
    this.check_render_shot_trace.show();
    this.create_note_rushes.show();
    this.shot_creation.checked(true);
    this.is_shot_creation = true;
    this.updateShotCreate();
    let div_wrap_rushes = select('#div_wrap_rushes');
    if(div_wrap_rushes) {
      div_wrap_rushes.show();
    }
    shots_timeline.updateSelectedShot();
  }

  /**
   * [showCreationElt => show the elements to create new a shot]
   */
  this.showCreationElt = function() {
    selectRatio();
    this.shot_selector.show();
    this.save_shot.show();
    this.ratio_selector.show();
    this.keep_out.show();
    this.pull_in.show();
    this.stage_position.show();
    this.gaze_direction.show();
  }


  function updateDrawShotTrace() {
    cadrage_editor.is_shots_frame_layout = this.checked();
  }

  function updateShotCreate() {
    cadrage_editor.is_shot_creation = this.checked();
    cadrage_editor.updateShotCreate();
  }

  function updateShotPerso() {
    cadrage_editor.is_shot_personalize = this.checked();
    cadrage_editor.updatePerso(this.checked());
    if(this.checked()) {
      select('#div_creation').hide();
    } else {
      select('#div_creation').show();
    }
  }

  /**
   * [updatePerso => allowed the user to create a personalize framing]
   * @param  {[Boolean]} checked               [is the chack box checked]
   */
  this.updatePerso = function(checked) {
    this.bottom_right_perso = undefined;
    this.top_left_perso = undefined;
    this.off_x = undefined;
    this.off_y = undefined;
    if(checked) {
      this.keep_out.hide();
      this.pull_in.hide();
      this.stage_position.hide();
      this.gaze_direction.hide();

      this.title_perso.show();
    } else {
      this.keep_out.show();
      this.pull_in.show();
      this.stage_position.show();
      this.gaze_direction.show();
      this.title_perso.hide();
    }
  }

  this.updateShotCreate = function() {
    if(!this.is_shot_creation) {
      this.save_shot.hide();
      // for(let a of preparation_editor.actors_timeline) {
      //   a.elem.remove();
      //   a.elem = createElement('h3', a.actor_name);
      //   a.elem.elt.contentEditable = 'true';
      //   a.elem.id('editor');
      //   preparation_editor.div_actors_timeline.child(a.elem);
      // }
    } else {
      this.showCreationElt();
      let div_creation = select('#div_creation');
      if(!div_creation) {
        div_creation = createDiv();
      }
      div_creation.id('div_creation');
      div_creation.position(mid_width,this.shot_creation.y+100);
      // div_creation.style('display','table');
      div_creation.size(((windowWidth - 160)-mid_width)-10);
      for(let a of preparation_editor.actors_timeline) {
        a.on = false;
        a.elem.style('float','left');
        a.elem.style('margin','0 5');
        a.elem.elt.contentEditable = false;
        div_creation.child(a.elem);
      }
    }
  }

  /**
   * [saveShot => Get the specification and launch the stabilization of the shot created by the user]
   */
  function saveShot() {

    if(cadrage_editor.is_shot_personalize) {
      if(cadrage_editor.bbox_perso) {
        let shot = new Shot();
        shot.aspect_ratio = cadrage_editor.ratio_type;
        shot.start_frame = 0;
        shot.end_frame = Math.round(frame_rate*video.duration());
        shot.type = cadrage_editor.title_perso.elt.innerText;
        shot.bbox_perso = cadrage_editor.bbox_perso;
        shot.is_personalize = true;
        let b = false;
        for(let s of montage_editor.shots) {
          if(s.equalTo(shot, false)) {
            b = true;
            break;
          }
        }
        if(!b) {
          montage_editor.shots.push(shot);
          $.post({
            url: "save_shot_perso",
            data: {'abs_path': abs_path, 'aspect_ratio':shot.aspect_ratio, 'bbox_perso':JSON.stringify(shot.bbox_perso), 'is_personalize':true, 'type':shot.type, 'end_frame':shot.end_frame},
            dataType: 'json',
            success: function (data) {
            }
          });
        } else {
          alert('already created');
        }

        return;
      }
    }

    var shot = new Shot();

    for(let act of preparation_editor.actors_timeline) {
      if(act.on) {
        shot.actors_involved.push(act);
      }
    }

    // console.log(aspect_ratio/ratio_type);
    shot.aspect_ratio = cadrage_editor.ratio_type;
    shot.start_frame = 0;
    shot.end_frame = Math.round(frame_rate*video.duration());

    shot.is_keep_out = cadrage_editor.is_keep_out;
    shot.is_pull_in = cadrage_editor.is_pull_in;
    shot.is_stage_position = cadrage_editor.is_stage_position;
    shot.is_gaze_direction = cadrage_editor.is_gaze_direction;
    shot.type = cadrage_editor.shot_type;
    if(shot.actors_involved.length==0) {
      shot.is_keep_out = false;
      shot.is_pull_in = false;
      shot.is_stage_position = false;
      shot.is_gaze_direction = false;
      shot.type = 'WS';
    }

    let b = false;
    for(let s of montage_editor.shots) {
      if(s.equalTo(shot, false)) {
        b = true;
        break;
      }
    }

    if(!b) {
      shot.calcBboxes(aspect_ratio);

      montage_editor.shots.push(shot);

      if(shot.actors_involved.length>=1) {
        cadrage_editor.add_shot.push(shot);
        shot.in_stabilize = true;
      }
      montage_editor.shots.sort(sortShotsByName);
      montage_editor.shots.sort(sortShotsByType);
      montage_editor.shots.sort(sortShotsByStabilize);

      // for(let a of preparation_editor.actors_timeline) {
      //   a.elem.remove();
      //   a.elem = createElement('h3', a.actor_name);
      //   a.elem.elt.contentEditable = 'true';
      //   a.elem.id('editor');
      //   preparation_editor.div_actors_timeline.child(a.elem);
      // }
      for(let act of preparation_editor.actors_timeline) {
        act.on = false;
        act.elem.style('color', 'rgb(0,0,0)');
      }
    } else {
      alert('already created');
    }

  }

  function createAllNotebookRushes() {
    cadrage_editor.createAllShots();
  }

  function updateShotKeepOut() {
    cadrage_editor.is_keep_out = this.checked();
  }

  function updateShotPullIn() {
    cadrage_editor.is_pull_in = this.checked();
  }

  function updateShotStagePosition() {
    cadrage_editor.is_stage_position = this.checked();
  }

  function updateShotGazeDirection() {
    cadrage_editor.is_gaze_direction = this.checked();
  }

  function selectShotType() {
    cadrage_editor.shot_type = cadrage_editor.shot_selector.value();
    if(is_montage_editor || cadrage_editor.is_show_shots) {
      montage_editor.all_types.checked(false);
      montage_editor.is_all_types = false;
    }
  }

  function selectRatio() {
    cadrage_editor.ratio_type = cadrage_editor.getAspectRatioFromSelect(cadrage_editor.ratio_selector.value());
    up_rough = true;
  }

  /**
   * [exploitRoughCut => Create all needed shots and assign them on the timeline based on the rough cut file specification]
   */
  this.exploitRoughCut = function() {
    shots_timeline.shots = [];
    var frames_no_info = [];
    this.createAllActorsFullShot();
    let len = rough_json.length;
    if(!len){len = Object.getOwnPropertyNames(rough_json).length;}
    for(let i=0; i<len; i++) {
      let r = rough_json[i];
      if(r.ActInvolved.length<1) {
        for(let a of preparation_editor.actors_timeline) {
          r.ActInvolved.push(a.actor_name);
        }
        r.Type = 'FS';
      } else {
        let test_name = [];
        for(let i=0; i<r.ActInvolved.length; i++) {
          if(test_name.includes(r.ActInvolved[i])) {
            r.ActInvolved.splice(i,1);
          } else {
            test_name.push(r.ActInvolved[i]);
          }
        }
      }
      if(!montage_editor.testShot(r)) {
        let s = new Shot();
        for(let a of r.ActInvolved) {
          s.actors_involved.push(preparation_editor.getAct(a));
        }
        // console.log(s.actors_involved.length);
        s.type = r.Type;

        s.start_frame = 0;
        s.end_frame = Math.round(frame_rate*video.duration());

        s.aspect_ratio = aspect_ratio;

        s.calcBboxes(aspect_ratio);

        montage_editor.shots.push(s);
        // shots_timeline.addShotJson(s);
        this.add_shot.push(s);

        s.in_stabilize = true;

      } else {
        let ind = montage_editor.getShot(montage_editor.shots, r.Type, r.ActInvolved);
        if(this.add_shot.length < 1){
          let indexes = [];
          for(let i=0; i<r.EndFrame-r.StartFrame; i++) {
            let num = r.StartFrame + i;

            let acts = preparation_editor.getActOnStage(num);
            var b = true;
            for(let name of r.ActInvolved) {
              if(acts.includes(name)) {
                b = false;
                break;
              }
            }
            if(b) {
              indexes.push(num);
            }
          }
          let nb=1;
          for(let i=0; i < indexes.length; i++) {
            if(i>=1 && indexes[i-1] != indexes[i]-1) {
              nb++;
            }
          }
          if(indexes.length>1) {
            if(indexes.length==r.EndFrame - r.StartFrame) {
              console.log('oui');
              if(nb==1) {
                console.log(r.StartFrame, r.EndFrame);
                frames_no_info.push(indexes);
              }
            }else if(nb == 1 && indexes.length>frame_rate) {
              if(indexes[indexes.length-1] == r.EndFrame-1) {
                console.log('end');
                r.EndFrame = indexes[0];
              }
              if(indexes[0] == r.StartFrame) {
                console.log('start');
                r.StartFrame = indexes[0];
              }
            }
          }
          if(indexes.length!=r.EndFrame - r.StartFrame) {
            let s_stab = {};//new Shot();
            s_stab.actors_involved = montage_editor.shots[ind].actors_involved;
            // s_stab.bboxes = shots[ind].bboxes;
            s_stab.type = montage_editor.shots[ind].type;
            s_stab.start_frame = r.StartFrame;
            s_stab.end_frame = r.EndFrame;
            s_stab.aspect_ratio = montage_editor.shots[ind].aspect_ratio;
            shots_timeline.addShotJson(s_stab);
          }
        }
      }
    }
    if(this.add_shot.length < 1){
      shots_timeline.fillRough(frames_no_info);
    }
    montage_editor.shots.sort(sortShotsByName);
    montage_editor.shots.sort(sortShotsByType);
  }

  /**
   * [createAllShots => Create 3 shots for each actor (one close up, one medium and one full shot)]
   */
  this.createAllShots = function() {
    this.is_keep_out = true;
    this.keep_out.checked(true);
    this.is_pull_in = true;
    this.pull_in.checked(true);
    this.is_gaze_direction = false;
    this.gaze_direction.checked(false);
    this.is_stage_position = false;
    this.stage_position.checked(false);
    for(let s_t of ['CU', 'MS', 'FS']) {
      for(let act of preparation_editor.actors_timeline) {
        for(let act of preparation_editor.actors_timeline) {
          act.on = false;
        }
        let shot = new Shot();
        act.on = true;
        shot.actors_involved.push(act);
        shot.type = s_t;
        shot.aspect_ratio = aspect_ratio;
        shot.is_keep_out = true;
        shot.is_pull_in = true;
        shot.is_stage_position = false;
        shot.is_gaze_direction = false;
        let b = false;
        for(let s of montage_editor.shots) {
          if(s.equalTo(shot, false)) {
            b = true;
            break;
          }
        }
        if(!b) {
          shot.start_frame = 0;
          shot.end_frame = Math.round(frame_rate*video.duration());
          shot.calcBboxes(aspect_ratio);
          montage_editor.shots.push(shot);
          if(shot.actors_involved.length>=1) {
            this.add_shot.push(shot);
            shot.in_stabilize = true;
          }
        }
      }
    }
    montage_editor.shots.sort(sortShotsByName);
    montage_editor.shots.sort(sortShotsByType);
    for(let a of preparation_editor.actors_timeline) {
      a.on = false;
    }
  }

  /**
   * [getActInvolved => return a list of actors included in a shot bounding box]
   * @param  {[list]} bbox               [shot bounding box for the frame f_n (x_min, y_min, x_max, y_max)]
   * @param  {[integer]} f_n                [frame number]
   * @return {[list]}      [list of actors included in the bounding box]
   */
  this.getActInvolved = function(bbox, f_n) {
    if(!f_n) {
      f_n = frame_num;
    }
    let tab = [];
    if(bbox) {
      for(let a of preparation_editor.actors_timeline) {
        let center_act = a.getCenterAct(f_n);
        if(center_act.x > bbox[0] && center_act.x < bbox[2] && center_act.y > bbox[1] && center_act.y < bbox[3]) {
          tab.push(a);
        }
      }
    }
    return tab;
  }

  /**
   * [createAllActorsFullShot => Create a full shots with every actors included]
   */
  this.createAllActorsFullShot = function() {
    let r = {};
    r.Type = 'FS';
    let tab = [];
    for(let a of actors_timeline) {
      tab.push(a.actor_name);
    }
    r.ActInvolved = tab;
    if(!montage_editor.testShot(r)) {
      let s = new Shot();
      for(let a of r.ActInvolved) {
        s.actors_involved.push(preparation_editor.getAct(a));
      }
      s.type = r.Type;

      s.start_frame = 0;
      s.end_frame = Math.round(frame_rate*video.duration());

      s.aspect_ratio = aspect_ratio;
      console.log(s);

      s.calcBboxes(aspect_ratio);

      montage_editor.shots.push(s);
      this.add_shot.push(s);

      s.in_stabilize = true;

      montage_editor.shots.sort(sortShotsByName);
      montage_editor.shots.sort(sortShotsByType);
    }

  }

  /**
   * [getAspectRatioFromSelect => Get the selected aspect ratio]
   * @param  {[string]} sel               [string from the size selector]
   * @return {[float]}     [shot aspect ratio]
   */
  this.getAspectRatioFromSelect = function(sel) {
    let a_s = aspect_ratio;
    switch (sel){
      case 'original':
        a_s = aspect_ratio;
        break;
      case 'half width':
        a_s = aspect_ratio/2;
        break;
      case 'twice width':
        a_s = aspect_ratio*2;
        break;
      case '16:9':
        a_s = 16/9;
        break;
      case '4:3':
        a_s = 4/3;
        break;
      case '4k - 2k':
        a_s = 2048/1080;
        break;
      case '1':
        a_s = 1;
        break;
      case '2':
        a_s = 2;
        break;
      case '3':
        a_s = 3;
        break;
      case '4':
        a_s = 4;
        break;
      default:
        a_s = aspect_ratio;
        break;
    }
    return a_s;
  }

  /**
   * [drawCreationShot => Show the creation shot interface]
   */
  this.drawCreationShot = function() {
    let top_shot = Math.max(this.stage_position.position().y + 60 - can.elt.offsetTop,this.ratio_selector.position().y +60 - can.elt.offsetTop);
    let keypoints = frames_data[frame_num];
    let w = windowWidth-180-mid_width;
    let h = Math.floor(w/this.ratio_type);
    if(h>height/3) {
      h = height/3;
      w = Math.floor(h*this.ratio_type);
    }
    let arr = getBBoxShot(this.shot_type, this.ratio_type);
    if(this.is_shot_personalize) {
      if(this.top_left_perso && this.bottom_right_perso) {
        arr = [Math.max(this.top_left_perso.x,0),Math.max(this.top_left_perso.y,0),Math.min(this.bottom_right_perso.x,Number(original_width)),Math.min(this.bottom_right_perso.y,Number(original_height))];
      }
    }
    push();
    if(x_off<0){x_off=0;}
    if(y_off<0){y_off=0;}
    translate(x_off,y_off);
    scale(vid_h/Number(original_height));
    stroke('skyblue');
    noFill();
    rect(arr[0], arr[1], arr[2]-arr[0], arr[3]-arr[1]);
    if(!this.is_shot_personalize) {
      noStroke();
      fill(255);
      text(this.shot_type, arr[0]+5, arr[1]+10);
    }
    pop();
    var bbox = [];
    for(var j=0; j<arr.length; j++) {
      bbox.push((arr[j]*scale_ratio));
    }
    if(this.is_shot_personalize) {
      this.bbox_perso = arr;
    }
    if(img_hd) {
      let ratio = img_hd.width / video.elt.videoWidth;
      image(img_hd, (viewer_width+10), top_shot, w, h, bbox[0]*ratio, bbox[1]*ratio, bbox[2]*ratio - bbox[0]*ratio, bbox[3]*ratio - bbox[1]*ratio);
    } else {
      image(image_frame, (viewer_width+10), top_shot, w, h, bbox[0], bbox[1], bbox[2] - bbox[0], bbox[3] - bbox[1]);
    }
    push();
    fill(255);
    text('Curr '+round_prec(video.time(),2),viewer_width+10, top_shot+10);
    pop();
  }

  /**
   * [drawShotsLayout => Draw a colored rectangle on the player based on the shots specification (only when the actors involved in the shots are on stage)]
   */
  this.drawShotsLayout = function() {
    let shots_on_stage = montage_editor.getShotsOnStage();
    for(let s of shots_on_stage) {
      if(s.selected) {
        let bbox = s.getCurrStabShot(frame_num);
        if(bbox) {
          let type = s.getUpdatedSizeShot(s.getCurrStabShot(frame_num)[3]);
          if(!type) {
            type = s.type;
          }
          push();
          strokeWeight(1);
          switch(s.type){
            case 'CU':
              stroke('green');
              break;
            case 'MCU':
              stroke('orange');
              break;
            case 'MS':
              stroke('red');
              break;
            case 'MLS':
              stroke('purple');
              break;
            case 'FS':
              stroke('blue');
              break;
            default:
              stroke('white');
              break;
          }
          noFill();
          rect(bbox[0], bbox[1], bbox[2]-bbox[0], bbox[3]-bbox[1]);
          noStroke();
          fill(255);
          // text(s.type, bbox[0]+5, bbox[1]+10);
          // text(s.type, bbox[2]-20, bbox[1]+10);
          pop();
        }
      }
    }
  }

  /**
   * [display => function called in the main p5 draw function]
   */
  this.display = function() {
    if(this.is_shot_creation) {
      this.drawCreationShot();
    }
    if(this.is_shot_personalize) {
      push();
      if(x_off<0){x_off=0;}
      if(y_off<0){y_off=0;}
      translate(x_off,y_off);
      scale(vid_h/Number(original_height));
      // console.log(x_off,y_off,viewer_width/Number(original_width));
      stroke('blue');
      noFill();
      rectMode(CORNER);
      if(this.top_left_perso && this.bottom_right_perso) {
        rect(this.top_left_perso.x,this.top_left_perso.y,this.bottom_right_perso.x-this.top_left_perso.x,this.bottom_right_perso.y-this.top_left_perso.y);
      }
      pop();
    }

    montage_editor.drawShots();

    push();
    if(x_off<0){x_off=0;}
    if(y_off<0){y_off=0;}
    translate(x_off,y_off);
    scale(vid_h/Number(original_height));
    // console.log(x_off,y_off,viewer_width/Number(original_width));
    preparation_editor.drawTracklets();
    if(this.is_shots_frame_layout) {
      this.drawShotsLayout();
    }
    pop();
    if(show_shot) {
      push();
      fill(150);
      rect(0,viewer_height,mid_width,windowHeight-viewer_height);
      pop();
    }
  }
}
