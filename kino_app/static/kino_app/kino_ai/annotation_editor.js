/*
This file is part of KinoAi project.
It is subject to the license terms in the LICENSE file found in the top-level directory
*/

/**
 * [AnnotationEditor => the annotation editor handle all the annotation tab]
 * @constructor
 */
function AnnotationEditor()  {

  this.note_editor = new NoteEditor();
  this.partition_editor = new PartitionEditor();

  this.note_book = createCheckbox('Table', false);
  this.note_book.mouseOver(processToolTip('Show the table interface'));
  this.note_book.mouseOut(processToolTip(''));
  html_elements.push(this.note_book);
  this.note_book.size(150,30);
  this.note_book.changed(updateNoteBook);

  this.partition_check = createCheckbox('Playscript', false);
  this.partition_check.mouseOver(processToolTip('Show the playscript editor'));
  this.partition_check.mouseOut(processToolTip(''));
  html_elements.push(this.partition_check);
  this.partition_check.size(150,30);
  this.partition_check.changed(updatePartition);

  this.is_note_book = false;
  this.is_note_editor = false;
  this.is_partition_editor = false;

  /**
   * [mousePressed => handle mouse click event]
   * @param  {[float]} mx               [mouse x position]
   * @param  {[float]} my               [mouse y position]
   */
  this.mousePressed = function(mx, my) {
    if(this.is_partition_editor) {
      this.partition_editor.click(mouseX, mouseY);
    }

    if(this.is_note_book && this.note_editor.div_sub) {
      for(let s of this.note_editor.tab_sub) {
        var y1 = s.p.position().y - this.note_editor.div_sub.elt.scrollTop;
        if(mouseX>this.note_editor.div_sub.position().x && mouseX <this.note_editor.div_sub.position().x+this.note_editor.div_sub.width && mouseY>y1 && mouseY <y1+s.p.height) {
          video.time(s.start);
        }
      }
      this.note_editor.click(mouseX,mouseY);
    } else if (this.is_note_book) {
      this.note_editor.click(mouseX,mouseY);
    }
  };

  /**
   * [drop => handle drop event]
   * @param  {[float]} mx               [mouse x position]
   * @param  {[float]} my               [mouse y position]
   */
  this.drop = function(mx, my) {

  }

  /**
   * [drag => handle drag event]
   * @param  {[float]} mx               [mouse x position]
   * @param  {[float]} my               [mouse y position]
   */
  this.drag = function(mx, my) {

  }

  /**
   * [keyPressed => handle keyPressed event]
   * @param  {[key code]} keyCode               [key press code]
   */
  this.keyPressed = function(keyCode) {
    if(this.is_partition_editor) {
      this.partition_editor.keyPressed(keyCode);
    }

  }

  /**
   * [mouseWheel => handle mouseWheel event]
   * @param  {[event object]} event               [mouse wheel event]
   */
  this.mouseWheel = function(event) {
    this.partition_editor.mouseWheel(event);
  }

  /**
   * [updateAndShow => update annotation tab, show and resize the elements]
   * @param  {[Boolean]} checked               [is tab checked]
   */
  this.updateAndShow = function(checked) {
    resetTabs();
    is_annotation_editor = true;
    clickTab(annotation_editor_button);
    this.showElts();
    // act_input.show();
    // Side el
    check_render_pose.show();
    submit.show();
    reset_pos.show();
    reload_button.show();
    hide_show_header_button.show();
    documentation_button.show();
    updateSideElems();
    up_rough = true;
  }

  /**
   * [hideElts => hide all elements]
   */
  this.hideElts = function() {
    this.note_editor.hideAllElt();
    this.partition_editor.hideAllElt();
    this.note_editor.hideAllElt();
    this.note_book.hide();
    this.partition_check.hide();
    this.note_editor.update(false);
    this.is_note_book = false;
    this.note_book.checked(false);
    $('#div_sub').hide();
    this.partition_editor.update(false);
    this.is_partition_editor = false;
    this.partition_check.checked(false);
  }

  /**
   * [showElts => show all the elements]
   */
  this.showElts = function() {
    this.note_book.show();
    this.partition_check.show();
    this.note_editor.update(true);
    this.is_note_book = true;
    this.note_book.checked(true);
  }

  /**
   * [resizeElt => resize and adapt the position of the elements]
   */
  this.resizeElt = function() {
    shots_timeline.y = viewer_height+70;
    shots_timeline.resizeElt();
    if(this.is_note_book)
      this.note_editor.resizeElt();
    if(this.is_partition_editor)
      this.partition_editor.setDivSizePos();
  }

  /**
   * [loadAnnotationEditorData => load annotation data from the server]
   */
  this.loadAnnotationEditorData = function() {
    this.note_editor.updateChilds();
  }

  /**
   * [updateNoteBook => show the note book interface]
   */
  function updateNoteBook() {
    annotation_editor.is_note_book = this.checked();
    annotation_editor.is_note_editor = this.checked();
    annotation_editor.note_editor.update(annotation_editor.is_note_editor);
    if(annotation_editor.is_note_book)
      annotation_editor.note_editor.resizeElt();
  }

  /**
   * [updatePartition => show the partition editor]
   */
  function updatePartition() {
    annotation_editor.is_partition_editor = this.checked();
    annotation_editor.partition_editor.update(annotation_editor.is_partition_editor);
  }

  /**
   * [display => function called in the main p5 draw function]
   */
  this.display = function() {
    push();
    if(x_off<0){x_off=0;}
    if(y_off<0){y_off=0;}
    translate(x_off,y_off);
    scale(vid_h/Number(original_height));
    // console.log(x_off,y_off,viewer_width/Number(original_width));
    if(this.is_note_book) {
      preparation_editor.displayTrackBBox();
    }
    preparation_editor.drawTracklets();

    pop();
    if (this.is_partition_editor) {
      this.partition_editor.display();
    } else if (this.is_note_book) {
      this.note_editor.display();
    }
  }
}
