/*
This file is part of KinoAi project.
It is subject to the license terms in the LICENSE file found in the top-level directory
*/

/**
 * [PreparationEditor => this class create all the elements and function for the preparation tab]
 * @constructor
 */
function PreparationEditor()  {

  // tous actor timeline
  // state hidden + offstage
  // show track check + tableau tracklets

  this.table_scroll;
  this.table_tracks;

  this.annotation_timeline = new AnnotationTimeline();

  this.actors_timeline = [];
  this.erase_button = [];
  this.tracklets_line = [];
  this.go_track = [];

  this.div_actors_timeline = createDiv();
  this.div_actors_timeline.style('overflow','auto');
  this.div_actors_timeline.id('div_wrap_actor');

  this.show_tracks = createCheckbox('Show tracks', false);
  this.show_tracks.side = false;
  this.show_tracks.mouseOver(processToolTip('Show all the tracklets'));
  this.show_tracks.mouseOut(processToolTip(''));
  html_elements.push(this.show_tracks);
  this.show_tracks.position(mid_width + 10, 10);
  this.show_tracks.size(150,20);
  this.show_tracks.changed(updateShowTracks);

  this.draw_track = createCheckbox('Draw track', false);
  this.draw_track.mouseOver(processToolTip('Show the selected track path'));
  this.draw_track.mouseOut(processToolTip(''));
  html_elements.push(this.draw_track);
  this.draw_track.size(150,30);
  this.draw_track.changed(updateDrawTrack);

  this.get_actors_on_stage = createButton('Actors On Stage');
  this.get_actors_on_stage.mouseOver(processToolTip('Download JSON with actors on stage for each frame'));
  this.get_actors_on_stage.mouseOut(processToolTip(''));
  html_elements.push(this.get_actors_on_stage);
  this.get_actors_on_stage.mousePressed(getListOnStage);

  this.get_meta_data = createButton('Meta Data');
  this.get_meta_data.mouseOver(processToolTip('Download JSON with meta data informations for each frame'));
  this.get_meta_data.mouseOut(processToolTip(''));
  html_elements.push(this.get_meta_data);
  this.get_meta_data.mousePressed(getFrameMetaData);

  this.annotation_edit = createCheckbox('Stage actions', false);
  this.annotation_edit.mouseOver(processToolTip('Stage actions annotation'));
  this.annotation_edit.mouseOut(processToolTip(''));
  html_elements.push(this.annotation_edit);
  this.annotation_edit.size(150,30);
  this.annotation_edit.changed(updateAnnotationEdit);

  this.check_create_bbox = createCheckbox('Personalize tracklet', false);
  this.check_create_bbox.mouseOver(processToolTip('Create a personalize tracklet based on two bounding boxes for a selected actor'));
  this.check_create_bbox.mouseOut(processToolTip(''));
  html_elements.push(this.check_create_bbox);
  this.check_create_bbox.changed(updateCheckCreateBBox);

  this.create_bbox = createButton('Create personalize track');
  this.create_bbox.mouseOver(processToolTip('Create the personalize tracklet for a selected actor'));
  this.create_bbox.mouseOut(processToolTip(''));
  html_elements.push(this.create_bbox);
  this.create_bbox.mousePressed(createBBox);


  // sanitize = createButton('Sanitize');
  // sanitize.mouseOver(processToolTip('Clean the timeline'));
  // sanitize.mouseOut(processToolTip(''));
  // html_elements.push(sanitize);
  // sanitize.mousePressed(sanitizeFramesData);

  this.is_show_tracks = false;
  this.is_draw_track = false;
  this.is_annotation = false;

  this.is_bbox_creation = false;
  this.curr_creation;
  this.first_and_last_keyframes_set = false;

  this.offstage_state = new StateButton(330,viewer_height+15,40,5,'Offstage','red','rgba(255,0,0,0.7)');

  this.hidden_state = new StateButton(405,viewer_height+15,40,5,'Hidden','rgba(12,158,242,0.95)','rgba(12,158,242,0.7)');

  /**
   * [mousePressed => function to handle a mouse presse event]
   * @param  {[float]} mx               [mouse x position]
   * @param  {[float]} my               [mouse y position]
   */
  this.mousePressed = function(mx, my) {
    if(this.is_annotation) {
      this.annotation_timeline.click(mouseX, mouseY);
    } else {
      if(this.is_show_tracks && this.table_scroll) {
        for(let g of this.go_track) {
          g.on = false;
          g.click(mouseX, mouseY);
          if(g.on){
            retime = true;
          }
        }
      }

      if (mouseX<width && mouseY < height) {
        for(let act of this.actors_timeline) {
          act.click(mouseX, mouseY);
        }
        if(mouseButton === CENTER) {
          for(let act of this.actors_timeline) {
            act.removeState(mouseX, mouseY);
            var b = act.removeTrack(mouseX, mouseY);
          }
        }
        for (var i = 0; i < this.erase_button.length; i++) {
          this.erase_button[i].on = false;
          this.erase_button[i].click(mouseX, mouseY);
          if(this.erase_button[i].on && this.erase_button[i]) {
            for(var j=0; j<this.tracklets_line.length; j++) {
              if(this.tracklets_line[j].actor_name == this.actors_timeline[i].actor_name && this.table_tracks) {
                this.table_tracks.elt.rows[j+1].cells[0].innerHTML = 'unknown';
              }
            }
            let ind;
            for(let j=0;j<this.annotation_timeline.actors_annotation.length; j++) {
              if(this.annotation_timeline.actors_annotation[j].actor_name == this.actors_timeline[i].actor_name) {
                ind = j;
              }
            }
            this.actors_timeline[i].removeAll();
            this.actors_timeline[i].elem.remove();
            this.annotation_timeline.actors_annotation[ind].elem.remove();
            this.annotation_timeline.actors_annotation.splice(ind,1);
            this.actors_timeline.splice(i, 1);
            this.erase_button.splice(i, 1);
          }
        }
        var clic = false;
        var prev = undefined;
        for (let i=0; i<this.tracklets_line.length; i++) {
          if(this.tracklets_line[i].on) {
            prev = i;
          }
          this.tracklets_line[i].on = false;
          if(!clic)
            clic = this.tracklets_line[i].click(mouseX, mouseY);
        }
        if(!clic) {
          if(prev || prev==0) {
            this.tracklets_line[prev].on = true;
          }
        }
      }
      // Creation of a custom bbox for a specific actor
      this.createCustomBBox(mx,my);
    }

  };

  /**
   * [createCustomBBox => Creation of a custom bounding box for a specific actor based on a mouse click]
   * @param  {[float]} mx               [mouse x position]
   * @param  {[float]} my               [mouse y position]
   */
  this.createCustomBBox = function(mx, my) {
    // Check player intersection
    if(!(mx > player.x && mx < player.x + player.w && my > player.y && my < player.y + player.h) && !player.clickOnPlayPause(mx,my)) {
      if (this.is_bbox_creation && mouseX<viewer_width && mouseY < viewer_height){
        if(x_off<0){x_off=0;}
        if(y_off<0){y_off=0;}
        let mx = mouseX-x_off;
        let my = mouseY-y_off;
        mx /=(vid_h/Number(original_height));
        my /=(vid_h/Number(original_height));
        let bbox;
        if(this.curr_creation && this.curr_creation.first_bbox && !this.curr_creation.last_bbox.created) {
          bbox = this.curr_creation.first_bbox;
          this.curr_creation.first_bbox.click(mx,my);
        } else if(this.curr_creation && this.curr_creation.last_bbox) {
          bbox = this.curr_creation.last_bbox;
          this.curr_creation.first_bbox.click(mx,my);
          this.curr_creation.last_bbox.click(mx,my);
        }
        if(bbox && bbox.x == 0 && bbox.y == 0) {
          bbox.setPosition(Math.round(mx), Math.round(my));
          console.log('x, y');
        } else if (bbox && bbox.w == 0 && bbox.h == 0){
          bbox.setDimension(Math.round(mx) - bbox.x, Math.round(my) - bbox.y);
          console.log('w, h');
        } else if (bbox && !bbox.center_x && !bbox.center_y) {
          bbox.setCenter(Math.round(mx), Math.round(my));
          console.log('center');
          if( this.curr_creation.last_bbox.created)
            this.first_and_last_keyframes_set = true;
          this.curr_creation.last_bbox.created = true;
        }
      }
    }
  }

  /**
   * [drop => handle the drop event]
   * @param  {[float]} mx               [mouse x position]
   * @param  {[float]} my               [mouse y position]
   */
  this.drop = function(mx, my) {
    if(this.is_annotation) {
      this.annotation_timeline.drop(mouseX, mouseY);
    } else {
      let state;
      if(this.offstage_state.in_drag) {
        state = this.offstage_state;
      }
      if(this.hidden_state.in_drag) {
        state = this.hidden_state;
      }
      if(state) {
        for(let act of this.actors_timeline) {
          act.dropState(mouseX, mouseY, state);
        }
      }
      this.offstage_state.drop();
      this.hidden_state .drop();
      var ind = 0;
      var is_obj = false;
      for(let i=0; i<this.tracklets_line.length; i++) {
        if(this.tracklets_line[i].drag) {
          is_obj = true;
          ind = i;
        }
        this.tracklets_line[i].drag = false;
      }
      for(let act of this.actors_timeline) {
        if(is_obj) {
          if(mouseButton != CENTER && act.dropTrack(mouseX, mouseY, this.tracklets_line[ind]) && this.table_tracks) {
            this.table_tracks.elt.rows[ind+1].cells[0].innerHTML = act.actor_name;
          }
        }
        let t = act.drop(mouseX, mouseY, player.total_frame);
        if(t) {
          this.tracklets_line.push(t);
        }
      }
      if(this.curr_creation && this.curr_creation.first_bbox) {
        this.curr_creation.first_bbox.drop(mx,my);
      }
      if(this.curr_creation && this.curr_creation.last_bbox) {
        this.curr_creation.last_bbox.drop(mx,my);
      }
    }

  }

  /**
   * [drag => handle drag events]
   * @param  {[float]} mx               [mouse x position]
   * @param  {[float]} my               [mouse y position]
   */
  this.drag = function(mx, my) {
    if(this.is_annotation) {
      this.annotation_timeline.drag(mouseX, mouseY);
    } else {
      this.offstage_state.drag(mouseX, mouseY);
      this.hidden_state.drag(mouseX, mouseY);
    }
    if(this.curr_creation && this.curr_creation.first_bbox && this.curr_creation.first_bbox.in_drag) {
      this.curr_creation.first_bbox.drag(mx,my);
    }
    if(this.curr_creation && this.curr_creation.last_bbox && this.curr_creation.last_bbox.in_drag) {
      this.curr_creation.last_bbox.drag(mx,my);
    }
  }

  /**
   * [keyPressed => handle key press event]
   * @param  {[keycode]} keyCode               [keycode event]
   */
  this.keyPressed = function(keyCode) {
    if(this.is_annotation) {
      this.annotation_timeline.keyPressed(keyCode);
    } else {
      // key del or backspace
      if(keyCode===46 || keyCode === 8) {
        // this.removeTracklet();
      }

      // ctrl z
      if (!key_down && keyCode == 17) {
        key_down = 17;
      } else if(key_down == 17 && keyCode == 90) {
        for(let act of this.actors_timeline) {
          act.undoAction();
        }
        key_down = undefined;
      }

      // key s
      if(keyCode===83) {
        this.splitTracklet();
      }

      // key del or backspace remove tracklets from timeline
      if(keyCode===46 || keyCode === 8) {
        for(let act of this.actors_timeline) {
          act.removeTrackletFromKey();
          for(let i=0; i<act.states.length;i++) {
            if(act.states[i].on) {
              act.states.splice(i,1);
            }
          }
        }
      }

    }
  }

  /**
   * [mouseWheel => handle mouse wheel events]
   * @param  {[event object]} event               [mouse wheel event]
   */
  this.mouseWheel = function(event) {
    this.annotation_timeline.mouseWheel(event);
  }

  this.update = function(checked) {

  }

  /**
   * [updateAndShow => select the preparation editor]
   */
  this.updateAndShow = function() {
    resetTabs();
    is_preparation_editor = true;
    clickTab(preparation_editor_button);
    this.showElts();
    act_input.show();
    for(let a of this.actors_timeline) {
      a.on = false;
    }
    // Side el
    check_render_pose.show();
    submit.show();
    reset_pos.show();
    reload_button.show();
    hide_show_header_button.show();
    documentation_button.show();
    updateSideElems();
    up_rough = true;
  }

  /**
   * [hideElts => hide all html elements]
   */
  this.hideElts = function() {
    this.show_tracks.hide();
    this.is_show_tracks = false;
    this.show_tracks.checked(false);
    if(this.table_scroll) {
      this.table_scroll.remove();
      this.table_scroll = undefined;
    }
    this.draw_track.hide();
    this.is_draw_track = false;
    this.draw_track.checked(false);
    this.annotation_timeline.update(false);
    this.is_annotation = false;
    this.annotation_edit.checked(false);
    this.annotation_timeline.hideAllElt();
    this.check_create_bbox.hide();
    this.div_actors_timeline.hide();
    for(let t of this.tracklets_line) {
      t.on = false;
    }
    for(let a of this.actors_timeline) {
      a.on = false;
      for(let t of a.tracks) {
        t.on = false;
      }
    }
    this.annotation_edit.hide();
    this.get_meta_data.hide();
    this.get_actors_on_stage.hide();
  }

  /**
   * [showElts => show html elements]
   */
  this.showElts = function() {
    this.annotation_edit.show();
    this.show_tracks.show();
    this.draw_track.show();
    this.show_tracks.show();
    this.check_create_bbox.show();
    this.div_actors_timeline.show();
    this.get_meta_data.show();
    this.get_actors_on_stage.show();
  }

  /**
   * [loadActorTimelines => load the actor timelines data]
   * @param  {[array]} data_timelines               [JSON data of actor timelines]
   */
  this.loadActorTimelines = function(data_timelines) {
    // console.log(data_timelines);
    if(data_timelines[0]) {
      const data_own_t = Object.getOwnPropertyNames(data_timelines);
      // console.log(data_timelines.length);
      for(let i=0; i<data_timelines.length; i++) {
        let act = new ActorTimeline(frames_data);
        act.frames_data = frames_data;
        if(data_timelines[i].Color) {
          act.color = data_timelines[i].Color;
        } else {
          act.color = randomColor();
        }
        act.actor_name = data_timelines[i].Name;
        act.prev_name = act.actor_name;
        for(let j=0; j<data_timelines[i].Timeline.length; j++) {
          if(data_timelines[i].Timeline[j].Detections) {
            let t = new Track();
            t.setActorName(act.actor_name);
            t.setFirstFrame(data_timelines[i].Timeline[j].FirstFrame);
            t.detections = data_timelines[i].Timeline[j].Detections;
            preparation_editor.tracklets_line.push(t);
            act.addTrack(t);
          }
          if(data_timelines[i].Timeline[j].BBoxes) {
            let t = new TrackBboxShot(act);
            t.first_frame = data_timelines[i].Timeline[j].FirstFrame;
            t.last_frame = data_timelines[i].Timeline[j].LastFrame;
            for(let b of data_timelines[i].Timeline[j].BBoxes) {
              let bb = new BboxShot(b[0],b[1],b[2],b[3]);
              bb.setCenter(b[4],b[5]);
              t.bboxes.push(bb);
            }
            act.track_bbox_shot.push(t);
          }
          if(data_timelines[i].Timeline[j].State) {
            let new_state = {};
            new_state.first_frame = data_timelines[i].Timeline[j].FirstFrame;
            new_state.last_frame = data_timelines[i].Timeline[j].EndFrame;
            new_state.name =  data_timelines[i].Timeline[j].State;
            // new_state.x = this.x+new_state.first_frame*unit;
            // new_state.w = this.x+new_state.last_frame*unit-new_state.x;
            new_state.color = data_timelines[i].Timeline[j].Color;
            if(!data_timelines[i].Timeline[j].Color_On) {
              new_state.color_on = data_timelines[i].Timeline[j].Color;
            } else {
              new_state.color_on = data_timelines[i].Timeline[j].Color_On;
            }
            // new_state.h = this.h/2;
            act.states.push(new_state);
          }
        }
        let elem = createElement('h3', act.actor_name);
        elem.elt.contentEditable = 'true';
        elem.id('editor');
        act.elem = elem;
        preparation_editor.div_actors_timeline.child(act.elem);
        preparation_editor.actors_timeline.push(act);
        preparation_editor.sortActorTimelines();
        preparation_editor.erase_button.push(new EraseButton(i));

        if(!preparation_editor.getActorAnnotationTimeline(act.actor_name)) {
          let a = new ActorAnnotation(act.actor_name);
          preparation_editor.annotation_timeline.actors_annotation.push(a);
          preparation_editor.annotation_timeline.erase_button.push(new EraseButton(preparation_editor.annotation_timeline.actors_annotation.length-1));
          preparation_editor.annotation_timeline.div_wrap.child(a.elem);
        }
      }
    }
    for(let s of montage_editor.shots) {
      if(s.actors_involved.length!=s.temp_tab_act.length) {
        s.setActInvoled(s.temp_tab_act);
      }
    }
    if(shots_timeline.list_data) {
      for(let scene of shots_timeline.list_data) {
        for(let s of scene.Data) {
          if(s.temp_tab_act && s.actors_involved.length!=s.temp_tab_act.length) {
            let tab = [];
            for(let n of s.temp_tab_act) {
              tab.push(preparation_editor.getAct(n));
            }
            s.actors_involved = tab;
          }
        }
      }
    }
  }

  /**
   * [loadDataTracks => load tracklets data]
   * @param  {[array]} data_tracks               [tracklets data]
   */
  this.loadDataTracks = function(data_tracks) {
    let l_t;
    if(data_tracks.length) {
      l_t = data_tracks.length
    } else {
      const data_own = Object.getOwnPropertyNames(data_tracks);
      l_t = data_own.length;
    }
    for (var i = 0; i < l_t; i++) {
      tracks_data.push(data_tracks[i]);
    }
    if(tracks_data[0]) {
      for (var i = 0; i < tracks_data.length; i++) {
        var t = new Track();
        t.setActorName("unknown");
        t.setFirstFrame(tracks_data[i]['FirstFrame']);
        t.detections = tracks_data[i]['Detections'];
        preparation_editor.tracklets_line.push(t);
      }
    }
  }

  /**
   * [loadDataAnnotationTimeline => load annotation timeline data]
   * @param  {[array]} data_annotation_timeline               [annotation timeline data]
   */
  this.loadDataAnnotationTimeline = function(data_annotation_timeline) {
    if(data_annotation_timeline[0]) {
      for(let i=0; i<data_annotation_timeline.length; i++) {
        let a = preparation_editor.getActorAnnotationTimeline(data_annotation_timeline[i].Name);
        let b = false;
        if(!a) {
          b = true;
          a = new ActorAnnotation(data_annotation_timeline[i].Name);
        }
        for(let act of data_annotation_timeline[i].Actions) {
          let obj = {};
          obj.name = act.Name;
          obj.first_frame = act.FirstFrame;
          obj.end_frame = act.EndFrame;
          obj.color = act.Color;
          a.setAction(obj);
        }
        if(b) {
          preparation_editor.annotation_timeline.actors_annotation.push(a);
          preparation_editor.annotation_timeline.erase_button.push(new EraseButton(preparation_editor.annotation_timeline.actors_annotation.length-1));
          preparation_editor.annotation_timeline.div_wrap.child(a.elem);
        }
      }
    }
  }

  /**
   * [getTimelinesData => convert javascript data of actors timeline into JSON ready to save object]
   */
  this.getTimelinesData = function() {
    let json_act_name = [];

    for(let act of this.actors_timeline) {
      // json_act_name.id =
      var timeline = {};
      timeline.Name = act.actor_name;
      timeline.Color = act.color;
      timeline.PrevName = act.prev_name;
      act.prev_name = act.actor_name;
      timelines = [];
      if(act.tracks.length>0){
        for(let t of act.tracks) {
          var track = {};
          track.Detections = t.detections;
          track.FirstFrame = t.first_frame;
          timelines.push(track);
        }
      }
      if(act.states.length>0){
        for(let st of act.states) {
          var state = {};
          state.State = st.name;
          state.FirstFrame = st.first_frame;
          state.EndFrame = st.last_frame;
          state.Color = st.color;
          state.Color_On = st.color_on;
          timelines.push(state);
        }
      }
      if(act.track_bbox_shot.length>0){
        for(let t of act.track_bbox_shot) {
          var t_bbox = {};
          t_bbox.FirstFrame = t.first_frame;
          t_bbox.LastFrame = t.last_frame;
          t_bbox.BBoxes = [];
          for(let b of t.bboxes){
            let val = [b.x, b.y, b.w, b.h, b.center_x, b.center_y];
            t_bbox.BBoxes.push(val);
          }
          timelines.push(t_bbox);
        }
      }
      timeline.Timeline = timelines;
      json_act_name.push(timeline);
    }
    return json_act_name;
  }

  /**
   * [getAnnotationData => convert javascript data of annotation timeline into JSON objects ready to save]
   */
  this.getAnnotationData = function() {
    let annot_t = [];
    for(let a_t of this.annotation_timeline.actors_annotation) {
      let obj = {};
      obj.Name = a_t.actor_name;
      obj.Actions = [];
      for(let act of a_t.actions) {
        let o = {};
        o.Name = act.name;
        o.FirstFrame = act.first_frame;
        o.EndFrame = act.end_frame;
        o.Color = act.color;
        obj.Actions.push(o);
      }
      annot_t.push(obj);
    }
    return annot_t;
  }

  /**
   * [getTrackletsData => convert tracklets data into JSON]
   */
  this.getTrackletsData = function() {
    let json_tracks = [];
    for(let t of this.tracklets_line) {
      if(!t.added && !t.old) {
        var track = {};
        track.Detections = t.detections;
        track.FirstFrame = t.first_frame;
        json_tracks.push(track);
      }
    }
    return json_tracks;
  }

  /**
   * [resizeElt => resize html elements]
   */
  this.resizeElt = function() {
    this.show_tracks.original_x = mid_width + 10;
    this.div_actors_timeline.position(0,viewer_height+can.elt.offsetTop+45);
    this.div_actors_timeline.size(mid_width, windowHeight-this.div_actors_timeline.y-5);
  }


  function updateAnnotationEdit() {
    preparation_editor.is_annotation = this.checked();
    preparation_editor.annotation_timeline.update(preparation_editor.is_annotation);
  }

  function updateShowTracks() {
    preparation_editor.is_show_tracks = this.checked();
  }

  function updateDrawTrack() {
    preparation_editor.is_draw_track = this.checked();
  }

  function updateCheckCreateBBox() {
    preparation_editor.is_bbox_creation = this.checked();

    if(preparation_editor.is_bbox_creation) {
      let selected_act;
      for(let a of preparation_editor.actors_timeline){
        if(a.on) {
          selected_act = a;
        }
      }
      if(!selected_act || preparation_editor.getActOnStage(frame_num).includes(selected_act.actor_name)) {
        if(!selected_act) {
          alert('Select a timeline');
        } else {
          alert('The selected actor '+selected_act.actor_name+' is already detected at the current frame');
        }
        this.is_bbox_creation = false;
        preparation_editor.curr_creation = undefined;
        preparation_editor.check_create_bbox.elt.firstChild.checked = false;
      }else {
        preparation_editor.curr_creation = new TrackBboxShot(selected_act);
        preparation_editor.curr_creation.first_bbox = new BboxShot(0,0,0,0);
        preparation_editor.curr_creation.first_frame = frame_num;
        preparation_editor.curr_creation.last_bbox = new BboxShot(0,0,0,0);
        preparation_editor.curr_creation.last_frame = frame_num;
        preparation_editor.create_bbox.show();
      }
    } else {
      preparation_editor.create_bbox.hide();
      preparation_editor.is_bbox_creation = false;
      preparation_editor.curr_creation = undefined;
    }
  }

  /**
   * [getListOnStage => Extract a Json file with the list of actors on stage at each frame]
   */
  function getListOnStage() {
    let ret = [];
    for(let i=1; i<total_frame+1; i++) {
      ret.push(preparation_editor.getActOnStage(i));
    }
    createStringDict(ret).saveJSON('actors_on_stage');
  }

  /**
   * [getActorAnnotationTimeline => Get annotation timeline by act name]
   * @param  {[string]} name               [actor name]
   * @return {[object]}      [actor object]
   */
  this.getActorAnnotationTimeline = function(name) {
    for(let a of this.annotation_timeline.actors_annotation) {
      if(a.actor_name == name) {
        return a;
      }
    }
    return;
  }

  /**
   * [getAct => Get the Object actor timeline from an actor name]
   * @param  {[string]} name               [actor name]
   * @return {[object]}      [actor object]
   */
  this.getAct = function(name) {
    let ret;
    for(let a of preparation_editor.actors_timeline) {
      if(a.actor_name == name) {
        ret = a;
        break;
      }
    }
    return ret;
  }

  /**
   * [getActOnStage => Get the names of the actors present on stage]
   * @param  {[integer]} fr_n               [frame number]
   * @return {[list of string]}      [list of actors name]
   */
  this.getActOnStage = function(fr_n) {
    let ret = [];
    for(let a of this.actors_timeline) {
      if(a.isOnstage(fr_n)) {
        ret.push(a.actor_name);
      }
    }
    return ret;
  }

  /**
   * [getTopPosition => Get the y position of the top actor on stage]
   * @param  {[integer]} [f_n=undefined]               [frame number]
   * @return {[integer]}                 [the y position of the upper actor on stage between (0,original_height/4)]
   */
   this.getTopPosition = function(f_n=undefined) {
    if(!f_n) {
      f_n = frame_num;
    }
    let top_pos = parseInt(original_height);
    for(let a of this.actors_timeline) {
      if(a.isOnstage(f_n)) {
        top_pos = Math.min(parseInt(original_height)/4,Math.min(a.getActPosition(f_n).y-50,top_pos));
      }
    }
    return top_pos;
  }

  /**
   * [getFrameMetaData => Extract the meta data for each actors on stage at each frame]
   * @return {[list of objects]} [for each frame multiple data for allthe actors on stage (size,gaze,surface)]
   */
  function getFrameMetaData() {
    let ret = [];
    for(let i=1; i<total_frame+1; i++) {
      let tab_indexes = preparation_editor.getTrackletsIndexes(i, false, true);
      let keypoints = frames_data[i];
      let o = {};
      o.ActsOnStage = [];
      for( let obj of tab_indexes) {
        if(obj.act && keypoints[obj.ind]) {
          let meta_data = {};
          meta_data.ActName = obj.act.actor_name;
          let bbox = getBBox(keypoints[obj.ind]['KeyPoints']);
          meta_data.BoundingBox = {w:int(bbox[2]-bbox[0]),l:int(bbox[3]-bbox[1])};
          meta_data.Surface = meta_data.BoundingBox.w * meta_data.BoundingBox.l;
          meta_data.GazeVect = p5VectToJson(getGazevect(keypoints[obj.ind]['KeyPoints']));
          meta_data.VelocityVect = p5VectToJson(obj.act.getVelocityVect(i));
          meta_data.KeyPoints = keypoints[obj.ind]['KeyPoints'];
          o.ActsOnStage.push(meta_data);
        }
      }
      ret.push(o);
    }
    // console.log(ret);
    createStringDict(ret).saveJSON('frames_metadata');
  }

  /**
   * [sortTable => Sort the tracks table (short length ==> long length)]
   * @param  {[element]} [_table=undefined]               [table element]
   */
  function sortTable(_table=undefined) {
    var table, rows, switching, i, x, y, shouldSwitch;
    if(!_table) {
      table = preparation_editor.table_scroll.elt.firstElementChild;
    }
    switching = true;
    // Make a loop that will continue until no switching has been done:
    while (switching) {
      //start by saying: no switching is done:
      switching = false;
      rows = table.rows;
      // Loop through all table rows (except the first, which contains table headers):
      for (i = 1; i < (rows.length - 1); i++) {
        //start by saying there should be no switching:
        shouldSwitch = false;
        // Get the two elements you want to compare, one from current row and one from the next:
        x = rows[i].getElementsByTagName("TD")[2];
        y = rows[i + 1].getElementsByTagName("TD")[2];
        //check if the two rows should switch place:
        if (Number(x.innerHTML) > Number(y.innerHTML)) {
          //if so, mark as a switch and break the loop:
          shouldSwitch = true;
          break;
        }
      }
      if (shouldSwitch) {
        // If a switch has been marked, make the switch and mark that a switch has been done:
        rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
        switching = true;
      }
    }
  }

  /**
   * [createTableTracks => Create a html table to navigate into the tracklets]
   */
  this.createTableTracks = function() {

    if(this.is_show_tracks && !this.table_scroll) {
      this.go_track = [];
      this.table_scroll = createElement('div');
      this.table_scroll.id('table-scroll');
      this.table_scroll.position(viewer_width + 20, can.elt.offsetTop+60);
      table_tracks = createElement('table');
      var head = createElement('thead');
      var row = createElement('tr');
      row.child(createElement('th','Actor'));
      row.child(createElement('th','First'));
      let le = createElement('th','Length');
      le.mouseClicked(sortTable);
      row.child(le);
      row.child(createElement('th','Go'));
      head.child(row);
      table_tracks.child(head);

      for (let t of this.tracklets_line) {
        var row = createElement('tr');
        row.child(createElement('td',t.actor_name));
        row.child(createElement('td',t.first_frame));
        row.child(createElement('td',t.detections.length));
        var g = createElement('td','Go');
        row.child(g);
        table_tracks.child(row);
        var x1 = this.table_scroll.position().x+g.position().x;
        var y1 = this.table_scroll.position().y+g.position().y;
        this.go_track.push(new GoButton(g.width, g.height, t, g));
      }
      this.table_scroll.child(table_tracks);
    } else {
      if(this.table_scroll && !this.is_show_tracks) {
        this.table_scroll.remove();
        this.table_scroll = undefined;
      }
    }

  }

  /**
   * [sortActorTimelines => sort the actors timeline by the actor name]
   */
  this.sortActorTimelines = function() {
    this.actors_timeline.sort(compare_name);
    while(this.div_actors_timeline.firstChild){this.div_actors_timeline.firstChild.remove();}
    for(let a of this.actors_timeline) {
      this.div_actors_timeline.child(a.elem);
    }
  }

  /**
   * [createActTimeline => Create an actor timeline with all the html elements]
   */
  this.createActTimeline = function() {
    if(this.is_annotation) {
      this.createAnnotTimeline();
    } else {
      let actors = [];
      for(let act of this.actors_timeline) {
        actors.push(act.actor_name);
      }
      if(act_input.value() && !actors.includes(act_input.value())) {
        actors.push(act_input.value());
        let elem = createElement('h3', actors[actors.length-1]);
        elem.elt.contentEditable = 'true';
        elem.id('editor');
        let x = 100;
        let y = viewer_height+40+(actors.length-1-1)*20+25;
        let act = new ActorTimeline(x, y, player.w, 15);
        act.elem = elem;
        this.div_actors_timeline.child(act.elem);
        act.actor_name = actors[actors.length-1];
        act.prev_name = act.actor_name;
        this.actors_timeline.push(act);
        this.sortActorTimelines();
        this.erase_button.push(new EraseButton(this.actors_timeline.length-1));
        this.createAnnotTimeline();
      }
    }
  }

  /**
   * [createAnnotTimeline => Create annotation timeline object]
   */
  this.createAnnotTimeline = function() {
    let a = new ActorAnnotation(act_input.value());
    this.annotation_timeline.actors_annotation.push(a);
    this.annotation_timeline.div_wrap.child(a.elem);
    act_input.value('');
    this.annotation_timeline.erase_button.push(new EraseButton(this.annotation_timeline.actors_annotation.length-1));
    this.annotation_timeline.sortActorAnnotation();
  }

  /**
   * [removeTracklet => Remove the selected tracklet]
   */
  this.removeTracklet = function() {
    for(var i=0; i<this.tracklets_line.length; i++) {
      if(this.tracklets_line[i].on && !this.tracklets_line[i].added) {
        this.tracklets_line.splice(i,1);
        if(this.table_scroll) {
          this.table_scroll.remove();
          this.table_scroll = undefined;
        }
      }
    }
  }

  /**
   * [removeUnknownTracks => Remove all the unknown trackelts]
   */
  this.removeUnknownTracks = function() {
    for(let i=0;i<this.tracklets_line.length;i++) {
      if(this.tracklets_line[i].actor_name == "unknown"){
        this.tracklets_line.splice(i,1);
      }
    }
  }

  /**
   * [splitTracklet => split selected tracklet at the current frame]
   */
  this.splitTracklet = function() {
    let new_track;
    // Split an unknown tracklet
    for(let t of this.tracklets_line) {
      if(t.on && !t.added) {
        var first = t.first_frame;
        new_track = new Track();
        new_track.first_frame = frame_num;
        var detec = t.detections;
        var len = detec.length;
        detec = detec.slice(new_track.first_frame-first, len);
        t.detections = t.detections.slice(0,new_track.first_frame-first);
        t.drag = false;
        t.on = false;
        t.setPosition(0,0,0,0);
        new_track.detections = detec;
        new_track.actor_name = 'unknown';
        this.tracklets_line.push(new_track);
      }
    }

    // Split an assigned tracklet
    for(let a of this.actors_timeline) {
      for(let t of a.tracks) {
        if(t.on) {
          var first = t.first_frame;
          new_track = new Track();
          new_track.first_frame = frame_num-1;
          var detec = t.detections;
          var len = detec.length;
          detec = detec.slice(new_track.first_frame-first, len);
          t.detections = t.detections.slice(0,new_track.first_frame-first);
          t.drag = false;
          t.on = false;
          t.setPosition(0,0,0,0);
          new_track.detections = detec;
          a.addTrack(new_track);
          a.updateTrackPos(total_frame);
          this.tracklets_line.push(new_track);
        }
      }

      // Split a personalize tracklet
      for(let t of a.track_bbox_shot) {
        if(t.on) {
          new_track = new TrackBboxShot(a);
          new_track.first_frame = t.first_frame;
          new_track.last_frame = frame_num-1;
          new_track.bboxes = t.bboxes.slice(0,new_track.last_frame-t.first_frame);
          t.bboxes = t.bboxes.slice(frame_num-t.first_frame, t.bboxes.length);
          t.first_frame = frame_num;
          a.track_bbox_shot.push(new_track);
          a.track_bbox_shot.sort(compare_first);
        }
      }
    }
    if(new_track)
      new_track.on = true;
  }

  /**
   * [getTrackletsIndexes => Get the tracklets indexes of the actors on stage]
   * @param  {[integer]}  [fr_n=undefined]                [frame number]
   * @param  {Boolean} [added=false]                   [is the tracklet unknown]
   * @param  {Boolean} [just_acts=false]               [if true only the tracklets related to an actor]
   * @return {[list of objects]}                    [list of objects (index of detection, tracklet object)]
   */
  this.getTrackletsIndexes = function(fr_n=undefined,added=false,just_acts=false) {
    if(fr_n == undefined) {
      fr_n =frame_num;
    }
    var keypoints = frames_data[fr_n];
    let tab = [];
    if(keypoints) {
      if(!just_acts) {
        for (let t of this.tracklets_line) {
          if((added || !t.added) && !t.old) {
            var detections_track = t.detections;
            var first_frame = t.first_frame;
            if(first_frame <= fr_n) {
              if(detections_track.length > (fr_n-first_frame)) {
                if(detections_track[fr_n-first_frame] < keypoints.length) {
                  let obj = {};
                  obj.ind = detections_track[fr_n-first_frame];
                  obj.track = t;
                  tab.push(obj);
                }
              }
            }
          }
        }
      }
      for(let act of this.actors_timeline) {
        for(let t of act.tracks) {
          var detections_track = t.detections;
          var first_frame = t.first_frame;
          if(first_frame <= fr_n) {
            if(detections_track.length > (fr_n-first_frame)) {
              if(detections_track[fr_n-first_frame] < keypoints.length) {
                let obj = {};
                obj.ind = detections_track[fr_n-first_frame];
                obj.track = t;
                obj.act = act;
                tab.push(obj);
              }
            }
          }
        }
      }
    }
    return tab;
  }

  /**
   * [displayTimeline => Draw the actors timeline below the player]
   */
  this.displayTimeline = function() {
    let ret = 0;
    for(var i=0; i < this.actors_timeline.length; i++) {
      let act = this.actors_timeline[i];
      act.elem.size(player.x);
      if(!cadrage_editor.is_shot_creation) {
        act.setPosition(player.x, act.elem.elt.offsetTop+act.elem.elt.parentNode.offsetTop-can.elt.offsetTop-$('#div_wrap_actor').scrollTop(),player.w,20);
      }
      act.updateTrackPos(player.total_frame);
      this.actors_timeline[i].display();
      this.erase_button[i].setPosition(mid_width+10, this.actors_timeline[i].y+(this.actors_timeline[i].h/2));
      this.erase_button[i].display();
      ret = this.actors_timeline[i].y+this.actors_timeline[i].h-player.nav_bar.y;
    }
    return ret;
  }

  function compare_first(a,b) {
    if (a.first_frame < b.first_frame)
      return -1;
    if (a.first_frame > b.first_frame)
      return 1;
    return 0;
  }

  /**
   * [createBBox => launch the creation of the personalize bounding box when the actor is not detected]
   */
  function createBBox() {
    if(preparation_editor.first_and_last_keyframes_set){
      preparation_editor.curr_creation.last_frame = frame_num+1;
      if(preparation_editor.curr_creation.act.checkCollideTrackletsPerso(preparation_editor.curr_creation.first_frame, preparation_editor.curr_creation.last_frame)) {
        preparation_editor.curr_creation.interpolate();
        if(preparation_editor.curr_creation.act) {
          preparation_editor.curr_creation.act.track_bbox_shot.push(preparation_editor.curr_creation);
          preparation_editor.curr_creation.act.track_bbox_shot.sort(compare_first);
        }
        preparation_editor.curr_creation = undefined;
        preparation_editor.check_create_bbox.elt.firstChild.checked = false;
        preparation_editor.is_bbox_creation = false;
        preparation_editor.create_bbox.hide();
      } else {
        alert('The personalize box collide a tracklet');
      }
    }
  }

  /**
   * [displayCounter => display the timing of the video in the player and the number of tracklets (known/unknown)]
   */
   this.displayCounter = function() {
    let cpt = 0;
    let l = 0;
    for(let t of this.tracklets_line) {
      if(t.actor_name!='unknown' && !t.old) {
        cpt++;
        l++;
      } else if(!t.old) {
        l++;
      }
    }
    push();
    fill(255);
    text(cpt+'/'+l,108,15);

    let min = toTwoDigit(Math.floor(video.time()/60).toString());
    let sec = toTwoDigit(Math.floor(video.time()%60).toString());
    let mil = toTwoDigit(round_prec((video.time()%1)*100,0).toString());
    // text(round_prec(scale_ratio), 200,15);
    text(frame_num, 175,15);
    text(Math.round(frameRate()), viewer_width-40,15);
    textSize(20);
    text(min +':'+sec+'.'+mil, 10,20);
    min = toTwoDigit(Math.floor(video.duration()/60).toString());
    sec = toTwoDigit(Math.floor(video.duration()%60).toString());
    mil = toTwoDigit(round_prec((video.duration()%1)*100,0).toString());
    text(min +':'+sec+'.'+mil, 10,45);
    stroke(255);
    line(10,25,90,25);
    pop();
  }

  /**
   * [drawPreview => Draw a preview of the selected tracklet]
   */
  this.drawPreview = function() {
    let k=0;
    for(let t of this.tracklets_line) {
      if(t.on && !t.added && !t.old) {
        push();
        textSize(15);
        text('Preview '+k,10,viewer_height+37);
        t.updatePos(player.w/player.total_frame, player.x, viewer_height+30);
        t.display();
        pop();
      }
      if(t.drag) {
        push();
        rect(mouseX, mouseY, 25, 7);
        pop();
      }
      k++;
    }
  }

  /**
   * [drawBBox => Draw the bounding box arround the actor]
   * @param  {[list]} keypoints               [openpose keypoints]
   * @param  {[object]} t                       [tracklet object]
   */
  this.drawBBox = function(keypoints, t) {
    var bbox = getBBox(keypoints);
    push();
    noFill();
    strokeWeight(3);
    if(t.on) {
      stroke(40);
      // stroke(170,56,35);
    }
    else{
      stroke(255);
    }
    rect(bbox[0], bbox[1], bbox[2]-bbox[0], bbox[3]-bbox[1]);
    noStroke();
    fill(255);
    textSize(15);
    text(t.actor_name, bbox[0], bbox[1]-5);
    pop();
    t.setBBox(bbox);
  }

  /**
   * [drawTracklets => Draw the current frame open pose detections wich are in a tracklet]
   */
  this.drawTracklets = function() {
    let keypoints = frames_data[frame_num];

    let tab_indexes = this.getTrackletsIndexes();
    for( let obj of tab_indexes) {
      if(!render_pose && is_preparation_editor) {
        this.drawBBox(keypoints[obj.ind]['KeyPoints'], obj.track);
      } else if(render_pose) {
        if(!obj.act) {
          drawPose(keypoints[obj.ind]['KeyPoints']);
        } else {
          drawPose(keypoints[obj.ind]['KeyPoints'],obj.act);
        }
      }
    }
  }

  /**
   * [displayTrackBBox => Draw the personalize bounding box create by an user]
   */
  this.displayTrackBBox = function(){
    for(let a of this.actors_timeline) {
      for(let t of a.track_bbox_shot) {
        if(is_preparation_editor) {
          t.display();
        }
      }
    }
  }

  /**
   * [drawTrackOn => Draw the actor movement on stage from the tracklet beginning  until the current frame]
   */
  this.drawTrackOn = function() {
    push();
    colorMode(HSB);
    let from = color(120, 100, 20);
    let to = color(120, 100, 80);
    for(let t of this.tracklets_line) {
      var detections_track = t.detections;
      if(t.on) {
        if(video.duration()) {
          var last_frames_track = t.first_frame+detections_track.length;
          if(frame_num>t.first_frame && frame_num < last_frames_track) {
            for(var j = t.first_frame; j < frame_num; j++) {
              var keypoints = frames_data[j];
              if(keypoints[detections_track[j-t.first_frame]]) {
                var center = getCenter(keypoints[detections_track[j-t.first_frame]]['KeyPoints']);
                push();
                let color = lerpColor(from, to, j/(frame_num-t.first_frame));
                fill(color);
                ellipse(center[0],center[1],3);
                pop();
              }
            }
          }
        }
      }
    }
    pop();
  }

  /**
   * [drawFramesData => Function to draw all the open pose detections even the untracked one]
   */
  this.drawFramesData = function() {
    for(let keypoints of frames_data[frame_num]) {
      if(keypoints.KeyPoints)
        drawPose(keypoints.KeyPoints);
    }
  }

  /**
   * [display => function called in the main p5 draw function]
   */
  this.display = function() {

    if(viewer_height+80<mouseY && mouseY<height && mouseX<mid_width) {
      // Display a helping message
      push();
      fill(255);
      rect(0, height-20,windowWidth,20);
      fill('black');
      noStroke();
      textSize(16);
      text('Press Z + scroll the mouse wheel to zoom',0,height-5);
      pop();
    }

    this.offstage_state.y = viewer_height+13;
    this.hidden_state.y = viewer_height+13;
    push();
    if(x_off<0){x_off=0;}
    if(y_off<0){y_off=0;}
    translate(x_off,y_off);
    scale(vid_h/Number(original_height));
    // console.log(x_off,y_off,viewer_width/Number(original_width));
    this.displayTrackBBox();
    this.drawTracklets();
    if(this.is_draw_track) {
      this.drawTrackOn();
    }
    if(this.curr_creation && !this.is_annotation)
      this.curr_creation.display();
    pop();

    push();
    fill(0)
    textSize(17);
    text('Actor name', 10, viewer_height+15);
    pop();

    if(this.is_annotation) {
      this.annotation_timeline.display();
    } else {
      let off_y_last_act = this.displayTimeline();
      this.drawPreview();
      this.offstage_state.display();
      this.hidden_state.display();
      push();
      stroke('black');
      strokeWeight(2);
      line(player.nav_bar.cursor,player.nav_bar.y,player.nav_bar.cursor,player.nav_bar.y+off_y_last_act);
      pop();
    }
    this.createTableTracks();

  }
}
