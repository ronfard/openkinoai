/*
This file is part of KinoAi project.
It is subject to the license terms in the LICENSE file found in the top-level directory
*/

/**
 * [NoteEditor => notebook editor, manage the table interface in the annotation tab]
 * @constructor
 */
function NoteEditor()  {
  this.x  = 0;
  this.y  = 0;
  this.w  = 0;
  this.h  = 0;
  this.on = false;

  this.input_note = createInput();
  this.input_note.changed(addNote);
  this.input_note.hide();

  this.select_note = createSelect();

  this.notes = [];
  for(let note of json_data_note) {
    if(note.User == username)
      this.notes = note.Note;
    this.select_note.option(note.User);
  }
  this.select_note.elt.value = username;
  this.select_note.changed(selectAuthor);
  this.select_note.hide();

  this.div_notes = createDiv();
  this.div_notes.style('overflow','auto');
  this.div_notes.id('div_notes');

  this.div_sub;
  this.tab_sub = [];

  /**
   * [click => handle mouse click event]
   * @param  {[float]} mx               [mouse x position]
   * @param  {[float]} my               [mouse y position]
   */
  this.click = function(mx, my) {
    // Check to see if a point is inside the rectangle
    for(let n of this.notes) {
      if(this.div_notes.position().x<mx && mx<this.div_notes.position().x+n.elem.position().x+n.elem.size().width &&
      this.div_notes.position().y+n.elem.position().y-can.elt.offsetTop<my && my<this.div_notes.position().y+n.elem.position().y-can.elt.offsetTop+n.elem.size().height) {
        video.time(n.Time);
        break;
      }
    }

    for(let a of preparation_editor.actors_timeline) {
      a.click(mx, my);
    }
  };

  this.drop = function(mx, my) {

  }

  this.drag = function(mx, my) {

  }

  this.keyPressed = function(keyCode) {

  }

  this.mouseWheel = function(event) {

  }

  /**
   * [saveNote => save your annotation]
   */
  this.saveNote = function() {
    if(this.select_note.elt.value == username) {
      let tab = [];
      for(let n of this.notes) {
        if(n.Text!='') {
          tab.push({'Text':n.Text,'Time':n.Time});
        }
      }
      $.post({
        url: "save_note",
        async: true,
        data: {'abs_path': abs_path, 'notes':JSON.stringify(tab)},
        dataType: 'json',
        success: function (data) {
          // console.log(data);
        }
      });
    } else {
      alert('You have to select yout own notes');
    }
  }

  /**
   * [update => hide or show the table interface]
   * @param  {[Boolean]} checked               [is checkbox 'Table' checked]
   */
  this.update = function(checked) {
    if(checked) {
      this.showAllElt();
      annotation_editor.partition_editor.update(false);
      annotation_editor.is_partition_editor = false;
      annotation_editor.partition_check.checked(false);
      act_input.hide();
      let div_creation = select('#div_creation');
      if(!div_creation) {
        div_creation = createDiv();
      }
      div_creation.id('div_creation');
      div_creation.position(windowWidth-150,annotation_editor.partition_check.y+30);
      div_creation.size(150, height+can.elt.offsetTop-div_creation.position().y);
      for(let a of preparation_editor.actors_timeline) {
        a.on = false;
        a.elem.style('margin','5% 0 5% 0');
        a.elem.show();
        div_creation.child(a.elem.elt);
      }
    } else {

      this.hideAllElt();
      for(let a of preparation_editor.actors_timeline) {
        a.elem.remove();
        a.elem = createElement('h3', a.actor_name);
        a.elem.elt.contentEditable = 'true';
        a.elem.id('editor');
        preparation_editor.div_actors_timeline.child(a.elem);
      }

      // showAllElt();

    }
  }

  /**
   * [resizeElt => resize the elements]
   */
  this.resizeElt = function() {
    this.setDivSize();
    this.input_note.position(this.div_notes.position().x+10,can.elt.offsetTop+45);
    this.input_note.size(this.div_notes.size().width-15);
    this.resizeNoteBook();
  }

  /**
   * [resizeNoteBook => Create the notebook and resize it]
   * @return {[type]} [description]
   */
  this.resizeNoteBook = function() {
    let div_creation = select('#div_creation');
    if(!div_creation) {
      div_creation = createDiv();
      div_creation.id('div_creation');
      for(let a of actors_timeline) {
        a.on = false;
        a.elem.style('margin','5% 0 5% 0');
        div_creation.child(a.elem.elt);
      }
    }
    div_creation.position(windowWidth-150,annotation_editor.partition_check.y+30);
    div_creation.size(150, height+can.elt.offsetTop-div_creation.position().y);
    this.div_sub = select('#div_sub');
    // tab_sub = [];
    let cpt=0;
    for(let t of video.elt.textTracks) {
      if(t.mode == "showing" && cpt==0) {
        let i=0;
        cpt++;
        if(!this.div_sub) {
          this.tab_sub = [];
          this.div_sub = createDiv();
          this.div_sub.id('div_sub');
          this.div_sub.position(mid_width+10,can.elt.offsetTop);
          this.div_sub.size(((windowWidth - 160)-(mid_width+10))/2,height);
          for(let c of t.cues) {
            let obj = {};
            let p = createP(annotation_editor.partition_editor.parseTextAction(c.text));
            obj.p = p;
            obj.start = c.startTime;
            obj.end = c.endTime;
            this.tab_sub.push(obj);
            this.div_sub.child(p);
          }
        } else {
          this.div_sub.position(mid_width+10,can.elt.offsetTop);
          this.div_sub.size(((windowWidth - 160)-(mid_width+10))/2,height);
        }
      }
    }
  }

  /**
   * [displayTable => display 3 shots per actor (CU,MS,FS) below the player, sorted by the actor position on stage]
   * @param  {[list]} [printed_shots=undefined]               [note book shots]
   */
  this.displayTable = function(printed_shots=undefined) {
    let split_shot = [];

    if(printed_shots) {
      split_shot = printed_shots;
    }
    let bboxes = [];
    for(let s of split_shot) {
      if(s) {
        let b = s.getCurrStabShot(frame_num);
        if(b) {
          let bb = {};
          bb.bbox = b;
          bb.shot = s;
          bboxes.push(bb);
        }
      }
    }

    let max_by_raw=2;
    if(annotation_editor.is_note_book) {
      max_by_raw=0;
      for(let a of preparation_editor.actors_timeline) {
        if(a.on) {
          max_by_raw++;
        }
      }
    }
    let nb_raw = Math.ceil(bboxes.length/max_by_raw);
    if(nb_raw!=0 && offset_split>(nb_raw-1)*max_by_raw) {
      offset_split = (nb_raw-1)*max_by_raw;
    } else if(nb_raw==0) {
      offset_split = 0;
    }
    if(Math.ceil(offset_split/nb_raw)>0) {
      bboxes.splice(0,Math.ceil(offset_split/nb_raw)*max_by_raw);
    }
    let j=0;
    let y_vid=0;
    let x_vid=0;
    let max_h = 0;
    let curr_raw = 1;
    let total_height = 0;
    let total_width = 0;

    for(let b of bboxes) {
      let bb = b.bbox;
      let a_s = aspect_ratio;
      if(b.shot && b.shot.aspect_ratio) {
        a_s = b.shot.aspect_ratio;
      }
      let w=0;
      if(nb_raw==1) {
        w = viewer_width/bboxes.length;
      } else {
        if(j<(max_by_raw*curr_raw)){
          w = viewer_width/max_by_raw;
        } else {
          if(curr_raw==nb_raw) {
            w = viewer_height/(bboxes.length-(max_by_raw*curr_raw));
          } else {
            w = viewer_width/max_by_raw;
          }
          y_vid = max_h*curr_raw;
          max_h = 0;
          x_vid=0;
          curr_raw++;
        }
      }
      x_vid = (j%max_by_raw) * w;
      if(w/a_s>viewer_height) {
        w = viewer_height*a_s;
      }
      if(w/a_s>max_h) {
        max_h = w/a_s;
      }
      total_height += max_h;
      total_width += w;
      b.shot.bbox_show = [x_vid,viewer_height+y_vid,w,w/a_s];
      j++;
    }

    let sc = 1;
    let off_height = 0;
    let off_width = 0;
    if(max_by_raw!=0 && (height-viewer_height)<(total_height/max_by_raw)) {
      sc = (height-viewer_height)/(total_height/max_by_raw);
      off_height = viewer_height-bboxes[0].shot.bbox_show[1]*sc;
      off_width = (mid_width-(total_width/3)*sc)/2;
    }
    for(let b of bboxes) {
      b.shot.bbox_show = b.shot.bbox_show.map(x => x*sc);
      b.shot.bbox_show[1] = b.shot.bbox_show[1]+off_height;
      if(annotation_editor.is_note_book)
        b.shot.bbox_show[0] = b.shot.bbox_show[0]+off_width;
      let bb = b.bbox;
      let a_s = aspect_ratio;
      if(b.shot && b.shot.aspect_ratio) {
        a_s = b.shot.aspect_ratio;
      }
      if (bb) {
        // let acts = b.shot.getUpdateActInvolved();
        bbox = [bb[0]*scale_ratio, bb[1]*scale_ratio, bb[2]*scale_ratio, bb[3]*scale_ratio];
        if(bbox) {
          if(img_hd) {
            let ratio = img_hd.width / video.elt.videoWidth;
            bbox = [bbox[0]*ratio, bbox[1]*ratio, bbox[2]*ratio, bbox[3]*ratio];
            image(img_hd, b.shot.bbox_show[0],b.shot.bbox_show[1],b.shot.bbox_show[2],b.shot.bbox_show[3], bbox[0], bbox[1], bbox[2] - bbox[0], bbox[3] - bbox[1]);
          } else {
            image(image_frame, b.shot.bbox_show[0],b.shot.bbox_show[1],b.shot.bbox_show[2],b.shot.bbox_show[3],bbox[0], bbox[1], bbox[2] - bbox[0], bbox[3] - bbox[1]);
          }
          push();
          fill(255);
          text(b.shot.type/*+round_prec(a_s,2)*/, b.shot.bbox_show[0],b.shot.bbox_show[1]+10);
          for(var i=0; i<b.shot.actors_involved.length; i++) {
            text(b.shot.actors_involved[i].actor_name, b.shot.bbox_show[0],b.shot.bbox_show[1]+20+i*10);
          }
          pop();
        }
      }
    }
  }

  /**
   * [showNoteBook => Show the subtitle next to the player and display the notebook shots + allow the user to navigate by clicking on the shots]
   */
  this.showNoteBook = function() {

    if(this.tab_sub.length>1) {
      for(let s of this.tab_sub) {
        if(video.time() >= s.start && video.time() < s.end) {
          s.p.style('color','red');
          let pos = s.p.position().y - ($('#div_sub').height()/2);
          if(!dash_player.isPaused())
            $('#div_sub').scrollTop(pos);
        } else {
          s.p.style('color','rgb(50,50,50)');
        }
        s.p.style('font-size','18');
        s.p.elt.style.cursor = 'pointer';
      }
    }
    // getShotsFromActs();
    this.displayTable(montage_editor.getShotsFromActs());
    for(let s of montage_editor.shots) {
      if(s.type != 'WS')
        s.selected = false;
    }
    for(let s of montage_editor.getShotsFromActs()) {
      s.selected = true;
    }
    push();
    if(x_off<0){x_off=0;}
    if(y_off<0){y_off=0;}
    translate(x_off,y_off);
    scale(vid_h/Number(original_height));
    cadrage_editor.drawShotsLayout();
    pop();
  }

  /**
   * [hideAllElt => hide all the elements]
   * @return {[type]} [description]
   */
  this.hideAllElt = function() {
    this.input_note.hide();
    this.div_notes.hide();
    this.select_note.hide();
    for(let el of html_elements) {
      if(!el.side)
        el.hide();
    }
    if(this.div_sub) {
      $("#div_sub").remove();
      this.div_sub = undefined;
      this.tab_sub = [];
    }
  }

  /**
   * [showAllElt => Show all the elements]
   */
  this.showAllElt = function() {
    this.setDivSize();
    this.input_note.show();
    this.div_notes.show();
    this.select_note.show();
  }

  /**
   * [setDivSize => set the size of note part]
   */
  this.setDivSize = function() {
    this.div_notes.position(mid_width+((windowWidth - 160)-mid_width)/2,can.elt.offsetTop+70);
    this.div_notes.size(((windowWidth - 160)-mid_width)/2-10,height-70);
    this.select_note.position((windowWidth - 160)-100,can.elt.offsetTop+10);
    this.select_note.size((windowWidth - 160)-this.select_note.x-15);
  }

  /**
   * [addNote => add a note]
   */
  function addNote() {
    if(annotation_editor.note_editor.select_note.elt.value == username) {
      let note = {};
      let b = false;
      for(let n of annotation_editor.note_editor.notes) {
        if(n.Time == Math.floor(video.time())) {
          n.Text += "\n"+annotation_editor.note_editor.input_note.value()+'';
          b = true;
          break;
        }
      }
      if(!b) {
        note.Text = annotation_editor.note_editor.input_note.value()+'';
        note.Time = Math.floor(video.time());
        annotation_editor.note_editor.notes.push(note);
      }
      annotation_editor.note_editor.updateChilds();
      annotation_editor.note_editor.input_note.value('');
    } else {
      alert("Select your notes before editing")
    }
  }

  /**
   * [selectAuthor => select a user to show the related notes]
   */
  function selectAuthor() {
    for(let note of json_data_note) {
      if(note.User == this.elt.value)
        note_editor.notes = note.Note;
    }
    note_editor.updateChilds(this.elt.value);
  }

  this.updateChilds = function(name=username) {
    $('#div_notes').empty();
    this.notes.sort(compareTime);
    for(let note of this.notes) {
      let div = createDiv();
      let p = createP(note.Text);
      p.style('font-size','18');
      let h3 = createElement('h3',this.getTimeFrame(note.Time));
      if(name == username)
        p.elt.contentEditable = true;
      note.elem = p;
      div.child(h3);
      div.child(p);
      this.div_notes.child(div);
    }
  }

  /**
   * [getTimeFrame => convert sec to 'min:sec.mil']
   * @param  {[float]} sec               [time in seconds]
   */
  this.getTimeFrame = function(sec) {
    let s = sec%60;
    let m = Math.floor((sec/60));
    return ''+m+':'+s;
  }

  /**
   * [compareTime => sort note by time]
   * @param  {[note object]} a               [note a]
   * @param  {[note object]} b               [note b]
   */
  function compareTime(a,b) {
    if (a.Time < b.Time)
      return -1;
    if (a.Time > b.Time)
      return 1;
    return 0;
  }

  /**
   * [display => function called in the main p5 draw function]
   */
  this.display = function() {

    for(let note of this.notes) {
      note.Text = note.elem.elt.innerText;
    }
    push();
    stroke(0);
    line(this.div_notes.position().x+5,0,this.div_notes.position().x+5,height);
    noStroke();
    textSize(20);
    fill(50);
    text('Add a note :',this.div_notes.position().x+10,30);
    text(this.getTimeFrame(Math.floor(video.time())),this.div_notes.position().x+130,30);
    // let i =0;
    // for(let note of this.notes) {
    //   text(this.getTimeFrame(note.Frame)+'  '+note.Text, mid_width+10, 200+i*20);
    // }
    pop();
    push();
    fill(150);
    rect(0,viewer_height,mid_width,windowHeight-viewer_height);
    pop();
    this.showNoteBook();
  }
}
