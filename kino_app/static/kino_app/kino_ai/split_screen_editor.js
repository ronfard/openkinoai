/*
This file is part of KinoAi project.
It is subject to the license terms in the LICENSE file found in the top-level directory
*/

function SplitScreenEditor()  {

  this.split_screen_shots = [];

  this.states_optimized = {};

  this.split_screen_bboxes_sequence = {};

  this.is_display_split = false;
  this.is_display_player = false;
  this.is_active = false;

  this.is_create_split_screen = false;

  this.size_selector = createSelect();
  this.size_selector.side = false;
  this.size_selector.mouseOver(processToolTip('Select the rush size'));
  this.size_selector.mouseOut(processToolTip(''));
  html_elements.push(this.size_selector);
  this.size_selector.position(windowWidth/2 + 10, 40);
  this.size_selector.option('CU');
  this.size_selector.option('MCU');
  this.size_selector.option('MS');
  this.size_selector.option('MLS');
  this.size_selector.option('FS');
  this.size_selector.changed(selectSize);

  this.create_button = createButton('Create all rushes');
  this.create_button.side = false;
  this.create_button.mouseOver(processToolTip('Create all the rushes to export in splitscreen'));
  this.create_button.mouseOut(processToolTip(''));
  html_elements.push(this.create_button);
  this.create_button.mousePressed(createAllSplit);

  this.export_split_button = createButton('Export split');
  this.export_split_button.side = false;
  this.export_split_button.mouseOver(processToolTip('Export a video with the current name and size of shots'));
  this.export_split_button.mouseOut(processToolTip(''));
  html_elements.push(this.export_split_button);
  this.export_split_button.mousePressed(launchExportSplit);

  this.remove_split_button = createButton('Remove split');
  this.remove_split_button.side = false;
  this.remove_split_button.mouseOver(processToolTip('Remove the split screen shots'));
  this.remove_split_button.mouseOut(processToolTip(''));
  html_elements.push(this.remove_split_button);
  this.remove_split_button.mousePressed(removeSplitScreen);

  this.div_actors_split = createDiv();
  this.div_actors_split.side = false;
  html_elements.push(this.div_actors_split);

  this.slider_top = createSlider(0, Number(original_height)*3/4, Number(original_height)/4);
  this.slider_top.side = false;
  this.slider_top.mouseOver(processToolTip('Set the top position of the stage to crop the original video'));
  this.slider_top.mouseOut(processToolTip(''));
  this.slider_top.size(((windowWidth - 160)-mid_width),30);
  html_elements.push(this.slider_top);

  this.export_name = createP('Video name');
  this.export_name.elt.contentEditable = true;
  this.export_name.side = false;
  html_elements.push(this.export_name);
  this.export_name.style('margin',0);
  this.export_name.style('font-size',20);

  this.mousePressed = function(mx, my) {
  };

  this.drop = function(mx, my) {
  }

  this.drag = function(mx, my) {
  }

  this.keyPressed = function(keyCode) {
  }

  this.mouseWheel = function(event) {
  }

  /**
   * [updateAndShow => reset the tabs information and show the split screen tab objects]
   */
  this.updateAndShow = function() {
    resetTabs();
    clickTab(split_screen_editor_button);
    is_split_screen_editor = true;
    // // Side el
    updateSideElems();
    up_rough = true;
    this.showElts();
    submit.show();
    reload_button.show();
    hide_show_header_button.show();
    documentation_button.show();

    this.slider_top.show();
    this.is_display_split = true;
    this.is_display_player = true;
    updateCreateSplitScreen(split_screen_editor.states_optimized[this.size_selector.value()]==undefined);
  }

  /**
   * [resizeElt => resize the html elements]
   */
  this.resizeElt = function() {
    this.size_selector.original_x = mid_width + 90;
    this.size_selector.original_y = 12;

    this.create_button.original_x = mid_width+10;
    this.create_button.original_y = 240;

    this.slider_top.original_x = mid_width +10;
    this.slider_top.original_y = 200;
    this.slider_top.size(((windowWidth - 210)-mid_width),30);

    this.export_split_button.original_x = mid_width+10;
    this.export_split_button.original_y = 240;

    this.remove_split_button.original_x = mid_width+200;
    this.remove_split_button.original_y = 240;

    this.export_name.original_x = mid_width + 160;
    this.export_name.original_y = 55;

    this.div_actors_split.original_x= mid_width+100;
    this.div_actors_split.original_y= 50;
    this.div_actors_split.size(((windowWidth - 160)-mid_width-100));
  }

  /**
   * [hideElts => hide all the html elements related to the split screen tab]
   */
  this.hideElts = function() {
    this.is_display_split = false;
    this.create_button.hide();
    this.is_active = false;
    this.slider_top.hide();

    this.size_selector.hide();
    this.export_split_button.hide();
    this.remove_split_button.hide();
    this.export_name.hide();
  }

  /**
   * [showElts => show all the html elements related to the split screen tab]
   */
  this.showElts = function() {
    this.is_active = true;

    this.size_selector.show();
    // this.create_button.show();
    this.export_split_button.show();
    this.remove_split_button.show();
    this.export_name.show();
  }

  /**
   * [loadSplitScreenEditorData => load the split screen shots from the server]
   */
  this.loadSplitScreenEditorData = function() {
    for (var i=0; i<data_shots.length; i++) {
      if(data_shots[i].SplitScreenShot) {
        let s = new Shot();
        s.type = data_shots[i].Type;
        s.start_frame = data_shots[i].StartFrame;
        s.end_frame = data_shots[i].EndFrame;
        s.bboxes = data_shots[i].BBoxes;
        s.setActInvoled(data_shots[i].ActInvolved);
        if(s.actors_involved.length!=data_shots[i].ActInvolved.length) {
          s.temp_tab_act = data_shots[i].ActInvolved;
        }
        s.scenes_involved = [];
        if(data_shots[i].ScenesInvolved) {
          for(let scene of data_shots[i].ScenesInvolved) {
            if(scene.split('._.').length == 1) {
               s.scenes_involved.push(scene+'._.'+username);
            } else {
              s.scenes_involved.push(scene);
            }
          }
        }
        s.aspect_ratio = data_shots[i].AspectRatio;
        if(data_shots[i].GazeDir==undefined) {
          data_shots[i].GazeDir = false;
        }
        s.is_gaze_direction = data_shots[i].GazeDir;
        if(data_shots[i].KeepOut==undefined) {
          data_shots[i].KeepOut = false;
        }
        s.is_keep_out = data_shots[i].KeepOut;
        if(data_shots[i].PullIn==undefined) {
          data_shots[i].PullIn = false;
        }
        s.is_pull_in = data_shots[i].PullIn;
        if(data_shots[i].StagePos==undefined) {
          data_shots[i].StagePos = false;
        }
        s.is_stage_position = data_shots[i].StagePos;
        s.is_split_screen_shot = data_shots[i].SplitScreenShot;
        this.split_screen_shots.push(s);
      }
    }
    this.split_screen_shots.sort(sortShotsByName);
    this.split_screen_shots.sort(sortShotsByType);
  }

  /**
   * [loadSequenceSplitScreen => load the split screen layout from the server]
   * @param  {[list]} data               [layout information (one layout for each frame)]
   */
  this.loadSequenceSplitScreen = function(data) {
    split_screen_editor.states_optimized = {};
    for(let obj of data) {
      split_screen_editor.states_optimized[obj.Size] = obj.Sequence;
    }
  }

  /**
   * [loadSplitScreenSequences => load the split screen sequence of bounding boxes from the server]
   * @param  {[list]} data               [sequence (one bounding box for each actor for each frame)]
   */
  this.loadSplitScreenSequences = function(data) {
    split_screen_editor.split_screen_bboxes_sequence = {};
    for(let obj of data) {
      split_screen_editor.split_screen_bboxes_sequence[obj.Size] = obj.Shots;
    }
  }

  /**
   * [updateCreateSplitScreen => if the split screen of the selected size is not created show the interface to create it]
   * @param  {[boolean]} check_value               [is the the split screen already created]
   */
  function updateCreateSplitScreen(check_value) {
    split_screen_editor.is_create_split_screen = check_value;
    if(check_value) {
      split_screen_editor.div_actors_split.show();
      split_screen_editor.updateActorsDiv();
      split_screen_editor.create_button.show();
      split_screen_editor.export_split_button.hide();
      split_screen_editor.remove_split_button.hide();
      split_screen_editor.export_name.hide();
    } else {
      split_screen_editor.div_actors_split.hide();
      split_screen_editor.create_button.hide();
      split_screen_editor.export_split_button.show();
      split_screen_editor.remove_split_button.show();
      split_screen_editor.export_name.elt.innerText = document.getElementById('video_title').innerHTML + ' size ' + split_screen_editor.size_selector.value();
      split_screen_editor.export_name.show();
    }
  }

  /**
   * [updateActorsDiv => show the names of the actors on stage]
   */
  this.updateActorsDiv = function() {
    while(this.div_actors_split.elt.firstChild){this.div_actors_split.elt.firstChild.remove();}
    // this.div_actors_split.style('border','1px solid black');
    for(let p of preparation_editor.actors_timeline) {
      p.split = true;
      let name = createElement('h3',p.actor_name);
      name.style('float','left');
      name.style('border','2px solid grey');
      name.style('margin','5');
      name.style('padding','0 5');
      name.style('color','#2E5C9C');
      // name.elt.onclick = function() { selectActor(name,p); }
      name.src_video = p;
      this.div_actors_split.child(name);
    }
  }

  /**
   * [selectActor => include or remove an actor from the split screen]
   * @param  {[element]} elem               [html element ]
   * @param  {[actor]} act                [actor object]
   */
  function selectActor(elem, act) {
    if(elem.elt.style.color != 'rgb(46, 92, 156)') {
      elem.style('color','#2E5C9C');
      act.split = true;
    } else {
      elem.style('color','black');
      act.split = false;
    }
  }

  /**
   * [updateDisplaySplitScreen => if the split screen of the selected size is created display it]
   */
  function updateDisplaySplitScreen() {
    split_screen_editor.is_display_split = this.checked();
  }

  /**
   * [selectSize => select a size]
   */
  function selectSize() {
    if(split_screen_editor.states_optimized[split_screen_editor.size_selector.value()]) {
      updateCreateSplitScreen(false);
    } else {
      updateCreateSplitScreen(true);
    }
  }

  /**
   * [createAllSplit => create all the shots for the split screen and launch the stabilization]
   */
  function createAllSplit() {
    if(split_screen_editor.shots_in_creation == undefined) {
      split_screen_editor.shots_in_creation = true;
      split_screen_editor.size_in_creation = split_screen_editor.size_selector.value();
      let act_inv = [];
      for(let a of preparation_editor.actors_timeline) {
        // if(a.split) {
          act_inv.push(a);
        // }
      }
      split_screen_editor.createAllShotsPerSizeAndPos(split_screen_editor.size_selector.value(), act_inv);
    } else {
      alert('Wait until the end of the creation of the rushes for the split screen of size '+split_screen_editor.size_in_creation);
    }
  }

  /**
   * [callbackLastShot => this function is called when the last shot of the split screen is stabilized,
   * it launch the layout stabilization ]
   */
  this.callbackLastShot = function() {
    split_screen_editor.stabilizeLayout(split_screen_editor.size_in_creation);
    updateCreateSplitScreen(false);
  }

  /**
   * [callbackLastSplitAndMerge => this function is called when all the split and merge constraints are stabilized,
   * then it creates a sequence (one bounding box for each actor for each frame) and delete all the split screen shots]
   * @param  {[string]} size               [shot size]
   */
  this.callbackLastSplitAndMerge = function(size) {
    let shots_info = [];
    for(let i=0; i<total_frame;i++) {
      let frame_split_screen = [];
      for(let s of this.getRushesPerStateForAFrame(size,i)) {
        if(s) {
          let b = s.getCurrStabShot(i);
          if(b) {
            let bb = {};
            bb.bbox = b;
            bb.aspect_ratio = s.aspect_ratio;
            bb.type = s.type;
            bb.bbox_show;
            bb.actors_involved = [];
            for(var j=0; j<s.actors_involved.length; j++) {
              bb.actors_involved.push(s.actors_involved[j].actor_name);
            }
            frame_split_screen.push(bb);
          }
        }
      }
      shots_info.push(frame_split_screen);
    }
    split_screen_editor.split_screen_bboxes_sequence[size] = shots_info;
    split_screen_editor.shots_in_creation = undefined;
    split_screen_editor.size_in_creation = undefined;
    // console.log(shots_info);
    $.post({
      url: "save_split_screen_final",
      async: true,
      data: {'abs_path': abs_path, 'shots_info':JSON.stringify(shots_info), 'size':size},
      dataType: 'json',
      success: function (data) {
        console.log(data['message']);
        split_screen_editor.removeSplitShots();
      },
      error: function() {
      }
    });
  }

  /**
   * [getSplitShot => Get a shot with a set of actors and a size]
   * @param  {[string]} type                          [shot size]
   * @param  {[list]} actors_involved               [actors involved in the shot]
   * @param  {[integer]} number_acts                   [number of actors involved in the shot]
   * @return {[shot]}                 [the shot related to the parameter]
   */
  this.getSplitShot = function(type, actors_involved, number_acts) {
    let ret = undefined;
    let acts = [];
    for(let a of actors_involved) {
      acts.push(a.actor_name);
    }
    // let a_s = number_acts;
    let a_s = (((Number(original_width)*2)/preparation_editor.actors_timeline.length)*number_acts)/Number(original_height);
    for(let s of this.split_screen_shots) {
      if(s.type == type && s.aspect_ratio == a_s) {
        let b1 = true;
        for(let a of s.actors_involved) {
          if(!acts.includes(a.actor_name)) {
            b1 = false;
            break;
          }
        }
        if(b1 && s.actors_involved.length == acts.length) {
          ret = s;
          break;
        }
      }
    }
    return ret;
  }

  /**
   * [removeSplitScreen => remove the split screen (remove all the shots and layout) related to the selected size]
   */
  function removeSplitScreen() {
    if(confirm('Are you sure you want to remove all the split screen data of size '+split_screen_editor.size_selector.value()+'?') == true){
      split_screen_editor.states_optimized[split_screen_editor.size_selector.value()] = undefined;
      split_screen_editor.split_screen_bboxes_sequence[split_screen_editor.size_selector.value()] = undefined;
      updateCreateSplitScreen(true);
      $.post({
        url: "remove_split_screen",
        async: true,
        data: {'abs_path': abs_path, 'size':split_screen_editor.size_selector.value()},
        dataType: 'json',
        success: function (data) {
          console.log(data['message']);
        },
        error: function() {
        }
      });
      split_screen_editor.removeSplitShots();
    }
  }

  /**
   * [removeSplitShots => once the split screen layout is created, we keep only the framing data for each frame
   * and we remove the unnecessary shots to save disk space. This function removed all the split screen shots]
   */
  this.removeSplitShots = function() {
    while(this.split_screen_shots.length != 0) {
      for(let i=0; i<this.split_screen_shots.length; i++) {
        var shot = {};
        shot.Type = this.split_screen_shots[i].type;
        shot.ActInvolved = this.split_screen_shots[i].getActNameInvolved();
        shot.IsPersonalize = this.split_screen_shots[i].is_personalize;
        shot.ScenesInvolved = this.split_screen_shots[i].scenes_involved;
        shot.AspectRatio = this.split_screen_shots[i].aspect_ratio;
        shot.GazeDir = this.split_screen_shots[i].is_gaze_direction;
        shot.KeepOut = this.split_screen_shots[i].is_keep_out;
        shot.PullIn = this.split_screen_shots[i].is_pull_in;
        shot.StagePos = this.split_screen_shots[i].is_stage_position;
        shot.SplitScreenShot = this.split_screen_shots[i].is_split_screen_shot;
        $.post({
          url: "remove_shot",
          async: true,
          data: {'abs_path': abs_path, 'shot':JSON.stringify(shot)},
          dataType: 'json',
          success: function (data) {
            console.log('removed');
          }
        });
        this.split_screen_shots.splice(i,1);
      }
    }
  }

  /**
   * [removeSpecificShot => remove a specific shot based on the actors involved]
   * @param  {[list]} tab               [list of actors involved]
   */
  this.removeSpecificShot = function(tab) {
    for(let i=0;i<this.split_screen_shots.length;i++) {
      let s = this.split_screen_shots[i];
      if(s.actors_involved.length == tab.length) {
        let b = true;
        for(let a of s.actors_involved) {
          if(!tab.includes(a.actor_name)) {
            b =false;
            break;
          }
        }
        if(b){
          var shot = {};
          shot.Type = this.split_screen_shots[i].type;
          shot.ActInvolved = this.split_screen_shots[i].getActNameInvolved();
          shot.IsPersonalize = this.split_screen_shots[i].is_personalize;
          shot.ScenesInvolved = this.split_screen_shots[i].scenes_involved;
          shot.AspectRatio = this.split_screen_shots[i].aspect_ratio;
          shot.GazeDir = this.split_screen_shots[i].is_gaze_direction;
          shot.KeepOut = this.split_screen_shots[i].is_keep_out;
          shot.PullIn = this.split_screen_shots[i].is_pull_in;
          shot.StagePos = this.split_screen_shots[i].is_stage_position;
          shot.SplitScreenShot = this.split_screen_shots[i].is_split_screen_shot;
          $.post({
            url: "remove_shot",
            async: true,
            data: {'abs_path': abs_path, 'shot':JSON.stringify(shot)},
            dataType: 'json',
            success: function (data) {
              console.log('removed');
            }
          });
          this.split_screen_shots.splice(i,1);
          break;
        }
      }
    }
  }

  /**
   * [removeAllShotFromAName => remove all the shots where the actor is involved]
   * @param  {[string]} actor_name               [actor name]
   */
  this.removeAllShotFromAName = function(actor_name) {
    for(let i=0;i<this.split_screen_shots.length;i++) {
      let s = this.split_screen_shots[i];
      let tab_name = [];
      for(let a of s.actors_involved) {
        tab_name.push(a.actor_name);
      }
      if(tab_name.includes(actor_name)){
        var shot = {};
        shot.Type = this.split_screen_shots[i].type;
        shot.ActInvolved = this.split_screen_shots[i].getActNameInvolved();
        shot.IsPersonalize = this.split_screen_shots[i].is_personalize;
        shot.ScenesInvolved = this.split_screen_shots[i].scenes_involved;
        shot.AspectRatio = this.split_screen_shots[i].aspect_ratio;
        shot.GazeDir = this.split_screen_shots[i].is_gaze_direction;
        shot.KeepOut = this.split_screen_shots[i].is_keep_out;
        shot.PullIn = this.split_screen_shots[i].is_pull_in;
        shot.StagePos = this.split_screen_shots[i].is_stage_position;
        shot.SplitScreenShot = this.split_screen_shots[i].is_split_screen_shot;
        $.post({
          url: "remove_shot",
          async: true,
          data: {'abs_path': abs_path, 'shot':JSON.stringify(shot)},
          dataType: 'json',
          success: function (data) {
            console.log('removed');
          }
        });
        this.split_screen_shots.splice(i,1);
      }
    }
  }

  /**
   * [recursAllShots => compute all the combinations of actors possible for the split screen]
   * @return {[list]} [list of set of actors]
   */
  this.recursAllShots = function() {
    let act_inv = [];
    var tab_res = [];
    for(let a of preparation_editor.actors_timeline){
      act_inv.push(a.actor_name);
    }
    this.recursion(act_inv,tab_res);
    let acts_tab_result = [];
    for(let tab of tab_res) {
      let temp_tab = [];
      for(let name of tab) {
        temp_tab.push(preparation_editor.getAct(name));
      }
      acts_tab_result.push(temp_tab);
    }
    return acts_tab_result;
  }

  /**
   * [testNameInTab => Check if a name is included in a list of name]
   * @param  {[string]} names_to_add               [actor name]
   * @param  {[list]} tab_res                    [lsit of actor name]
   * @return {[boolean]}              [true if the name is included]
   */
  this.testNameInTab = function(names_to_add, tab_res) {
    let b = false;
    for(let tab of tab_res) {
      if(names_to_add.length == tab.length) {
        let b1 = true;
        for(let name of tab) {
          if(!names_to_add.includes(name)) {
            b1 = false;
            break;
          }
        }
        if(b1) {
          b = true;
          break;
        }
      }
    }
    return b;
  }

  /**
   * [recursion => recursive function called in recursAllShots]
   * @param  {[list]} act_inv               [actors involved]
   * @param  {[list]} tab_res               [list of actors combination]
   */
  this.recursion = function(act_inv,tab_res) {
    if(act_inv.length==1) {
      if(!this.testNameInTab(act_inv,tab_res)){
        tab_res.push(act_inv);
      }
      return tab_res;
    } else {
      if(!this.testNameInTab(act_inv,tab_res)) {
        tab_res.push(act_inv);
      }
      for(let a of act_inv) {
        let act_inv_2 = Array.from(act_inv);
        act_inv_2.splice(act_inv_2.indexOf(a),1);
        this.recursion(act_inv_2,tab_res);
      }
    }
  }

  /**
   * [createAllShotsPerSizeAndPos => create all the shots based on the actors combination and the size.
   * If there are 4 actors on stage, it will create 15 shots]
   * @param  {[string]} size                              [shot size]
   * @param  {[list]} [act_inv=undefined]               [actors involved]
   */
  this.createAllShotsPerSizeAndPos = function(size, act_inv=undefined) {
    this.removeSplitShots();
    let tab_act_name = [];
    if(!act_inv) {
      act_inv = preparation_editor.actors_timeline;
    }
    for(let a of act_inv) {
      tab_act_name.push(a.actor_name);
    }
    cadrage_editor.is_keep_out = true;
    cadrage_editor.keep_out.checked(true);
    cadrage_editor.is_pull_in = false;
    cadrage_editor.pull_in.checked(false);
    cadrage_editor.is_gaze_direction = false;
    cadrage_editor.gaze_direction.checked(false);
    cadrage_editor.is_stage_position = false;
    cadrage_editor.stage_position.checked(false);

    let tab_result = this.recursAllShots();

    for(let act_tab of tab_result) {
      for(let act of preparation_editor.actors_timeline) {
        act.on = false;
      }
      let a_s = (((Number(original_width)*2)/act_inv.length)*act_tab.length)/Number(original_height);
      // console.log(a_s);
      // let a_s = 1*act_tab.length;
      let shot = new Shot();
      for(let a of act_tab) {
        a.on = true;
        shot.actors_involved.push(a);
      }
      shot.type = size;
      shot.aspect_ratio = a_s;
      shot.is_keep_out = true;
      shot.is_pull_in = false;
      shot.is_stage_position = false;
      shot.is_gaze_direction = false;
      shot.is_split_screen_shot = true;
      let b = false;
      for(let s of this.split_screen_shots) {
        if(s.equalTo(shot, false)) {
          b = true;
          break;
        }
      }
      if(!b) {
        shot.start_frame = 0;
        shot.end_frame = Math.round(frame_rate*video.duration());
        shot.calcBboxes(a_s);
        this.split_screen_shots.push(shot);
        if(shot.actors_involved.length>=1) {
          cadrage_editor.add_shot.push(shot);
          shot.in_stabilize = true;
        }
      }
    }

    this.split_screen_shots.sort(sortShotsByName);
    this.split_screen_shots.sort(sortShotsByType);
  }

  /**
   * [getSplitShotOnlyOneActor => get a shot where only one specified actor is involved]
   * @param  {[string]} name               [actor name]
   * @return {[shot]}      [shot object]
   */
  this.getSplitShotOnlyOneActor = function(name) {
    let ret;
    for(let s of this.split_screen_shots) {
      if(s.actors_involved.length == 1) {
        if(s.actors_involved[0].actor_name == name) {
          ret = s;
          break;
        }
      }
    }
    return ret;
  }

  /**
   * [getLayoutFromState => return a list of actor sorted by their stage position based on a specific layout]
   * @param  {[integer]} state_index                                  [layout index]
   * @param  {[list]} [actors_sort_by_pos=undefined]               [list of actors]
   * @return {[type]}                                [description]
   */
  this.getLayoutFromState = function(state_index, actors_sort_by_pos=undefined) {
    if(!actors_sort_by_pos) {
      actors_sort_by_pos = preparation_editor.actors_timeline;
      actors_sort_by_pos.sort(sortByActPosition);
    }
    let state_layout = this.getStatesLayout(actors_sort_by_pos)[state_index];
    let index = 0;
    let layout_actor = [];
    let temp = [];
    let index_digit = 0;
    for(let digit of state_layout) {
      if(digit == '0') {
        if(temp.length!=0) {
          layout_actor.push(temp);
          temp = [];
          index++;
        }
        if(!((index_digit > 0 && index_digit < state_layout.length) && (state_layout[index_digit-1]=='1' && state_layout[index_digit+1]=='1'))) {
          layout_actor.push([actors_sort_by_pos[index].actor_name]);
          index++;
          if(index_digit == state_layout.length-1 && index == actors_sort_by_pos.length-1) {
            layout_actor.push([actors_sort_by_pos[index].actor_name]);
          }
        } else {
          temp.push(actors_sort_by_pos[index].actor_name);
          index++;
        }
      } else {
        if(temp.length==0 && index+1 < actors_sort_by_pos.length) {
          temp.push(actors_sort_by_pos[index].actor_name,actors_sort_by_pos[index+1].actor_name);
          if(index+1 == actors_sort_by_pos.length-1) {
            layout_actor.push(temp);
          }
          index+=1;
        } else{
          if(index == actors_sort_by_pos.length-1) {
            temp.push(actors_sort_by_pos[index].actor_name);
            layout_actor.push(temp);
            temp = [];
          }
          if(index+1 < actors_sort_by_pos.length) {
            temp.push(actors_sort_by_pos[index+1].actor_name);
          }
          if(index+1 == actors_sort_by_pos.length-1) {
            layout_actor.push(temp);
            temp = [];
          }
          index++;
        }
      }
      index_digit++;
    }
    // console.log(layout_actor);
    return layout_actor;
  }

  this.getRushesPerStateForAFrame = function(size='FS', f_num=undefined) {
    if(f_num == undefined) {
      f_num = frame_num;
    }
    let layout_shot = [];
    if(this.states_optimized[size]) {
      let actors_sort_by_pos = [];
      for(let a of preparation_editor.actors_timeline) {
        actors_sort_by_pos.push(a);
      }
      for(let a of actors_sort_by_pos) {
        a.layout_frame_num = f_num;
      }
      actors_sort_by_pos.sort(sortByActPositionPerFrame);
      for(let acts of this.getLayoutFromState(this.states_optimized[size][f_num],actors_sort_by_pos)) {
        let tab = [];
        for(let a of acts) {
          tab.push(preparation_editor.getAct(a));
        }
        layout_shot.push(this.getSplitShot(size, tab, tab.length));
      }
      return layout_shot;
    }
  }

  this.getActorsInvolvedForAFrame = function(size='FS', f_num=undefined) {
    if(f_num == undefined) {
      f_num = frame_num;
    }

    if(this.states_optimized[size]) {
      let actors_sort_by_pos = [];
      for(let a of preparation_editor.actors_timeline) {
        actors_sort_by_pos.push(a);
      }
      for(let a of actors_sort_by_pos) {
        a.layout_frame_num = f_num;
      }
      actors_sort_by_pos.sort(sortByActPositionPerFrame);
      return this.getLayoutFromState(this.states_optimized[size][f_num], actors_sort_by_pos);
    }else {
      return [];
    }
  }

  // Get all frames where there is a transition of split screen layout
  this.getTransitionsPoints = function(size = 'FS') {
    let frames_of_transition = [];
    let layout_for_split_screen = [];

    // Find all the transition frame where there is a change of layout
    if(this.states_optimized[size]) {
      let curr_state = this.states_optimized[size][0];
      layout_for_split_screen.push(curr_state);
      for(let i = 0; i<this.states_optimized[size].length; i++) {
        if(curr_state!=this.states_optimized[size][i]){
          frames_of_transition.push(i);
          curr_state = this.states_optimized[size][i];
          layout_for_split_screen.push(curr_state);
        }
      }
    }

    // For each transition frame find the actors involved in the shots
    let rushes_transition = [];
    for(let frame of frames_of_transition) {
      // Actors and shots for pre transition frame
      rushes_transition.push(this.getActorsInvolvedForAFrame(size,frame-1));
      // Actors and shots for transition frame
      rushes_transition.push(this.getActorsInvolvedForAFrame(size,frame));
    }

    let i = 0;
    let index_frame = 0;
    let final_array = [];
    while(i<rushes_transition.length) {
      let pre_shot;
      let post_shot;
      let merge_shot;
      let shot_merged = {};
      let split_shot;
      let shot_splited = {};
      let actors_sort_by_pos = [];
      for(let a of preparation_editor.actors_timeline) {
        actors_sort_by_pos.push(a);
      }
      for(let a of actors_sort_by_pos) {
        a.layout_frame_num = frames_of_transition[index_frame];
      }
      actors_sort_by_pos.sort(sortByActPositionPerFrame);
      for(let a of actors_sort_by_pos) {
        // Get the pre transition shot where actor a is involved
        for(let s of rushes_transition[i]) {
          if(s.includes(a.actor_name)) {
            pre_shot = s;
            break;
          }
        }
        // Get the post transition shot where actor a is involved
        for(let s of rushes_transition[i+1]) {
          if(s.includes(a.actor_name)) {
            post_shot = s;
            break;
          }
        }
        // Test if it's a merge or a split
        if(pre_shot.length < post_shot.length) {
          // console.log('merge',pre_shot,' to ',post_shot,i);
          if(!merge_shot) {
            merge_shot = post_shot;
            shot_merged['higher'] = merge_shot;
            shot_merged['lower'] = [];
            shot_merged['lower'].push(pre_shot);
          }else {
            if(!shot_merged['lower'].includes(pre_shot)) {
              shot_merged['lower'].push(pre_shot);
            }
          }
        } else if(pre_shot.length > post_shot.length){
          // console.log('split ',pre_shot,' to ',post_shot,i);
          if(!split_shot) {
            split_shot = pre_shot;
            shot_splited['higher'] = split_shot;
            shot_splited['lower'] = [];
            shot_splited['lower'].push(post_shot);
          }else {
            if(!shot_splited['lower'].includes(post_shot)) {
              shot_splited['lower'].push(post_shot);
            }
          }
        } else {
          // console.log('no change ',pre_shot,post_shot,i);
        }
      }
      final_array.push({'frame':frames_of_transition[index_frame],'merge':shot_merged,'split':shot_splited});
      i+=2;
      index_frame++;
    }
    // console.log('final_array',final_array);
    // console.log([final_array,frames_of_transition,layout_for_split_screen,rushes_transition]);
    return final_array;
  }

  // For each transitions points compute the higer and lower order constraints
  this.getTransitionsConstraints = function(size='FS') {
    let shots_for_stabilize = [];
    for(let obj of this.getTransitionsPoints(size)) {
      let shots_for_merge = this.getConstraintsFromHigherLowerOrder(obj.merge,obj.frame,size,'merge');
      let shots_for_split = this.getConstraintsFromHigherLowerOrder(obj.split,obj.frame,size,'split');
      for(let s of shots_for_merge.concat(shots_for_split)) {
        let is_added = false;
        for(let old_s of shots_for_stabilize) {
          if(s.equalTo(old_s)) {
            is_added = true;
            break;
          }
        }
        if(!is_added) {
          shots_for_stabilize.push(s);
        }
      }
    }
    for(let s of shots_for_stabilize) {
      s.stabilizeSplitAndMerge();
    }
    return shots_for_stabilize;
  }

  // Set the constraint for all the constrained shot + return the shots
  this.getConstraintsFromHigherLowerOrder = function(obj, frame, size='FS', type) {
    let lower_shots = [];
    if(obj.higher) {
      let tab_act = [];
      for(let name of obj.higher) {
        tab_act.push(preparation_editor.getAct(name));
      }
      // Get the higher order shot object
      let higher_shot = this.getSplitShot(size, tab_act, tab_act.length);
      if(obj.lower && obj.higher.length == obj.lower.flat().length) {
        let offset_index = 0;
        // Compute the constraints for the lower order shots
        for(let low of obj.lower) {
          tab_act = [];
          for(let name of low){
            tab_act.push(preparation_editor.getAct(name));
          }
          let lower_shot = this.getSplitShot(size, tab_act, tab_act.length);
          let constraint_obj = {};
          let bbox_higher = higher_shot.getCurrStabShot(frame);
          let offset = (bbox_higher[2]-bbox_higher[0])*(offset_index/obj.higher.length);
          constraint_obj['X'] = bbox_higher[0]+offset;
          constraint_obj['W'] = ((bbox_higher[2]-bbox_higher[0])/obj.higher.length)*tab_act.length;
          constraint_obj['Y'] = bbox_higher[1];
          constraint_obj['H'] = bbox_higher[3] - bbox_higher[1];
          constraint_obj['Frame'] = frame;
          constraint_obj['Act'] = low;
          constraint_obj['Higher'] = obj.higher;
          lower_shot.constraints_split_screen.push(constraint_obj);
          lower_shots.push(lower_shot);
          offset_index+=tab_act.length;
        }
      } else if(obj.lower) {
        for(let low of obj.lower) {
          let index_offset = obj.higher.indexOf(low[0]);
          tab_act = [];
          for(let name of low){
            tab_act.push(preparation_editor.getAct(name));
          }
          let lower_shot = this.getSplitShot(size, tab_act, tab_act.length);
          let constraint_obj = {};
          let bbox_higher = higher_shot.getCurrStabShot(frame);
          let offset = (bbox_higher[2]-bbox_higher[0])*(index_offset/obj.higher.length);
          constraint_obj['X'] = bbox_higher[0]+offset;
          constraint_obj['W'] = ((bbox_higher[2]-bbox_higher[0])/obj.higher.length)*tab_act.length;
          constraint_obj['Y'] = bbox_higher[1];
          constraint_obj['H'] = bbox_higher[3] - bbox_higher[1];
          constraint_obj['Frame'] = frame;
          constraint_obj['Act'] = low;
          constraint_obj['Higher'] = obj.higher;
          constraint_obj['Bbox higher'] = bbox_higher;
          lower_shot.constraints_split_screen.push(constraint_obj);
          lower_shots.push(lower_shot);
        }
      }
    }

    return lower_shots;
  }

  // Add 0 if str len != len digit
  function fillWithZeroDigit(str, len) {
    if(str.length == len) {
      return str;
    } else {
      return fillWithZeroDigit('0'+str,len);
    }
  }

  this.getStatesLayout = function(actors_involved=undefined) {
    if(!actors_involved) {
      actors_involved = preparation_editor.actors_timeline;
    }
    let states = [];
    for(let i=0;i<Math.pow(2,actors_involved.length-1);i++) {
      states.push(fillWithZeroDigit(i.toString(2),actors_involved.length-1));
    }
    return states;
  }

  function sortByActPositionPerFrame(a, b) {
    if(a.layout_frame_num!=undefined) {
      if (a.getActPosition(a.layout_frame_num).x < b.getActPosition(a.layout_frame_num).x)
        return -1;
      if (a.getActPosition(a.layout_frame_num).x > b.getActPosition(a.layout_frame_num).x)
        return 1;
      return 0;
    }
  }

  // Get the layout information (actor position) for each frame
  this.getLayoutActorsInformations = function(size_shot='MS', act_inv=undefined) {
    if(!act_inv) {
      act_inv = preparation_editor.actors_timeline;
    }
    let layout_tab = [];
    for(let i=0; i<total_frame;i++) {
      for(let a of act_inv) {
        a.layout_frame_num = i;
      }
      act_inv.sort(sortByActPositionPerFrame);
      let frame_layout = [];
      for(let a of act_inv) {
        let act_info = {};
        act_info.actor_name = a.actor_name;
        act_info.center_x = a.getActPosition(i).x;
        let s_act;
        let shot_act = this.getSplitShot(size_shot, [a], 1);
        if(shot_act) {
          s_act = (shot_act.bboxes[i][2] - shot_act.bboxes[i][0])/2;
        }
        act_info.half_width = s_act;
        frame_layout.push(act_info);
      }
      layout_tab.push(frame_layout);
    }
    return layout_tab;
  }

  this.stabilizeLayout = function(size) {
    $.post({
      url: "split_screen_layout_configuration",
      async: true,
      data: {'abs_path': abs_path, 'layout_actors_info':JSON.stringify(split_screen_editor.getLayoutActorsInformations(size)), 'size':size},
      dataType: 'json',
      success: function (data) {
        split_screen_editor.states_optimized[data['size']] = data['data'][2];
        let cons = split_screen_editor.getTransitionsConstraints(data['size']);
        if(cons.length == 0) {
          split_screen_editor.callbackLastSplitAndMerge(data['size']);
        }
      },
      error: function() {
        split_screen_editor.removeSplitShots();
        split_screen_editor.shots_in_creation = undefined;
        split_screen_editor.size_in_creation = undefined;
      }
    });
  }

  function sortSplit(a,b) {
    if (a.bbox[0] < b.bbox[0])
      return -1;
    if (a.bbox[0] > b.bbox[0])
      return 1;
    return 0;
  }

  /**
   * [displaySplitScreen => display the split screen in the video player.
   * The split screen shots are displayed in the upper part of the screen and the original video in the lower part]
   * @param  {[string]} size               [size of the split screen]
   */
  this.displaySplitScreen = function(size) {
    let bboxes = this.split_screen_bboxes_sequence[size][frame_num];

    let total_height = Math.min(viewer_height/2,mid_width/(aspect_ratio*2));
    let total_width = total_height*aspect_ratio*2;
    let off_x = (mid_width-total_width)/2;
    let off_w = 0;
    for(let b of bboxes) {
      b.bbox_show = [off_x+off_w,y_off-vid_h/2,total_height*b.aspect_ratio,total_height];
      off_w += total_height*b.aspect_ratio;
    }
    let sc = 1;
    let off_height = 0;
    let off_width = 0;

    for(let b of bboxes) {
      b.bbox_show = b.bbox_show.map(x => x*sc);
      b.bbox_show[1] = b.bbox_show[1]+off_height;
      let bb = b.bbox;
      let a_s = aspect_ratio;
      if(b.aspect_ratio) {
        a_s = b.aspect_ratio;
      }
      if (bb) {
        bbox = [bb[0]*scale_ratio, bb[1]*scale_ratio, bb[2]*scale_ratio, bb[3]*scale_ratio];
        if(bbox) {
          if(img_hd) {
            let ratio = img_hd.width / video.elt.videoWidth;
            bbox = [bbox[0]*ratio, bbox[1]*ratio, bbox[2]*ratio, bbox[3]*ratio];
            image(img_hd, b.bbox_show[0],b.bbox_show[1],b.bbox_show[2],b.bbox_show[3], bbox[0], bbox[1], bbox[2] - bbox[0], bbox[3] - bbox[1]);
          } else {
            image(image_frame, b.bbox_show[0],b.bbox_show[1],b.bbox_show[2],b.bbox_show[3],bbox[0], bbox[1], bbox[2] - bbox[0], bbox[3] - bbox[1]);
          }
          push();
          fill(255);
          text(b.type, b.bbox_show[0],b.bbox_show[1]+10);
          for(var i=0; i<b.actors_involved.length; i++) {
            text(b.actors_involved[i], b.bbox_show[0],b.bbox_show[1]+20+i*10);
          }
          pop();
        }
      }
    }
  }

  function launchExportSplit() {
    split_screen_editor.exportFullSplitScreen(split_screen_editor.size_selector.value());
  }

  // Launch the export of a splitscreen video based on a size for the rushes and a top position for crop the original video
  this.exportFullSplitScreen = function(size='FS') {
    let tab_split_screen_bboxes = [];
    if(split_screen_editor.states_optimized[size]) {
      for(let split_frame of split_screen_editor.split_screen_bboxes_sequence[size]) {
        let bboxes_frame = [];
        for(let obj of split_frame) {
          if(obj.actors_involved.length > 1) {
            let nb_acteurs = obj.actors_involved.length;
            let w = (obj.bbox[2]-obj.bbox[0])/nb_acteurs;
            for(let j=0;j<nb_acteurs;j++) {
              let split_bbox = [obj.bbox[0]+w*j,obj.bbox[1],(obj.bbox[0]+w*j)+w,obj.bbox[3]];
              bboxes_frame.push(split_bbox);
            }
          } else {
            bboxes_frame.push(obj.bbox);
          }
        }
        tab_split_screen_bboxes.push(bboxes_frame);
      }
      $.post({
        url: "reframeMov",
        data: {'abs_path': abs_path, 'bboxes':JSON.stringify(tab_split_screen_bboxes), 'is_full_split_screen':true, 'top_position':this.slider_top.value(),'t_start':0,
        'name_export':split_screen_editor.export_name.elt.innerText, 't_end':video.duration(), 'width':Number(original_width), 'aspect_ratio':aspect_ratio},
        dataType: 'json',
        success: function (data) {
          console.log(data);
          return callbackExportSplitScreen(data);
        }
      });
      const intervalLength = 2000;
      const interval = setInterval(() => {
        $.post({
          url: "check_progress_bar",
          data: {'abs_path':abs_path},
          dataType: 'json',
          success: function (data) {
            split_screen_editor.progress_value = int(data['progress']);
            if(split_screen_editor.progress_value>=100) {
              clearInterval(interval);
              split_screen_editor.progress_value=undefined;
            }
            // return callbackReframe(data);
          }
        });
      }, intervalLength);
    } else {
      alert('The rushes of size '+size+' are not created');
    }
  }

  function callbackExportSplitScreen(data) {
    console.log(data);
    let a = createA(data['src']);
    a.elt.download = split_screen_editor.export_name.elt.innerText;
    a.id('click');
    a.hide();
    // console.log(a);
    document.getElementById('click').click();
    a.remove();
  }

  // Display the framing red for the current layout, yellow for the next one
  this.displayFramingLayoutSplitScreen = function(size='FS') {
    if(this.split_screen_bboxes_sequence[size]) {
      for(let obj of this.split_screen_bboxes_sequence[size][frame_num]){
        push();
        strokeWeight(3);
        stroke('red');
        noFill();
        rect(obj.bbox[0], obj.bbox[1], obj.bbox[2]-obj.bbox[0], obj.bbox[3]-obj.bbox[1]);
        pop();
      }
    }
  }

  /**
   * [display => function called in the main p5 draw function]
   */
  this.display = function() {

    const top_position_split = this.slider_top.value();

    if(this.is_display_split) {
      if(this.states_optimized[this.size_selector.value()] && this.split_screen_bboxes_sequence[this.size_selector.value()]) {
        push();
        if(x_off<0){x_off=0;}
        if(y_off<0){y_off=0;}
        if(this.is_display_player) {
          y_off+=vid_h/2;
          translate(x_off,y_off-this.slider_top.value()/(Number(original_height)/vid_h));
          scale(vid_h/Number(original_height));
        }
        this.displayFramingLayoutSplitScreen(this.size_selector.value());
        pop();
        this.displaySplitScreen(this.size_selector.value());
      }
    }

    if(this.is_display_player) {
      push();
      fill(0);
      textSize(20);
      text('Top position :',mid_width+10, 140);
      text(round_prec((top_position_split/Number(original_height))*100,2),this.slider_top.x+(top_position_split/Number(this.slider_top.elt.max))*this.slider_top.size().width,185);
      text(0,mid_width+10,170);
      text(3/4*100,mid_width+this.slider_top.size().width+10,170);
      pop();
    }
    push();
    fill(0);
    textSize(20);
    text('Size: ',mid_width+10,30);
    if(this.is_create_split_screen) {
      text('Actors: ',mid_width+10,70);
    } else {
      text('Export name: ',mid_width+10,70);
    }
    if(this.shots_in_creation) {
      textSize(15);
      text("Split screen rushes are in processing leave the window open",mid_width+10,310);
    }
    pop();

    // Display the progression bar when the video is exported
    push();
    fill(0);
    textSize(20);
    if(this.progress_value!=undefined) {
      text('Progression',mid_width+10,height-180);
      fill('#2E5C9C');
      rect(mid_width+10,height-150,(windowWidth-170)-(mid_width+10),15);
      fill("#28A1A8");
      rect(mid_width+10,height-150,((windowWidth-170)-(mid_width+10))*(this.progress_value/100),15);
      fill(255);
      textSize(15);
      text(this.progress_value+"%",mid_width+10,height-138);
    }
    pop();

  }
}
