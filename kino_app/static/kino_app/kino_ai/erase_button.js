/*
This file is part of KinoAi project.
It is subject to the license terms in the LICENSE file found in the top-level directory
*/

/**
 * [EraseButton => this object create a button related to an actor timeline]
 * @param       {[Number]} ind          [index of the actor timeline]
 * @param       {Number} [tempX=0]    [x position]
 * @param       {Number} [tempY=0]    [y position]
 * @param       {Number} [tempRad=5]  [radius]
 * @constructor
 */
function EraseButton(ind, tempX=0, tempY=0, tempRad = 5)  {

  // Is the button on or off?
  // Button always starts as off

  this.x = tempX;
  this.y = tempY;

  this.rad = tempRad;

  /**
   * [click => handle mouse click event]
   * @param  {[float]} mx               [mouse x position]
   * @param  {[float]} my               [mouse y position]
   */
  this.click = function(mx, my) {
    // Check to see if a point is inside the rectangle
    if (mx > this.x - this.rad && mx < this.x + this.rad && my > this.y - this.rad && my < this.y + this.rad) {
      this.on = !this.on;
    }
  };

  /**
   * [setPosition => set the position of the button]
   * @param  {[integer]} tX               [x position]
   * @param  {[integer]} tY               [y position]
   */
  this.setPosition = function(tX, tY) {
    this.x = tX;
    this.y = tY;
  }

  /**
   * [setRad => set the radius of the button]
   * @param  {[integer]} ra               [radius size]
   */
  this.setRad = function(ra) {
    this.rad = ra;
  }

  /**
   * [display => function called in the main p5 draw function]
   */
  this.display = function() {
    if(this.y > preparation_editor.div_actors_timeline.elt.offsetTop-can.elt.offsetTop) {
      push();
      stroke('red');
      strokeWeight(3);
      let x1=this.x-this.rad;
      let y1=this.y-this.rad;
      let x2=this.x+this.rad;
      let y2=this.y+this.rad;
      line(x1,y1,x2,y2);
      line(x1,y2,x2,y1);
      pop();
    }
  }

}
