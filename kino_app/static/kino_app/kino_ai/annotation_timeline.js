/*
This file is part of KinoAi project.
It is subject to the license terms in the LICENSE file found in the top-level directory
*/

/**
 * [AnnotationTimeline => Create an annotation timeline. One timeline per actor, and a list of actions (playing, moving, speaking)]
 * @constructor
 */
function AnnotationTimeline()  {
  this.x  = 0;
  this.y  = 0;
  this.w  = 0;
  this.h  = 0;
  this.on = false;

  this.scale = 1;

  this.move = true;

  this.actors_annotation = [];
  this.erase_button = [];

  this.div_wrap = createDiv();
  this.div_wrap.style('overflow','auto');
  this.div_wrap.id('div_wrap');
  this.div_wrap.hide();

  this.curr_action = {'drag':false};

  this.actions_available = [{'name':'Playing','color':'blue'},{'name':'Moving','color':'green'},{'name':'Speaking','color':'orange'}];

  //action creator editor

  //actor addition

  /**
   * [click => handle click event]
   * @param  {[float]} mx               [mouse x position]
   * @param  {[float]} my               [mouse y position]
   */
  this.click = function(mx, my) {
    // Check to see if a point is inside the rectangle
    for(let a of this.actions_available) {
      if(mx > a.x && mx < a.x + a.w && my > a.y-20 && my < a.y-20 + a.h+20) {
        this.curr_action =a;
        this.curr_action.drag = true;
        if(keyIsPressed) {
          this.addAction();
        }
        break;
      }
    }
    for(let a of this.actors_annotation) {
      a.click(mx, my);
    }
    for (var i = 0; i < this.erase_button.length; i++) {
      this.erase_button[i].on = false;
      this.erase_button[i].click(mouseX, mouseY);
      if(this.erase_button[i].on && this.erase_button[i]) {
        this.actors_annotation[i].elem.remove();
        this.actors_annotation.splice(i,1);
        this.erase_button.splice(i, 1);
      }
    }
  };

  /**
   * [drop => handle drop event]
   * @param  {[float]} mx               [mouse x position]
   * @param  {[float]} my               [mouse y position]
   */
  this.drop = function(mx, my) {
    for(let a of this.actors_annotation) {
      a.drop(mx, my, this.curr_action);
    }
    this.curr_action.drag = false;
  }

  /**
   * [drag => handle drag event]
   * @param  {[float]} mx               [mouse x position]
   * @param  {[float]} my               [mouse y position]
   */
  this.drag = function(mx, my) {
    if(this.curr_action.drag) {
      this.curr_action.x  = mx;
      this.curr_action.y  = my;
    }
    for(let a of this.actors_annotation) {
      a.drag(mx,my);
    }
  }

  /**
   * [keyPressed => handle keyPressed event]
   * @param  {[key code]} keyCode               [key pressed code]
   */
  this.keyPressed = function(keyCode) {
    for(let a of this.actors_annotation) {
      a.keyPressed(keyCode);
    }
  }

  /**
   * [mouseWheel => handle mouseWheel event]
   * @param  {[event]} event               [mouse wheel event]
   */
  this.mouseWheel = function(event) {
    if(keyIsPressed) {
      for(let a of this.actors_annotation) {
        for(let act of a.actions) {
          if(this.curr_action.name == act.name)
            act.on = false;
        }
      }
    }
  }

  /**
   * [update => show or hide the annotation timeline]
   * @param  {[type]} checked               [is the check box checked]
   */
  this.update = function(checked) {
    if(checked) {
      this.showAllElt();
      this.setWrap();
      preparation_editor.div_actors_timeline.hide();
    } else {
      this.sortActorAnnotation();
      preparation_editor.div_actors_timeline.show();
      this.hideAllElt();
    }
  }

  /**
   * [hideAllElt => hide all elements]
   */
  this.hideAllElt = function() {
    this.div_wrap.hide();
  }

  /**
   * [showAllElt => show all elements]
   */
  this.showAllElt = function() {
    act_input.show();
    this.div_wrap.show();
  }

  /**
   * [addAction => add an action to the timeline]
   */
  this.addAction = function() {
    for(let a of this.actors_annotation) {
      if(a.on) {
        a.addAction(this.curr_action);
      }
    }
  }

  /**
   * [sortActorAnnotation => sort the actor by their name]
   */
  this.sortActorAnnotation = function() {
    this.actors_annotation.sort(compare_name);
    while(this.div_wrap.firstChild){this.div_wrap.firstChild.remove();}
    for(let a of this.actors_annotation) {
      this.div_wrap.child(a.elem);
    }
  }

  /**
   * [setWrap => resize the div that contains the timelines]
   */
  this.setWrap = function() {
    this.div_wrap.position(0,viewer_height+can.elt.offsetTop+45);
    this.div_wrap.size(mid_width, windowHeight-this.div_wrap.y-5);
  }

  /**
   * [selectAction => select an action in the timeline]
   */
  this.selectAction = function() {
    preparation_editor.annotation_timeline.curr_action.name = this.value();
  }

  /**
   * [setCurrActionPosition => resize and set the position of the current action]
   * @param  {[float]} tX               [x position]
   * @param  {[float]} tY               [y position]
   * @param  {[float]} tW               [width]
   * @param  {[float]} tH               [height]
   */
  this.setCurrActionPosition = function(tX, tY, tW, tH) {
    this.curr_action.x  = tX;
    this.curr_action.y  = tY;
    this.curr_action.w  = tW;
    this.curr_action.h  = tH;
  }

  /**
   * [setPosition => resize and set the position of the timeline]
   * @param  {[float]} tX               [x position]
   * @param  {[float]} tY               [y position]
   * @param  {[float]} tW               [width]
   * @param  {[float]} tH               [height]
   */
  this.setPosition = function(tX, tY, tW, tH) {
    this.x  = tX;
    this.y  = tY;
    this.w  = tW;
    this.h  = tH;
  }

  /**
   * [setActorName => set actor name]
   * @param  {[string]} name               [actor name related to the timeline]
   */
  this.setActorName = function(name) {
    this.actor_name = name;
    for(let t of this.tracks) {
      t.actor_name = name;
    }
  }

  /**
   * [getTimeFrame => convert a frame into a string 'm:s:mil']
   * @param  {[integer]} frame               [frame number]
   * @return {[string]}       ['m:s:mil']
   */
  this.getTimeFrame = function(frame) {
    let mil = round_prec(((frame/frame_rate)%1)*100,0);
    let s = Math.floor((frame/frame_rate)%60);
    let m = Math.floor((frame/frame_rate/60));
    return ''+m+':'+s+':'+mil;
  }

  /**
   * [display => function called in the main p5 draw function]
   */
  this.display = function() {
    this.setWrap();

    let off_y_last_act = 0;
    for(let i=0; i<this.actors_annotation.length; i++) {
      let a = this.actors_annotation[i];
      a.display();
      this.erase_button[i].setPosition(mid_width+10, a.y+(a.h/2));
      off_y_last_act = a.y+a.h-player.nav_bar.y;
      this.erase_button[i].display();
    }

    push();
    stroke('black');
    strokeWeight(2);
    line(player.nav_bar.cursor,player.nav_bar.y,player.nav_bar.cursor,player.nav_bar.y+off_y_last_act);
    pop();

    push();
    let x_act = act_input.position().x+act_input.elt.offsetWidth+10;
    let y_act = viewer_height+20;
    for(let a of this.actions_available) {
      if(!a.drag) {
        a.x = x_act;
        a.y = y_act;
      }
      a.w = 40;
      a.h = 7;
      fill(0);
      textSize(15);
      text(a.name,a.x,a.y-7);
      fill(a.color);
      rect(a.x,a.y, a.w, a.h);
      x_act += 60;
    }

    pop();
  }
}
