/*
This file is part of KinoAi project.
It is subject to the license terms in the LICENSE file found in the top-level directory
*/

var video;
var can;
var playing = false;
var viewer_height = Number(original_height);
var viewer_width = Number(original_width);
var vid_w;
var vid_h;
var mid_width = viewer_width;
var x_off = 0;
var y_off = 0;
var back_color = 150;

var data_detects;
var data_shots;
var tracks_data = [];
var frames_data = [];
var act_input;

var scale_ratio;
var frame_rate = Number(original_frame_rate);
var total_frame;
var frame_num;
var viewer_scale = 1;
var act_timeline_scale = 1;
var act_timeline_x_off = 0;
var pos_wheel = 0;

var preparation_editor_button;
var is_preparation_editor;
var cadrage_editor_button;
var is_cadrage_editor;
var montage_editor_button;
var is_montage_editor;
var annotation_editor_button;
var is_annotation_editor;
var export_editor_button;
var is_export_editor;
var is_split_screen_editor;
var split_screen_editor_button;

var html_elements = [];
var submit;
// var reframe_button;
var hide_show_header_button;
var documentation_button;
var render_pose = false;
var check_render_pose;
var exploit_rough;
var sanitize;
var reset_pos;
var reload_button;
var get_actors_on_stage;
var get_meta_data;
var extract_video_book;
var extract_keyframes;
var offset_split = 0;
var is_timer = false;
var player;
var dash_player;
var key_down;
var shots_timeline;
var show_shot;
var aspect_ratio;
var img;
var img_hd;
var image_frame;
var time_hd=0;
var stock_img=0;
var detec_modif = false;
var first_load = false;
var shots_modif = false;
var auto_save = false;
var already_save = false;
var rough_json = undefined;
var up_rough = false;
var double_click = false;
var sound_file;
var sound_el;
var tool_tip = {};

function UrlExists(url)
{
    var http = new XMLHttpRequest();
    http.open('HEAD', url, false);
    http.send();
    return http.status!=404;
}

/**
 * [loadKinoAiJSON => This function is used to load a JSON file from the server and parse the JSON data with a callback function]
 * @param  {[type]}   path                    [absolute path of the json file]
 * @param  {Function} callback                [name of the callback function]
 * @param  {String}   [name='']               [username if necessary]
 */
function loadKinoAiJSON(path, callback, name='') {
  $.post({
    url: "load_json",
    async: true,
    data: {'json_path': path, 'username':name},
    dataType: 'json',
    success: function (data) {
      callback(data);
    },
    error: function() {
    }
  });
}

/**
 * [loadJSONNotesAndTimeline load all the data (shot, annotation, scenes, partition, actors timelines, split screen)]
 */
function loadJSONNotesAndTimeline() {
  loadKinoAiJSON(abs_path+'/actors_timeline.json', preparation_editor.loadActorTimelines);
  loadKinoAiJSON(abs_path+'/annotation_timeline.json', preparation_editor.loadDataAnnotationTimeline);
  loadKinoAiJSON(abs_path+'/tracklets.json', preparation_editor.loadDataTracks);
  loadKinoAiJSON(abs_path+'/split_screen_sequence.json', split_screen_editor.loadSequenceSplitScreen);
  loadKinoAiJSON(abs_path+'/split_screen_shots.json', split_screen_editor.loadSplitScreenSequences);
  loadKinoAiJSON(abs_path+"/user_data/"+username+'/'+username+"_timelines.json", montage_editor.loadShotsTimelineData);
  for(let name of user_timeline) {
    if(name!=username) {
        loadKinoAiJSON(abs_path+"/user_data/"+name+'/'+name+"_timelines.json", montage_editor.loadShotsTimelineData, name);
    }
  }
  loadKinoAiJSON(abs_path+"/user_data/"+username+'/'+username+"_partitions_objects.json", annotation_editor.partition_editor.loadPartition);
  for(let name of user_partitions) {
    if(name!=username) {
        loadKinoAiJSON(abs_path+"/user_data/"+name+'/'+name+"_partitions_objects.json", annotation_editor.partition_editor.loadPartition, name);
    }
  }
}

/**
 * [preload => function called before the setup function]
 */
function preload() {
  // Create the global canvas element
  can = createCanvas();
  var div = select('#div_player');
  can.child(div);
  div.hide();
  // Create the p5 video element from the dash player
  video = new p5.MediaElement(dash_player.getVideoElement());
  video.hide();
}

// -----------------------------------------------------------------------------------------------------------------

/*
  Getters functions
*/

/**
 * [sortShotsByStabilize => Sort by stabilization process]
 * @param  {[shot]} a               [shot a]
 * @param  {[shot]} b               [shot b]
 * @return {[-1,0,1]}   []
 */
function sortShotsByStabilize(a,b) {
    if(a.in_stabilize && !b.in_stabilize)
      return -1;
    if (!a.in_stabilize && b.in_stabilize)
      return 1;
    return 0;
}

/**
 * [sortShotsBySelected => Sort shot selected]
 * @param  {[shot]} a               [shot a]
 * @param  {[shot]} b               [shot b]
 * @return {[-1,0,1]}   []
 */
function sortShotsBySelected(a, b) {
  if (a.selected && !b.selected)
    return -1;
  if (!a.selected && b.selected)
    return 1;
  return 0;
}

/**
 * [sortShotsByType => Sort the created shots]
 * @param  {[shot]} a               [shot a]
 * @param  {[shot]} b               [shot b]
 * @return {[-1,0,1]}   []
 */
function sortShotsByType(a, b) {
  if (getFactor(a.type) < getFactor(b.type))
    return -1;
  if (getFactor(a.type) > getFactor(b.type))
    return 1;
  return 0;
}

/**
 * [sortShotsByName => Sort the created shots]
 * @param  {[shot]} a               [shot a]
 * @param  {[shot]} b               [shot b]
 * @return {[-1,0,1]}   []
 */
function sortShotsByName(a, b) {
  if(a.actors_involved.length==1&&b.actors_involved.length==1) {
    if (a.actors_involved[0].actor_name < b.actors_involved[0].actor_name)
      return -1;
    if (a.actors_involved[0].actor_name > b.actors_involved[0].actor_name)
      return 1;
    return 0;
  } else {
    return -1;
  }
}

/**
 * [sortShotsByActPosition => Sort 2 shots with 1 actor based on the position of the involved actor]
 * @param  {[shot]} a               [shot a]
 * @param  {[shot]} b               [shot b]
 * @return {[-1,0,1]}   []
 */
function sortShotsByActPosition(a, b) {
  if(a.actors_involved.length==1&&b.actors_involved.length==1) {
    if (a.actors_involved[0].getActPosition().x < b.actors_involved[0].getActPosition().x)
      return -1;
    if (a.actors_involved[0].getActPosition().x > b.actors_involved[0].getActPosition().x)
      return 1;
    return 0;
  } else {
    return -1;
  }
}

/**
 * [sortByActPosition => Sort 2 actors by their x position on stage]
 * @param  {[actor]} a               [actor a]
 * @param  {[actor]} b               [actor b]
 * @return {[-1,0,1]}   []
 */
function sortByActPosition(a, b) {
  if (a.getActPosition().x < b.getActPosition().x)
    return -1;
  if (a.getActPosition().x > b.getActPosition().x)
    return 1;
  return 0;
}

/**
 * [sortShotsByPosition => Sort 2 shots based on the center of the framing at the current frame]
 * @param  {[shot]} a               [actor a]
 * @param  {[shot]} b               [actor b]
 * @return {[-1,0,1]}   []
 */
function sortShotsByPosition(a,b) {
  if(a.bboxes[frame_num]&&b.bboxes[frame_num]) {
    let x1 = int((a.bboxes[frame_num][0]+a.bboxes[frame_num][2])/2);
    let x2 = int((b.bboxes[frame_num][0]+b.bboxes[frame_num][2])/2);
    if (x1 < x2)
      return -1;
    if (x1 > x2)
      return 1;
    return 0;
  } else {
    return -1;
  }
}

/**
 * [getFactor => get the scale factor based on the size of the shot]
 * @param  {[string]}  type                       [name of the size]
 * @param  {Boolean} [prev=false]               [get the zoom limit for keep out]
 * @return {[float]}               [scale factor]
 */
function getFactor(type, prev=false) {
  var shot_factor = 1;
  switch (type){
    case 'BCU':
      if(!prev)
        shot_factor = 1/8;
      else
        shot_factor = 1/9;
      break;
    case 'CU':
      if(!prev)
        shot_factor = 1/6;
      else
        shot_factor = 1/7;
      break;
    case 'MCU':
      if(!prev)
        shot_factor = 1/4;
      else
        shot_factor = 1/5;
      break;
    case 'MS':
      if(!prev)
        shot_factor = 1/3;
      else
        shot_factor = 1/5;
      break;
    case 'MLS':
      if(!prev)
        shot_factor = 3/5;
      else
        shot_factor = 1/2;
      break;
    case 'FS':
      if(!prev)
        shot_factor = 1;
      else
        shot_factor = 4/5;
      break;
    default:
      shot_factor = 1;
      break;
  }
  return shot_factor;
}

/**
 * [getBBox => Get the bounding box from open pose detect]
 * @param  {[array]} keypoints               [75 points (x,y,c) of an open pose detection]
 * @param  {Number} [scale=0]               [a value between 0 and 1 to scale the bounding box]
 * @return {[array]}           [return a bounding box [xmin, ymin, xmax, ymax]]
 */
function getBBox(keypoints, scale=0) {
  // console.log(keypoints);
  if(keypoints) {
    var len = keypoints.length / 3;
    var bbox = [Number.MAX_VALUE, Number.MAX_VALUE, 0, 0];
    for(var i = 0; i<len; i++)
    {
      if(keypoints[i*3] != "null" && keypoints[i*3+1] != "null")
      {
        if(keypoints[i*3]<bbox[0])
        {
          bbox[0] = keypoints[i*3]
        }
        if(keypoints[i*3]>bbox[2])
        {
          bbox[2] = keypoints[i*3]
        }
        if(keypoints[i*3+1]<bbox[1])
        {
          bbox[1] = keypoints[i*3+1]
        }
        if(keypoints[i*3+1]>bbox[3])
        {
          bbox[3] = keypoints[i*3+1]
        }
      }
    }
    return [bbox[0]*(1-scale),bbox[1]*(1-scale),bbox[2]*(1+scale),bbox[3]*(1+scale)];
  } else {
    return [0,0,0,0];
  }
}

/**
 * [getCenter => compute the center point(x,y) of the detections]
 * @param  {[array]} keypoints               [75 points (x,y,c) of an open pose detection]
 * @return {[x,y]}           []
 */
function getCenter(keypoints) {
  var len = keypoints.length / 3;
  var cpt = 0;
  var total_x = 0;
  var total_y = 0;

  for(var i = 0; i<len; i++) {
    if(keypoints[i*3] != "null" && keypoints[i*3+1] != "null") {
      cpt++;
      total_x+=keypoints[i*3];
      total_y+=keypoints[i*3+1];
    }
  }

  var center = [total_x/cpt, total_y/cpt];
  return center;
}

/**
 * [getBBoxShotAdapted => Get the shot bounding box following the specification for one specific actor]
 * @param  {[float]}  aspectRatio                         [shot aspect ratio]
 * @param  {[array]}  keypoints                           [75 points (x,y,c) of an open pose detection]
 * @param  {[float]}  shot_factor                         [scale factor from the shot size]
 * @param  {[actor]}  actor                               [actor involved]
 * @param  {Boolean} [check_collide=false]               [description]
 * @param  {[type]}  [curr_bbox=undefined]               [description]
 * @param  {[type]}  [c_x=undefined]                     [description]
 * @param  {[type]}  [c_y=undefined]                     [description]
 * @return {[array]}                        [adapt the bounding box based on the size and the aspect ratio]
 */
function getBBoxShotAdapted(aspectRatio, keypoints, shot_factor, actor, check_collide = false, curr_bbox = undefined, c_x = undefined, c_y = undefined) {
  if(cadrage_editor.is_split_screen) {
    aspectRatio = 1;
  }
  var cx;
  var cy;
  var oppbbox;
  if(curr_bbox && c_x && c_y) {
    cx = c_x;
    cy = c_y;
    oppbbox = curr_bbox;
  } else {
    cx = keypoints[1*3];
    cy = keypoints[1*3+1];
    oppbbox = getBBox(keypoints);
    if(cy == 'null') {
      cy = oppbbox[1];
    }
    if(cx == 'null') {
      cx = (oppbbox[0] + oppbbox[2])/2;
    }
    let xNose = keypoints[0*3];
    var yNose = keypoints[0*3+1];
    let xNeck = keypoints[1*3];
    let yNeck = keypoints[1*3+1];
    let xMid = keypoints[8*3];
    let yMid = keypoints[8*3+1];
    var fact = int(dist(xNeck, yNeck, xMid, yMid)/dist(xNeck, yNeck, xNose, yNose));
    var sizeHead;
    var sizeBody;
    if((xNeck && yNeck && xMid && yMid) &&
    (xNeck != 'null' && yNeck != 'null' && xMid != 'null' && yMid != 'null')){
      sizeBody=int(dist(xNeck, yNeck, xMid, yMid));
      sizeHead = int((sizeBody/3));
    }else if((xNose && yNose && xNeck && yNeck) &&
     (xNose != 'null' && yNose != 'null' && xNeck != 'null' && yNeck != 'null')){
      sizeHead=int(dist(xNose, yNose, xNeck, yNeck)*2/3);
    }
    if(actor && !actor.size_head) {
      actor.updateHeadSize();
    }
    if(actor && (!sizeHead)) {
      // console.log(actor.actor_name, sizeHead, actor.size_head);
      sizeHead = actor.size_head;
    }
  }
  if(sizeHead) {
    var offset = [cx - oppbbox[0], cy - oppbbox[1], oppbbox[2] - cx, oppbbox[3] - cy];
    var bbox = [0,0,0,0];
    //left
    bbox[0] = oppbbox[0];
    //top
    fact = 1.8;
    bbox[1] = int(cy - (sizeHead*fact));
    //right
    bbox[2] = oppbbox[2];
    //bottom ===> bottom = center y - top offset + size head * 10 / shot_factor
    // bbox[3] = cy - offset[1] + (oppbbox[3] - oppbbox[1]) * shot_factor;
    bbox[3] = bbox[1] + int((sizeHead*10) * shot_factor);
    var shot_height = bbox[3] - bbox[1];
    bbox[1] -= shot_height/8;
    bbox[3] += shot_height/8;//(3/shot_factor);
  } else {
    var offset = [cx - oppbbox[0], cy - oppbbox[1], oppbbox[2] - cx, oppbbox[3] - cy];
    var shot_height = Math.max(oppbbox[3] - oppbbox[1],oppbbox[2] - oppbbox[0]);
    var bbox = [0,0,0,0];
    //left
    bbox[0] = oppbbox[0];
    //top
    bbox[1] = cy - (shot_height / 8);
    //right
    bbox[2] = oppbbox[2];
    //bottom ===> bottom = center y - top offset + (top offset - bottom offset) / shot_factor
    bbox[3] = bbox[1] + shot_height * shot_factor;

    bbox[1] -= (bbox[3] - bbox[1])/3;
    bbox[3] += (bbox[3] - bbox[1])/3;
    // console.log(shot_height, cx, cy, (bbox[3] - bbox[1]));
  }

  bbox = [int(bbox[0]), int(bbox[1]), int(bbox[2]), int(bbox[3])];

  // enlarge width or reduce width
  if(!check_collide || aspectRatio * (bbox[3] - bbox[1]) < bbox[2]-bbox[0]) {
    halfdim = aspectRatio * (bbox[3] - bbox[1]) / 2;
    bbox[0] = cx - halfdim;
    bbox[2] = cx + halfdim;
  }

  return bbox;
}

/**
 * [getBBoxShotInvolved => compute a framing based on the shot specification for a specific frame]
 * @param  {[array]} actors_involved               [actors name involved in the shot]
 * @param  {[float]} aspectRatio                   [shot aspect ratio width/height]
 * @param  {[float]} shot_factor                   [scale factor from the size]
 * @param  {[array]} imageSize                     [sizez of the original image [0,0,image width, image height]]
 * @param  {[integer]} fr_num                        [frame number]
 * @return {[array]}                 [a bounding wich describe the shot framing for the frame [x_min,y_min, x_max,y_max]]
 */
function getBBoxShotInvolved(actors_involved, aspectRatio, shot_factor, imageSize, fr_num) {
  let gaze_vect;
  let bbox = [];
  let x_centers = [];
  let y_centers = [];
  let actor_neck_position = [];
  let k=0;
  for(let act of actors_involved) {
    for(let t of act.tracks) {
      let keypointsB = frames_data[fr_num];
      let detections_track = t.detections;
      let first_frame = t.first_frame;
      let keypoints_tab;
      if(keypointsB != null && keypointsB != undefined) {
        keypoints_tab = keypointsB[detections_track[fr_num-first_frame]];
      }
      if(first_frame <= fr_num && detections_track.length > (fr_num-first_frame) && keypoints_tab) {
        var boxB = getBBoxShotAdapted(aspectRatio, keypoints_tab['KeyPoints'], shot_factor, act);
        if(actors_involved.length != 1) {
          boxB[0] = getBBoxShotAdapted(aspectRatio, keypoints_tab['KeyPoints'], getFactor('CU'), act)[0];
          boxB[2] = getBBoxShotAdapted(aspectRatio, keypoints_tab['KeyPoints'], getFactor('CU'), act)[2];
        }
        let neck_position  = keypoints_tab['KeyPoints'][1*3];
        if(neck_position == 'null') {
          let oppbbox = getBBox(keypoints_tab['KeyPoints']);
          neck_position = (oppbbox[0] + oppbbox[2])/2;
        }
        actor_neck_position.push(neck_position);
        if(!gaze_vect) {
          gaze_vect = getGazevect(keypointsB[detections_track[fr_num-first_frame]]['KeyPoints']);
          let vel = act.getVelocityVect(fr_num);
          if(vel) {
            gaze_vect = p5.Vector.add(gaze_vect, vel);
          }
        } else {
          gaze_vect = p5.Vector.add(gaze_vect, getGazevect(keypointsB[detections_track[fr_num-first_frame]]['KeyPoints']));
        }
        x_centers.push((boxB[0]+boxB[2])/2);
        y_centers.push((boxB[1]+boxB[3])/2);
        if(k==0) {
          bbox = boxB;
          k++;
        }
        bbox[0] = min(bbox[0], boxB[0]);
        bbox[1] = min(bbox[1], boxB[1]);
        bbox[2] = max(bbox[2], boxB[2]);
        bbox[3] = max(bbox[3], boxB[3]);
      }
    }
    for(let t of act.track_bbox_shot) {
      if(t.first_frame < fr_num && t.last_frame > fr_num) {
        let b = t.bboxes[fr_num-t.first_frame];
        let curr_bbox = [b.x, b.y,b.x+b.w, b.y+b.h];
        var boxB = getBBoxShotAdapted(aspectRatio, undefined, shot_factor, act, false, curr_bbox, b.center_x, b.center_y);
        x_centers.push((boxB[0]+boxB[2])/2);
        y_centers.push((boxB[1]+boxB[3])/2);
        if(bbox.length > 0) {
          bbox[0] = min(bbox[0], boxB[0]);
          bbox[1] = min(bbox[1], boxB[1]);
          bbox[2] = max(bbox[2], boxB[2]);
          bbox[3] = max(bbox[3], boxB[3]);
        } else {
          bbox = boxB;
          k++;
        }
      }
    }
  }

  bbox = getAdaptedBBox(bbox, aspectRatio);

  let stage_off;
  if(cadrage_editor.is_stage_position && actor_neck_position.length!=0 && actors_involved.length==1) {
    let stage_position = int((actor_neck_position.reduce((pv, cv) => pv + cv, 0))/actor_neck_position.length);
    // console.log(actor_neck_position, stage_position);
    let stage_position_factor = Math.min(2/3,Math.max(1/3,stage_position/Number(original_width)));
    let prev_w = int(bbox[2]-bbox[0]);
    let offset = int(prev_w*stage_position_factor);
    if(!(cadrage_editor.is_gaze_direction && gaze_vect && actors_involved.length==1)) {
      bbox[0] = stage_position-offset;
      bbox[2] = bbox[0]+prev_w;
    } else {
      stage_off = (stage_position-offset)-bbox[0];
    }
  }

  if(cadrage_editor.is_gaze_direction && gaze_vect && actors_involved.length==1) {
    let s_gaze = abs((gaze_vect.x*shot_factor)/150);
    let off = gaze_vect.normalize().x*((bbox[2]-bbox[0])*s_gaze);
    if(stage_off) {
      let n_o = off;
      if(stage_off<0 && off<0) {
        n_o = Math.min(off, stage_off);
      } else if(stage_off>0 && off>0) {
        n_o = Math.max(off, stage_off);
      } else {
        n_o = off + stage_off;
      }
      off = n_o;
    }
    // console.log(off);
    bbox = [int(bbox[0]+off), bbox[1], int(bbox[2]+off), bbox[3]];
  }

  bbox = [int(bbox[0]), int(bbox[1]), int(bbox[2]), int(bbox[3])];

  if((bbox[3] - bbox[1])>int(imageSize[3])) {
    let s = (1/((bbox[3] - bbox[1])/int(imageSize[3]*0.9)));
    let mid = int((x_centers.reduce((pv, cv) => pv + cv, 0))/x_centers.length);
    let half = int((int(bbox[2]*s)-int(bbox[0]*s))/2);
    bbox = [mid-half,int(bbox[1]*s),mid+half,int(bbox[3]*s)];
  } else if(bbox[2] - bbox[0] > imageSize[2]) {
    let s = 1/((bbox[2] - bbox[0])/ imageSize[2]);
    let mid = int((y_centers.reduce((pv, cv) => pv + cv, 0))/y_centers.length);
    let half = int((int(bbox[3]*s)-int(bbox[1]*s))/2);
    bbox = [int(bbox[0]*s),mid-half,int(bbox[2]*s),mid+half];
  }

  return bbox;
}

/**
 * [getAdaptedBBox => adapt the size of the bounding box within the aspect ratio]
 * @param  {[array]} bbox                      [bounding box [x_min,y_min,x_max,y_max]]
 * @param  {[float]} aspectRatio               [shot aspect ratio]
 * @return {[array]}             [bounding box with the proper aspect ratio [x_min,y_min,x_max,y_max]]
 */
function getAdaptedBBox(bbox, aspectRatio) {
  if ((bbox[2] - bbox[0]) < aspectRatio * (bbox[3] - bbox[1])){
    // enlarge width
    halfdim = aspectRatio * (bbox[3] - bbox[1]) / 2;
    center = (bbox[0] + bbox[2]) / 2;
    bbox[0] = center - halfdim;
    bbox[2] = center + halfdim;
  } else {
    // enlarge height
    halfdim = (1 / aspectRatio) * (bbox[2] - bbox[0]) / 2;
    center = (bbox[1] + bbox[3]) / 2;
    bbox[1] = center - halfdim;
    bbox[3] = center + halfdim;
  }
  return [int(bbox[0]), int(bbox[1]), int(bbox[2]), int(bbox[3])];
}

/**
 * [getGazevect => compute the actor gaze vector from open pose keypoints]
 * @param  {[array]} keypoints               [open pose array of detections (75 points)]
 * @return {[vector]}           [p5 vector (x,y)]
 */
function getGazevect(keypoints) {
  if(keypoints) {
    let xNose = keypoints[0*3];
    let yNose = keypoints[0*3+1];
    let xNeck = keypoints[1*3];
    let yNeck = keypoints[1*3+1];
    var xMid = keypoints[8*3];
    var yMid = keypoints[8*3+1];
    let bbox = getBBox(keypoints);
    let vect = createVector(xMid - xNeck,0).normalize();
    if(bbox && (bbox[2] - bbox[0]) > (bbox[3]- bbox[1])) {
      // console.log(p5.Vector.mult(createVector(xNose-xNeck, yNeck-yNeck),vect.x),createVector(xNose-xNeck, yNeck-yNeck));
      return createVector(0);
    }
    return createVector(xNose-xNeck, yNeck-yNeck);
  } else {
    return createVector(0);
  }
}

/**
 * [getActorsBBoxIntersect => check the intersection with the onstage and not involved actors]
 * @param  {[array]} bbox                       [bounding box of the shot at the frame]
 * @param  {[array]} not_involved               [actors not involved in the shot]
 * @param  {[float]} aspectRatio                [shot aspect ratio]
 * @param  {[float]} shot_factor                [scale factor from the shot size]
 * @param  {[integer]} fr_num                     [frame number]
 * @return {[object]}              [description]
 */
function getActorsBBoxIntersect(bbox, not_involved, aspectRatio, shot_factor, fr_num) {
  let bboxes_intersect = [];
  let lim_x_right = Math.round(original_width);
  let lim_x_left = 0;
  for(let act of not_involved) {
    for(let t of act.tracks) {
      let keypointsB = frames_data[fr_num];
      let detections_track = t.detections;
      let first_frame = t.first_frame;
      let keypoints_tab;
      if(keypointsB != null && keypointsB != undefined) {
        keypoints_tab = keypointsB[detections_track[fr_num-first_frame]];
      }
      if(first_frame <= fr_num && detections_track.length > (fr_num-first_frame) && keypoints_tab) {
        let boxB = getBBoxShotAdapted(aspectRatio, keypoints_tab['KeyPoints'], shot_factor, act, true);
        // let boxB = getBBox(keypoints_tab['KeyPoints']);
        if(boxB && bbox) {
          if(!(bbox[2]<boxB[0] || boxB[2]<bbox[0] || bbox[3]<boxB[1] || boxB[3] < bbox[1])) {
            bboxes_intersect.push({'bbox':boxB,'x_center':(boxB[0]+boxB[2])/2,'y_center':(boxB[1]+boxB[3])/2,'act':act});
          } else {
            if(boxB[2] < bbox[0] && boxB[2]>lim_x_left) {
              lim_x_left = boxB[2];
            } else if(boxB[0] > bbox[2] && boxB[0]<lim_x_right) {
              lim_x_right = boxB[0];
            }
          }
        }
      }
    }
    for(let t of act.track_bbox_shot) {
      if(t.first_frame <= fr_num && t.last_frame > fr_num) {
        let b = t.bboxes[fr_num-t.first_frame];
        let curr_bbox = [b.x, b.y,b.x+b.w, b.y+b.h];
        let boxB = getBBoxShotAdapted(aspectRatio, undefined, shot_factor, act, true, curr_bbox, b.center_x, b.center_y);
        if(boxB && bbox) {
          if(!(bbox[2]<boxB[0] || boxB[2]<bbox[0] || bbox[3]<boxB[1] || boxB[3] < bbox[1])) {
            bboxes_intersect.push({'bbox':boxB,'x_center':b.center_x,'y_center':b.center_y,'act':act});
          } else {
            if(boxB[2] < bbox[0] && boxB[2]>lim_x_left) {
              lim_x_left = boxB[2];
            } else if(boxB[0] > bbox[2] && boxB[0]<lim_x_right) {
              lim_x_right = boxB[0];
            }
          }
        }
      }
    }
  }
  return {'bboxes_intersect':bboxes_intersect,'lim_x_left':lim_x_left,'lim_x_right':lim_x_right};
}

/**
 * [getBBoxShot => compute the shot framing based on the size, the actors involved, the aspect ratio]
 * @param  {[string]} shotType                         [shot size]
 * @param  {[float]} aspectRatio                      [shot aspect ratio]
 * @param  {[integer]} [fr_num=undefined]               [frame number]
 * @return {[array]}                    [framing bounding box [x_min,y_min,x_max,y_max]]
 */
function getBBoxShot(shotType, aspectRatio, fr_num=undefined) {
  let draw = true;
  if(fr_num == undefined) {
    fr_num = frame_num;
  } else {
    draw = false;
  }
  let not_involved = [];
  let involved = [];
  for(let a of preparation_editor.actors_timeline) {
    if(!a.on){
      not_involved.push(a);
    } else {
      involved.push(a);
    }
  }
  let imageSize = [0, 0, Number(original_width), Number(original_height)];
  let shot_factor = getFactor(shotType);
  let lim_shot_factor = getFactor(shotType, true);
  let inter_shot_factor = (lim_shot_factor+shot_factor)/2;
  // let inter_shot_factor = (lim_shot_factor+shot_factor)/4;
  // let inter_shot_factor1 = (lim_shot_factor+shot_factor)/3;
  // let inter_shot_factor3 = (lim_shot_factor+shot_factor)/3*2;
  // let inter_shot_factor4 = (lim_shot_factor+shot_factor)/4*3;

  let bbox;

  if(cadrage_editor.is_keep_out || cadrage_editor.is_pull_in) {
    let i=0;
    for(let f of [lim_shot_factor, inter_shot_factor, shot_factor]) {

      if(!cadrage_editor.is_keep_out) {
        f = shot_factor;
      }

      let new_bbox = getBBoxShotInvolved(involved, aspectRatio, f, imageSize, fr_num);
      let bboxes_intersect = getActorsBBoxIntersect(new_bbox, not_involved, aspectRatio, f, fr_num).bboxes_intersect;

      if(bboxes_intersect.length>0 && i>0) {
        break;
      }else if (cadrage_editor.is_pull_in && bboxes_intersect.length>0 && i==0) {
        for(let b of bboxes_intersect) {
          involved.push(b.act);
        }
        bbox = getBBoxShotInvolved(involved, aspectRatio, f, imageSize, fr_num);
        break;
      }
      i++;
      bbox = new_bbox;
    }
  } else {
    bbox = getBBoxShotInvolved(involved, aspectRatio, shot_factor, imageSize, fr_num);
  }
  // bbox.forEach(function(item, i) {if(item==undefined) bbox=undefined;});
  const testUndefined = bbox.findIndex(bbox => bbox === undefined);
  if(!draw && bbox.indexOf(undefined)!=-1 && testUndefined!=-1) {
    return undefined;
  }

  return bbox;
}

/**
 * [p5VectToJson => Transform a p5vect to classic vector]
 * @param  {[object]} vect               [p5 vector]
 * @return {[object]}      [{x:value,y:value}]
 */
function p5VectToJson(vect) {
  if(vect) {
    return {x:vect.x, y:vect.y};
  } else {
    return {x:0,y:0};
  }
}

/**
 * [p5ImageFromDash => Extract a p5 Image object from the current video frame]
 * @return {[object]} [return a p5 image object]
 */
function p5ImageFromDash() {
  if(video.elt.videoWidth) {
    if(image_frame.width==0 || video.elt.videoWidth != image_frame.width) {
      image_frame = new p5.Image(video.elt.videoWidth, video.elt.videoHeight);
      image_frame.drawingContext.drawImage(video.elt, 0, 0);
    } else {
      image_frame.width = video.elt.videoWidth;
      image_frame.height = video.elt.videoHeight;
      image_frame.drawingContext.drawImage(video.elt, 0, 0);
    }
  } else {
    image_frame = new p5.Image(100,100);
  }
}

function getIndex(first_frame, name) {
  var index = 0;
  let i =0;
  for(let t of tracklets_line) {
    if(t.first_frame == first_frame && t.actor_name == name) {
      index = i;
      break;
    }
    i++;
  }
  return index;
}

function round_prec(value, precision) {
    var multiplier = Math.pow(10, precision || 0);
    return Math.round(value * multiplier) / multiplier;
}


// -----------------------------------------------------------------------------------------------------------------

/*
  Update functions (for checkbox input) and select function (select element)
*/

function updateDrawPose() {
  render_pose = this.checked();
}

// -----------------------------------------------------------------------------------------------------------------

/*
  UI elements manager
*/

function componentToHex(c) {
  var hex = c.toString(16);
  return hex.length == 1 ? "0" + hex : hex;
}

function rgbToHex(r, g, b) {
  return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
}

/**
 * [randomColor => random color generator with settable brightness]
 * @return {[string]} [an HEX string color like #FFFFFF for white]
 */
function randomColor(){
  let h = Math.random(); s = 1; v = 0.6;
  let i = Math.floor( h * 6 );
  let f = h * 6 - i;
  let p = v * ( 1 - s );
  let q = v * ( 1 - f * s );
  let t = v * ( 1 - ( 1 - f ) * s );
  let r=0,g=0,b=0;

  switch( i % 6 ) {
    case 0:
      r = v; g = t; b = p;
      break;
    case 1:
      r = q; g = v; b = p;
      break;
    case 2:
      r = p; g = v; b = t;
      break;
    case 3:
      r = p; g = q; b = v;
      break;
    case 4:
      r = t; g = p; b = v;
      break;
    case 5:
      r = v; g = p; b = q;
      break;
  }
  r = Math.floor(r*255), g=Math.floor(g*255),b=Math.floor(b*255);
  return rgbToHex(r,g,b);
}

/**
 * [compare_name => Compare two actor timelines by name]
 * @param  {[actor]} a               [actor a]
 * @param  {[actor]} b               [actor b]
 * @return {[-1,0,1]}   []
 */
function compare_name(a,b) {
  if (a.actor_name < b.actor_name)
    return -1;
  if (a.actor_name > b.actor_name)
    return 1;
  return 0;
}

/**
 * [hideShowHeader => hide or show the header part of the window]
 */
function hideShowHeader() {
  if($('#header_info').css('display') !== 'none') {
    $('#header_info').hide();
    for(let el of selectAll('.tabs')) {
      el.position(el.position().x,0);
    }
    up_rough = true;
  } else {
    $('#header_info').show();
    for(let el of selectAll('.tabs')) {
      el.position(el.position().x,can.elt.offsetTop-30);
    }
    up_rough = true;
  }
}

/**
 * [goToDocumentation => open the git lab wiki]
 */
function goToDocumentation() {
  window.open('https://gitlab.inria.fr/ronfard/openkinoai/-/wikis/User-guide', "_blank");
}

/**
 * [setCursor => set the player cursor position]
 */
function setCursor() {
  var unit_time = player.w/total_frame;
  var x_cursor = 95+Math.round(frame_num*unit_time);
  player.setXCursor(x_cursor);
}

/**
 * [createActTimeline => Create an actor timeline with the name and the related erase button]
 */
function createActTimeline() {
  preparation_editor.createActTimeline();
}

/**
 * [sanitizeFramesData => remove all the data that not used]
 */
function sanitizeFramesData() {
  for(let i=0; i<frames_data.length; i++) {
    let tab_indexes = preparation_editor.getTrackletsIndexes(i,true);
    var ind = [];
    for(let obj of tab_indexes){
      ind.push(obj.ind)
    }
    if(frames_data[i] && ind.length != frames_data[i].length) {
      for(let j=0; j<frames_data[i].length; j++){
        if(!ind.includes(j)){
          frames_data[i][j]='null';
        }
      }
    }
  }
  detec_modif = true;
}

/**
 * [resetPos => reset the zooming factor]
 */
function resetPos() {
  viewer_scale=1;
  act_timeline_scale=1;
  act_timeline_x_off=0;
  player.scale = 1;
}

/**
 * [reloadVideo => refresh the video]
 */
function reloadVideo() {
  let prev_time = video.time();
  let url = '/media/'+abs_path.split('media')[abs_path.split('media').length-1]+'/mpd/original_dash.mpd';
  dash_player.attachSource(url)
  video.time(prev_time);
  if(playing) {
    dash_player.play();
  } else {
    dash_player.pause();
  }
}

/**
 * [autoSave => auto save the current project]
 */
function autoSave() {
  auto_save = true;
  already_save = false;
  saveTimeline();
}

/**
 * [saveTimeline => Save a json file with tracklets, actors timelines and shots information]
 */
function saveTimeline() {

  var data_timelines;
  var data_tracks;

  if(!already_save) {
    data_timelines = 'null';
    data_tracks = 'null'
    if(is_preparation_editor || annotation_editor.is_partition_editor) {
      let json_act_name = preparation_editor.getTimelinesData();
      let json_tracks = preparation_editor.getTrackletsData();
      data_tracks = JSON.stringify(json_tracks);
      data_timelines = JSON.stringify(json_act_name);
    }

    let new_json_shots = montage_editor.getShotsData();

    let annot_t = preparation_editor.getAnnotationData();

    data_shots = new_json_shots;

    let detections;
    if(detec_modif) {
     console.log('detec modif');
     let new_fr_data = {'Frames':['First for maintain detection'].concat(frames_data)};
     data_detects = new_fr_data;
     detections = JSON.stringify(data_detects);
    } else {
     console.log('detec modif false');
     detections = 'null';
    }

    let shots_data;
    let scenes_involved = 'null';
    if(shots_modif) {
      shots_data = JSON.stringify(data_shots);
    } else {
      shots_data = 'null';
      scenes_involved = JSON.stringify(data_shots);
    }
    // console.log(data_detects, data_tracks, data_timelines, data_shots);
    $.post({
      url: "submit",
      async: true,
      data: {'abs_path': abs_path, 'detec':detections, 'timeline':data_timelines, 'scenes_involved':scenes_involved,
      'track':data_tracks, 'shots':shots_data, 'annotation':JSON.stringify(annot_t)},
      dataType: 'json',
      success: function (data) {
        if(!auto_save) {
          alert(data['success']);
        }
        auto_save = false;
        already_save = false;
      },
      error: function() {
        auto_save = false;
        already_save = false;
        shots_modif = true;
      }
    });

    shots_timeline.saveShotsTimeline();
    if(is_annotation_editor) {
      annotation_editor.note_editor.saveNote();
      annotation_editor.partition_editor.savePartitionTimeline();
    }

    detec_modif = false;
    shots_modif = false;
    already_save = true;
  }
}

/**
 * [positionUIElements => Set the position of the side elements according to the window size]
 */
function positionUIElements() {
  let k=0;
  let off = 5;
  for(let i=0; i<html_elements.length;i++) {
    let elem = html_elements[i];
    if(elem.side == true) {
      if(html_elements[i-1].elt.clientHeight>30) {
        off += html_elements[i-1].elt.clientHeight-27;
      }
      elem.position(windowWidth-160,off+k*30);
      elem.class('side')
      k++;
      elem.original_x = elem.position().x;
      elem.original_y = elem.position().y;
    }
  }
  up_rough = true;
}

/**
 * [processToolTip => Create a tool tip assigned to an element with an information text]
 * @param  {[string]} text               [text information]
 */
function processToolTip(text) {
  return function() {
    tool_tip.is_on = !tool_tip.is_on;
    if(tool_tip.is_on) {
      tool_tip.p = createP(text);
    } else {
      tool_tip.p.remove();
    }
  }
}

/**
 * [toTwoDigit => Add 0 if one digit in the string]
 * @param  {[string]} str               [string number]
 */
function toTwoDigit(str) {
  if(str.length == 1) {
    return '0'+str;
  } else {
    return str;
  }
}

/**
 * [clickTab => Select a tab and change the color]
 * @param  {[element]} elt               [tab button object]
 */
function clickTab(elt) {
  for( let el of document.getElementsByClassName('tabs')){el.style["background-color"] = '#2E5C9C';el.style['color'] = 'white';}
  elt.style('background-color', 'white');
  elt.style('color', '#2E5C9C');
}

/**
 * [resetTabs => reset all the tabs information (hide all)]
 */
function resetTabs() {
  is_preparation_editor = false;
  is_cadrage_editor = false;
  is_montage_editor = false;
  is_annotation_editor = false;
  is_export_editor = false;
  is_split_screen_editor = false;
  split_screen_editor.is_display_player = false;
  for(let el of html_elements) {
    if(el.side == true) {
      el.hide();
    }
  }
  preparation_editor.hideElts();
  cadrage_editor.hideElts();
  montage_editor.hideElts();
  annotation_editor.hideElts();
  export_editor.hideElts();
  split_screen_editor.hideElts();
}

/**
 * [updateSideElems => update the positions for all displayed side buttons]
 */
function updateSideElems() {
  let off = 5;
  let k=0;
  let last_elem;
  for(let i=0; i<html_elements.length;i++) {
    let elem = html_elements[i];
    if(elem.side == true && elem.elt.style.display != "none") {
      if(!last_elem) {
        last_elem = html_elements[i];
      }
      if(last_elem && last_elem.elt.clientHeight>30) {
        off += last_elem.elt.clientHeight-27;
      }
      elem.position(windowWidth-160,off+k*32+ can.elt.offsetTop);
      k++;
      last_elem = html_elements[i];
    }
  }
}

// select the preparation editor
function showPreparationEditor() {
  preparation_editor.updateAndShow();
}

// select the framing editor
function showCadrageEditor() {
  cadrage_editor.updateAndShow();
}

// select the video editing editor
function showMontageEditor() {
  montage_editor.updateAndShow();
}

// select the annotation editor
function showAnnotationEditor() {
  annotation_editor.updateAndShow();
}

// select the export editor
function showExportEditor() {
  export_editor.updateAndShow();
}

// select the export editor
function showSplitScreenPlayer() {
  split_screen_editor.updateAndShow();
}

/**
 * [hideAllElt => Hide all hmtl elements for showing the player in fullscreen]
 */
function hideAllElt() {
  $("#header_info").hide();
  var elems = selectAll('.aside');
  for(let el of elems){
    el.hide();
  }
  for(let el of html_elements) {
    el.hide();
  }
  for(let act of preparation_editor.actors_timeline) {
    act.elem.hide();
  }
  preparation_editor.hideElts();
  cadrage_editor.hideElts();
  montage_editor.hideElts();
  annotation_editor.hideElts();
  export_editor.hideElts();
  for(let el of selectAll('.tabs')) {
    el.hide();
  }
}

/**
 * [showAllElt => Show all hmtl elements]
 */
function showAllElt() {

  let name = "";
  for(let el of document.getElementsByClassName('tabs')) {
    if(el.style.color != "white") {
      name = el.innerText;
    }
  }
  switch (name) {
    case 'Export':
      showExportEditor();
      break;
    case 'Cadrage':
      showCadrageEditor();
      break;
    case 'Montage':
      showMontageEditor();
      break;
    case 'Split screen':
      showSplitScreenPlayer();
      break;
    case 'Annotation':
      showAnnotationEditor();
      break;
    case 'Preparation':
      showPreparationEditor();
      break;
    default:
      showPreparationEditor();
  }
  for(let el of selectAll('.tabs')) {
    el.show();
  }

}

// -----------------------------------------------------------------------------------------------------------------

/*
  Ajax request manager
*/

/**
 * [callBackImg => get the 4k image from the server]
 * @param  {[string]} data               [path of the image]
 */
function callBackImg(data) {
  if(!playing && stock_img == 1) {
    img_hd = loadImage(data['src']);
    // console.log('New Im HD ',data['src']);
  }
  stock_img--;
}

/**
 * [imgHDRequest => Ask a 4k screenshot from the original source]
 */
function imgHDRequest() {
  stock_img++;
  $.post({
    url: "fullhd",
    data: {'abs_path': abs_path, 'time':time_hd},
    dataType: 'json',
    success: function (data) {
      return callBackImg(data);
    }
  });
}

/**
 * [gotDetects => load openpose detection]
 * @param  {[array]} data               [open pose detections]
 */
function gotDetects(data) {
  data_detects = data;
  loadDetec();
}

/**
 * [loadSubtile => load a subtitle file and parse it]
 */
function loadSubtile() {
  if(UrlExists('/media/'+abs_path.split('media')[abs_path.split('media').length-1]+'/subtitle.vtt')) {
    $.post({
      url : "load_sub",
      data: {'abs_path': abs_path},
      dataType: 'json',
      success: function (data) {
        return setSubtitle(data);
      },
      error: function (error) {
        console.log('error');
      }
    });
  }
}

/**
 * [parseDetec => Parse the openpose detections json]
 * @param  {[array]} data               [open pose detections 75 points (x,y,c)]
 */
function parseDetec(data) {
  var json_detec_test = data['data_detec'];
  data_detects = JSON.parse(json_detec_test.replace(/&quot;/g,'"'));
  if(!Object.getOwnPropertyNames(data_detects['Frames'])) {
    frames_data = data_detects['Frames'];
  } else {
    const detec_own = Object.getOwnPropertyNames(data_detects['Frames']);
    let len  = detec_own.length;
    if(detec_own[len-1] == "length") {
      len = len-2;
    } else {
      detec_modif = true;
      first_load = true;
    }
    if(!total_frame) {
      total_frame = Math.floor(video.duration()*frame_rate);
      if(isNaN(total_frame)) {
        total_frame = len;
      }
    }
    for (var i = 1; i <= Math.min(total_frame,len); i++) {
      if(!data_detects['Frames'][i]) {
        frames_data.push([]);
      } else {
        frames_data.push(data_detects['Frames'][i]);
      }
    }
  }
  // for(let t of preparation_editor.tracklets_line) {
    // console.log('here');
    // t.updateFirstFrame(1);
    // console.log(t.first_frame);
  // }
  // for(let s of shots) {
  //   s.accuracy_rate = s.getAccuracyRate();
  // }
}

/**
 * [parseShots => Parse the shots data]
 * @param  {[JSON]} data               [shots data]
 */
function parseShots(data) {
  data_shots = data['data_shots'];

  montage_editor.loadMontageEditorData();
  split_screen_editor.loadSplitScreenEditorData();
}

/**
 * [setSubtitle => create html subtitle object for the video element]
 * @param {[array]} data  [subtitle data]
 */
function setSubtitle(data) {
  var track = video.elt.addTextTrack("captions", "French", "fr");
  track.mode = "showing";
  let arr = [];
  for(let c of data.sub){
    track.addCue(new VTTCue(c.start, c.end, c.text));
    arr.push({'Start':c.start,'End':c.end,'Text':c.text});
  }
  annotation_editor.partition_editor.partitions_scenes.push({'Name':'TheSubtitles','PartArray':arr})
}

/**
 * [loadDetec => load openpose detection]
 */
function loadDetec() {
  $.post({
    url: "get_data_detec",
    data: {'abs_path': abs_path},
    dataType: 'json',
    success: function (data) {
      return parseDetec(data);
    }
  });
}

/**
 * [loadShots => load shots data]
 */
function loadShots() {
  $.post({
    url: "get_data_shots",
    data: {'abs_path': abs_path},
    dataType: 'json',
    success: function (data) {
      return parseShots(data);
    }
  });
}

// -----------------------------------------------------------------------------------------------------------------

/*
  Drawing functions
*/


/**
 * [drawPose => draw in the player the openpose skeleton based on the openpose set of keypoints]
 * @param  {[array]} keypoints                   [openpose keypoints]
 * @param  {[actor]} [a=undefined]               [actor object]
 */
function drawPose(keypoints, a=undefined) {
  let bbox;
  let x_off=0;
  let y_off=0;
  let alpha=200;
  if(is_montage_editor && !cadrage_editor.is_shots_frame_layout || (annotation_editor.is_partition_editor && annotation_editor.partition_editor.is_scene)) {
    bbox = shots_timeline.getCurrStabShotNoScale(frame_num);
  }
  if((is_montage_editor || is_cadrage_editor || annotation_editor.is_note_book) && show_shot && !cadrage_editor.is_shots_frame_layout) {
    bbox = show_shot.getCurrStabShot(frame_num);
  }
  let sc_f;
  let downscale_size = 5;
  if(bbox) {
    sc_f = Number(original_height)/(bbox[3]-bbox[1]);
    x_off=bbox[0];
    y_off=bbox[1];
    alpha=150;
    downscale_size = 5/sc_f*3;
  }
  let vel = undefined;
  if(a) {
    vel = a.getVelocityVect(frame_num);
  }

  // draw the velocity (blue) and gaze direction (red) vector
  push();
  var c = color(250,100,250,alpha);
  var xNose = keypoints[0*3]-x_off;
  var yNose = keypoints[0*3+1]-y_off;
  var xNeck = keypoints[1*3]-x_off;
  var yNeck = keypoints[1*3+1]-y_off;
  var xMid = keypoints[8*3]-x_off;
  var yMid = keypoints[8*3+1]-y_off;
  if(sc_f && keypoints[0*3]>bbox[0] && keypoints[0*3]<bbox[2] && keypoints[0*3+1]>bbox[1] && keypoints[0*3+1]<bbox[3]) {
    scale(sc_f);
  }
  stroke(c);
  strokeWeight(downscale_size);
  let fact = int(dist(xNeck, yNeck, xMid, yMid)/dist(xNeck, yNeck, xNose, yNose));
  if(fact>2) {
    fact = 1.5;
  } else {
    fact = 2;
  }
  if(!bbox) {
    push();
    translate(0,-60);
    if(a) {
      fill(255);
      noStroke();
      textSize(15);
      text(a.actor_name, xNeck-10, yNeck-20);
    }
    stroke('red');
    strokeWeight(downscale_size);
    let v = getGazevect(keypoints);
    line(xNeck, yNeck, xNeck+v.x, yNeck);
    let temp = xNeck+v.x;
    if(xNeck<temp) {
      triangle(temp, yNeck - 3, temp, yNeck + 3, temp +3, yNeck);
    }else {
      triangle(temp, yNeck - 3, temp, yNeck + 3, temp -3, yNeck);
    }
    if(vel) {
      translate(0,20);
      stroke('blue');
      line(xNeck, yNeck, xNeck+vel.x, yNeck);
      let temp = xNeck+vel.x;
      if(xNeck<temp) {
        triangle(temp, yNeck - 3, temp, yNeck + 3, temp +3, yNeck);
      }else {
        triangle(temp, yNeck - 3, temp, yNeck + 3, temp -3, yNeck);
      }
    }
    // if(vel && v) {
    //   translate(0,-50);
    //   stroke('green');
    //   let vect = p5.Vector.add(vel,v);
    //   line(xNeck, yNeck, xNeck+vect.x, yNeck);
    //   let temp = xNeck+vect.x;
    //   if(xNeck<temp) {
    //     triangle(temp, yNeck - 3, temp, yNeck + 3, temp +3, yNeck);
    //   }else {
    //     triangle(temp, yNeck - 3, temp, yNeck + 3, temp -3, yNeck);
    //   }
    // }
    pop();
  }

  // draw the openpose detection

  // let sizehead = int(dist(xNeck, yNeck, xMid, yMid)/3);
  // line(xNeck+20, yNeck, xNeck+20, yNeck - sizehead*fact);
  // line(xNeck+20, yNeck, xNeck+20, (yNeck + sizehead*8));
  // text(fact, xNeck-50, yNeck-20);
  if(!bbox || (keypoints[0*3]>bbox[0] && keypoints[0*3]<bbox[2] && keypoints[0*3+1]>bbox[1] && keypoints[0*3+1]<bbox[3])) {
    line(xNose,yNose,xNeck,yNeck);
    line(xNeck, yNeck, xMid, yMid);
    for(let x of [2,5,9,12]) {
      c = color(0,250,100,alpha);
      var x3 = keypoints[(x)*3]-x_off;
      var y3 = keypoints[(x)*3+1]-y_off;
      stroke(c);
      strokeWeight(downscale_size);
      if(x<6)
        line(xNeck,yNeck,x3,y3);
      else {
        line(xMid,yMid,x3,y3);
      }
      for(var i=0; i<2; i++) {
        c = color(0,100,250,alpha);
        var j = x+i;
        var x1 = keypoints[j*3]-x_off;
        var y1 = keypoints[j*3+1]-y_off;
        var x4 = keypoints[(j+1)*3]-x_off;
        var y4 = keypoints[(j+1)*3+1]-y_off;
        stroke(c);
        strokeWeight(downscale_size);
        line(x1,y1,x4,y4);
      }
    }
    for(var i=0; i<25; i++) {
      noStroke();
      c = color(255,100,0,alpha);
      // c.setAlpha(100);
      fill(c);
      ellipse(keypoints[i*3]-x_off,keypoints[i*3+1]-y_off,downscale_size);
    }
  }
  pop();
}

/**
 * [drawBuffer => Draw the waveform of an audio file]
 */
function drawBuffer() {
  let w = player.w;
  let h = 150;
  let res = Math.round(video.duration()/60)*30;
  if(sound_file) {
    let data = sound_file.buffer.getChannelData(1);
    let step = Math.ceil(data.length/w);
    let amp = h/2;
    for(let i=0; i < w; i++){
      let min = 1.0;
      let max = -1.0;
      for (let j=0; j<step; j+=Math.ceil(step/res)) {
          let datum = data[(i*step)+j];
          if (datum < min)
              min = datum;
          if (datum > max)
              max = datum;
      }
      push();
      translate(0,height-h);
      if(i/w < video.time()/video.duration()) {
        let len = w * video.time()/video.duration();
        colorMode(HSB);
        fill(200+i/len*80,100,100);
      } else {
        fill('blue');
      }
      rect(i+player.x, (1+min)*amp, 1 ,Math.max(1,(max-min)*amp));
      pop();
    }
  }
}

// -----------------------------------------------------------------------------------------------------------------

/*
  unused functions
*/

function enhancePose(l_offset, r_offset, index, k, first_frame, detections_track) {
  const reducer = (accumulator, currentValue) => accumulator + currentValue;
  var keypoints = k;

  var new_poses = [];
  var poses_null = [];
  var len = keypoints.length / 3;
  for(var i = 0; i<len; i++) {
    if(!keypoints[i*3] || keypoints[i*3] == "null") {
      poses_null.push(i*3);
    }
  }

  for(var x=0; x<poses_null.length; x++) {
    var j = poses_null[x];
    var n_p = {};
    n_p.Id = j;
    var means_x = [];
    var means_y = [];
    var means_c = [];
    for(var i=index-l_offset; i<index+r_offset; i++){
      if(i!=index) {
        var frame_keypoint = frames_data[first_frame+i];
        var keypoints_to_compute = frame_keypoint[detections_track[i]]['KeyPoints'];
        if(keypoints_to_compute[j] && keypoints_to_compute[j]!='null') {
          means_x.push(keypoints_to_compute[j]);
          means_y.push(keypoints_to_compute[j+1]);
          means_c.push(keypoints_to_compute[j+2]);
        }
      }
    }
    n_p.MeansX = means_x;
    n_p.MeansY = means_y;
    n_p.MeansC = means_c;
    new_poses.push(n_p);
  }
  for(var i = 0; i<new_poses.length; i++) {
    if(new_poses[i].MeansX.length>1)
      keypoints[new_poses[i].Id] = new_poses[i].MeansX.reduce(reducer) / new_poses[i].MeansX.length;
    if(new_poses[i].MeansY.length>1)
      keypoints[new_poses[i].Id+1] = new_poses[i].MeansY.reduce(reducer) / new_poses[i].MeansY.length;
    if(new_poses[i].MeansC.length>1)
      keypoints[new_poses[i].Id+2] = new_poses[i].MeansC.reduce(reducer) / new_poses[i].MeansC.length;
  }
  return keypoints;
}

function getStartEndImg() {
  if(shots_timeline.released) {
    loadPixels();
    var b = false;
    for(var i=0; i<viewer_height; i++) {
      for(var j=0; j<viewer_width; j++) {
        var index = (i*windowWidth+j)*4;
        if(pixels[index+0]!=back_color) {
          b = true;
          break;
        }
      }
    }
    if(b) {
      img = createImage(viewer_width, viewer_height);
      img.loadPixels();
      for(var i=0; i<viewer_height; i++) {
        for(var j=0; j<viewer_width; j++) {
          var index = (i*windowWidth+j)*4;
          var c = color(pixels[index+0], pixels[index+1], pixels[index+2]);
          img.set(j, i, c);
        }
      }
      img.updatePixels();
      shots_timeline.drop_shot.img_start = img;
      shots_timeline.released = false;
    }
    updatePixels();
  }
}

// Return true if all key points of openpose are detected
function isFull(keypoints) {
  var ret = true;
  for(var i = 0; i<keypoints.length; i++) {
    if(!keypoints[i] || keypoints[i] == 'null') {
      ret = false;
      break;
    }
  }
  return ret;
}

function smoothDetections(off) {
  for(let t of tracklets_line) {
    var detections_track = t.detections;
    var first_frame = t.first_frame;
    for(var j=0; j<detections_track.length; j++) {
      var keypoints = frames_data[first_frame+j];
      var l_offset
      if(j<off)
        l_offset = j;
      else {
        l_offset = off;
      }
      var r_offset;
      if(j>detections_track.length-off)
        r_offset = detections_track.length-j;
      else {
        r_offset = off;
      }
      if(!isFull(keypoints[detections_track[j]]['KeyPoints'])) {
        var test = enhancePose(l_offset, r_offset, j, keypoints[detections_track[j]]['KeyPoints'], first_frame, detections_track);
        frames_data[first_frame+j][detections_track[j]]['KeyPoints'] = test;
      }
    }
  }
}

// Extract the coordinates of the ROI of all actors present in the shot bounding box
function getROI(acts, x_off, y_off, lim_w, lim_h) {
  let roi = [];
  let keypoints = frames_data[frame_num];
  for(let act_name of acts) {
    let act = preparation_editor.getAct(act_name);
    for(let t of act.tracks) {
      var detections_track = t.detections;
      var first_frame = t.first_frame;
      if(first_frame < frame_num) {
        if(detections_track.length > (frame_num-first_frame)) {
          if(detections_track[frame_num-first_frame] < keypoints.length) {
            if(keypoints[detections_track[frame_num-first_frame]]) {
              let bbox = getBBox(keypoints[detections_track[frame_num-first_frame]]['KeyPoints'], 0.05);
              bbox[1] = bbox[1]-(bbox[3]-bbox[1])*0.05;
              bbox[3] = bbox[3]+(bbox[3]-bbox[1])*0.05;
            }
            if(bbox) {
              let scale = Number(original_height)/(lim_h-y_off);
              let x = bbox[2]-x_off;
              let y = bbox[3]-y_off;
              let w = x -(bbox[0]-x_off);
              let h = y - (bbox[1]-y_off);
              bbox = [(bbox[0]-x_off)*scale, (bbox[1]-y_off)*scale, w*scale, h*scale];
              roi.push(bbox);
            }
          }
        }
      }
    }
  }
  return roi;
}

// -----------------------------------------------------------------------------------------------------------------

/*
  Main setup and draw P5
*/

/**
 * [setup => create and setup all the objects.]
 */
function setup() {
  tool_tip.is_on = false;
  tool_tip.text = "";
  // loadJSON(json_detec, gotDetects);

  player = new Player();
  shots_timeline = new ShotsTimeline();

  loadDetec();
  loadShots();
  if(video) {

    submit = createButton('Save');
    submit.id('submit');
    if(!sub_granted) {
      submit.hide();
    } else {
      submit.mouseOver(processToolTip('Save your work'));
      submit.mouseOut(processToolTip(''));
      html_elements.push(submit);
      submit.mousePressed(saveTimeline);
    }

    documentation_button = createButton('Access to documentation');
    documentation_button.mouseOver(processToolTip('Open the wiki'));
    documentation_button.mouseOut(processToolTip(''));
    html_elements.push(documentation_button);
    documentation_button.mousePressed(goToDocumentation);

    hide_show_header_button = createButton('Hide/Show Header');
    hide_show_header_button.mouseOver(processToolTip('Hide or show the header'));
    hide_show_header_button.mouseOut(processToolTip(''));
    html_elements.push(hide_show_header_button);
    hide_show_header_button.mousePressed(hideShowHeader);

    reset_pos = createButton('Reset Zoom');
    reset_pos.mouseOver(processToolTip('Reset the zoom scale factor'));
    reset_pos.mouseOut(processToolTip(''));
    html_elements.push(reset_pos);
    reset_pos.mousePressed(resetPos);

    reload_button = createButton('Refresh');
    reload_button.mouseOver(processToolTip('Reload the stream when the video freeze'));
    reload_button.mouseOut(processToolTip(''));
    html_elements.push(reload_button);
    reload_button.mousePressed(reloadVideo);

    check_render_pose = createCheckbox('Render pose', false);
    check_render_pose.mouseOver(processToolTip('Show the openpose detections on the viewer'));
    check_render_pose.mouseOut(processToolTip(''));
    html_elements.push(check_render_pose);
    check_render_pose.size(150,30);
    check_render_pose.changed(updateDrawPose);

    cadrage_editor = new CadrageEditor();
    montage_editor = new MontageEditor();
    annotation_editor = new AnnotationEditor();
    export_editor = new ExportEditor();
    preparation_editor = new PreparationEditor();
    split_screen_editor = new SplitScreenEditor();

    loadJSONNotesAndTimeline();

    loadSubtile();
    scale_ratio = video.elt.videoWidth/Number(original_width);
    aspect_ratio = Number(original_width)/Number(original_height);
    ratio_type = aspect_ratio;
    viewer_height = windowHeight*((1/2)*viewer_scale);

    annotation_editor.loadAnnotationEditorData();

    cadrage_editor_button = createButton('Framing');
    cadrage_editor_button.mouseOver(processToolTip('Framing interface'));
    // cadrage_editor_button = createButton('Cadrage');
    // cadrage_editor_button.mouseOver(processToolTip('Interface de cadrage'));
    cadrage_editor_button.mouseOut(processToolTip(''));
    cadrage_editor_button.position(150, can.elt.offsetTop-30);
    cadrage_editor_button.class('tabs');
    cadrage_editor_button.mousePressed(showCadrageEditor);

    montage_editor_button = createButton('Video Editing');
    montage_editor_button.mouseOver(processToolTip('Video editing interface'));
    // montage_editor_button = createButton('Montage');
    // montage_editor_button.mouseOver(processToolTip('Interface de montage'));
    montage_editor_button.mouseOut(processToolTip(''));
    montage_editor_button.position(300, can.elt.offsetTop-30);
    montage_editor_button.class('tabs');
    montage_editor_button.mousePressed(showMontageEditor);

    annotation_editor_button = createButton('Annotation');
    annotation_editor_button.mouseOver(processToolTip("Annotation interface"));
    // annotation_editor_button.mouseOver(processToolTip("Interface d'annotation"));
    annotation_editor_button.mouseOut(processToolTip(''));
    annotation_editor_button.position(450, can.elt.offsetTop-30);
    annotation_editor_button.class('tabs');
    annotation_editor_button.mousePressed(showAnnotationEditor);

    export_editor_button = createButton('Export');
    export_editor_button.mouseOver(processToolTip("Export interface"));
    // export_editor_button.mouseOver(processToolTip("Interface d'export"));
    export_editor_button.mouseOut(processToolTip(''));
    export_editor_button.position(750, can.elt.offsetTop-30);
    export_editor_button.class('tabs');
    export_editor_button.mousePressed(showExportEditor);

    split_screen_editor_button = createButton('Split screen');
    split_screen_editor_button.mouseOver(processToolTip('Split screen player'));
    // split_screen_editor_button.mouseOver(processToolTip('Interface de preparation'));
    split_screen_editor_button.mouseOut(processToolTip(''));
    split_screen_editor_button.position(600, can.elt.offsetTop-30);
    split_screen_editor_button.class('tabs');
    split_screen_editor_button.mousePressed(showSplitScreenPlayer);

    preparation_editor_button = createButton('Preparation');
    preparation_editor_button.mouseOver(processToolTip('Preparation interface'));
    // preparation_editor_button.mouseOver(processToolTip('Interface de preparation'));
    preparation_editor_button.mouseOut(processToolTip(''));
    preparation_editor_button.position(0, can.elt.offsetTop-30);
    preparation_editor_button.class('tabs');
    preparation_editor_button.mousePressed(showPreparationEditor);

    act_input = createInput();
    act_input.side = false;
    html_elements.push(act_input);
    act_input.changed(createActTimeline);
    act_input.position(0, viewer_height+1);

    let k=0;
    let off = 5;
    for(let i=0; i<html_elements.length;i++) {
      let elem = html_elements[i];
      if(elem.side == undefined) {
        elem.side = true;
        if(html_elements[i-1] && html_elements[i-1].elt.clientHeight>30) {
          off += html_elements[i-1].elt.clientHeight-27;
        }
        elem.position(windowWidth-160,off+k*30);
        elem.class('side');
        k++;
      }
      elem.original_x = elem.position().x;
      elem.original_y = elem.position().y;
    }
  }
  frameRate(frame_rate);
  textFont('Courier, monospace');
  total_frame = Math.floor(video.duration()*frame_rate);
  montage_editor.createContextShot();
  showAllElt();

  // auto save interval
  const interval_auto_save = setInterval(autoSave, 5*60*1000);
}

/**
 * [draw => This function is the main display function.
 *          It's called every frame.
 *          It's managed the display of all the objects.]
 */
function draw() {

  if(!img_hd && (annotation_editor.is_note_book || is_montage_editor || is_cadrage_editor)) {
    p5ImageFromDash();
  } else if(!img_hd){
    image_frame = video;
  }
  if(video.duration()) {
    total_frame = Math.floor(video.duration()*frame_rate);
    frame_num = Math.floor(video.time()*frame_rate)%total_frame;
  }

  // ---------------------------------------------------------------
  // Manage the UI elements and resize them
  // ---------------------------------------------------------------
  var x_vid = 0;
  var y_vid = 0;
  mid_width = windowWidth*(3/5);
  viewer_height = windowHeight*((1/2)*viewer_scale);
  if(split_screen_editor.is_display_player) {
    mid_width = windowWidth*(3/5);
    viewer_height = windowHeight/2;
  }

  if(double_click) {
    if(!fullscreen()) {
      showAllElt();
      hideShowHeader();
    } else {
      hideAllElt();
    }
    double_click = false;
  }

  if(viewer_width!=mid_width || up_rough || can.height != windowHeight-can.elt.offsetTop-5){

    if($('#header_info').css('display') !== 'none') {
      can.size(windowWidth, windowHeight-can.elt.offsetTop-5);
    } else {
      can.size(windowWidth, windowHeight-can.elt.offsetTop-5);
      can.position(0,30);
    }

    if(!fullscreen()) {
      updateSideElems();
      if(is_preparation_editor) {
        preparation_editor.resizeElt();
      }
      if(is_cadrage_editor) {
        cadrage_editor.resizeElt();
      }
      if(is_montage_editor) {
        montage_editor.resizeElt();
      }
      if(is_annotation_editor) {
        annotation_editor.resizeElt();
      }
      if(is_export_editor) {
        export_editor.resizeElt();
      }
      if(is_split_screen_editor){
        split_screen_editor.resizeElt();
      }
      for(let elem of html_elements) {
        if(!elem.side) {
          let x = elem.original_x;
          let y = elem.original_y + can.elt.offsetTop;
          elem.position(x,y)
        }
      }
      act_input.position(130, can.elt.offsetTop+viewer_height+1);

      for(let el of selectAll('.tabs')) {
        el.position(el.position().x,can.elt.offsetTop-30);
      }
    }

    up_rough = false;
  }

  // ---------------------------------------------------------------
  // Compute the canvas size + the player and offset size
  // ---------------------------------------------------------------
  x_off = 0;
  y_off = 0;
  if(fullscreen()) {
    mid_width = screen.width;
    if(can.height<=screen.height){
      can.size(screen.width, screen.height);
    }
    if(screen.width/aspect_ratio < screen.height) {
      viewer_width = screen.width;
      viewer_height = screen.width/aspect_ratio;
      y_vid = (screen.height - viewer_height) / 2;
    } else {
      viewer_height = screen.height;
      viewer_width = viewer_height*aspect_ratio;
      x_vid = (screen.width - viewer_width) / 2;
    }
    vid_h = viewer_height;
    vid_w = viewer_width;
    background(0);
  } else {
    if(can.height>=screen.height){
      can.size(windowWidth, viewer_height * 2-5);
    }
    viewer_width = mid_width;
    let a_s = aspect_ratio;
    if(split_screen_editor.is_display_player) {
      viewer_height = windowHeight*((3/4)*viewer_scale);
    }
    if(viewer_height*a_s>mid_width){
      vid_h = mid_width/a_s;
      vid_w = mid_width;
    } else {
      vid_h = viewer_height;
      vid_w = vid_h*a_s;
    }
    x_off = (mid_width - vid_w)/2;
    y_off = (viewer_height-vid_h)/2;
    background(255);
    push();
    fill(0);
    rect(0,0,mid_width, viewer_height);
    fill(back_color);
    rect(0,viewer_height,viewer_width, viewer_height*2);
    pop();
  }
  scale_ratio = video.elt.videoWidth/Number(original_width);

  // ---------------------------------------------------------------
  // Manage the helping message system
  // ---------------------------------------------------------------
  noStroke();
  if(tool_tip.is_on) {
    tool_tip.p.style('background-color','white');
    tool_tip.p.style('border','1px solid grey');
    tool_tip.p.position(mouseX, can.elt.offsetTop+mouseY);
  }

  // ---------------------------------------------------------------
  // Display the original video or the choosen rush
  // ---------------------------------------------------------------
  push();
  let x = x_off;
  let y = y_off;
  if(x < 0){ x=0;}
  if(y<0){y=0;}
  translate(x, y)
  if( (!cadrage_editor.is_shots_frame_layout && (is_montage_editor || is_cadrage_editor || is_export_editor || is_annotation_editor))) {
    if(video.duration()) {
      let bbox;
      if((is_montage_editor || is_export_editor || (annotation_editor.is_partition_editor && annotation_editor.partition_editor.is_scene)) && !(montage_editor.is_split)) {
        bbox = shots_timeline.getCurrStabShot(frame_num);
      }
      if(((is_montage_editor || is_cadrage_editor) || annotation_editor.is_note_book) && show_shot) {
        bbox = show_shot.getCurrStabShotScale(frame_num);
      }
      if (bbox) {
          var a_s;
          if(show_shot) {
            a_s = show_shot.aspect_ratio;
          }
          if((is_montage_editor || is_export_editor || annotation_editor.partition_editor.is_scene) && !show_shot) {
            a_s = shots_timeline.getCurrShot(frame_num).aspect_ratio;
          }
          if(!a_s){a_s = aspect_ratio;}
          if(vid_h*a_s<mid_width) {
            x_vid = (vid_w - vid_h*a_s)/2;
            vid_w = vid_h*a_s;
            if(vid_h == screen.height) {
              x_vid = (screen.width - viewer_width) / 2;
            }
          } else {
            vid_w = mid_width;
            x_vid=0;
            translate(-x, 0);
            x=0;
            y_vid = (vid_h - vid_w/a_s)/2;
            if(vid_w == screen.width) {
              y_vid = (screen.height - viewer_height) / 2;
            }
            vid_h = vid_w/a_s;
          }
          x_off+=x_vid;
          y_off+=y_vid;
          if(img_hd) {
            let ratio = img_hd.width / video.elt.videoWidth;
            image(img_hd, x_vid,y_vid,vid_w,vid_h, bbox[0]*ratio, bbox[1]*ratio, bbox[2]*ratio - bbox[0]*ratio, bbox[3]*ratio - bbox[1]*ratio);
          } else {
            image(image_frame, x_vid,y_vid,vid_w,vid_h, bbox[0], bbox[1], bbox[2] - bbox[0], bbox[3] - bbox[1]);
          }
      } else {
        if(img_hd) {
          image(img_hd, x_vid,y_vid,vid_w,vid_h);
        } else {
          image(image_frame, x_vid,y_vid,vid_w,vid_h); // draw a second copy to canvas
        }
      }
    }
  } else {
    if(split_screen_editor.is_display_player && !fullscreen()) {
      let top_pos = split_screen_editor.slider_top.value();
      bbox = [0, top_pos*scale_ratio, (Number(original_width))*scale_ratio, (top_pos+(Number(original_height)/2))*scale_ratio];
      y_vid += vid_h/2;
      image(image_frame, x_vid, y_vid, vid_w, vid_h/2, bbox[0], bbox[1], bbox[2] - bbox[0], bbox[3] - bbox[1]);
    }else if(img_hd) {
      image(img_hd, x_vid, y_vid, vid_w, vid_h); // draw a second copy to canvas
    } else {
      image(image_frame, x_vid, y_vid, vid_w, vid_h); // draw a second copy to canvas
    }
  }
  pop();

  // getStartEndImg();

  if(!fullscreen()) {
    let w = player.w;
    player.updatePos(95, viewer_height-15, (viewer_width-90-20), 10);
    setCursor();

    if(w!=player.w){shots_timeline.updatePos();}

    if(is_montage_editor) {
      montage_editor.display();
    }

    player.display();

    if(is_preparation_editor) {
      preparation_editor.display();
    }
    if(is_cadrage_editor) {
      cadrage_editor.display();
    }

    if(is_annotation_editor) {
      annotation_editor.display();
    }
    if(is_export_editor) {
      export_editor.display();
    }
    if(is_split_screen_editor) {
      split_screen_editor.display();
    }

    if(keyIsDown(17)) {
      key_down = 17;
    }

  } else {
    // hideAllElt();
    // createVideoTimer(y_vid + viewer_height-15);
    player.updatePos(95, y_vid + viewer_height-15, (viewer_width-90-20), 10);
    push();
    if(vid_h == screen.height) {translate(x_vid,0);}
    setCursor();
    player.display();

    if(x_off<0){x_off=0;}
    if(y_off<0){y_off=0;}
    push();
    if(vid_h != screen.height) {translate(x_vid,y_vid);}
    scale(vid_h/Number(original_height));
    if((is_montage_editor || is_cadrage_editor) && cadrage_editor.is_shots_frame_layout) {
      montage_editor.drawShotsLayout();
    }
    if(render_pose)
      preparation_editor.drawTracklets();
    pop();
    pop();
  }

  // if(is_preparation_editor) {
  //   drawBuffer();
  // }

  // Show the informations in the player
  preparation_editor.displayCounter();

}

// -----------------------------------------------------------------------------------------------------------------

/*
  Events manager
*/

function mousePressed() {

  if(is_preparation_editor) {
    preparation_editor.mousePressed(mouseX, mouseY);
  }
  if(is_cadrage_editor) {
    cadrage_editor.mousePressed(mouseX, mouseY);
  }
  if(is_montage_editor) {
    montage_editor.mousePressed(mouseX, mouseY);
  }
  if(is_annotation_editor) {
    annotation_editor.mousePressed(mouseX, mouseY);
  }
  if(is_export_editor) {
    export_editor.mousePressed(mouseX, mouseY);
  }
  if(is_split_screen_editor) {
    split_screen_editor.mousePressed(mouseX, mouseY);
  }

  if((montage_editor.is_split || annotation_editor.is_note_book) && (mouseY>viewer_height || mouseX>viewer_width)) {
    let b = false;
    for(let s of montage_editor.shots) {
      if(s.showInViewer(mouseX, mouseY)) {
        show_shot = s;
        b = true;
        break;
      }
    }
    if(!b) {
      show_shot = undefined;
    }
  }

  if(mouseX<width && mouseY < height) {
    var retime = false;

    player.on = false;
    player.click(mouseX, mouseY);
    if(player.on && player.time) {
      retime = true;
      video.time(player.time);
      img_hd = undefined;
    }
    if(!retime) {
      if(mouseX >0 && mouseY>0 && mouseY < viewer_height && mouseX < viewer_width) {
        if(preparation_editor.is_bbox_creation && !(mouseX > player.x-85 && mouseX < player.x-25 && mouseY > player.y-50 && mouseY< player.y)) {
          playing = true;
          dash_player.pause();
          img_hd = undefined;
        }else if (playing) {
          time_hd = video.time();
          // imgHDRequest();
          dash_player.pause();
        } else {
          if(is_montage_editor) {
            shots_timeline.previous_time = video.time();
          }
          if(!cadrage_editor.is_shot_personalize && !(is_preparation_editor && !player.clickOnPlayPause(mouseX,mouseY)))
            dash_player.play();
          img_hd = undefined;
        }
        if(!(is_preparation_editor && !player.clickOnPlayPause(mouseX,mouseY) && !preparation_editor.curr_creation))
          playing = !playing; // set the video to loop and start playing
      }
    }
  }
  keyCode = undefined;
}


function mouseDragged() {
  if(!is_annotation_editor) {
    event.preventDefault();
  }

  if(is_preparation_editor) {
    preparation_editor.drag(mouseX, mouseY);
  }
  if(is_cadrage_editor) {
    cadrage_editor.drag(mouseX, mouseY);
  }
  if(is_montage_editor) {
    montage_editor.drag(mouseX, mouseY);
  }
  if(is_annotation_editor) {
    annotation_editor.drag(mouseX, mouseY);
  }
  if(is_export_editor) {
    export_editor.drag(mouseX, mouseY);
  }
  if(is_split_screen_editor) {
    split_screen_editor.drag(mouseX, mouseY);
  }

  dash_player.pause();
  playing = false;

  if(!cadrage_editor.is_drag) {
    player.dragNavBar(mouseX, mouseY);

    player.drag(mouseX, mouseY);
  }

}

function mouseReleased() {
  player.drop();

  if(is_preparation_editor) {
    preparation_editor.drop(mouseX, mouseY);
  }
  if(is_cadrage_editor) {
    cadrage_editor.drop(mouseX, mouseY);
  }
  if(is_montage_editor) {
    montage_editor.drop(mouseX, mouseY);
  }
  if(is_annotation_editor) {
    annotation_editor.drop(mouseX, mouseY);
  }
  if(is_export_editor) {
    export_editor.drop(mouseX, mouseY);
  }
  if(is_split_screen_editor) {
    split_screen_editor.drop(mouseX, mouseY);
  }
}


function mouseWheel(event) {
  if(keyIsPressed) {
    //shift key = 16
    if(keyCode===16) {
      if(event.delta<0) {
          viewer_scale += 0.1;
          up_rough=true;
      } else {
          viewer_scale -= 0.1;
          up_rough=true;
      }
      // console.log(event.delta, viewer_scale);
    }
    //z = 122
  }
  if((montage_editor.is_split || annotation_editor.is_note_book) &&
  (mouseX >0 && mouseX < mid_width)) {
    // increase offset_split
    if(event.delta<0) {
      if(offset_split>0)
        offset_split--;
    } else {
        offset_split++;
    }
    // offset_split++;
  }
  player.mouseWheel(event);
  if(is_preparation_editor) {
    preparation_editor.mouseWheel(event);
  }
  if(is_annotation_editor) {
    annotation_editor.mouseWheel(event);
  }
  if(is_cadrage_editor) {
    cadrage_editor.mouseWheel(event);
  }
}

function doubleClicked() {
  var fs = fullscreen();
  if(mouseX<viewer_width && mouseY<viewer_height) {
    fullscreen(!fs);
    double_click = true;
  }
  if(is_montage_editor) {
    montage_editor.doubleClicked(mouseX, mouseY);
  }
  if(is_annotation_editor && annotation_editor.is_partition_editor) {
    annotation_editor.partition_editor.doubleClicked(mouseX, mouseY);
  }
}

document.addEventListener("fullscreenchange", function( event ) {
    double_click = true;
});

$(document).keypress(function(e) {
    // prevent the default event when the space bar is pressed
    if(e.keyCode === 32) {
      event.preventDefault();
    }
});

function keyPressed() {

  if(is_preparation_editor) {
    preparation_editor.keyPressed(keyCode);
  }
  if(is_cadrage_editor) {
    cadrage_editor.keyPressed(keyCode);
  }
  if(is_montage_editor) {
    montage_editor.keyPressed(keyCode);
  }
  if(is_annotation_editor) {
    annotation_editor.keyPressed(keyCode);
  }
  if(is_export_editor) {
    export_editor.keyPressed(keyCode);
  }
  if(is_split_screen_editor) {
    split_screen_editor.keyPressed(keyCode);
  }
  if (keyCode === 32 && !document.activeElement.isContentEditable && !(document.activeElement.type!=undefined && document.activeElement.type=="text")) {
    if(!annotation_editor.is_note_editor && !annotation_editor.is_partition_editor && !annotation_editor.is_scene_annotation) {
      if (playing) {
        time_hd = video.time();
        // imgHDRequest();
        dash_player.pause();
      } else {
        if(is_montage_editor) {
          shots_timeline.previous_time = video.time();
        }
        dash_player.play();
        img_hd = undefined;
      }
      playing = !playing;
    }
  } else if(keyCode == 37 && !annotation_editor.is_partition_editor) {
    img_hd = undefined;
    video.time(video.time()-0.05);
  } else if(keyCode == 39 && !annotation_editor.is_partition_editor) {
    img_hd = undefined;
    video.time(video.time()+0.05);
  }

}

// -----------------------------------------------------------------------------------------------------------------
