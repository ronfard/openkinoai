/*
This file is part of KinoAi project.
It is subject to the license terms in the LICENSE file found in the top-level directory
*/

/**
 * [BboxShot => a personalize bounding box (x,y,width,height) arround an actor on stage create by the user]
 * @param       {[float]} tempX  [x top left]
 * @param       {[float]} tempY  [y top left]
 * @param       {[float]} tempW  [width]
 * @param       {[float]} tempH  [height]
 * @constructor
 */
function BboxShot(tempX, tempY, tempW, tempH)  {
  this.x  = tempX;
  this.y  = tempY;
  this.w = tempW;
  this.h = tempH;
  this.on = false;

  this.created = false;
  this.in_creation = false;

  this.in_drag = false;

  this.center_x;
  this.center_y;

  this.off_center_x;
  this.off_center_y;

  this.off_x;
  this.off_y;

  /**
   * [click => handle mouse click event]
   * @param  {[float]} mx               [mouse x position]
   * @param  {[float]} my               [mouse y position]
   */
  this.click = function(mx, my) {
    // Check to see if a point is inside the rectangle
    this.on = false;
    this.in_drag = false;

    if (mx > this.x && mx < this.x + this.w && my > this.y && my < this.y + this.h) {
      this.on = true;
      this.modifCenter(mx,my);
      this.in_drag = true;
      let x = mouseX-x_off;
      let y = mouseY-y_off;
      x /=(vid_h/Number(original_height));
      y /=(vid_h/Number(original_height));
      x = Math.round(x);
      y = Math.round(y);
      this.off_x = x - this.x;
      this.off_y = y - this.y;
      this.off_center_x = x - this.center_x;
      this.off_center_y = y - this.center_y;
    }
  };

  /**
   * [modifCenter => modif the center of the bounding box]
   * @param  {[float]} mx               [mouse x position]
   * @param  {[float]} my               [mouse y position]
   */
  this.modifCenter = function(mx, my) {
    // If key c is pressed modif center
    if(keyIsPressed && keyCode == 99) {
      this.center_x = mx;
      this.center_y = my;
    }
  }

  /**
   * [drag => handle drag event]
   * @param  {[float]} mx               [mouse x position]
   * @param  {[float]} my               [mouse y position]
   */
  this.drag = function(mx, my) {
    let x = mouseX-x_off;
    let y = mouseY-y_off;
    x /=(vid_h/Number(original_height));
    y /=(vid_h/Number(original_height));
    x = Math.round(x);
    y = Math.round(y);
    // If key ctrl is pressed extend box
    if(!keyIsPressed) {
      this.x = x -this.off_x;
      this.y = y -this.off_y;
      this.center_x = x - this.off_center_x;
      this.center_y = y -this.off_center_y;
    }
    if(keyIsPressed && (event.ctrlKey || event.metaKey)) {
      this.w = x-this.x;
      this.h = y-this.y;
    }
  }

  /**
   * [drop => handle drop event]
   * @param  {[float]} mx               [mouse x position]
   * @param  {[float]} my               [mouse y position]
   */
  this.drop = function(mx, my) {
    this.on = false;
    this.in_drag = false;
  }

  /**
   * [setPosition => set the top left position (x,y) of the bounding box]
   * @param  {[float]} tx               [x position]
   * @param  {[float]} ty               [y position]
   */
  this.setPosition = function(tx, ty) {
    this.x = tx;
    this.y = ty;
  }

  /**
   * [setDimension => set the size of the bounding box (width,height)]
   * @param  {[float]} tempW               [width]
   * @param  {[float]} tempH               [height]
   */
  this.setDimension = function(tempW, tempH) {
    this.w = tempW;
    this.h = tempH;
  }

  /**
   * [setCenter => set the center (neck position of the actor) of the bounding box]
   * @param  {[float]} tx               [x center]
   * @param  {[float]} ty               [y center]
   */
  this.setCenter = function(tx, ty) {
    this.center_x = tx;
    this.center_y = ty;
  }

  /**
   * [display => function called in the main p5 draw function]
   * @param  {[actor object]} [act=undefined]               [actor]
   */
  this.display = function(act=undefined) {
    push();
    fill(255);
    if(act){
      textSize(15);
      text(act.actor_name, this.x, this.y-10);
    }
    noFill();
    strokeWeight(3);
    stroke('green');
    rect(this.x,this.y,this.w,this.h);
    stroke('blue');
    ellipse(this.center_x, this.center_y, 3);
    pop();

    if(x_off<0){x_off=0;}
    if(y_off<0){y_off=0;}
    let mx = (mouseX - x_off)/(vid_h/Number(original_height));
    let my = (mouseY - y_off)/(vid_h/Number(original_height));
    if(this.in_creation && this.x<mx && mx<this.x+this.w && this.y<my && my<this.y+this.h) {
      // Display a helping message
      push();
      scale(1);
      scale(Number(original_height)/vid_h);
      translate(-x_off,-y_off)
      fill(255);
      rect(0, height-20,windowWidth,20);
      fill('black');
      noStroke();
      textSize(16);
      text('Press C + click to set a new center, drag to move the box, press ctrl + drag to enlarge the box',0,height-5);
      pop();
    }

  }
}
