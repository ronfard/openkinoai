/*
This file is part of KinoAi project.
It is subject to the license terms in the LICENSE file found in the top-level directory
*/

/**
 * [ExportEditor => manage the export tab, create the html elements and the function for the export tab]
 * @constructor
 */
function ExportEditor()  {

  this.name_video = createP('Default');;
  this.name_video.side = false;
  html_elements.push(this.name_video);
  this.name_video.elt.contentEditable = true;
  this.name_video.style('margin',0);
  this.name_video.style('font-size',20);

  this.video_export_selected = createVideo();
  this.video_export_selected.side = false;
  html_elements.push(this.video_export_selected);

  this.div_export_video = createDiv();
  this.div_export_video.side = false;
  html_elements.push(this.div_export_video);

  this.reframe_button = createButton('Process Reframing');
  this.reframe_button.side = false;
  this.reframe_button.mouseOver(processToolTip('Reframe your video according to the timeline'));
  this.reframe_button.mouseOut(processToolTip(''));
  html_elements.push(this.reframe_button);
  this.reframe_button.mousePressed(reframeRequest);

  this.download_button = createButton('Download Video');
  this.download_button.side = false;
  this.download_button.mouseOver(processToolTip('Download the selected video'));
  this.download_button.mouseOut(processToolTip(''));
  html_elements.push(this.download_button);
  this.download_button.mousePressed(downloadVideo);

  this.delete_button = createButton('Delete Video');
  this.delete_button.side = false;
  this.delete_button.mouseOver(processToolTip('Delete the selected video'));
  this.delete_button.mouseOut(processToolTip(''));
  html_elements.push(this.delete_button);
  this.delete_button.mousePressed(deleteVideo);

  this.hide_button = createButton('Hide Video');
  this.hide_button.side = false;
  this.hide_button.mouseOver(processToolTip('Hide the selected video'));
  this.hide_button.mouseOut(processToolTip(''));
  html_elements.push(this.hide_button);
  this.hide_button.mousePressed(hideVideo);

  this.video_name = abs_path;
  this.progress_value;

  this.mousePressed = function(mx, my) {

  };

  this.drop = function(mx, my) {

  }

  this.drag = function(mx, my) {

  }

  this.keyPressed = function(keyCode) {

  }

  this.mouseWheel = function(event) {

  }

  /**
   * [updateAndShow => resize and show the export elements]
   * @param  {[Boolean]} checked               [is the tab checked]
   */
  this.updateAndShow = function(checked) {
    resetTabs();
    is_export_editor = true;
    clickTab(export_editor_button);
    this.showElts();
    act_input.hide();
    // Side el
    reset_pos.show();
    reload_button.show();
    hide_show_header_button.show();
    documentation_button.show();
    updateSideElems();
    up_rough = true;
  }

  /**
   * [hideElts => hide all elements]
   */
  this.hideElts = function() {
    this.name_video.hide();
    this.reframe_button.hide();
    this.download_button.hide();
    this.delete_button.hide();
    this.hide_button.hide();
    this.video_export_selected.hide();
    this.div_export_video.hide();
    cadrage_editor.shot_selector.hide();
    shots_timeline.select_timeline.hide();
    shots_timeline.user_title.side = true;
    shots_timeline.user_title.hide();
    shots_timeline.select_user.side = true;
    shots_timeline.select_user.hide();
  }

  /**
   * [showElts => show all elements]
   */
  this.showElts = function() {
    shots_timeline.x = player.x;
    shots_timeline.h = 100;
    shots_timeline.updateTimeline();
    for(let o of shots_timeline.list_data) {
      if(o.On) {
        this.name_video.elt.innerText = o.Name;
      }
    }
    this.div_export_video.show();
    this.updateExportVideo();
    this.reframe_button.show();
    this.name_video.show();
    shots_timeline.select_timeline.show();
    submit.hide();
    shots_timeline.user_title.side = false;
    shots_timeline.user_title.show();
    shots_timeline.select_user.side = false;
    shots_timeline.select_user.show();
  }

  /**
   * [updateExportVideo => show the videos already exported]
   */
  this.updateExportVideo = function() {
    while(this.div_export_video.elt.firstChild){this.div_export_video.elt.firstChild.remove();}
    for(let p of data_path_video_export) {
      let name = createElement('h3',p.split('/')[p.split('/').length-1]);
      name.style('float','left');
      name.style('border','2px solid grey');
      name.style('margin','5');
      name.style('padding','0 5');
      name.elt.onclick = function() { selectVideo(name); }
      name.src_video = p;
      this.div_export_video.child(name);
    }
  }

  /**
   * [resizeElt => resize the elements]
   */
  this.resizeElt = function() {

    this.name_video.original_x = mid_width + 90;
    this.name_video.original_y = 14;
    this.reframe_button.original_x = mid_width+10;
    this.reframe_button.original_y = height-100;

    this.div_export_video.original_x= mid_width+10;
    this.div_export_video.original_y= 170;
    this.div_export_video.size(((windowWidth - 160)-mid_width)-10);

    this.download_button.original_x = mid_width+10;
    this.download_button.original_y = height-190;

    this.delete_button.original_x = mid_width+170;
    this.delete_button.original_y = height-190;

    this.hide_button.original_x = mid_width+330;
    this.hide_button.original_y = height-190;

    shots_timeline.y = viewer_height+70;
    shots_timeline.select_timeline.original_x = mid_width-175;
    shots_timeline.select_timeline.original_y = viewer_height;
    shots_timeline.select_timeline.size(150);
    shots_timeline.select_user.original_x = mid_width-175-160;
    shots_timeline.select_user.original_y = viewer_height;
    shots_timeline.user_title.original_x = mid_width-175-160-100;
    shots_timeline.user_title.original_y = viewer_height;
  }

  /**
   * [selectVideo => select a video in the list]
   * @param  {[element]} elem               [video element]
   */
  function selectVideo(elem) {
    while(export_editor.video_export_selected.elt.firstChild){export_editor.video_export_selected.elt.firstChild.remove();}
    export_editor.video_export_selected.src = elem.src_video;
    export_editor.video_export_selected.video_name = elem.elt.innerText;
    export_editor.video_export_selected.elt.currentTime =0;
    export_editor.video_export_selected.elt.load();
    export_editor.video_export_selected.showControls();
    export_editor.video_export_selected.show();
    export_editor.video_export_selected.position(mid_width+10,export_editor.div_export_video.position().y+50);
    export_editor.video_export_selected.size(100,100);
    export_editor.download_button.show();
    export_editor.delete_button.show();
    export_editor.hide_button.show();
  }

  /**
   * [callbackReframe => Add the reframed video to the list when the process is finished]
   * @param  {[list]} data               [callback data]
   */
  function callbackReframe(data) {
    let p = data['src'];
    data_path_video_export.push(p);
    export_editor.div_export_video.show();
    export_editor.updateExportVideo();
    alert('Your video is ready you can download it');

    export_editor.progress_value=undefined;
  }

  /**
   * [downloadVideo => download the selected video]
   */
  function downloadVideo() {
    let a = createA(export_editor.video_export_selected.src, 'link');
    a.elt.download = export_editor.video_export_selected.video_name;
    a.id('click');
    a.hide();
    // console.log(a);
    document.getElementById('click').click();
    a.remove();
  }

  /**
   * [callbackDelete => update the video list]
   * @param  {[list]} list               [path of video to remove]
   */
  function callbackDelete(list) {
    data_path_video_export = list;
    export_editor.updateExportVideo();
    hideVideo();
  }

  /**
   * [deleteVideo => delete the selected video]
   */
  function deleteVideo() {
    if(confirm('Are you sure ?')) {
      $.post({
        url: "delete_video",
        data: {'abs_path': abs_path, 'video_name':export_editor.video_export_selected.video_name},
        dataType: 'json',
        success: function (data) {
          console.log(data);
          if(data['message']=='done')
            return callbackDelete(data['list']);
        }
      });
    }
  }

  /**
   * [hideVideo => hide the video element]
   */
  function hideVideo() {
    export_editor.download_button.hide();
    export_editor.delete_button.hide();
    export_editor.hide_button.hide();
    export_editor.video_export_selected.hide();
  }

  /**
   * [reframeRequest => Launch the reframing of the video based on the shots timeline information]
   */
  function reframeRequest() {
    // console.log(shots_timeline.compressBBoxes(),export_editor.name_video.elt.innerText, shots_timeline.getAspectRatio());
    hideVideo();
    $.post({
      url: "reframeMov",
      data: {'abs_path': abs_path, 'bboxes':JSON.stringify(shots_timeline.compressBBoxes()), 't_start':shots_timeline.start_frame/frame_rate, 'name_export':export_editor.name_video.elt.innerText,
      't_end':shots_timeline.end_frame/frame_rate, 'is_full_split_screen':false, 'top_position':0, 'width':Number(original_width), 'aspect_ratio':shots_timeline.getAspectRatio()},
      dataType: 'json',
      success: function (data) {
        // console.log(data);
        if(data['msg']) {
          alert(data['msg']);
          clearInterval(interval);
          export_editor.progress_value=undefined;
        } else {
          return callbackReframe(data);
        }
      }
    });
    const intervalLength = 2000;
    const interval = setInterval(() => {
      $.post({
        url: "check_progress_bar",
        data: {'abs_path':abs_path},
        dataType: 'json',
        success: function (data) {
          export_editor.progress_value = int(data['progress']);
          if(export_editor.progress_value>=100) {
            clearInterval(interval);
            export_editor.progress_value=undefined;
          }
          // return callbackReframe(data);
        },
        error: function (data) {
          export_editor.progress_value = 'In process';
        }
      });
    }, intervalLength);
  }

  function sortSplitTab(a,b) {
    if (a[0] < b[0])
      return -1;
    if (a[0] > b[0])
      return 1;
    return 0;
  }

  /**
   * [timeToString => convert sec to min:sec:mil]
   * @param  {[float]} time               [time in seconds]
   */
  this.timeToString = function(time) {
    let min = toTwoDigit(Math.floor(time/60).toString());
    let sec = toTwoDigit(Math.floor(time%60).toString());
    let mil = toTwoDigit(round_prec((time%1)*100,0).toString());
    return min +':'+sec+'.'+mil;
  }

  /**
   * [display => function called in the main p5 draw function]
   */
  this.display = function() {

    push();
    fill(0);
    textSize(20);
    text('Title: ',mid_width+10,30);
    text('Start: '+this.timeToString(shots_timeline.start_frame/frame_rate),mid_width+10,70);
    text('End: '+this.timeToString(shots_timeline.end_frame/frame_rate),mid_width+210,70);
    text('Duration: '+this.timeToString((shots_timeline.end_frame/frame_rate)-(shots_timeline.start_frame/frame_rate)),mid_width+10,110);
    text('Resolution: 540p',mid_width+240,110);
    if(this.progress_value!=undefined) {
      text('Progression',mid_width+10,height-180);
      fill('#2E5C9C');
      rect(mid_width+10,height-150,(windowWidth-170)-(mid_width+10),15);
      fill("#28A1A8");
      rect(mid_width+10,height-150,((windowWidth-170)-(mid_width+10))*(this.progress_value/100),15);
      fill(255);
      textSize(15);
      text(this.progress_value+"%",mid_width+10,height-138);
    }
    pop();
    shots_timeline.display();
    if(this.is_split_export) {
      push();
      fill(0);
      textSize(20);
      text('Size : ',mid_width+10,150);
      text('Select actors : ',mid_width+105,150);
      pop();
    } else {
      push();
      fill(0);
      textSize(20);
      if(data_path_video_export.length>0)
        text('Videos exported',mid_width+10,150);
      pop();
    }

  }
}
