/*
This file is part of KinoAi project.
It is subject to the license terms in the LICENSE file found in the top-level directory
*/


/**
 * [MontageEditor => this class create all the elements and function for the video editing tab]
 * @constructor
 */
function MontageEditor()  {

  this.shots = [];

  this.all_types = createCheckbox('All rushes', true);
  this.all_types.side = false;
  this.all_types.mouseOver(processToolTip('Display all sizes'));
  this.all_types.mouseOut(processToolTip(''));
  html_elements.push(this.all_types);
  this.all_types.position(windowWidth/2 + 140, 40);
  this.all_types.changed(updateAllShotSelect);

  this.show_context = createCheckbox('Show context', true);
  this.show_context.side = false;
  this.show_context.mouseOver(processToolTip('Display the stage'));
  this.show_context.mouseOut(processToolTip(''));
  html_elements.push(this.show_context);
  this.show_context.position(windowWidth/2 + 320, 40);
  this.show_context.changed(updateShowContextSelect);

  this.show_sub = createCheckbox('Show subtitles', false);
  this.show_sub.mouseOver(processToolTip('Show subtitles in the player'));
  this.show_sub.mouseOut(processToolTip(''));
  html_elements.push(this.show_sub);
  this.show_sub.size(150,30);
  this.show_sub.changed(updateShowSub);

  this.is_all_types = true;
  this.is_show_context = true;
  this.all_types.hide();
  this.show_context.hide();

  /**
   * [mousePressed => handle mouse click event for the video editing tab]
   * @param  {[integer]} mx               [mouse X position]
   * @param  {[integer]} my               [mouse y position]
   */
  this.mousePressed = function(mx, my) {
    for(let s of this.shots) {
      if(mx > mid_width ) {
        s.on = false;
      }
      s.click(mouseX, mouseY);
    }
    keyCode = undefined;
    let b = shots_timeline.click(mouseX, mouseY);
  };

  /**
   * [doubleClicked => handle double click event for the video editing tab]
   * @param  {[integer]} mx               [mouse x position]
   * @param  {[integer]} my               [mouse y position]
   */
  this.doubleClicked = function(mx,my) {
    shots_timeline.resetButton(mx,my);
  }

  /**
   * [drop => handle drop event for the video editing tab]
   * @param  {[integer]} mx               [mouse x position]
   * @param  {[integer]} my               [mouse y position]
   */
  this.drop = function(mx, my) {
    let b = shots_timeline.drop(mouseX, mouseY);
    if(b!=undefined) {
      video.time(b);
      shots_timeline.released = true;
      img_hd = undefined;
    }
    for(let s of this.shots) {
      s.drag = false;
    }
  }

  /**
   * [drag => handle drag event for the video editing tab]
   * @param  {[integer]} mx               [mouse x position]
   * @param  {[integer]} my               [mouse y position]
   */
  this.drag = function(mx, my) {
    for(let s of this.shots) {
      s.draggin(mouseX, mouseY);
    }
    shots_timeline.drag(mouseX,mouseY);
  }

  /**
   * [keyPressed => handle keyPressed event for the video editing tab]
   * @param  {[integer]} keyCode               [key code event]
   */
  this.keyPressed = function(keyCode) {
    // delete key or backspace key
    if(keyCode===46 || keyCode===8) {
      this.removeShot();
      shots_timeline.stackHistory();
      shots_timeline.removeShot();
    }

    // S key
    if(keyCode===83) {
      shots_timeline.splitShot();
    }
  }

  this.mouseWheel = function(event) {
  }

  /**
   * [updateAndShow => reset all tabs and show the video editing elements]
   */
  this.updateAndShow = function() {
    resetTabs();
    is_montage_editor = true;
    clickTab(montage_editor_button);
    act_input.hide();
    this.showElts();
    // Side el
    check_render_pose.show();
    submit.show();
    reset_pos.show();
    reload_button.show();
    hide_show_header_button.show();
    documentation_button.show();
    updateSideElems();
    up_rough = true;
  }

  /**
   * [hideElts => hide all elements]
   */
  this.hideElts = function() {
    shots_timeline.hideElts();
    this.show_sub.hide();
    this.show_context.hide();
    let div_wrap_rushes = select('#div_wrap_rushes');
    if(div_wrap_rushes) {
      div_wrap_rushes.hide();
    }
  }

  /**
   * [showElts => show all elements]
   */
  this.showElts = function() {
    shots_timeline.showElts();
    if(video.elt.textTracks[0] && video.elt.textTracks[0].cues.length >0) {
      this.is_show_sub = this.show_sub.checked();
      this.show_sub.show();
    }
    this.show_context.show();
    let div_wrap_rushes = select('#div_wrap_rushes');
    if(div_wrap_rushes) {
      div_wrap_rushes.show();
    }
  }

  /**
   * [resizeElt => resize all elements]
   */
  this.resizeElt = function() {
    this.show_context.original_x = mid_width + 10;
    this.show_context.original_y = 10;
    cadrage_editor.shot_selector.original_x = mid_width+10;
    this.top_shot = this.show_context.original_y+40;
    let context_height = Math.min(height/3,(windowWidth-180-mid_width)/aspect_ratio);
    if(!is_cadrage_editor && this.is_show_context) {
      this.top_shot += context_height+10;
    }
    let div_wrap_rushes = select('#div_wrap_rushes');
    if(div_wrap_rushes) {
      div_wrap_rushes.size(windowWidth-160-mid_width, height-this.top_shot);
      div_wrap_rushes.position(mid_width,can.elt.offsetTop+this.top_shot);
    }

    shots_timeline.y = viewer_height+70;
    shots_timeline.resizeElt();
  }

  /**
   * [loadMontageEditorData => load the data from the server]
   */
  this.loadMontageEditorData = function() {
    for (var i=0; i<data_shots.length; i++) {
      if(!data_shots[i].SplitScreenShot) {
        if(data_shots[i].IsPersonalize) {
          let s = new Shot();
          s.aspect_ratio = data_shots[i].AspectRatio;
          s.start_frame = data_shots[i].StartFrame;
          s.end_frame = data_shots[i].EndFrame;
          s.type = data_shots[i].Type;
          s.bbox_perso = data_shots[i].BBoxPerso;
          s.is_personalize = data_shots[i].IsPersonalize;
          this.shots.push(s);
        }else if(data_shots[i].Timeline == 0) {
          let s = new Shot();
          s.type = data_shots[i].Type;
          s.start_frame = data_shots[i].StartFrame;
          s.end_frame = data_shots[i].EndFrame;
          s.bboxes = data_shots[i].BBoxes;
          s.setActInvoled(data_shots[i].ActInvolved);
          if(s.actors_involved.length!=data_shots[i].ActInvolved.length) {
            s.temp_tab_act = data_shots[i].ActInvolved;
          }
          s.scenes_involved = [];
          if(data_shots[i].ScenesInvolved) {
            for(let scene of data_shots[i].ScenesInvolved) {
              if(scene.split('._.').length == 1) {
                 s.scenes_involved.push(scene+'._.'+username);
              } else {
                s.scenes_involved.push(scene);
              }
            }
          }
          s.aspect_ratio = data_shots[i].AspectRatio;
          if(data_shots[i].GazeDir==undefined) {
            data_shots[i].GazeDir = false;
          }
          s.is_gaze_direction = data_shots[i].GazeDir;
          if(data_shots[i].KeepOut==undefined) {
            data_shots[i].KeepOut = false;
          }
          s.is_keep_out = data_shots[i].KeepOut;
          if(data_shots[i].PullIn==undefined) {
            data_shots[i].PullIn = false;
          }
          s.is_pull_in = data_shots[i].PullIn;
          if(data_shots[i].StagePos==undefined) {
            data_shots[i].StagePos = false;
          }
          s.is_stage_position = data_shots[i].StagePos;
          this.shots.push(s);
        }
      }
    }

    this.shots.sort(sortShotsByName);
    this.shots.sort(sortShotsByType);
    // this.shots.sort(sortShotsBySelected);
  }

  /**
   * [loadShotsTimelineData => load the video editing data from the server to create the timeline]
   * @param  {[JSON data]} data_time               [data]
   */
  this.loadShotsTimelineData = function(data_time) {
    let name = username;
    let timeline_data = data_time;
    if(data_time.Name) {
      name = timeline_data.Name;
      timeline_data = data_time.Data;
    }
    if(!shots_timeline.list_data)
      shots_timeline.list_data = [];
    for(let obj of Object.values(timeline_data)) {
      let tab_shots = [];
      let b = false;
      if(!obj.Data) {
        obj = {};
        obj.Data = Object.values(timeline_data);
        obj.Name = 'Default';
        obj.On = true;
        b=true;
      }
      for(let data_shots of obj.Data) {
        // console.log(data_shots);
        let s = {};
        s.type = data_shots.Type;
        s.start_frame = data_shots.StartFrame;
        s.end_frame = data_shots.EndFrame;
        s.aspect_ratio = data_shots.AspectRatio;
        if(data_shots.IsPersonalize) {
          s.is_personalize = data_shots.IsPersonalize;
          s.bbox_perso = data_shots.BBoxPerso;
          s.actors_involved = [];
        } else {
          if(data_shots.GazeDir ==undefined) {
            data_shots.GazeDir = false;
          }
          s.is_gaze_direction = data_shots.GazeDir;
          if(data_shots.KeepOut==undefined) {
            data_shots.KeepOut = false;
          }
          s.is_keep_out = data_shots.KeepOut;
          if(data_shots.PullIn==undefined) {
            data_shots.PullIn = false;
          }
          s.is_pull_in = data_shots.PullIn;

          if(data_shots.StagePos ==undefined) {
            data_shots.StagePos = false;
          }
          s.is_stage_position = data_shots.StagePos;
          let tab = [];
          for(let n of data_shots.ActInvolved) {
            if (preparation_editor.getAct(n))
              tab.push(preparation_editor.getAct(n));
          }
          s.actors_involved = tab;
          if(s.actors_involved.length!=data_shots.ActInvolved.length) {
            s.temp_tab_act = data_shots.ActInvolved;
          }
        }
        tab_shots.push(s);
        if(obj.On) {
          shots_timeline.addShotJson(s, true);
        }
      }
      obj.Data = tab_shots;
      obj.UserName = name;
      shots_timeline.list_data.push(obj);
      if(b)
        break;
    }
    shots_timeline.updateSelectedShot();
    shots_timeline.select_user.option(name);
  }

  /**
   * [getShotsData => convert data to json in order to save it]
   */
  this.getShotsData = function() {
    let new_json_shots = [];

    for(let s of this.shots) {
      let shot = {};
      if(shots_modif) {
        shot.Type = s.type;
        shot.ActInvolved = s.getActNameInvolved();
        shot.ScenesInvolved = s.scenes_involved;
        shot.AspectRatio = s.aspect_ratio;
        shot.GazeDir = s.is_gaze_direction;
        shot.KeepOut = s.is_keep_out;
        shot.PullIn = s.is_pull_in;
        shot.StagePos = s.is_stage_position;
        shot.BBoxes = s.bboxes;
        shot.StartFrame = s.start_frame;
        shot.EndFrame = s.end_frame;
        shot.Timeline = 0;
        new_json_shots.push(shot);
      } else if (s.is_modified) {
        shot.Type = s.type;
        shot.ActInvolved = s.getActNameInvolved();
        shot.ScenesInvolved = s.scenes_involved;
        shot.AspectRatio = s.aspect_ratio;
        shot.GazeDir = s.is_gaze_direction;
        shot.KeepOut = s.is_keep_out;
        shot.PullIn = s.is_pull_in;
        shot.StagePos = s.is_stage_position;
        new_json_shots.push(shot);
        s.is_modified = false;
      }
    }

    return new_json_shots;
  }

  function updateAllShotSelect() {
    montage_editor.is_all_types = this.checked();
  }

  function updateShowContextSelect() {
    montage_editor.is_show_context = this.checked();
    up_rough = true;
  }

  function updateShowSub() {
    montage_editor.is_show_sub = this.checked();
  }

  /**
   * [createContextShot => create a wide shot]
   */
  this.createContextShot = function() {
    let shot = new Shot();
    shot.selected = true;
    shot.aspect_ratio = aspect_ratio;
    shot.start_frame = 0;
    shot.end_frame = Math.round(frame_rate*video.duration());
    shot.is_pull_in = false;
    shot.is_keep_out = false;
    shot.is_stage_position = false;
    shot.is_gaze_direction = false;
    shot.type = 'WS';
    let b = false;
    for(let i=this.shots.length-1; i>=0; i--) {
      let s=this.shots[i];
      if(s.equalTo(shot, false)) {
        this.shots.splice(i,1);
      }
    }
    if(!b) {
      shot.calcBboxes(aspect_ratio);
      this.shots.push(shot);
    }
  }

  /**
   * [getShotsFromActs => Select the shots choosen by the user in the note tab with a click on a name]
   */
  this.getShotsFromActs = function() {
    let acts = [];
    for(let a of preparation_editor.actors_timeline) {
      if(a.on) {
        acts.push(a.actor_name);
      }
    }
    // if(all_on_stage) {
    // }
    let printed_shots = [];
    for(let s of this.shots) {
      if(s.type == "CU" || s.type == "MS" || s.type == "FS") {
        let b = false;
        for(let a of s.actors_involved) {
          if(acts.includes(a.actor_name)) {
            b = true;
            break;
          }
        }
        if(b && s.actors_involved.length == 1 && s.aspect_ratio == aspect_ratio && s.is_pull_in == true &&
          s.is_keep_out == true && s.is_gaze_direction == false && s.is_stage_position == false) {
          s.on = true;
          printed_shots.push(s);
        } else {
          s.on = false;
          s.bbox_show = [];
        }
      }
    }

    printed_shots.sort(sortShotsByActPosition);
    printed_shots.sort(sortShotsByType);
    return printed_shots;
  }

  this.isAlreadyAct = function(shots, act_inv) {
    let ret = false;
    for(let s of shots) {
      for(let name of s.getActNameInvolved()) {
        if(act_inv.includes(name)) {
          ret = true;
          break;
        }
      }
    }
    return ret;
  }

  /**
   * [getShot => Get the index of a specific shot (type and actors involved)]
   * @param  {[list]} tab                           [list of shots]
   * @param  {[string]} type                          [shot size]
   * @param  {[list]} actors_involved               [list of actors]
   * @param  {[float]} aspect_ratio                  [shot aspect ratio]
   * @param  {[Boolean]} keepout                       [is keep out]
   * @param  {[Boolean]} pullin                        [is pull in]
   * @param  {[Boolean]} stage_pos                     [is stage position]
   * @param  {[Boolean]} gaze_dir                      [is gaze direction]
   * @param  {[Boolean]} split_shot                    [is it use for split screen]
   * @return {[integer]}                 [return the shot index]
   */
  this.getShot = function(tab, type, actors_involved, aspect_ratio, keepout, pullin, stage_pos, gaze_dir, split_shot) {
    let ret = undefined;
    for(let i=0; i< tab.length; i++) {
      let s = tab[i];
      if(s.type == type && s.aspect_ratio == aspect_ratio && keepout == s.is_keep_out && pullin == s.is_pull_in
        && stage_pos == s.is_stage_position && gaze_dir == s.is_gaze_direction && split_shot == s.is_split_screen_shot) {
        let b1 = true;
        for(let a of s.actors_involved) {
          if(!actors_involved.includes(a.actor_name)) {
            b1 = false;
            break;
          }
        }
        if(b1 && s.actors_involved.length == actors_involved.length) {
          ret = i;
          break;
        }
      }
    }
    return ret;
  }

  /**
   * [getShotAspect => Get shot]
   * @param  {[string]}  type                          [shot size]
   * @param  {[list]}  actors_involved               [list of actors involved]
   * @param  {[float]}  aspect_ratio                  [shot aspect ratio]
   * @param  {[Boolean]}  keepout                       [is keep out]
   * @param  {[Boolean]}  pullin                        [is pull in]
   * @param  {[Boolean]}  stage_pos                     [is stage position]
   * @param  {[Boolean]}  gaze_dir                      [is gaze direction]
   * @param  {Boolean} is_personalize                [is it a personalize shot]
   * @return {[shot object]}                  [return a shot object]
   */
  this.getShotAspect = function(type, actors_involved, aspect_ratio, keepout, pullin, stage_pos, gaze_dir, is_personalize) {
    let ret = undefined;
    let acts = [];
    for(let a of actors_involved) {
      acts.push(a.actor_name);
    }
    for(let s of this.shots) {
      if(s.type == type && s.is_personalize && s.is_personalize == is_personalize) {
        return s;
      }
      if(s.type == type && s.aspect_ratio == aspect_ratio && keepout == s.is_keep_out && pullin == s.is_pull_in && stage_pos == s.is_stage_position && gaze_dir == s.is_gaze_direction) {
        let b1 = true;
        for(let a of s.actors_involved) {
          if(!acts.includes(a.actor_name)) {
            b1 = false;
            break;
          }
        }
        if(b1 && s.actors_involved.length == acts.length) {
          ret = s;
          break;
        }
      }
    }
    return ret;
  }

  /**
   * [getShotsOnStage => Get a list of all the shots where at least one of actor involved is present on stage]
   * @return {[list]} [return a list of shots on stage at the current frame]
   */
  this.getShotsOnStage = function() {
    let ret = [];
    for(let s of this.shots) {
      let is_on=false;
      for(let a of s.actors_involved) {
        if(preparation_editor.getActOnStage(frame_num).includes(a.actor_name)) {
          is_on = true;
          break;
        }
      }
      if(is_on) {
        ret.push(s);
      }
    }
    return ret;
  }

  /**
   * [removeShot => Remove the selected shot]
   */
  this.removeShot = function() {
    for(var i=0; i<this.shots.length; i++) {
      if(this.shots[i].on) {
        console.log('here');
        console.log(this.shots[i], i);
        if(confirm('Are your sure ?')) {
          shots_timeline.removeSpecificShot(this.shots[i]);
          for(let tab of shots_timeline.list_data) {
            shots_timeline.removeSpecificShot(this.shots[i],tab.Data);
          }
          var shot = {};
          shot.Type = this.shots[i].type;
          shot.ActInvolved = this.shots[i].getActNameInvolved();
          shot.IsPersonalize = this.shots[i].is_personalize;
          shot.ScenesInvolved = this.shots[i].scenes_involved;
          shot.AspectRatio = this.shots[i].aspect_ratio;
          shot.GazeDir = this.shots[i].is_gaze_direction;
          shot.KeepOut = this.shots[i].is_keep_out;
          shot.PullIn = this.shots[i].is_pull_in;
          shot.StagePos = this.shots[i].is_stage_position;
          shot.SplitScreenShot = this.shots[i].is_split_screen_shot;
          $.post({
            url: "remove_shot",
            async: true,
            data: {'abs_path': abs_path, 'shot':JSON.stringify(shot)},
            dataType: 'json',
            success: function (data) {
              console.log('removed');
            }
          });
          this.shots.splice(i,1);
        }
      }
    }
  }

  /**
   * [drawShots => Draw a grid with a preview for all the shots]
   */
  this.drawShots = function() {

    let top_shot = this.top_shot;
    let context_height = Math.min(height/3,(windowWidth-180-mid_width)/aspect_ratio);;

    let tab_rushes = [];
    for(let s of this.shots) {
      s.on = false;
      // s.setPosition(0,0,0,0);
      if((is_montage_editor && s.selected) || (is_cadrage_editor && (s.selected  || this.is_all_types || cadrage_editor.testShot(s)) ) ) {
        if((s.isOnStage() || s.type == "WS" || s.is_personalize) )
          tab_rushes.push(s);
      }
    }

    let div_wrap_rushes = select('#div_wrap_rushes');
    if(!div_wrap_rushes) {
      div_wrap_rushes = createDiv();
      div_wrap_rushes.id('div_wrap_rushes');
      div_wrap_rushes.elt.style.overflow = 'auto';
      div_wrap_rushes.size(windowWidth-160-mid_width, height-top_shot);
      div_wrap_rushes.position(mid_width,can.elt.offsetTop+top_shot);
    }

    let size = (Math.round(((windowWidth - 160)-mid_width-40)/3)/aspect_ratio)+20;

    let inner_wrap_rushes = select('#inner_wrap_rushes');
    if(!inner_wrap_rushes) {
      inner_wrap_rushes = createDiv();
      inner_wrap_rushes.id('inner_wrap_rushes');
      div_wrap_rushes.child(inner_wrap_rushes);
    }

    inner_wrap_rushes.size(1, size*(tab_rushes.length/3));

    let slice_ind = Math.round(div_wrap_rushes.elt.scrollTop / size) * 3;
    tab_rushes = tab_rushes.slice(slice_ind);

    let k=0;
    let off_x = 0;
    let off_y = 0;

    show_shot = undefined;
    for(let s of tab_rushes) {
      s.on = false;
      if(!s.aspect_ratio){
        s.aspect_ratio = aspect_ratio;
      }
      let arr = s.getCurrStabShot(frame_num);
      if(arr && s.type != 'WS') {
        let bbox = [];
        for(let j=0; j<arr.length; j++) {
          bbox.push(arr[j]*scale_ratio);
        }
        w = Math.round(((windowWidth - 160)-mid_width-40)/3);
        h = Math.floor(w/aspect_ratio);
        let n_w=w;let n_h=h;
        if(h*s.aspect_ratio>w){
          n_h = w/s.aspect_ratio;
        }else{ n_w = h*s.aspect_ratio;}
        if(k%3==2) {
          off_x = 2;
          s.setPosition((viewer_width+10)+(off_x*w)+(off_x*10), top_shot + (off_y*h)+ (off_y*10), n_w, n_h);
          off_y++;
        } else if(k%3==1) {
          off_x = 1;
          s.setPosition((viewer_width+10)+(off_x*w)+(off_x*10), top_shot + (off_y*h)+ (off_y*10), n_w, n_h);
        }else {
          off_x=0;
          s.setPosition(viewer_width+10+(off_x*w), top_shot + (off_y*h) + (off_y*10), n_w, n_h);
        }
        k++;

        if(is_cadrage_editor && s.selected) {
          push();
          fill('#FF5954');
          rect(s.x-5, s.y-5, s.w+10, s.h+10);
          pop();
        }
        if(is_cadrage_editor && s.in_stabilize) {
          push();
          fill('red');
          rect(s.x-5, s.y-5, s.w+10, s.h+10);
          pop();
        }
        if(mouseX > s.x && mouseX<s.x+s.w && mouseY > s.y && mouseY < s.y+s.h) {
          // Display a helping message
          push();
          fill(255);
          rect(0, height-20,windowWidth,20);
          fill('black');
          noStroke();
          textSize(16);
          if(is_montage_editor) {
            text('Click to add to the timeline, press maj + click to replace the selected rush',0,height-5);
          } else {
            text('Click to select, press S to show in the viewer, press del / backspace to remove',0,height-5);
          }
          pop();
          if(keyIsPressed && (keyCode == 17 || keyCode == 115)) {
            show_shot = s;
          }
          s.on = true;
          push();
          fill('#2E5C9C');
          rect(s.x-5, s.y-5, s.w+10, s.h+10);
          pop();
        }
        if(img_hd) {
          let ratio = img_hd.width / video.elt.videoWidth;
          image(img_hd, s.x, s.y, s.w, s.h, bbox[0]*ratio, bbox[1]*ratio, bbox[2]*ratio - bbox[0]*ratio, bbox[3]*ratio - bbox[1]*ratio);
        } else {
          if(s.w!=0 && s.h!=0) {
            image(image_frame, s.x, s.y, s.w, s.h, bbox[0], bbox[1], bbox[2] - bbox[0], bbox[3] - bbox[1]);
          }
        }
        s.displayText();

      } else if (!is_cadrage_editor && this.is_show_context && s.type == 'WS') {
        s.setPosition((viewer_width+10), top_shot-(context_height+10), context_height*aspect_ratio, context_height);
        if(mouseX > s.x && mouseX<s.x+s.w && mouseY > s.y && mouseY < s.y+s.h) {
          if(keyIsPressed && (keyCode == 17 || keyCode == 115)) {
            show_shot = s;
          }
        }
        if(img_hd) {
          image(img_hd, s.x, s.y, s.w, s.h);
        } else {
          image(image_frame, s.x, s.y, s.w, s.h);
        }
        s.displayText();
      }
    }
  }

  /**
   * [display => function called in the main p5 draw function, display all the video editing elementsin the canvas]
   */
  this.display = function() {
    push();
    if(x_off<0){x_off=0;}
    if(y_off<0){y_off=0;}
    translate(x_off,y_off);
    scale(vid_h/Number(original_height));
    // console.log(x_off,y_off,viewer_width/Number(original_width));
    preparation_editor.drawTracklets();

    pop();

    push();
    fill(150);
    rect(0,viewer_height,mid_width,windowHeight-viewer_height);
    pop();

    shots_timeline.display();
    this.drawShots();

  }
}
