/*
This file is part of KinoAi project.
It is subject to the license terms in the LICENSE file found in the top-level directory
*/

/**
 * [ActorTimeline => actor timeline object, it contains all the tracklets for a specific actor]
 * @constructor
 */
function ActorTimeline()  {
  this.x  = 0;
  this.y  = 0;
  this.w  = 0;
  this.h  = 0;

  this.on = false;

  this.split = false;

  this.actor_name = "";

  this.size_head;

  this.color = 'black';

  this.tracks = [];

  this.t_dragged;

  this.rect_drag = [0,0,0,0];

  this.states = [];

  this.track_bbox_shot = [];

  this.history_actions = [];

  this.elem;

  /**
   * [click => handle mouse click event]
   * @param  {[float]} mx               [mouse x position]
   * @param  {[float]} my               [mouse y position]
   */
  this.click = function(mx, my) {
    if(is_preparation_editor) {

      if (mx > this.x && mx < this.x + this.w && my > this.y && my < this.y + this.h) {
        for(let act of preparation_editor.actors_timeline) {
          act.on = false;
        }
        this.on = true;
        for(var i=0; i< this.tracks.length; i++) {
          this.tracks[i].on = false;
          if (mx > this.tracks[i].x && mx < this.tracks[i].x + this.tracks[i].w && my > this.tracks[i].y && my < this.tracks[i].y + this.tracks[i].h) {
            this.tracks[i].on = true;
            this.t_dragged = this.tracks[i];
          }
        }
        for(let t of this.track_bbox_shot) {
          t.on = false;
          if (mx > t.x && mx < t.x + t.w && my > t.y && my < t.y + t.h) {
            this.t_dragged = t;
          }
        }
        for(let s of this.states) {
          s.on = false;
          if( s.x < mx && mx < s.x+s.w) {
            s.on = true;
          }
        }
      }
      if(mx > this.elem.x && mx < this.x && my > this.y && my < this.y + this.h) {
        for(let act of preparation_editor.actors_timeline) {
          act.on = false;
        }
        this.on = true;
      }
    } else{
      if(is_cadrage_editor || annotation_editor.is_note_book) {
        my += can.elt.offsetTop - $('#div_creation').position().top + $('#div_creation').scrollTop();
        mx -= $('#div_creation').position().left;
      }
      if(mx > this.elem.elt.offsetLeft && mx < this.elem.elt.offsetLeft+this.elem.elt.offsetWidth && my > this.elem.elt.offsetTop && my < (this.elem.elt.offsetTop + this.elem.elt.offsetHeight)) {
        this.on = !this.on;
        if(this.on) {
          this.elem.style('color', 'rgb(46,92,156)');
        } else {
          this.elem.style('color', 'rgb(0,0,0)');
        }
      }
    }
    for(let t of this.track_bbox_shot) {
      t.click(mx, my);
    }

  };

  /**
   * [drag => handle drag event]
   * @param  {[float]} mx               [mouse x position]
   * @param  {[float]} my               [mouse y position]
   */
  this.drag = function(mx,my) {

  }

  /**
   * [getBestKeypoints => Get the best openpose detection around a specific frame +- 0.5s]
   * @param  {[tracklet object]} t                 [tracklet selected]
   * @param  {[integer]} off               [number of frame for the offset]
   * @return {[list]}     [set of openpose detection]
   */
  this.getBestKeypoints = function(t, off){
    var ret;
    var cpt=75;
    for(let i=0; i<5; i++) {
      var tab = frames_data[t.first_frame+off+i];
      if(tab[t.detections[off+i]]) {
        var keypoints = tab[t.detections[off+i]]['KeyPoints'];
        if(keypoints && cpt > keypoints.filter(v => v == 'null').length){
          cpt = keypoints.filter(v => v == 'null').length;
          ret = keypoints;
        }
      }
    }
    return ret;
  }

  /**
   * [drop => handle drop event]
   * @param  {[float]} mx               [mouse x position]
   * @param  {[float]} my               [mouse y position]
   */
  this.drop = function(mx, my) {
    let unit = player.total_frame/this.w;
    if(this.t_dragged) {
      if(player.nav_bar.cursor-10 < mx && mx < player.nav_bar.cursor+10) {
        mx = player.nav_bar.cursor;
      }
      let frame_num_drop = Math.min(Math.max(1,Math.round(player.first + (mx-this.x)*unit)),total_frame);
      let last_frame_t_dragged = this.t_dragged.last_frame || this.t_dragged.first_frame+this.t_dragged.detections.length;
      if(frame_num_drop<this.t_dragged.first_frame || last_frame_t_dragged < frame_num_drop) {
        let track_intersected = this.getTrackIntersection(frame_num_drop);

        // If intersection not empty we interpole the tracklets
        if(track_intersected) {
          let first_frame_right, last_frame_left;
          if(track_intersected.first_frame < this.t_dragged.first_frame) {
            first_frame_right = this.t_dragged.first_frame;
            last_frame_left = track_intersected.last_frame || track_intersected.first_frame+track_intersected.detections.length;
          } else {
            first_frame_right = track_intersected.first_frame;
            last_frame_left = this.t_dragged.last_frame || this.t_dragged.first_frame+this.t_dragged.detections.length;
          }
          if(abs(first_frame_right-last_frame_left)>1) {
            if(!this.t_dragged.last_frame&& !track_intersected.last_frame) {
              // Create a new tracklet based on two differents ones
              this.interpolateTrackDragged(track_intersected, frame_num_drop);
            } else {
              // Create a new personalize track
              this.interpolatePersonalizeTrack(track_intersected);
            }
          }
        } // If no intersection and no we just extend the tracklet dragged
         else {
           // Test prev or next
           let prev_track = this.getPrec(this.t_dragged);
           let next_track = this.getNext(this.t_dragged);
           if((!prev_track || frame_num_drop > prev_track.first_frame) && (!next_track || frame_num_drop < next_track.first_frame)) {
             this.extendsTrackDragged(frame_num_drop);
           }
        }
      }
      this.t_dragged = undefined;
    }
  }

  /**
   * [dropTrack => add a tracklet object when a user drop it in a timeline]
   * @param  {[float]} mx                        [mouse x position]
   * @param  {[float]} my                        [mouse y position]
   * @param  {[tracklet object]} t                         [tracklet dragged]
   * @return {[Boolean]}             [return true if the tracklet is added]
   */
  this.dropTrack = function(mx, my, t) {
    var ret = false;
    if (mx > this.x && mx < this.x + this.w && my > this.y && my < this.y + this.h) {
      this.tracks.sort(compare_first);
      this.removeState(mx, my);
        if(!this.isInTracks(t)) {
          this.addTrack(t);
          ret = true;
        }
    }
    return ret;
  }

  /**
   * [dropState => add the dragged state object into the timeline]
   * @param  {[float]} mx                  [mouse x position]
   * @param  {[float]} my                  [mouse y position]
   * @param  {[state object]} state               [state (hidden, offstage)]
   */
  this.dropState = function(mx, my, state) {
    if ((mx > this.x && mx < this.x + this.w && my > this.y && my < this.y + this.h) && !this.onTrack(mx)){
      this.tracks.sort(compare_first);
      this.removeState(mx, my);
      var new_state;
      let prec = this.getPrecX(mx);
      let next = this.getNextX(mx);
      let x;
      let w;
      let f_f;
      let e_f;
      if(!prec && !next) {
        f_f = 0;
        e_f = player.total_frame;
      } else if (prec && !next) {
        if(!prec.last_frame) {
          f_f = prec.first_frame + prec.detections.length + 1;
        } else {
          f_f = prec.last_frame + 1;
        }
        e_f = player.total_frame;
      } else if (!prec && next) {
        f_f = 0;
        e_f = next.first_frame-1;
      } else {
        if(!prec.last_frame) {
          f_f = prec.first_frame + prec.detections.length + 1;
        } else {
          f_f = prec.last_frame + 1;
        }
        e_f = next.first_frame-1;
      }
      new_state = {};
      new_state.first_frame = f_f;
      new_state.last_frame = e_f;
      new_state.name = state.name;
      new_state.color = state.color;
      new_state.color_on = state.color_on;
      if(new_state) {
       this.states.push(new_state);
      }
    }
  }

  /**
   * [getTrackIntersection => Check if a tracklet is already added at the frame_num_drop]
   * @param  {[integer]} frame_num_drop               [frame number of the drop event]
   * @return {[tracklet object or undefined]}                [Return the tracklet or the personalize track wich is present at the drop frame. If not return undefined]
   */
  this.getTrackIntersection = function(frame_num_drop) {
    let last_frame;
    if(this.t_dragged) {
      last_frame = this.t_dragged.last_frame || this.t_dragged.first_frame + this.t_dragged.detections.length;
    }
    for(let t of this.tracks) {
      if(t.first_frame < frame_num_drop && frame_num_drop < t.first_frame+t.detections.length) {
        if(frame_num_drop < this.t_dragged.first_frame) {
          return this.getPrec(this.t_dragged);
        } else if(last_frame < frame_num_drop) {
          return this.getNext(this.t_dragged);
        }
        return t;
      }
    }
    for(let t of this.track_bbox_shot) {
      if(t.first_frame < frame_num_drop && frame_num_drop < t.last_frame) {
        if(frame_num_drop < this.t_dragged.first_frame) {
          return this.getPrec(this.t_dragged);
        } else if(last_frame < frame_num_drop) {
          return this.getNext(this.t_dragged);
        }
        return t;
      }
    }
    return;
  }

  /**
   * [getIndexOf => Get the index of a specific tracklet in the tracklets list]
   * @param  {[tracklet object]} track               [tracklet object]
   * @return {[integer]}       [Return the index of a specific tracklet]
   */
  this.getIndexOf = function(track) {
    for(let i=0;i < this.tracks.length;i++) {
      if(track.first_frame == this.tracks[i].first_frame) {
        return i;
      }
    }
    return;
  }

  /**
   * [getPrec => Get the previous tracklet of the timeline]
   * @param  {[tracklet object]} track               [tracklet selected]
   * @return {[tracklet object]}       [return the i-1 tracklets based on the tracklet first frame]
   */
  this.getPrec = function(track) {
    let ret = undefined;
    let last_frame_prec = 0;
    let f_f = track.first_frame;
    if(track.last_frame) {
      for(let i=0; i<this.track_bbox_shot.length; i++) {
        let t = this.track_bbox_shot[i];
        if(f_f == t.first_frame) {
          if(this.track_bbox_shot[i-1]) {
            ret = this.track_bbox_shot[i-1];
            last_frame_prec = this.track_bbox_shot[i-1].last_frame;
            break;
          }
        }
      }
      for(let i=0; i<this.tracks.length; i++) {
        if(last_frame_prec < this.tracks[i].first_frame && this.tracks[i].first_frame + this.tracks[i].detections.length-1 < f_f) {
          ret = this.tracks[i];
          last_frame_prec = ret.first_frame+ret.detections.length;
        }
      }
    } else {
      for(let i=0; i<this.tracks.length; i++) {
        if(f_f == this.tracks[i].first_frame) {
          if(this.tracks[i-1]) {
            ret = this.tracks[i-1];
            last_frame_prec = ret.first_frame+ret.detections.length;
            break;
          }
        }
      }
      for(let i=0; i<this.track_bbox_shot.length; i++) {
        let t = this.track_bbox_shot[i];
        if(last_frame_prec < t.first_frame && t.last_frame < f_f) {
          ret = t;
          last_frame_prec = t.last_frame;
        }
      }
    }
    return ret;
  }

  /**
   * [getNext => get the next tracklet of the timeline]
   * @param  {[tracklet object]} track               [tracklet selected]
   * @return {[tracklet object]}       [Return the i+1 tracklets based on the tracklet first frame]
   */
  this.getNext = function(track) {
    let ret = undefined;
    let first_frame_prec = total_frame;
    let f_f = track.first_frame;

    if(track.last_frame) {
      for(let i=0; i<this.track_bbox_shot.length; i++) {
        let t = this.track_bbox_shot[i];
        if(f_f == t.first_frame) {
          if(this.track_bbox_shot[i+1]) {
            ret = this.track_bbox_shot[i+1];
            first_frame_prec = this.track_bbox_shot[i+1].first_frame;
            break;
          }
        }
      }
      for(let i=0; i<this.tracks.length; i++) {
        if(this.tracks[i].first_frame < first_frame_prec && this.tracks[i].first_frame > f_f) {
          ret = this.tracks[i];
          first_frame_prec = this.tracks[i].first_frame;
        }
      }
    } else {
      for(let i=0; i<this.tracks.length; i++) {
        if(f_f == this.tracks[i].first_frame) {
          if(this.tracks[i+1]) {
            ret = this.tracks[i+1];
            first_frame_prec = ret.first_frame;
            break;
          }
        }
      }
      for(let i=0; i<this.track_bbox_shot.length; i++) {
        let t = this.track_bbox_shot[i];
        if(t.first_frame < first_frame_prec && t.first_frame > f_f) {
          ret = t;
          first_frame_prec = t.first_frame;
        }
      }
    }
    return ret;
  }

  /**
   * [getPrecX => get the previous tracklet of the timeline based on the mouse x position]
   * @param  {[float]} mx               [mouse x position]
   * @return {[tracklet object]}    [return the i-1 tracklets]
   */
  this.getPrecX = function(mx) {
    var ret = undefined;
    let last_x = 0;
    for(var i=0; i<this.tracks.length; i++) {
      if(mx > last_x && this.tracks[i].x+this.tracks[i].w < mx) {
        ret = this.tracks[i];
        last_x = this.tracks[i].x+this.tracks[i].w;
      }
    }
    for(var i=0; i<this.track_bbox_shot.length; i++) {
      let t = this.track_bbox_shot[i];
      if(mx > last_x && t.x+t.w < mx) {
        ret = t;
        last_x = t.x+t.w;
      }
    }
    return ret;
  }

  /**
   * [getNextX => get the next tracklet of the timeline based on the mouse x position]
   * @param  {[float]} mx               [mouse x position]
   * @return {[tracklet object]}    [return the i-1 tracklets]
   */
  this.getNextX = function(mx) {
    var ret = undefined;
    let last_x = this.x+this.w;
    for(var i=0; i<this.tracks.length; i++) {
      if(this.tracks[i].x > mx) {
        ret = this.tracks[i];
        last_x = this.tracks[i].x;
        break;
      }
    }
    for(var i=0; i<this.track_bbox_shot.length; i++) {
      let t = this.track_bbox_shot[i];
      if(t.x > mx && t.x < last_x) {
        ret = t;
        last_x = t.x;
        break;
      }
    }
    return ret;
  }

  /**
   * [undoAction => handle undo event]
   */
  this.undoAction = function() {
    let action = this.history_actions.pop();
    if(action) {
      switch (action.action_type) {
        case 'Ext_Track':
          this.undoExtTrack(action);
          break;
        case 'Ext_Track_Perso':
          this.undoExtTrackPerso(action);
          break;
        case 'Interpole_Track':
          this.removeTracklet(action.prev_track);
          break;
        case 'Interpolate_Track_Perso':
          this.undoInterpolePersonalizeTrack(action);
          break;
        default:

      }
    }
  }

  /**
   * [undoExtTrack => undo the tracklet extension]
   * @param  {[action object]} action               [last action]
   */
  this.undoExtTrack = function(action) {
    let track = action.prev_track;
    if(track) {
      track.first_frame = action.prev_first_frame;
      track.detections = action.prev_detections;
      track.on = false;
    }
  }

  /**
   * [undoExtTrackPerso => Undo the personalize tracklet extension]
   * @param  {[action object]} action               [last action]
   */
  this.undoExtTrackPerso = function(action) {
    if(action.prev_track) {
      action.prev_track.first_frame = action.prev_first_frame;
      action.prev_track.last_frame = action.prev_last_frame;
      action.prev_track.bboxes = action.prev_bboxes;
      action.prev_track.on = false;
    }
  }

  /**
   * [undoInterpolePersonalizeTrack => undo a tracklet interpolation]
   * @param  {[action object]} action               [last action]
   */
  this.undoInterpolePersonalizeTrack = function(action) {
    if(action.new_track) {
      this.removeTracklet(action.new_track);
      if(action.track_left) {
        this.track_bbox_shot.push(action.track_left);
      }
      if(action.track_right) {
        this.track_bbox_shot.push(action.track_right);
      }
    }
    for(let t of this.track_bbox_shot) {
      t.on = false;
    }
    this.track_bbox_shot.sort(compare_first);
  }

  /**
   * [updateTrackPos => update the tracklet position and size]
   * @param  {[integer]} total_frame               [total frame]
   */
  this.updateTrackPos = function(total_frame) {
    for(var i=0; i<this.tracks.length; i++) {
      let unit = this.w/player.total_frame;
      let off_x = player.first*unit;
      let start = this.x + Math.round((this.tracks[i].first_frame)*unit) - off_x;
      let end = start + Math.round((this.tracks[i].detections.length)*unit);
      this.tracks[i].setPosition(start, this.y, end-start, this.h/2);
    }
  }

  /**
   * [getTrackIndex => get a tracklet index based on the mouse position]
   * @param  {[float]} mx               [mouse x position]
   * @param  {[float]} my               [mouse y position]
   * @return {[integer]}    [tracklet index]
   */
  this.getTrackIndex = function(mx, my) {
    var ind = [];
    var offset = Number.MAX_VALUE;
    if ((mx > this.x && mx < this.x + this.w && my > this.y && my < this.y + this.h) && !this.onTrack(mx)) {
      this.tracks.sort(compare_first);
      ind = [0,0];
      for(var i=0; i < this.tracks.length; i++) {
        if(abs(mx - this.tracks[i].x) < offset) {
          offset = abs(mx - this.tracks[i].x);
          ind[0] = 0;
          ind[1] = i;
        }
        if(abs(mx - (this.tracks[i].x + this.tracks[i].w)) < offset) {
          offset = abs(mx - (this.tracks[i].x + this.tracks[i].w));
          ind[0] = 1;
          ind[1] = i;
        }
      }
    }
    return ind;
  }

  /**
   * [getBBoxObj => create a bbox object based on an openpose detections]
   * @param  {[list]} keypoints               [open pose set of keypoints]
   * @return {[bbox shot object]}           [bbox shot object]
   */
  this.getBBoxObj = function(keypoints) {
    let bb = getBBox(keypoints);
    let x = bb[0];
    let y = bb[1];
    let w = bb[2]-bb[0];
    let h = bb[3]-bb[1];
    let b = new BboxShot(x,y,w,h);
    let c_x = keypoints[1*3];
    if(c_x=='null') {
      c_x = (bb[0]+bb[2])/2;
    }
    let c_y = keypoints[1*3+1];
    if(c_y=='null') {
      c_y = (bb[1]+bb[3])/2;
    }
    b.setCenter(c_x,c_y);
    return b;
  }

  /**
   * [interpolatePersonalizeTrack => Create a new personalize track based on two]
   * @param  {[tracklet object]} track_intersected               [tracklet object]
   */
  this.interpolatePersonalizeTrack = function(track_intersected) {
    let bbox_left, bbox_right, track_left, track_right, lengh_interval;
    if(this.t_dragged.first_frame < track_intersected.first_frame) {
      track_left = this.t_dragged;
      track_right = track_intersected;
    } else {
      track_left = track_intersected;
      track_right = this.t_dragged;
    }
    if(!track_left.last_frame) {
      bbox_left = this.getBBoxObj(this.getBestKeypoints(track_left, track_left.detections.length-5));
    }
    if(!track_right.last_frame) {
      bbox_right = this.getBBoxObj(this.getBestKeypoints(track_right, 0));
    }
    if(!bbox_left) {
      bbox_left = track_left.bboxes[track_left.bboxes.length-1];
    }
    if(!bbox_right) {
      bbox_right = track_right.bboxes[0];
    }
    if((track_left.last_frame && track_right.last_frame) || (track_left.last_frame && !track_right.last_frame)) {
      lengh_interval = track_right.first_frame - track_left.last_frame;
    } else if(!track_left.last_frame && track_right.last_frame) {
      lengh_interval = track_right.first_frame - (track_left.first_frame+track_left.detections.length-1);
    }
    let new_bboxes = this.interpolate(bbox_left, bbox_right, lengh_interval);
    let new_track = new TrackBboxShot(this);
    if(track_left.last_frame) {
      new_track.first_frame = track_left.first_frame;
      new_bboxes = concat(track_left.bboxes,new_bboxes);
    } else {
      new_track.first_frame = track_right.first_frame + 1 - lengh_interval;
    }
    if(track_right.last_frame) {
      new_track.last_frame = track_right.last_frame;
      new_bboxes = concat(new_bboxes,track_right.bboxes);
    } else {
      new_track.last_frame = new_track.first_frame + new_bboxes.length;
    }
    new_track.bboxes = new_bboxes;

    let action_obj = {};
    action_obj.action_type = 'Interpolate_Track_Perso';
    action_obj.new_track = new_track;

    if(track_left.last_frame) {
      action_obj.track_left = track_left;
      this.removeTracklet(track_left);
    }
    if(track_right.last_frame) {
      action_obj.track_right = track_right;
      this.removeTracklet(track_right);
    }

    // Stack the tracklet information in history
    if(this.history_actions.length>=5) {
      this.history_actions.splice(0,1);
    }
    this.history_actions.push(action_obj);

    this.track_bbox_shot.push(new_track);
    this.track_bbox_shot.sort(compare_first);

  }

  /**
   * [interpolate => create a new personalize bounding box for each frame based on the linear interpolation of two bounding boxe]
   * @param  {[bbox object]} bbox_left                [first bounding box]
   * @param  {[bbox object]} bbox_right               [second bounding box (x,y,w,h,x_center,y_center)]
   * @param  {[integer]} len                      [length of frames]
   * @return {[list]}            [list of bounding box]
   */
  this.interpolate = function(bbox_left, bbox_right, len) {
    let ret = [];
    for(let i=0; i<len; i++) {
      let x = lerp(bbox_left.x, bbox_right.x, i/len);
      let y = lerp(bbox_left.y, bbox_right.y, i/len);
      let w = lerp(bbox_left.w, bbox_right.w, i/len);
      let h = lerp(bbox_left.h, bbox_right.h, i/len);
      let b = new BboxShot(x,y,w,h);
      let c_x = lerp(bbox_left.center_x, bbox_right.center_x, i/len);
      let c_y = lerp(bbox_left.center_y, bbox_right.center_y, i/len);
      b.setCenter(c_x, c_y);
      ret.push(b);
    }
    return ret;
  }

  /**
   * [extendsTrackDragged => Extend the last or first detections of a tracklet]
   * @param  {[integer]} frame_num_drop               [dropped frame number]
   */
  this.extendsTrackDragged = function(frame_num_drop) {
    // Check if is a personalize tracklet
    if(this.t_dragged.last_frame) {
      // Stack the tracklet information in history
      if(this.history_actions.length>=5) {
        this.history_actions.splice(0,1);
      }
      this.history_actions.push({'action_type':'Ext_Track_Perso','prev_track':this.t_dragged,'prev_first_frame':this.t_dragged.first_frame,'prev_last_frame':this.t_dragged.last_frame,'prev_bboxes':this.t_dragged.bboxes});
      if(frame_num_drop<this.t_dragged.first_frame) {
        let tab = [];
        let l = this.t_dragged.first_frame - frame_num_drop-1;
        for(let i=0; i<l; i++) {
          tab.push(this.t_dragged.bboxes[0]);
        }
        let new_bboxes = concat(tab,this.t_dragged.bboxes);
        this.t_dragged.first_frame = frame_num_drop-1;
        this.t_dragged.bboxes = new_bboxes;
      } else {
        let tab = [];
        let l = frame_num_drop-this.t_dragged.last_frame;
        for(let i=0; i<=l; i++) {
          tab.push(this.t_dragged.bboxes[this.t_dragged.bboxes.length-1]);
        }
        let new_bboxes = concat(this.t_dragged.bboxes,tab);
        this.t_dragged.last_frame = frame_num_drop+1;
        this.t_dragged.bboxes = new_bboxes;
      }
    }
    // If not personalize extend tracklet
    else {
      // Stack the tracklet information in history
      if(this.history_actions.length>=5) {
        this.history_actions.splice(0,1);
      }
      this.history_actions.push({'action_type':'Ext_Track','prev_track':this.t_dragged,'prev_first_frame':this.t_dragged.first_frame,'prev_detections':this.t_dragged.detections});

      // Extend in the past
      if(frame_num_drop<this.t_dragged.first_frame) {
        let l = this.t_dragged.first_frame - frame_num_drop-1;
        let new_detections = [];
        let best_keypoints = this.getBestKeypoints(this.t_dragged, 0);
        for(let i=0; i<l; i++) {
          let ind = frame_num_drop-1+i;
          let obj = {};
          obj.KeyPoints = best_keypoints;
          // Add the new keypoints in the array of openpose detections
          frames_data[ind].push(obj);
          // SAve the index of the new keypoints into the array of detections
          new_detections.push(frames_data[ind].length-1);
        }
        // Concat the new array of detection and the old one + set the new value of first_frame for the tracklet dragged
        this.t_dragged.detections = concat(new_detections, this.t_dragged.detections);
        this.t_dragged.first_frame = frame_num_drop - 1;
        detec_modif = true;
      }
      // Extend in the future
      else {
        let l = frame_num_drop - (this.t_dragged.first_frame + this.t_dragged.detections.length);
        let new_detections = [];
        let best_keypoints = this.getBestKeypoints(this.t_dragged, this.t_dragged.detections.length-1-5);
        for(let i=0; i<=l; i++) {
          let ind = this.t_dragged.first_frame+this.t_dragged.detections.length+i;
          let obj = {};
          obj.KeyPoints = best_keypoints;
          // Add the new keypoints in the array of openpose detections
          frames_data[ind].push(obj);
          // Save the index of the new keypoints into the array of detections
          new_detections.push(frames_data[ind].length-1);
        }
        // Concat the new array of detection and the old one + set the new value of first_frame for the tracklet dragged
        this.t_dragged.detections = concat(this.t_dragged.detections, new_detections);
        detec_modif = true;
      }
    }
  }

  /**
   * [interpolateTrackDragged => Interpolate two tracklets and create a new one]
   * @param  {[tracklet object]} track_intersect               [tracklet object]
   * @param  {[integer]} frame_num_drop                [dropped frame number]
   */
  this.interpolateTrackDragged = function(track_intersect, frame_num_drop) {
    // We set the t and t+1 track
    let track_left, track_right;
    if(track_intersect.first_frame < this.t_dragged.first_frame) {
      track_left = track_intersect;
      track_right = this.t_dragged;
    } else {
      track_left = this.t_dragged;
      track_right = track_intersect;
    }
    let left_keypoints = this.getBestKeypoints(track_left, track_left.detections.length-1-5);
    let right_keypoints = this.getBestKeypoints(track_right, 0);
    let lengh_interval = track_right.first_frame - (track_left.first_frame+track_left.detections.length);

    let new_detections = [];
    for(let i=0; i<=lengh_interval; i++) {
      let ind = track_left.first_frame+track_left.detections.length + i;
      let obj = {};
      let keypoints = [];
      for(let j=0; j<right_keypoints.length; j++) {
        let val = lerp(left_keypoints[j], right_keypoints[j], i/lengh_interval);
        if(isNaN(val)) {
          keypoints.push('null');
        } else {
          keypoints.push(val);
        }
      }
      obj.KeyPoints = keypoints;
      frames_data[ind].push(obj);
      new_detections.push(frames_data[ind].length-1);
    }
    // Create a new track based on the 2 previous
    let new_track = new Track();
    new_track.first_frame = track_left.first_frame + track_left.detections.length;
    new_track.is_an_interpolated_track = true;
    new_track.detections = new_detections;
    this.addTrack(new_track);
    // Stack the new tracklet information in history
    if(this.history_actions.length>=5) {
      this.history_actions.splice(0,1);
    }
    this.history_actions.push({'action_type':'Interpole_Track','prev_track':new_track});
    detec_modif = true;
  }

  /**
   * [checkCollideTrackletsPerso => check if there is a collision between new personalized tracklets with an other tracklet]
   * @param  {[integer]} first_frame               [personalized tracklet first frame]
   * @param  {[integer]} last_frame                [personalized tracklet last frame]
   * @return {[Boolean]}             [return true if collision is detected]
   */
  this.checkCollideTrackletsPerso = function(first_frame, last_frame) {
    for(let t of this.tracks) {
      if(t.first_frame < last_frame && last_frame< t.first_frame+t.detections.length) {
        return false;
      }
    }
    return true;
  }

  /**
   * [removeState => remove a state by clicking on it]
   * @param  {[float]} mx               [mouse x position]
   * @param  {[float]} my               [mouse y position]
   */
  this.removeState = function(mx, my) {
    if (mx > this.x && mx < this.x + this.w && my > this.y && my < this.y + this.h) {
      if(this.states.length==1) {
        if(mx > this.states[0].x && mx < this.states[0].x + this.states[0].w) {
          this.states = [];
        }
      }
      for(var i=0; i < this.states.length; i++) {
        if(mx > this.states[i].x && mx < this.states[i].x + this.states[i].w) {
          this.states.splice(i, 1);
        }
      }
    }
  }

  /**
   * [setPosition => set timeline position and size]
   * @param  {[integer]} tX               [x]
   * @param  {[integer]} tY               [y]
   * @param  {[integer]} tW               [width]
   * @param  {[integer]} tH               [height]
   */
  this.setPosition = function(tX, tY, tW, tH) {
    this.x  = tX;
    this.y  = tY;
    this.w  = tW;
    this.h  = tH;
  }

  /**
   * [setActorName => set a new actor name]
   * @param  {[string]} name               [new actor name]
   */
  this.setActorName = function(name) {
    for(let a of preparation_editor.annotation_timeline.actors_annotation) {
      if(this.actor_name == a.actor_name) {
        a.elem.elt.innerText = name;
        a.actor_name = name;
      }
    }
    this.actor_name = name;
    for(let t of this.tracks) {
      t.actor_name = name;
    }
  }

  /**
   * [isOnStage => is the actor on stage]
   * @return {[Boolean]} [return true if the actor is on stage]
   */
  this.isOnStage = function() {
    for(let s of this.states) {
      if(s.name == 'Offstage' && s.first_frame < frame_num && frame_num < s.last_frame) {
        return false;
      }
    }
    return true;
  }

  /**
   * [getKeyPoints => get the open pose keypoints from the open pose detections at the frame f_n]
   * @param  {[integer]} f_n               [frame number]
   * @return {[list]}     [open pose keypoints]
   */
  this.getKeyPoints = function(f_n) {
    let tab;
    for(let t of this.tracks) {
      if(t.first_frame <= f_n && f_n < t.first_frame + t.detections.length) {
        let ind = t.detections[f_n-t.first_frame];
        let keypoints;
        if(frames_data[f_n][ind]) {
          keypoints = frames_data[f_n][ind]['KeyPoints'];
        }
        if(keypoints) {
          tab = keypoints;
          break;
        }
      }
    }
    return tab;
  }

  /**
   * [getProfile => get the orientation of the actor on stage ]
   * @param  {[integer]} [f_n=undefined]               [frame number]
   * @return {[string]}                 [actor orientation and velocity]
   */
  this.getProfile = function(f_n = undefined) {
    if(f_n == undefined) {
      f_n = frame_num;
    }
    let ret = '';
    let keypoints = this.getKeyPoints(f_n);
    if(keypoints && keypoints != 'null') {
      let g_v = getGazevect(keypoints);
      if(g_v.mag() < 2) {
        if(keypoints[15*3] && keypoints[16*3]) {
          if(keypoints[15*3]<keypoints[16*3]) {
            ret += 'front';
          } else {
            ret += 'back';
          }
        } else {
          ret += 'front/back';
        }
      } else if(g_v.x < 0) {
        ret += 'left';
      } else if(g_v.x > 0) {
        ret += 'right';
      }
    }
    if(this.getVelocityVect(f_n)) {
      ret += ' vel '+Math.round(this.getVelocityVect(f_n).mag());
    } else {
      ret += ' vel '+0;
    }
    return ret;
  }

  /**
   * [extractMovement => get the actor movement]
   * @return {[string]} [actor movement]
   */
  this.extractMovement = function() {
    let ret = [];
    for(let i=0; i< total_frame; i++) {
      let obj = {};
      obj.Frame = ''+i;
      if(this.getVelocityVect(i)) {
        obj.Mvt = ''+Math.round(this.getVelocityVect(i).mag());
      } else {
        obj.Mvt = ''+0;
      }
      ret.push(obj);
    }
    return ret;
  }

  /**
   * [getCenterAct => get the center point of the actor]
   * @param  {[integer]} f_n               [frame number]
   * @return {[object (x,y)]}     [center point]
   */
  this.getCenterAct = function(f_n) {
    let center = {};
    for(let t of this.tracks) {
      if(t.first_frame <= f_n && f_n < t.first_frame + t.detections.length) {
        let ind = t.detections[f_n-t.first_frame];
        let keypoints;
        if(frames_data[f_n] && frames_data[f_n][ind]) {
          keypoints = frames_data[f_n][ind]['KeyPoints'];
        }
        if(keypoints && keypoints[1*3] != "null") {
          center.x = keypoints[1*3];
          center.y = keypoints[1*3+1];
          break;
        }
      }
    }
    if(!center.x) {
      for(let t of this.track_bbox_shot) {
        if(t.first_frame <= f_n && f_n < t.last_frame) {
          center.x = t.bboxes[f_n-t.first_frame].center_x;
          center.y = t.bboxes[f_n-t.first_frame].center_y;
          break;
        }
      }
    }
    if(!center.x && /*split_screen_editor.is_active &&*/ !split_screen_editor.is_split_screen) {
      let s = split_screen_editor.getSplitShotOnlyOneActor(this.actor_name);
      if(s && s.bboxes[f_n][0]) {
        center.x = int((s.bboxes[f_n][0] + s.bboxes[f_n][2])/2);
        center.y = int((s.bboxes[f_n][1] + s.bboxes[f_n][3])/2);
      }
    }
    return center;
  }

  /**
   * [getActPosition => get the actor poisition on stage (x,y)]
   * @param  {[integer]} [f_n=frame_num]               [frame number]
   * @return {[object (x,y)]}                 [actor poisition (x,y)]
   */
  this.getActPosition = function(f_n = frame_num) {
    let c = this.getCenterAct(f_n);
    if(!c.x) {
      for(let i=0; i<Math.min(total_frame-f_n,frame_rate*5);i++) {
        c = this.getCenterAct(f_n+i);
        if(c.x) {
          break;
        }
      }
    }
    if(!c.x) {
      c = {x:parseInt(original_width),y:parseInt(original_height)};
    }
    return c;
  }

  /**
   * [getVelocityVect => get the velocity vector of the actor movement]
   * @param  {[integer]} fr_num               [frame number]
   * @return {[vector]}        [velocity vector]
   */
  this.getVelocityVect = function(fr_num) {
    let c_1t = this.getCenterAct(Math.max(fr_num-1,0)).x;
    let c_t = this.getCenterAct(fr_num).x;
    let c_t1 = this.getCenterAct(Math.min(fr_num+1,total_frame-1)).x;
    let c_t2 = this.getCenterAct(Math.min(fr_num+2,total_frame-1)).x;
    if(c_t && c_t1 && c_t2) {
      let prev = p5.Vector.mult(createVector(c_t-c_1t,0),2);
      let curr = p5.Vector.mult(createVector(c_t1-c_t,0),2);
      let next = p5.Vector.mult(createVector(c_t2-c_t1,0),2);
      if((curr.mag()>1&& next.mag()>1&& prev.mag()>1) && ((prev.x>0 && curr.x<0 && next.x>0) || (prev.x<0 && curr.x>0 && next.x<0))) {
        return createVector(0);
      }
      let ratio;
      if(curr.mag()>1&& prev.mag()>1) {
        if(curr.mag() >  prev.mag()) {
          ratio=curr.mag()/ prev.mag();
        } else {
          ratio = prev.mag()/curr.mag();
        }
        // if(ratio && ratio > 3) {
        //   console.log(ratio);
        // }
      }
      if(ratio && ratio <3)
        return curr;
      else if(!ratio)
        return curr;
    } else if(c_t && c_t1) {
      return p5.Vector.mult(createVector(c_t1-c_t,0),2);
    } else {
      return createVector(0);
    }
  }

  function numMedian(a) {
    a = a.slice(0).sort(function(x, y) {
      return x - y;
    });
    var b = (a.length + 1) / 2;
    return (a.length % 2) ? a[b - 1] : (a[b - 1.5] + a[b - 0.5]) / 2;
  }

  /**
   * [getVariance => compute the variance of a list]
   * @param  {[list]} a               [list]
   * @return {[float]}   [variance]
   */
  function getVariance(a) {
    let mean = int((a.reduce((pv, cv) => pv + cv, 0))/a.length);
    return (a.reduce((pv, cv) => pv + Math.pow(cv-mean,2), 0))/a.length;
  }

  /**
   * [updateHeadSize => compute the head size of an actor based on all the detections]
   */
  this.updateHeadSize = function() {
    let tabHead = [];
    for(let t of this.tracks) {
      let ind=t.first_frame;
      for(let d of t.detections) {
        if(frames_data[ind][d] && frames_data[ind][d] != "null") {
          let keypoints;
          if(frames_data[ind][d]) {
            keypoints = frames_data[ind][d]['KeyPoints'];
          }
          let xNose = keypoints[0*3];
          let yNose = keypoints[0*3+1];
          let xNeck = keypoints[1*3];
          let yNeck = keypoints[1*3+1];
          let xMid = keypoints[8*3];
          let yMid = keypoints[8*3+1];
          if((xNeck && yNeck && xMid && yMid) &&
          (xNeck != 'null' && yNeck != 'null' && xMid != 'null' && yMid != 'null')){
            let sizeBody=int(dist(xNeck, yNeck, xMid, yMid));
            let sizeHead = int((sizeBody/3));
            tabHead.push(sizeHead);
          }else if((xNose && yNose && xNeck && yNeck) &&
           (xNose != 'null' && yNose != 'null' && xNeck != 'null' && yNeck != 'null')){
            let sizeHead=int(dist(xNose, yNose, xNeck, yNeck)*2/3);
            tabHead.push(sizeHead);
          }
        }
        ind++;
      }
    }
    this.size_head = numMedian(tabHead);//int((tabHead.reduce((pv, cv) => pv + cv, 0))/tabHead.length);
    // console.log(numMedian(tabHead), this.size_head, getVariance(tabHead), Math.pow(getVariance(tabHead),2));
  }

  /**
   * [addTrack => add a tracklet to the timeline]
   * @param  {[tracklet object]} t               [tracklet object]
   */
  this.addTrack = function(t) {
    t.added = true;
    t.on = false;
    t.actor_name = this.actor_name;
    this.tracks.push(t);
    this.tracks.sort(compare_first);
  }

  /**
   * [isInTracks => check if the new tracklet collide another one]
   * @param  {[tracklet object]} t               [new tracklet]
   * @return {[Boolean]}   [return true if collision]
   */
  this.isInTracks = function(t) {
    var is_in = false;
    var detections_track_ref = t.detections;
    var first_ref = t.first_frame;
    var last_ref = first_ref + detections_track_ref.length - 1;
    for(var i=0; i< this.tracks.length; i++) {
      var detections_track = this.tracks[i].detections;
      var first = this.tracks[i].first_frame;
      var last = first + detections_track.length - 1;
      if((first_ref > first && first_ref < last) || (last_ref > first && last_ref < last)
        || (first_ref < first && last_ref > last)) {
          is_in = true;
          break;
      }
    }
    return is_in;
  }

  /**
   * [onTrack => check if the mouse x is on a tracklet]
   * @param  {[float]} mX               [mouse x position]
   * @return {[Boolean]}    [return true if the mouse x is on a tracklet]
   */
  this.onTrack = function(mX) {
    var ret = false;
    for(var i=0; i< this.tracks.length; i++) {
      var start = this.tracks[i].x;
      var end = start + this.tracks[i].w;
      if(mX>start && mX < end) {
        ret = true;
        break;
      }
    }
    if(!ret) {
      for(var i=0; i< this.track_bbox_shot.length; i++) {
        var start = this.track_bbox_shot[i].x;
        var end = start + this.track_bbox_shot[i].w;
        if(mX>start && mX < end) {
          ret = true;
          break;
        }
      }
    }
    return ret;
  }

  /**
   * [removeTrack => remove a tracklet from the timeline by clicking on it]
   * @param  {[float]} mx               [mouse x position]
   * @param  {[float]} my               [mouse y position]
   */
  this.removeTrack = function(mx, my) {
    var ret = false;
    for(var i=0; i< this.tracks.length; i++) {
      var t = this.tracks[i];
      if (mx > t.x && mx < t.x + t.w && my > t.y && my < t.y + t.h) {
        if(this.tracks[i].first_frame == t.first_frame) {
          t.actor_name = 'unknown';
          t.added = false;
          this.tracks.splice(i, 1);
          ret = true;
          break;
        }
      }
    }
    for(let i=0; i<this.track_bbox_shot.length; i++) {
      let t = this.track_bbox_shot[i];
      if (mx > t.x && mx < t.x + t.w && my > t.y && my < t.y + t.h) {
        this.track_bbox_shot.splice(i,1);
      }
    }
    return ret;
  }

  /**
   * [removeTrackletFromKey => remove a selected tracklet with the delete key]
   */
  this.removeTrackletFromKey = function() {
    for(var i=0; i< this.tracks.length; i++) {
      if(this.tracks[i].on) {
        this.tracks[i].actor_name = 'unknown';
        this.tracks[i].added = false;
        this.tracks.splice(i, 1);
        break;
      }
    }
    for(var i=0; i< this.track_bbox_shot.length; i++) {
      if(this.track_bbox_shot[i].on) {
        this.track_bbox_shot.splice(i, 1);
        break;
      }
    }
  }

  /**
   * [removeTracklet => remove a selected tracklet]
   * @param  {[tracklet object]} t               [selected tracklet]
   */
  this.removeTracklet = function(t) {
    for(var i=0; i< this.tracks.length; i++) {
      if(this.tracks[i].x == t.x && this.tracks[i].w == t.w) {
        t.actor_name = 'unknown';
        t.added = false;
        this.tracks.splice(i, 1);
        break;
      }
    }
    for(var i=0; i< this.track_bbox_shot.length; i++) {
      if(this.track_bbox_shot[i].x == t.x && this.track_bbox_shot[i].w == t.w) {
        this.track_bbox_shot.splice(i, 1);
        break;
      }
    }
  }

  /**
   * [removeAll => remove all tracklets]
   */
  this.removeAll = function() {
    for(var i=0; i< this.tracks.length; i++) {
      this.tracks[i].actor_name = 'unknown';
      this.tracks[i].added = false;
    }
    this.tracks = [];
  }

  this.updateBBox = function() {
    for(var i=0; i< this.tracks.length; i++) {
      this.tracks[i].bbox = [0,0,0,0];
    }
  }

  /**
   * [isOnstage => check if an actor is on stage at a specific frame]
   * @param  {[integer]} f_n               [frame number]
   * @return {[Boolean]}     [return true if the actor is on stage]
   */
  this.isOnstage = function (f_n) {
    let ret = false;
    for(let t of this.tracks) {
      if(t.first_frame <= f_n && f_n < t.first_frame + t.detections.length) {
        ret = true;
        break;
      }
    }
    if(!ret) {
      for(let t of this.track_bbox_shot) {
        if(t.first_frame <= f_n && f_n < t.last_frame) {
          ret = true;
          break;
        }
      }
    }
    if(!ret) {
      for(let s of this.states) {
        if(s.Num == 0 && s.first_frame <= f_n && f_n <= s.last_frame) {
          ret = true;
          break;
        }
      }
    }
    return ret;
  }

  function compare_first(a,b) {
    if (a.first_frame < b.first_frame)
      return -1;
    if (a.first_frame > b.first_frame)
      return 1;
    return 0;
  }

  /**
   * [display => function called in the main p5 draw function]
   */
  this.display = function() {
    this.updateBBox();
    if(this.y > this.elem.elt.parentNode.offsetTop-can.elt.offsetTop) {
      // this.elem.position(this.x-90, can.elt.offsetTop+this.y-25);

      push();
      rectMode(CORNER);
      stroke(0);
      strokeWeight(0);
      // The color changes based on the state of the button
      if (this.on) {
        fill(46,92,156);
      } else {
        fill(102);
        this.elem.style('color', 'rgb(0,0,0)');
      }
      rect(this.x,this.y,this.w,this.h);
      for(let s of this.states) {
        if(s.on) {
          fill(s.color_on);
        } else {
          fill(s.color);
        }
        if(player.first<=s.first_frame && (s.last_frame<=player.last)) {
          let unit = this.w/player.total_frame;
          s.x = player.x+(s.first_frame-player.first)*unit;
          s.w = player.x+(s.last_frame-player.first)*unit-s.x;
          s.h = this.h/2;
          rect(s.x, this.y, s.w, s.h);
        }
      }
      pop();

      for(var i=0; i<this.tracks.length; i++) {
        this.tracks[i].display();
      }
      for(let t of this.track_bbox_shot) {
        t.displayTime();
      }
      push();
      fill(150);
      let mx =mouseX;
      if(player.nav_bar.cursor-10 < mx && mx < player.nav_bar.cursor+10) {
        mx = player.nav_bar.cursor;
      }
      if(this.t_dragged && mx < this.t_dragged.x) {
        rect(mx,this.y,this.t_dragged.x-mx,this.h/2);
      } else if(this.t_dragged && mx > this.t_dragged.x+this.t_dragged.w) {
        rect(this.t_dragged.x+this.t_dragged.w,this.y,mx-(this.t_dragged.x+this.t_dragged.w),this.h/2);
      }
      pop();

    }
  }
}
