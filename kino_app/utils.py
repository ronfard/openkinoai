import os
import json
import re
import cvxpy as cvx
import numpy as np
import scipy as sp
import scipy.sparse
import subprocess
import shutil
import ctypes
import math, random
import time
import cv2

def to_vtt(total_sec):
    min = math.floor(total_sec/60)
    sec = round(total_sec%60,2)
    return str(min)+':'+str(sec)

def to_srt(total_sec):
    min = math.floor(total_sec/60)
    hour = math.floor(min/60)
    sec = round(total_sec%60,2)
    return str(hour).zfill(2)+':'+str(min).zfill(2)+':'+str(sec)

def save_vtt(tab, file):
    # f = open(file, "w")
    with open(file, 'w') as fp:
        string_config = 'WEBVTT\n\n'
        for t in tab:
            string_config += to_vtt(t['start'])+'-->'+to_vtt(t['end'])+'\n'
            for part in t['text'].split('\n'):
                if part != '':
                    string_config += part+'\n'
            string_config += ''+'\n'
        fp.write('{0}'.format(string_config))

def save_srt(tab, file):
    with open(file, 'w') as fp:
        string_config = ''
        i = 1
        for t in tab:
            string_config += str(i)+'\n'
            string_config += to_srt(t['start'])+' --> '+to_srt(t['end'])+'\n'
            for part in t['text'].split('\n'):
                if part != '':
                    string_config += part+'\n'
            string_config += ''+'\n'
            i+=1
        fp.write('{0}'.format(string_config))

def add_sub_vtt(sub, file):
    string = to_vtt(sub['start'])+'-->'+to_vtt(sub['end'])+'\n'
    for part in sub['text'].split('\n'):
        if part != '':
            string += part+'\n'
    string += ''+'\n'
    fp = open(file, 'a')
    fp.write(string)
    fp.close()

def parser_vtt(file):
    f = open(file, "r")
    total = len(open(file, "r").readlines())
    tab = []
    start = 0
    end = 0
    text = ""
    cpt=0
    for line in f:
        if len(line.split('-->')) > 1:
            start = (float(line.split('-->')[0].split(":")[0])*60) + (float(line.split('-->')[0].split(":")[1]))
            end = (float(line.split('-->')[1].split(":")[0])*60) + (float(line.split('-->')[1].split(":")[1]))
            # print(line, start, end)
        elif (start != 0 or end != 0):
            if line.split('\n')[0] == "":
                dict = {"start": start, "end": end, "text": text}
                tab.append(dict)
                text = ""
                end = 0
                start = 0
            else:
                text += line.split('\n')[0]+"\n"
        cpt+=1
        if cpt == total and text != '' and (start != 0 or end != 0):
            dict = {"start": start, "end": end, "text": text}
            tab.append(dict)
    return tab
