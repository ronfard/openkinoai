"""
This file is part of KinoAi project.
It is subject to the license terms in the LICENSE file found in the top-level directory
"""

from django.shortcuts import get_object_or_404, render, redirect
from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect
from django.views import generic
from django.urls import reverse
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.hashers import make_password, check_password, is_password_usable
from django.contrib.auth.models import Permission, User
from django.views.decorators.csrf import csrf_exempt
from .models import FolderPath, Detections, Shots, Project
from django.core.files.storage import FileSystemStorage
from django.views.decorators.cache import never_cache
from django.utils.dateparse import parse_date

from . import utils

import os
import json
import time
import subprocess
import shutil
import math, random

@csrf_exempt
def download_notes(request):
    notes = json.loads(request.POST.get('notes',''))
    media = os.listdir('media')
    for item in media:
        if item.endswith(".txt"):
            os.remove(os.path.join('media/', item))
    response = []
    for note in notes:
        file = open("media/"+note['Title']+".txt","w")
        file.write(note['Title']+'\n')
        for content in note['Content']:
            file.write(content['Start']+' - '+content['End']+' : '+content['Text']+'\n')
        file.close()
        response.append({'src':'/media/'+note['Title']+'.txt','title':note['Title'],'extention':'.txt'})
    return HttpResponse(json.dumps(response))

@csrf_exempt
def save_notes(request):
    notes = json.loads(request.POST.get('notes',''))
    project = request.POST.get('project','')
    print(request.user.username)
    dir = os.path.join(settings.MEDIA_ROOT, 'kino_app/notes/'+str(project))
    if not os.path.isdir(dir):
        os.mkdir(dir)
    with open(dir+'/'+str(request.user.username)+'_notes.json', 'w') as fp:
        json.dump(notes, fp, indent=2)

    # media = os.listdir('media')
    # for item in media:
    #     if item.endswith(".txt"):
    #         os.remove(os.path.join('media/', item))
    # response = []
    # for note in notes:
    #     file = open("media/"+note['Title']+".txt","w")
    #     file.write(note['Title']+'\n')
    #     for content in note['Content']:
    #         file.write(content['Start']+' - '+content['End']+' : '+content['Text']+'\n')
    #     file.close()
    #     response.append({'src':'/media/'+note['Title']+'.txt','title':note['Title'],'extention':'.txt'})
    return HttpResponse('')

@csrf_exempt
def download_subs(request):
    notes = json.loads(request.POST.get('notes',''))
    media = os.listdir('media')
    for item in media:
        if item.endswith(".vtt"):
            os.remove(os.path.join('media/', item))
    response = []
    for note in notes:
        file = open("media/"+note['Title']+".vtt","w")
        file.write('WEBVTT\n\n')
        for content in note['Content']:
            file.write(content['Start']+'.000 --> '+content['End']+'.000\n'+content['Text']+'\n\n')
        response.append({'src':'/media/'+note['Title']+'.vtt','title':note['Title'],'extention':'.vtt'})
        file.close()
    return HttpResponse(json.dumps(response))

def noting_app(request, project):
    dir = os.path.join(settings.MEDIA_ROOT, "kino_app/notes/"+str(project))

    if not os.path.isdir(dir):
        os.mkdir(dir)

    if not os.path.isfile(dir+'/'+str(request.user.username)+'_notes.json'):
        file = open(dir+'/'+str(request.user.username)+'_notes.json','w')
        file.write('[]')
        file.close()

    user_data = []
    for root, subdirs, files in os.walk(dir):
        for file in files:
            obj = {}
            obj["user"] = file.split('_')[0]
            print(root+'/'+file)
            if os.path.isfile(root+'/'+file):
                with open(root+'/'+file) as json_file:
                    obj["data"] = json.load(json_file)
                    for note in obj["data"]:
                        for text in note["Content"]:
                            text["Text"] = text["Text"].replace('\"','\\"').replace('\n','\\n').replace('\t','\\t').replace('&amp;','&')
            user_data.append(obj)

    return render(request, 'kino_app/noting.html', {'username':str(request.user.username), 'project':str(project), 'user_notes':json.dumps(user_data)})
