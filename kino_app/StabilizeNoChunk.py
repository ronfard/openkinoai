
# Stabilize the shot data frame
def stabilize_shot(data_shots, mask, aspect_ratio, video_width, video_height, lambda1=0.002, lambda2=0.0001):
    # print(aspect_ratio, video_width, video_height)

    optimised_xcenter = []
    optimised_ycenter = []
    optimised_height = []

    all_half_width = aspect_ratio * data_shots[:,2]
    data_shots[mask,0] = video_width / 2.
    all_half_width[mask] = -video_width / 2.  # negative on purpose
    data_shots[mask,1] = video_height / 2.
    data_shots[mask,2] = -video_height / 2.  # negative on purpose

    l = len(data_shots)
    k = 0
    s = 100
    lim = int(np.floor(l/s)*s)
    for i in range(l) :
        n = min(s,l)
        if i == lim+1 or i %( (k+1)* s ) == 0:
            start = k*n
            if i == lim+1:
                n = min(l-lim,s)
            end = start + n
            x = cvx.Variable(n)
            y = cvx.Variable(n)
            h = cvx.Variable(n)

            x_center = data_shots[start:end,0]
            y_center = data_shots[start:end,1]
            half_height = data_shots[start:end,2]
            half_width = all_half_width[start:end]

            print(n, k, l, i, lim, start, end)

            #D term
            expr = (cvx.sum_squares(x_center - x) + cvx.sum_squares(y_center- y) + cvx.sum_squares(half_height - h))/2

            # # L11 term
            expr += lambda1 * (cvx.tv(x) + cvx.tv(y) + cvx.tv(h))
            #
            # # L13 term
            for t in range(n-3):
                expr += lambda1 * (cvx.abs(x[t+3] - 3 * x[t+2] + 3 * x[t+1] - x[t]) + cvx.abs(y[t+3] - 3 * y[t+2] + 3 * y[t+1] - y[t]) \
                        + cvx.abs(h[t+3] - 3 * h[t+2] + 3 * h[t+1] - h[t]))

            obj = cvx.Minimize(expr)

            constraints = [h >= 0,
                           (x - aspect_ratio * h) >= 0,
                           (x - aspect_ratio * h) <= (x_center - half_width),
                           (x + aspect_ratio * h) >= (x_center + half_width),
                           (x + aspect_ratio * h) <= video_width,
                           (y - h) >= 0,
                           (y - h) <= (y_center - half_height),
                           (y + h) >= (y_center + half_height),
                           (y + h) <= video_height]

            prob = cvx.Problem(obj, constraints)
            prob.solve()

            optimised_xcenter = optimised_xcenter + x.value.tolist()
            optimised_ycenter = optimised_ycenter + y.value.tolist()
            optimised_height = optimised_height + h.value.tolist()
            k+=1
        if n == l:
            break
    data_shots[:,0] = optimised_xcenter
    data_shots[:,1] = optimised_ycenter
    data_shots[:,2] = optimised_height
    data_shots[mask] = [0,0,0]

    return data_shots
