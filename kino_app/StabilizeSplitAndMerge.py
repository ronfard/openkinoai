"""
This file is part of KinoAi project.
It is subject to the license terms in the LICENSE file found in the top-level directory
"""


import cvxpy as cvx
import numpy as np
import scipy as sp
import scipy.sparse
from scipy.ndimage.filters import gaussian_filter

# see https://stackoverflow.com/a/19800305/2607517

def csr_zero_rows(csr, rows_to_zero):
    rows, cols = csr.shape
    mask = np.ones((rows,), dtype=np.bool)
    mask[rows_to_zero] = False
    nnz_per_row = np.diff(csr.indptr)

    mask = np.repeat(mask, nnz_per_row)
    nnz_per_row[rows_to_zero] = 0
    csr.data = csr.data[mask]
    csr.indices = csr.indices[mask]
    csr.indptr[1:] = np.cumsum(nnz_per_row)

# 3-point binomial filter, see (Marchand and Marmet 1983) sec. VI

def binomial_3(arr):
    x1 = arr[0]
    for i in range(len(arr)-1):
        arr[i] = 0.5 * (arr[i] + arr[i+1])
    for i in range(1, len(arr)-1):
        arr[i] = 0.5 * (arr[i-1] + arr[i])
    arr[0] = x1

def define_sam_constraint_for_cvxpy(x, y, h, sam_constraints, chunk_start, chunk_end):
    return_constraint = []
    for i in range(len(sam_constraints)):
        if chunk_start <= int(sam_constraints[i]['Frame']) and int(sam_constraints[i]['Frame']) < chunk_end:
            index_frame = int(sam_constraints[i]['Frame']) - chunk_start
            return_constraint += [x[index_frame] == sam_constraints[i]['x_center'],
                            y[index_frame] == sam_constraints[i]['y_center'],
                            h[index_frame] == sam_constraints[i]['half_height']]
    return return_constraint

def stabilize_split_and_merge_constraints(desiredShot, aspectRatio, imageSize, fps, sam_constraints, screen_pos, actors_involved, lambda1=0.005, lambda2=0.01, zoomSmooth=2, lambda3=0.00001, lambda4=0.00001, lambda5=0.0001, lambda6=0.0001):
    """From a time sequence of unstabilized frame boxes, compute a stabilized frame.

    All parameters are normalized with respect to frame size and time, so that simultaneaously doubling the imageSize and the desiredShot does not change the solution, and neither does using twice as many frames and doubling the fps.

    If a frame in desiredShot goes outside of the original image, it is cropped.

    Reference: Gandhi Vineet, Ronfard Remi, Gleicher Michael, Moneish Kumar
    Zooming On All Actors: Automatic Focus+Context Split Screen Video Generation
    https://hal.inria.fr/hal-01482165/document

    Keyword arguments:
    desiredShot -- a n x 4 numpy array containing on each line the box as [xmin, ymin, xmax, ymax]
    lambda1 -- see eq. (10) in paper
    lambda2 -- see eq. (10) in paper
    lambda3 -- see eq. (10) in paper
    lambda4 -- see eq. (10) in paper
    lambda5 -- see eq. (10) in paper
    lambda6 -- see eq. (10) in paper
    zoomSmooth -- a factor applied on the terms that deal with frame size in the regularization term: raise if the stabilized frame zooms in and out too much
    aspectRatio -- the desired output aspect ration (e.g. 16/9.)
    imageSize -- [xmin, ymin, xmax, ymax] for the original image (typically [0,0,1920,1080] for HD)
    fps -- number of frames per seconds in the video - used for normalization

    """

    imageHeight = float(imageSize[1])
    imageWidth = float(imageSize[0])

    len_w = imageWidth/2
    len_h = imageHeight/2
    if imageHeight* aspectRatio < imageWidth:
        len_w = round((imageHeight*aspectRatio)/2)
    elif imageWidth / aspectRatio < imageHeight:
        len_h = round((imageWidth / aspectRatio)/2)

    # crop the desiredShot to the image window
    # we keep a 1-pixel margin to be sure that constraints can be satisfied
    margin = 1
    low_x1_flags = desiredShot[:, 0] < (0. + margin)
    desiredShot[low_x1_flags, 0] = 0. + margin
    low_x2_flags = desiredShot[:, 2] < (0. + margin)
    desiredShot[low_x2_flags, 2] = 0. + margin
    high_x1_flags = desiredShot[:, 0] > (imageWidth - margin)
    desiredShot[high_x1_flags, 0] = imageWidth - margin
    high_x2_flags = desiredShot[:, 2] > (imageWidth - margin)
    desiredShot[high_x2_flags, 2] = imageWidth - margin
    low_y1_flags = desiredShot[:, 1] < (0. + margin)
    desiredShot[low_y1_flags, 1] = 0. + margin
    low_y2_flags = desiredShot[:, 3] < (0. + margin)
    desiredShot[low_y2_flags, 3] = 0. + margin
    high_y1_flags = desiredShot[:, 1] > (imageHeight - margin)
    desiredShot[high_y1_flags, 1] = imageHeight - margin
    high_y2_flags = desiredShot[:, 3] > (imageHeight - margin)
    desiredShot[high_y2_flags, 3] = imageHeight - margin

    # Make sure that a crop of the given aspectRatio can be contained in imageSize and can contain the desiredShot.
    # This may be an issue eg. when doing a 16/9 or a 4/3 movie from 2K.
    # else, we must cut the desiredshot on both sides.
    for k in range(desiredShot.shape[0]):
        if (desiredShot[k, 2] - desiredShot[k, 0]) > (imageHeight * aspectRatio - margin):
            xcut = (desiredShot[k, 2] - desiredShot[k, 0]) - \
                (imageHeight * aspectRatio - margin)
            desiredShot[k, 2] -= xcut / 2
            desiredShot[k, 0] += xcut / 2
        if (desiredShot[k, 3] - desiredShot[k, 1]) > (imageWidth / aspectRatio - margin):
            ycut = (desiredShot[k, 3] - desiredShot[k, 1]) - \
                (imageWidth / aspectRatio - margin)
            desiredShot[k, 3] -= ycut / 2
            desiredShot[k, 1] += ycut / 2

    x_center = (desiredShot[:, 0] + desiredShot[:, 2]) / 2.
    y_center = (desiredShot[:, 1] + desiredShot[:, 3]) / 2.
    # elementwise maximum of each array
    half_height_opt = np.maximum((desiredShot[:, 2] - desiredShot[:, 0]) /
                                 aspectRatio, ((desiredShot[:, 3] - desiredShot[:, 1]))) / 2

    # smooth x_center y_center and half_height_opt using a binomial filter (Marchand and Marmet 1983)
    # eg [1 2 1]/4 or [1 4 6 4 1]/16 (obtained by applying it twice)
    x_center_residual = x_center
    # binomial_3(x_center_residual)
    # binomial_3(x_center_residual)
    y_center_residual = y_center
    # binomial_3(y_center_residual)
    # binomial_3(y_center_residual)
    half_height_opt_residual = half_height_opt
    # binomial_3(half_height_opt_residual)
    # binomial_3(half_height_opt_residual)

    half_width = (desiredShot[:, 2] - desiredShot[:, 0]) / 2.
    zero_flags = half_width[:] < 0
    half_width[zero_flags] = 0.
    half_height = (desiredShot[:, 3] - desiredShot[:, 1]) / 2.
    zero_flags = half_height[:] < 0
    half_height[zero_flags] = 0.

    # Define x and y center constraint + half height constraint for each split and merge frame
    for constraint_obj in sam_constraints:
        constraint_obj['Frame'] = int(constraint_obj['Frame'])
        constraint_obj['x_center'] = float(constraint_obj['X']) + float(constraint_obj['W'])/2;
        constraint_obj['y_center'] = float(constraint_obj['Y']) + float(constraint_obj['H'])/2;
        constraint_obj['half_height'] = float(constraint_obj['H'])/2;


    assert ((x_center - half_width) >=
            0).all() and ((x_center + half_width) <= imageWidth).all()
    assert ((y_center - half_height) >=
            0).all() and ((y_center + half_height) <= imageHeight).all()

    n = x_center.size

    #print "n:", n

    # We split the problem into chunks of fixed duration.
    # We compute a subsolution for the current chunk and the next chunk, and ensure continuity for the
    # variation (1st derivative) and jerk (3rd derivative) terms.
    # Then we only keep the solution for the current chunk and advance.

    optimised_xcenter = np.zeros(n)
    optimised_ycenter = np.zeros(n)
    optimised_height = np.zeros(n)

    chunk_s = 5  # size of a chunk in seconds
    print(chunk_s, len(x_center))
    chunk_n = int(chunk_s * fps)  # number of samples in a chunk
    full_chunk_n = chunk_n * 2  # number of samples in a subproblem
    # starting index for the chunk (also used to check if it is the first chunk)
    chunk_start = 0

    while chunk_start < n:
        chunk_end = min(n, chunk_start + chunk_n)
        chunk_size = chunk_end - chunk_start
        full_chunk_end = min(n, chunk_start + full_chunk_n)
        full_chunk_size = full_chunk_end - chunk_start
        # print("chunk:", chunk_start, chunk_end, full_chunk_end)

        x = cvx.Variable(full_chunk_size)
        y = cvx.Variable(full_chunk_size)
        # half height (see sec. 4 in the paper)
        h = cvx.Variable(full_chunk_size)

        # weights = weightsAll[chunk_start:full_chunk_end]
        x_center_chunk = x_center[chunk_start:full_chunk_end]
        y_center_chunk = y_center[chunk_start:full_chunk_end]
        half_height_chunk = half_height[chunk_start:full_chunk_end]
        half_width_chunk = half_width[chunk_start:full_chunk_end]
        x_center_residual_chunk = x_center[chunk_start:full_chunk_end]
        y_center_residual_chunk = y_center[chunk_start:full_chunk_end]
        half_height_opt_residual_chunk = half_height_opt_residual[chunk_start:full_chunk_end]

        #Vector screen pos
        h_vector = screen_pos[chunk_start:full_chunk_end]

        #assert ((x_center_chunk - half_width_chunk) >= 0).all() and ((x_center_chunk + half_width_chunk) <= imageWidth).all()
        #assert ((y_center_chunk - half_height_chunk) >= 0).all() and ((y_center_chunk + half_height_chunk) <= imageHeight).all()

        # for f in [97, 98, 99, 100]:
        #    print f, weights[f], x_center[f], y_center[f], half_height_opt[f]
        # expr = cvx.sum_squares(x_center_residual_chunk - x) + cvx.sum_squares(y_center_residual_chunk - y) + cvx.sum_squares(half_height_opt_residual_chunk - h)
        expr = cvx.sum_squares(x - x_center_residual_chunk) + cvx.sum_squares(y - y_center_residual_chunk) + cvx.sum_squares(h - half_height_opt_residual_chunk)
        # expr /= n  # normalize by the number of images, get a cost per image


        if lambda1 != 0.:
            lambda1Factor = (60*fps) / imageHeight
            # print("lambda 1 ",lambda1Factor)
            if n > 1:
                expr += lambda1Factor * \
                    (cvx.tv(x) + cvx.tv(y) + cvx.tv(h) * zoomSmooth)

            # if not the first chunk, add continuity with previous samples
            if chunk_start >= 1:
                expr += lambda1Factor * (cvx.abs(x[0] - optimised_xcenter[chunk_start - 1]) +
                                         cvx.abs(y[0] - optimised_ycenter[chunk_start - 1]) +
                                         cvx.abs(h[0] - optimised_height[chunk_start - 1]) * zoomSmooth)

        if lambda2 != 0.:
            lambda2Factor = (60*fps) * fps / imageHeight
            # print("lambda 2 ",lambda2Factor)

            if n > 1:
                expr += lambda2Factor * (cvx.norm(x[2:] - 2*x[1:full_chunk_size-1] + x[0:full_chunk_size-2], 1) +
                                         cvx.norm(y[2:] - 2*y[1:full_chunk_size-1] + y[0:full_chunk_size-2], 1) +
                                         cvx.norm(h[2:] - 2*h[1:full_chunk_size-1] + h[0:full_chunk_size-2], 1) * zoomSmooth)
            # if not the first chunk, add continuity with previous samples
            if chunk_start >= 2 and chunk_size >= 2:
                expr += lambda2Factor * ((cvx.abs(x[0] - 2 * optimised_xcenter[chunk_start - 1] + optimised_xcenter[chunk_start - 2]) +
                                          cvx.abs(x[1] - 2 * x[0] + optimised_xcenter[chunk_start - 1])) +
                                         (cvx.abs(y[0] - 2 * optimised_ycenter[chunk_start - 1] + optimised_ycenter[chunk_start - 2]) +
                                          cvx.abs(y[1] - 2 * y[0] + optimised_ycenter[chunk_start - 1])) +
                                         (cvx.abs(h[0] - 2 * optimised_height[chunk_start - 1] + optimised_height[chunk_start - 2]) +
                                          cvx.abs(h[1] - 2 * h[0] + optimised_height[chunk_start - 1])) * zoomSmooth)

        if lambda2 != 0.:
            lambda3Factor = (60*fps) * fps * fps / imageHeight
            # print("lambda 2 ",lambda2Factor)

            if n > 2:
                expr += lambda3Factor * (cvx.norm(x[3:] - 3*x[2:full_chunk_size-1] + 3*x[1:full_chunk_size-2] - x[0:full_chunk_size-3], 1) +
                                         cvx.norm(y[3:] - 3*y[2:full_chunk_size-1] + 3*y[1:full_chunk_size-2] - y[0:full_chunk_size-3], 1) +
                                         cvx.norm(h[3:] - 3*h[2:full_chunk_size-1] + 3*h[1:full_chunk_size-2] - h[0:full_chunk_size-3], 1) * zoomSmooth)
            # if not the first chunk, add continuity with previous samples
            if chunk_start >= 3 and chunk_size >= 3:
                expr += lambda3Factor * ((cvx.abs(x[0] - 3 * optimised_xcenter[chunk_start - 1] + 3 * optimised_xcenter[chunk_start - 2] - optimised_xcenter[chunk_start - 3]) +
                                          cvx.abs(x[1] - 3 * x[0] + 3 * optimised_xcenter[chunk_start - 1] - optimised_xcenter[chunk_start - 2]) +
                                          cvx.abs(x[2] - 3 * x[1] + 3 * x[0] - optimised_xcenter[chunk_start - 1])) +
                                         (cvx.abs(y[0] - 3 * optimised_ycenter[chunk_start - 1] + 3 * optimised_ycenter[chunk_start - 2] - optimised_ycenter[chunk_start - 3]) +
                                          cvx.abs(y[1] - 3 * y[0] + 3 * optimised_ycenter[chunk_start - 1] - optimised_ycenter[chunk_start - 2]) +
                                          cvx.abs(y[2] - 3 * y[1] + 3 * y[0] - optimised_ycenter[chunk_start - 1])) +
                                         (cvx.abs(h[0] - 3 * optimised_height[chunk_start - 1] + 3 * optimised_height[chunk_start - 2] - optimised_height[chunk_start - 3]) +
                                          cvx.abs(h[1] - 3 * h[0] + 3 * optimised_height[chunk_start - 1] - optimised_height[chunk_start - 2]) +
                                          cvx.abs(h[2] - 3 * h[1] + 3 * h[0] - optimised_height[chunk_start - 1])) * zoomSmooth)


        obj = cvx.Minimize(expr)

        #print expr
        # print("H=%d, W=%d lambda1=%f lambda2=%f zoomSmooth=%f fps=%f imageHeight=%f" % (
        #     imageHeight, imageWidth, lambda1, lambda2, zoomSmooth, fps, imageHeight))
        # note that the following constraints are tricked (see above) at noDataFrames, using negative values for half_width and half_height
        constraints = [h >= 0,
                       (x - aspectRatio * h) >= 0,
                       (x - aspectRatio * h) <= (x_center_chunk - half_width_chunk),
                       (x + aspectRatio * h) >= (x_center_chunk + half_width_chunk),
                       (x + aspectRatio * h) <= imageWidth,
                       aspectRatio * h <= len_w,
                       (y - h) >= 0,
                       (y - h) <= (y_center_chunk - half_height_chunk),
                       (y + h) >= (y_center_chunk + half_height_chunk),
                       (y + h) <= imageHeight,
                       h <= len_h]

        # constraints += [x[60] == 200, y[60] == 200, h[60] == half_height[60]]
        # constraints += define_sam_constraint_for_cvxpy(x, y, h, sam_constraints, chunk_start, full_chunk_end)
        constraints += define_sam_constraint_for_cvxpy(x, y, h, sam_constraints, chunk_start, full_chunk_end)

        prob = cvx.Problem(obj, constraints)

        tryagain = True
        tryreason = ""
        if tryagain or prob.status == cvx.INFEASIBLE or prob.status == cvx.UNBOUNDED:
           tryagain = False
           try:
               # ECOS, the default solver, is much better at solving our problems, especially at handling frames where the actor is not visible
               # all tolerances are multiplied by 10
               # print('ECOS')
               result = prob.solve(solver=cvx.ECOS, verbose=False, abstol = 1e-6, reltol = 1e-5, abstol_inacc = 5e-4, reltol_inacc = 5e-4, feastol_inacc = 1e-3)
           except cvx.SolverError as e:
               tryagain = True
               tryreason = str(e)

        if tryagain or prob.status == cvx.INFEASIBLE or prob.status == cvx.UNBOUNDED:
            tryagain = False
            try:
                # print('SCS')
                result = prob.solve(solver=cvx.SCS, verbose=False, max_iters = 2500, eps = 1e-2)
            except cvx.SolverError as e:
                tryagain = True
                tryreason = str(e)

        if tryagain or prob.status == cvx.INFEASIBLE or prob.status == cvx.UNBOUNDED:
            tryagain = False
            try:
                # print('CVXOPT')
                result = prob.solve(solver=cvx.CVXOPT, verbose=False, abstol = 1e-6, reltol = 1e-5, feastol = 1e-6)
            except cvx.SolverError as e:
                tryagain = True
                tryreason = str(e)

        if tryagain or prob.status == cvx.INFEASIBLE or prob.status == cvx.UNBOUNDED:
            tryagain = False
            try:
                print('CVXOPT KKTSOLVER')
                result = prob.solve(
                    solver=cvx.CVXOPT, kktsolver=cvx.ROBUST_KKTSOLVER, verbose=False, abstol = 1e-6, reltol = 1e-5, feastol = 1e-6)
            except cvx.SolverError as e:
                tryagain = True
                tryreason = str(e)

        if tryagain:
            raise cvx.SolverError(tryreason)
        if prob.status == cvx.INFEASIBLE or prob.status == cvx.UNBOUNDED:
            raise cvx.SolverError('Problem is infeasible or unbounded')

        #raise ValueError('Yeah!')
        # print("result=", result, "\n")
        if full_chunk_end >= n:
            # last chunk - get the full chunk
            optimised_xcenter[chunk_start:full_chunk_end] = x.value.reshape(full_chunk_size)
            optimised_ycenter[chunk_start:full_chunk_end] = y.value.reshape(full_chunk_size)
            optimised_height[chunk_start:full_chunk_end] = h.value.reshape(full_chunk_size)
            chunk_start = full_chunk_end
        else:
            # only get the chunk and advance
            optimised_xcenter[chunk_start:chunk_end] = x.value[:chunk_size].reshape(chunk_size)
            optimised_ycenter[chunk_start:chunk_end] = y.value[:chunk_size].reshape(chunk_size)
            optimised_height[chunk_start:chunk_end] = h.value[:chunk_size].reshape(chunk_size)
            chunk_start = chunk_end

    return np.vstack([optimised_xcenter - aspectRatio * optimised_height, optimised_ycenter - optimised_height, optimised_xcenter + aspectRatio * optimised_height, optimised_ycenter + optimised_height]).transpose()
