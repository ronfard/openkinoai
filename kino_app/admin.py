"""
This file is part of KinoAi project.
It is subject to the license terms in the LICENSE file found in the top-level directory
"""


from django.contrib import admin

# Register your models here.

from .models import Detections, FolderPath, Project, Shots

admin.site.register(Detections)
admin.site.register(Shots)
admin.site.register(FolderPath)
admin.site.register(Project)
